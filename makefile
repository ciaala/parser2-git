MAKEFLAGS:=--no-print-directory 

.PHONY : test distribution distribution/rwxml.tar.bz2
	
test: def lef rwxml
	@make -C rwxml test_complete

def: 
	@printf "Decompressing - "
	@tar jxf def_5.7-p006.tar.bz2
	@printf ".\nCompiling - "
	@make -Cs def 
	@printf ".\n"
lef:
	@printf "Decompressing - "
	@tar jxf lef_5.7-p006.tar.bz2
	@printf ".\nCompiling - "
	@make -Cs lef 
	@printf ".\n"
rwxml: 
	@tar jxf rwxml.tar.bz2
	@make -C rwxml
	

distribution: distribution/rwxml.tar.bz2 distribution/def_5.7-p006.tar.bz2 distribution/def_5.7-p006.tar.bz2 distribution/README distribution/makefile
	@printf "Building - the distribution archive"
	@tar jcf distribution.tar.bz2 distribution
	@printf ".\n"
distribution/rwxml-distribution.tar.bz2:
	@printf "Cleaning"
	@make -C rwxml clean
	@printf "."
	@rm $(shell find . -name '*~') 
	@printf ".\n"
	@printf "Building - directory rwxml archive"
	@tar cjf distribution/rwxml.tar.bz2 rwxml
	@printf ".\n"

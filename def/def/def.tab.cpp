
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.4.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Using locations.  */
#define YYLSP_NEEDED 0

/* Substitute the variable and function names.  */
#define yyparse         defyyparse
#define yylex           defyylex
#define yyerror         defyyerror
#define yylval          defyylval
#define yychar          defyychar
#define yydebug         defyydebug
#define yynerrs         defyynerrs


/* Copy the first part of user declarations.  */

/* Line 189 of yacc.c  */
#line 45 "def.y"

#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include "defrReader.hpp"
#include "lex.h"
#include "defiUser.hpp"
#include "defrCallBacks.hpp"

#define YYDEBUG 1         /* this is temp fix for pcr 755132 */
// TX_DIR:TRANSLATION ON

extern int dumb_mode;     // Read next N tokens in dumb mode
extern int no_num;        // No numbers for the next N tokens
extern int real_num;      // Next token will be a real number
extern int by_is_keyword;
extern int bit_is_keyword;
extern int do_is_keyword;
extern int new_is_keyword;
extern int nondef_is_keyword;
extern int step_is_keyword;
extern int mustjoin_is_keyword;
extern int routed_is_keyword;
extern int cover_is_keyword;
extern int fixed_is_keyword;
extern int orient_is_keyword;
extern char* History_text;
extern int parsing_property;
extern int doneDesign;

extern int errors;        // from lex.cpph
extern int nlines;        // from lex.cpph
extern int defInPropDef;  // from lex.cpph, tracking if insided propDef


double save_x;
double save_y;

static double defVersionNum = 5.7;   // default is 5.7

static int defIgnoreVersion = 0; // ignore checking version number
                                 // this is redundant, since def does not have
                                 // either 5.3 or 5.4 code

static int hasVer = 0;        // keep track VERSION is in the file
static int hasNameCase = 0;   // keep track NAMESCASESENSITIVE is in the file
static int hasDivChar = 0;    // keep track DIVIDERCHAR is in the file
static int hasBusBit = 0;     // keep track BUSBITCHARS is in the file
static int hasDes = 0;        // keep track DESIGN is in the file
static int regTypeDef;        // keep track that region type is defined 
static int aOxide = 0;        // keep track for oxide
static int netOsnet = 0;      // net = 1 & snet = 2
static int viaRule = 0;       // keep track the viarule has called first
static int hasPort = 0;       // keep track is port defined in a Pin

static int               needNPCbk = 0;     // if cbk for net path is needed
static int               needSNPCbk = 0;    // if cbk for snet path is needed
static double            xStep, yStep;
static int               hasDoStep;
static int               hasBlkLayerComp;     // only 1 BLOCKAGE/LAYER/COMP
static int               hasBlkLayerSpac;     // only 1 BLOCKAGE/LAYER/SPACING
static int               hasBlkPlaceComp;     // only 1 BLOCKAGE/PLACEMENT/COMP

// the following variables to keep track the number of warnings printed.
static int assertionWarnings = 0;
static int blockageWarnings = 0;
static int caseSensitiveWarnings = 0;
static int componentWarnings = 0;
static int constraintWarnings = 0;
static int defaultCapWarnings = 0;
static int fillWarnings = 0;
static int gcellGridWarnings = 0;
static int iOTimingWarnings = 0;
static int netWarnings = 0;
static int nonDefaultWarnings = 0;
static int pinExtWarnings = 0;
static int pinWarnings = 0;
static int regionWarnings = 0;
static int rowWarnings = 0;
static int scanchainWarnings = 0;
static int sNetWarnings = 0;
static int stylesWarnings = 0;
static int trackWarnings = 0;
static int unitsWarnings = 0;
static int versionWarnings = 0;
static int viaWarnings = 0;

int names_case_sensitive = 1; // always true in 5.6
int reader_case_sensitive = 0; // save what the reader prefer
int defRetVal;

int shield = FALSE;       // To identify if the path is shield for 5.3

static char defPropDefType;   // save the current type of the property
static char* shieldName;      // to hold the shieldNetName
static char* rowName;         // to hold the rowName for message

int bitsNum;                  // Scanchain Bits value
char *warningMsg;
char *defMsg;

/* From def_keywords.cpp */
extern char* ringCopy(const char* string);


/* Macro to describe how we handle a callback.
 * If the function was set then call it.
 * If the function returns non zero then there was an error
 * so call the error routine and exit.
 */
#define CALLBACK(func, typ, data) \
    if (!errors) {\
      if (func) { \
        if ((defRetVal = (*func)(typ, data, defrReader::get()->getUserData())) == PARSE_OK) { \
        } else if (defRetVal == STOP_PARSE) { \
	  return defRetVal; \
        } else { \
          defError(6010, "Error in callback"); \
	  return defRetVal; \
        } \
      } \
    }

#define CHKERR() \
    if (errors > 20) {\
      defError(6011, "Too many syntax errors"); \
      errors = 0; \
      return 1; \
    }

#define CHKPROPTYPE(propType, propName, name) \
    if (propType == 'N') { \
       warningMsg = (char*)defMalloc(strlen(propName)+strlen(name)+35); \
       sprintf(warningMsg, "PropName %s is not defined for %s.", \
               propName, name); \
       defWarning(7010, warningMsg); \
       defFree(warningMsg); \
    }

double convert_defname2num(char *versionName)
{
    char majorNm[80];
    char minorNm[80];
    char *subMinorNm = NULL;
    char *p1;
    char *versionNm = strdup(versionName);

    double major = 0, minor = 0, subMinor = 0;
    double version;

    sscanf(versionNm, "%[^.].%s", majorNm, minorNm);
    if ((p1 = strchr(minorNm, '.'))) {
       subMinorNm = p1+1;
       *p1 = '\0';
    }
    major = atof(majorNm);
    minor = atof(minorNm);
    if (subMinorNm)
       subMinor = atof(subMinorNm);

    version = major;

    if (minor > 0)
       version = major + minor/10;

    if (subMinor > 0)
       version = version + subMinor/1000;

    free(versionNm);
    return version;
}

int numIsInt (char* volt) {
    if (strchr(volt, '.'))  // a floating point
       return 0;
    else
       return 1;
}

int defValidNum(int values) {
    char *outMsg;
    switch (values) {
        case 100:
        case 200:
        case 1000:
        case 2000:
                return 1;
        case 10000:
        case 20000:
             if (defVersionNum < 5.6) {
                if (defrReader::get()->getUnitsCbk()) {
                  if (unitsWarnings++ < defrReader::get()->getUnitsWarnings()) {
                    outMsg = (char*)defMalloc(1000);
                    sprintf (outMsg,
                    "Error found when processing DEF file '%s'\nUnit %d is a 5.6 or later syntax\nYour def file is not defined as 5.6.",
                    defrReader::get()->getFileName(), values);
                    defError(6501, outMsg);
                    defFree(outMsg);
                  }
                }
                return 0;
             } else
                return 1;
    }
    if (defrReader::get()->getUnitsCbk()) {
      if (unitsWarnings++ < defrReader::get()->getUnitsWarnings()) {
        outMsg = (char*)defMalloc(10000);
        sprintf (outMsg,
          "The value %d defined for DEF UNITS DISTANCE MICRON is invalid\n. Correct value is 100, 200, 1000, 2000, 10000, or 20000.", values);
        defError(6502, outMsg);
        defFree(outMsg);
        CHKERR();
      }
    }
    return 0;
}

#define FIXED 1
#define COVER 2
#define PLACED 3
#define UNPLACED 4


/* Line 189 of yacc.c  */
#line 305 "def.tab.c"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     QSTRING = 258,
     T_STRING = 259,
     SITE_PATTERN = 260,
     NUMBER = 261,
     K_HISTORY = 262,
     K_NAMESCASESENSITIVE = 263,
     K_DESIGN = 264,
     K_VIAS = 265,
     K_TECH = 266,
     K_UNITS = 267,
     K_ARRAY = 268,
     K_FLOORPLAN = 269,
     K_SITE = 270,
     K_CANPLACE = 271,
     K_CANNOTOCCUPY = 272,
     K_DIEAREA = 273,
     K_PINS = 274,
     K_DEFAULTCAP = 275,
     K_MINPINS = 276,
     K_WIRECAP = 277,
     K_TRACKS = 278,
     K_GCELLGRID = 279,
     K_DO = 280,
     K_BY = 281,
     K_STEP = 282,
     K_LAYER = 283,
     K_ROW = 284,
     K_RECT = 285,
     K_COMPS = 286,
     K_COMP_GEN = 287,
     K_SOURCE = 288,
     K_WEIGHT = 289,
     K_EEQMASTER = 290,
     K_FIXED = 291,
     K_COVER = 292,
     K_UNPLACED = 293,
     K_PLACED = 294,
     K_FOREIGN = 295,
     K_REGION = 296,
     K_REGIONS = 297,
     K_NETS = 298,
     K_START_NET = 299,
     K_MUSTJOIN = 300,
     K_ORIGINAL = 301,
     K_USE = 302,
     K_STYLE = 303,
     K_PATTERN = 304,
     K_PATTERNNAME = 305,
     K_ESTCAP = 306,
     K_ROUTED = 307,
     K_NEW = 308,
     K_SNETS = 309,
     K_SHAPE = 310,
     K_WIDTH = 311,
     K_VOLTAGE = 312,
     K_SPACING = 313,
     K_NONDEFAULTRULE = 314,
     K_NONDEFAULTRULES = 315,
     K_N = 316,
     K_S = 317,
     K_E = 318,
     K_W = 319,
     K_FN = 320,
     K_FE = 321,
     K_FS = 322,
     K_FW = 323,
     K_GROUPS = 324,
     K_GROUP = 325,
     K_SOFT = 326,
     K_MAXX = 327,
     K_MAXY = 328,
     K_MAXHALFPERIMETER = 329,
     K_CONSTRAINTS = 330,
     K_NET = 331,
     K_PATH = 332,
     K_SUM = 333,
     K_DIFF = 334,
     K_SCANCHAINS = 335,
     K_START = 336,
     K_FLOATING = 337,
     K_ORDERED = 338,
     K_STOP = 339,
     K_IN = 340,
     K_OUT = 341,
     K_RISEMIN = 342,
     K_RISEMAX = 343,
     K_FALLMIN = 344,
     K_FALLMAX = 345,
     K_WIREDLOGIC = 346,
     K_MAXDIST = 347,
     K_ASSERTIONS = 348,
     K_DISTANCE = 349,
     K_MICRONS = 350,
     K_END = 351,
     K_IOTIMINGS = 352,
     K_RISE = 353,
     K_FALL = 354,
     K_VARIABLE = 355,
     K_SLEWRATE = 356,
     K_CAPACITANCE = 357,
     K_DRIVECELL = 358,
     K_FROMPIN = 359,
     K_TOPIN = 360,
     K_PARALLEL = 361,
     K_TIMINGDISABLES = 362,
     K_THRUPIN = 363,
     K_MACRO = 364,
     K_PARTITIONS = 365,
     K_TURNOFF = 366,
     K_FROMCLOCKPIN = 367,
     K_FROMCOMPPIN = 368,
     K_FROMIOPIN = 369,
     K_TOCLOCKPIN = 370,
     K_TOCOMPPIN = 371,
     K_TOIOPIN = 372,
     K_SETUPRISE = 373,
     K_SETUPFALL = 374,
     K_HOLDRISE = 375,
     K_HOLDFALL = 376,
     K_VPIN = 377,
     K_SUBNET = 378,
     K_XTALK = 379,
     K_PIN = 380,
     K_SYNTHESIZED = 381,
     K_DEFINE = 382,
     K_DEFINES = 383,
     K_DEFINEB = 384,
     K_IF = 385,
     K_THEN = 386,
     K_ELSE = 387,
     K_FALSE = 388,
     K_TRUE = 389,
     K_EQ = 390,
     K_NE = 391,
     K_LE = 392,
     K_LT = 393,
     K_GE = 394,
     K_GT = 395,
     K_OR = 396,
     K_AND = 397,
     K_NOT = 398,
     K_SPECIAL = 399,
     K_DIRECTION = 400,
     K_RANGE = 401,
     K_FPC = 402,
     K_HORIZONTAL = 403,
     K_VERTICAL = 404,
     K_ALIGN = 405,
     K_MIN = 406,
     K_MAX = 407,
     K_EQUAL = 408,
     K_BOTTOMLEFT = 409,
     K_TOPRIGHT = 410,
     K_ROWS = 411,
     K_TAPER = 412,
     K_TAPERRULE = 413,
     K_VERSION = 414,
     K_DIVIDERCHAR = 415,
     K_BUSBITCHARS = 416,
     K_PROPERTYDEFINITIONS = 417,
     K_STRING = 418,
     K_REAL = 419,
     K_INTEGER = 420,
     K_PROPERTY = 421,
     K_BEGINEXT = 422,
     K_ENDEXT = 423,
     K_NAMEMAPSTRING = 424,
     K_ON = 425,
     K_OFF = 426,
     K_X = 427,
     K_Y = 428,
     K_COMPONENT = 429,
     K_PINPROPERTIES = 430,
     K_TEST = 431,
     K_COMMONSCANPINS = 432,
     K_SNET = 433,
     K_COMPONENTPIN = 434,
     K_REENTRANTPATHS = 435,
     K_SHIELD = 436,
     K_SHIELDNET = 437,
     K_NOSHIELD = 438,
     K_ANTENNAPINPARTIALMETALAREA = 439,
     K_ANTENNAPINPARTIALMETALSIDEAREA = 440,
     K_ANTENNAPINGATEAREA = 441,
     K_ANTENNAPINDIFFAREA = 442,
     K_ANTENNAPINMAXAREACAR = 443,
     K_ANTENNAPINMAXSIDEAREACAR = 444,
     K_ANTENNAPINPARTIALCUTAREA = 445,
     K_ANTENNAPINMAXCUTCAR = 446,
     K_SIGNAL = 447,
     K_POWER = 448,
     K_GROUND = 449,
     K_CLOCK = 450,
     K_TIEOFF = 451,
     K_ANALOG = 452,
     K_SCAN = 453,
     K_RESET = 454,
     K_RING = 455,
     K_STRIPE = 456,
     K_FOLLOWPIN = 457,
     K_IOWIRE = 458,
     K_COREWIRE = 459,
     K_BLOCKWIRE = 460,
     K_FILLWIRE = 461,
     K_BLOCKAGEWIRE = 462,
     K_PADRING = 463,
     K_BLOCKRING = 464,
     K_BLOCKAGES = 465,
     K_PLACEMENT = 466,
     K_SLOTS = 467,
     K_FILLS = 468,
     K_PUSHDOWN = 469,
     K_NETLIST = 470,
     K_DIST = 471,
     K_USER = 472,
     K_TIMING = 473,
     K_BALANCED = 474,
     K_STEINER = 475,
     K_TRUNK = 476,
     K_FIXEDBUMP = 477,
     K_FENCE = 478,
     K_FREQUENCY = 479,
     K_GUIDE = 480,
     K_MAXBITS = 481,
     K_PARTITION = 482,
     K_TYPE = 483,
     K_ANTENNAMODEL = 484,
     K_DRCFILL = 485,
     K_OXIDE1 = 486,
     K_OXIDE2 = 487,
     K_OXIDE3 = 488,
     K_OXIDE4 = 489,
     K_CUTSIZE = 490,
     K_CUTSPACING = 491,
     K_DESIGNRULEWIDTH = 492,
     K_DIAGWIDTH = 493,
     K_ENCLOSURE = 494,
     K_HALO = 495,
     K_GROUNDSENSITIVITY = 496,
     K_HARDSPACING = 497,
     K_LAYERS = 498,
     K_MINCUTS = 499,
     K_NETEXPR = 500,
     K_OFFSET = 501,
     K_ORIGIN = 502,
     K_ROWCOL = 503,
     K_STYLES = 504,
     K_POLYGON = 505,
     K_PORT = 506,
     K_SUPPLYSENSITIVITY = 507,
     K_VIA = 508,
     K_VIARULE = 509,
     K_WIREEXT = 510,
     K_EXCEPTPGNET = 511,
     K_FILLWIREOPC = 512,
     K_OPC = 513,
     K_PARTIAL = 514,
     K_ROUTEHALO = 515
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 214 of yacc.c  */
#line 268 "def.y"

        double dval ;
        int    integer ;
        char * string ;
        int    keyword ;  /* really just a nop */
        struct defpoint pt;
        defTOKEN *tk;



/* Line 214 of yacc.c  */
#line 612 "def.tab.c"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif


/* Copy the second part of user declarations.  */


/* Line 264 of yacc.c  */
#line 624 "def.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  5
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   1400

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  268
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  495
/* YYNRULES -- Number of rules.  */
#define YYNRULES  897
/* YYNRULES -- Number of states.  */
#define YYNSTATES  1567

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   515

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint16 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     264,   265,   266,   263,   267,   262,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,   261,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,   171,   172,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
     185,   186,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,   197,   198,   199,   200,   201,   202,   203,   204,
     205,   206,   207,   208,   209,   210,   211,   212,   213,   214,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   224,
     225,   226,   227,   228,   229,   230,   231,   232,   233,   234,
     235,   236,   237,   238,   239,   240,   241,   242,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,   254,
     255,   256,   257,   258,   259,   260
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     8,     9,    10,    15,    16,    20,    24,
      25,    28,    30,    32,    34,    36,    38,    40,    42,    44,
      46,    48,    50,    52,    54,    56,    58,    60,    62,    64,
      66,    68,    70,    72,    74,    76,    78,    80,    82,    84,
      86,    88,    90,    92,    94,    96,    98,   100,   102,   104,
     106,   108,   109,   114,   117,   118,   123,   124,   129,   130,
     135,   137,   138,   144,   145,   148,   149,   155,   156,   162,
     163,   169,   170,   176,   177,   183,   184,   190,   191,   197,
     198,   204,   205,   211,   212,   218,   221,   222,   227,   228,
     233,   235,   238,   241,   242,   244,   250,   254,   258,   259,
     274,   275,   290,   291,   306,   308,   310,   312,   314,   316,
     318,   320,   322,   323,   330,   334,   337,   338,   341,   347,
     350,   354,   358,   359,   362,   363,   364,   365,   376,   377,
     380,   383,   385,   389,   393,   394,   399,   400,   405,   409,
     412,   413,   414,   423,   424,   425,   436,   437,   446,   450,
     455,   460,   465,   470,   471,   478,   479,   486,   491,   492,
     499,   503,   504,   507,   510,   511,   514,   517,   519,   521,
     523,   525,   527,   529,   531,   533,   535,   537,   539,   541,
     542,   543,   547,   550,   551,   552,   564,   565,   571,   572,
     576,   577,   580,   581,   586,   587,   590,   593,   596,   599,
     600,   610,   613,   615,   617,   618,   619,   624,   625,   628,
     630,   639,   641,   644,   648,   652,   653,   656,   657,   658,
     665,   666,   669,   670,   677,   678,   679,   689,   690,   695,
     696,   697,   722,   724,   726,   731,   736,   743,   744,   749,
     751,   753,   754,   757,   762,   767,   772,   777,   780,   785,
     789,   790,   793,   794,   795,   803,   806,   810,   811,   814,
     815,   820,   824,   825,   828,   831,   834,   837,   839,   841,
     845,   849,   850,   853,   857,   860,   861,   866,   867,   870,
     873,   874,   877,   879,   881,   883,   885,   887,   889,   891,
     893,   895,   897,   899,   901,   902,   907,   908,   914,   915,
     917,   921,   923,   925,   927,   929,   932,   935,   938,   942,
     943,   952,   953,   955,   956,   963,   964,   969,   971,   974,
     977,   980,   983,   986,   987,   994,   996,   999,  1003,  1006,
    1011,  1014,  1017,  1020,  1024,  1027,  1031,  1035,  1036,  1039,
    1043,  1045,  1046,  1050,  1051,  1055,  1056,  1063,  1064,  1067,
    1068,  1075,  1076,  1083,  1084,  1091,  1092,  1094,  1097,  1098,
    1101,  1102,  1107,  1111,  1114,  1115,  1120,  1121,  1126,  1130,
    1134,  1138,  1142,  1146,  1150,  1151,  1156,  1158,  1159,  1164,
    1165,  1166,  1172,  1173,  1174,  1175,  1184,  1185,  1190,  1192,
    1194,  1197,  1200,  1203,  1206,  1208,  1210,  1212,  1214,  1216,
    1217,  1224,  1225,  1230,  1231,  1232,  1236,  1237,  1241,  1243,
    1245,  1247,  1249,  1251,  1253,  1255,  1258,  1259,  1263,  1264,
    1265,  1272,  1273,  1276,  1278,  1281,  1290,  1292,  1297,  1302,
    1307,  1312,  1318,  1324,  1330,  1336,  1337,  1340,  1342,  1344,
    1346,  1347,  1351,  1354,  1355,  1358,  1362,  1366,  1369,  1371,
    1373,  1375,  1377,  1379,  1381,  1383,  1385,  1387,  1389,  1391,
    1393,  1397,  1398,  1401,  1405,  1406,  1409,  1411,  1413,  1415,
    1417,  1420,  1421,  1426,  1427,  1428,  1435,  1436,  1437,  1447,
    1448,  1455,  1459,  1462,  1466,  1467,  1472,  1476,  1480,  1484,
    1488,  1492,  1493,  1498,  1500,  1501,  1502,  1505,  1506,  1512,
    1513,  1518,  1519,  1520,  1528,  1530,  1533,  1536,  1539,  1542,
    1543,  1547,  1548,  1552,  1554,  1556,  1558,  1560,  1562,  1565,
    1566,  1570,  1571,  1572,  1580,  1582,  1586,  1589,  1593,  1597,
    1598,  1601,  1606,  1607,  1611,  1612,  1615,  1617,  1618,  1621,
    1625,  1626,  1631,  1632,  1637,  1639,  1642,  1644,  1645,  1648,
    1651,  1654,  1657,  1658,  1661,  1664,  1667,  1670,  1673,  1677,
    1681,  1685,  1689,  1690,  1693,  1695,  1697,  1702,  1703,  1707,
    1708,  1715,  1720,  1725,  1727,  1730,  1734,  1735,  1744,  1745,
    1747,  1748,  1751,  1755,  1759,  1763,  1767,  1770,  1773,  1777,
    1781,  1782,  1785,  1789,  1790,  1794,  1795,  1798,  1799,  1801,
    1802,  1808,  1809,  1814,  1815,  1820,  1821,  1827,  1828,  1833,
    1834,  1840,  1842,  1843,  1848,  1857,  1858,  1861,  1862,  1866,
    1867,  1872,  1881,  1894,  1895,  1898,  1899,  1903,  1904,  1909,
    1918,  1931,  1932,  1935,  1938,  1942,  1946,  1947,  1950,  1954,
    1955,  1962,  1963,  1966,  1972,  1978,  1982,  1983,  1984,  1991,
    1993,  1994,  1995,  2002,  2003,  2004,  2008,  2009,  2012,  2014,
    2016,  2019,  2024,  2028,  2029,  2032,  2033,  2034,  2043,  2045,
    2047,  2049,  2052,  2055,  2058,  2059,  2062,  2063,  2068,  2069,
    2074,  2075,  2078,  2079,  2085,  2086,  2092,  2096,  2100,  2101,
    2104,  2105,  2106,  2117,  2118,  2125,  2126,  2133,  2137,  2138,
    2139,  2146,  2147,  2151,  2154,  2158,  2162,  2163,  2166,  2170,
    2171,  2176,  2177,  2181,  2182,  2184,  2186,  2187,  2189,  2191,
    2192,  2195,  2196,  2204,  2205,  2212,  2213,  2219,  2220,  2228,
    2229,  2236,  2237,  2243,  2245,  2246,  2251,  2252,  2255,  2259,
    2263,  2264,  2267,  2268,  2271,  2274,  2277,  2280,  2283,  2285,
    2288,  2292,  2296,  2300,  2304,  2307,  2308,  2311,  2312,  2319,
    2320,  2323,  2324,  2327,  2328,  2332,  2333,  2337,  2339,  2341,
    2343,  2345,  2349,  2353,  2354,  2356,  2359,  2360,  2363,  2364,
    2365,  2373,  2374,  2377,  2378,  2383,  2384,  2387,  2390,  2393,
    2396,  2400,  2404,  2407,  2408,  2411,  2416,  2417,  2418,  2425,
    2426,  2431,  2432,  2435,  2439,  2443,  2445,  2446,  2451,  2454,
    2457,  2460,  2463,  2464,  2465,  2470,  2473,  2476,  2480,  2481,
    2484,  2488,  2489,  2496,  2500,  2504,  2507,  2508,  2511,  2515,
    2516,  2517,  2524,  2525,  2528,  2532,  2533,  2540,  2544,  2548,
    2551,  2552,  2555,  2559,  2560,  2561,  2571,  2572,  2573,  2581,
    2582,  2585,  2589,  2590,  2597,  2598,  2601,  2602,  2605,  2610,
    2614,  2617,  2618,  2621,  2622,  2623,  2630,  2631,  2634,  2637,
    2638,  2639,  2648,  2649,  2654,  2655,  2660,  2661,  2667,  2669,
    2670,  2673,  2676,  2679,  2682,  2683,  2688,  2689,  2692,  2695,
    2698,  2701,  2705,  2709,  2712,  2713,  2716,  2717
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int16 yyrhs[] =
{
     269,     0,    -1,   270,   272,   273,   278,    -1,    -1,    -1,
     159,   271,     4,   261,    -1,    -1,     8,   170,   261,    -1,
       8,   171,   261,    -1,    -1,   273,   274,    -1,     1,    -1,
     275,    -1,   553,    -1,   693,    -1,   402,    -1,   554,    -1,
     366,    -1,   722,    -1,   615,    -1,   535,    -1,   597,    -1,
     439,    -1,   737,    -1,   644,    -1,   680,    -1,   389,    -1,
     571,    -1,   711,    -1,   498,    -1,   757,    -1,   631,    -1,
     368,    -1,   281,    -1,   306,    -1,   309,    -1,   311,    -1,
     276,    -1,   314,    -1,   305,    -1,   283,    -1,   365,    -1,
     285,    -1,   316,    -1,   321,    -1,   286,    -1,   347,    -1,
     307,    -1,   279,    -1,   357,    -1,   304,    -1,    -1,     9,
     277,     4,   261,    -1,    96,     9,    -1,    -1,    11,   280,
       4,   261,    -1,    -1,    13,   282,     4,   261,    -1,    -1,
      14,   284,     4,   261,    -1,     7,    -1,    -1,   162,   287,
     288,    96,   162,    -1,    -1,   288,   289,    -1,    -1,     9,
     290,     4,   300,   261,    -1,    -1,    76,   291,     4,   300,
     261,    -1,    -1,   178,   292,     4,   300,   261,    -1,    -1,
      41,   293,     4,   300,   261,    -1,    -1,    70,   294,     4,
     300,   261,    -1,    -1,   174,   295,     4,   300,   261,    -1,
      -1,    29,   296,     4,   300,   261,    -1,    -1,   125,   297,
       4,   300,   261,    -1,    -1,   179,   298,     4,   300,   261,
      -1,    -1,    59,   299,     4,   300,   261,    -1,     1,   261,
      -1,    -1,   165,   301,   524,   303,    -1,    -1,   164,   302,
     524,   303,    -1,   163,    -1,   163,     3,    -1,   169,     4,
      -1,    -1,     6,    -1,    12,    94,    95,     6,   261,    -1,
     160,     3,   261,    -1,   161,     3,   261,    -1,    -1,    15,
     308,     4,     6,     6,   313,    25,     6,    26,     6,    27,
       6,     6,   261,    -1,    -1,    16,   310,     4,     6,     6,
     313,    25,     6,    26,     6,    27,     6,     6,   261,    -1,
      -1,    17,   312,     4,     6,     6,   313,    25,     6,    26,
       6,    27,     6,     6,   261,    -1,    61,    -1,    64,    -1,
      62,    -1,    63,    -1,    65,    -1,    68,    -1,    67,    -1,
      66,    -1,    -1,    18,   315,   384,   385,   386,   261,    -1,
     317,   318,   320,    -1,    20,     6,    -1,    -1,   318,   319,
      -1,    21,     6,    22,     6,   261,    -1,    96,    20,    -1,
     322,   323,   346,    -1,    19,     6,   261,    -1,    -1,   323,
     324,    -1,    -1,    -1,    -1,   262,   325,     4,   263,    76,
     326,     4,   327,   328,   261,    -1,    -1,   328,   329,    -1,
     263,   144,    -1,   367,    -1,   263,   145,     4,    -1,   263,
     245,     3,    -1,    -1,   263,   252,   330,     4,    -1,    -1,
     263,   241,   331,     4,    -1,   263,    47,   343,    -1,   263,
     251,    -1,    -1,    -1,   263,    28,   332,     4,   333,   340,
     387,   387,    -1,    -1,    -1,   263,   250,   334,     4,   335,
     341,   384,   385,   385,   386,    -1,    -1,   263,   253,   336,
       4,   264,     6,     6,   265,    -1,   436,   387,   313,    -1,
     263,   184,     6,   344,    -1,   263,   185,     6,   344,    -1,
     263,   186,     6,   344,    -1,   263,   187,     6,   344,    -1,
      -1,   263,   188,     6,    28,   337,     4,    -1,    -1,   263,
     189,     6,    28,   338,     4,    -1,   263,   190,     6,   344,
      -1,    -1,   263,   191,     6,    28,   339,     4,    -1,   263,
     229,   342,    -1,    -1,    58,     6,    -1,   237,     6,    -1,
      -1,    58,     6,    -1,   237,     6,    -1,   231,    -1,   232,
      -1,   233,    -1,   234,    -1,   192,    -1,   193,    -1,   194,
      -1,   195,    -1,   196,    -1,   197,    -1,   198,    -1,   199,
      -1,    -1,    -1,    28,   345,     4,    -1,    96,    19,    -1,
      -1,    -1,    29,   348,     4,     4,     6,     6,   313,   349,
     350,   352,   261,    -1,    -1,    25,     6,    26,     6,   351,
      -1,    -1,    27,     6,     6,    -1,    -1,   352,   353,    -1,
      -1,   263,   166,   354,   355,    -1,    -1,   355,   356,    -1,
       4,     6,    -1,     4,     3,    -1,     4,     4,    -1,    -1,
     359,     6,   358,    25,     6,    27,     6,   361,   261,    -1,
      23,   360,    -1,   172,    -1,   173,    -1,    -1,    -1,    28,
     362,   364,   363,    -1,    -1,   364,   363,    -1,     4,    -1,
      24,   360,     6,    25,     6,    27,     6,   261,    -1,   167,
      -1,   263,   167,    -1,   369,   370,   388,    -1,    10,     6,
     261,    -1,    -1,   370,   371,    -1,    -1,    -1,   262,   372,
       4,   373,   374,   261,    -1,    -1,   374,   375,    -1,    -1,
     263,    30,   376,     4,   387,   387,    -1,    -1,    -1,   263,
     250,   377,     4,   378,   384,   385,   385,   386,    -1,    -1,
     263,    50,   379,     4,    -1,    -1,    -1,   263,   254,   380,
       4,   263,   235,     6,     6,   263,   243,   381,     4,     4,
       4,   263,   236,     6,     6,   263,   239,     6,     6,     6,
       6,    -1,   382,    -1,   367,    -1,   263,   248,     6,     6,
      -1,   263,   247,     6,     6,    -1,   263,   246,     6,     6,
       6,     6,    -1,    -1,   263,    49,   383,     4,    -1,   387,
      -1,   387,    -1,    -1,   385,   386,    -1,   264,     6,     6,
     265,    -1,   264,   266,     6,   265,    -1,   264,     6,   266,
     265,    -1,   264,   266,   266,   265,    -1,    96,    10,    -1,
     390,   391,    96,    42,    -1,    42,     6,   261,    -1,    -1,
     391,   392,    -1,    -1,    -1,   262,   393,     4,   394,   395,
     396,   261,    -1,   387,   387,    -1,   395,   387,   387,    -1,
      -1,   396,   397,    -1,    -1,   263,   166,   398,   399,    -1,
     263,   228,   401,    -1,    -1,   399,   400,    -1,     4,     6,
      -1,     4,     3,    -1,     4,     4,    -1,   223,    -1,   225,
      -1,   403,   404,   438,    -1,    31,     6,   261,    -1,    -1,
     404,   405,    -1,   406,   410,   261,    -1,   407,   409,    -1,
      -1,   262,   408,     4,     4,    -1,    -1,   409,   266,    -1,
     409,     4,    -1,    -1,   410,   411,    -1,   415,    -1,   418,
      -1,   435,    -1,   437,    -1,   432,    -1,   420,    -1,   413,
      -1,   422,    -1,   425,    -1,   427,    -1,   412,    -1,   367,
      -1,    -1,   263,    35,   414,     4,    -1,    -1,   263,    32,
     416,     4,   417,    -1,    -1,     4,    -1,   263,    33,   419,
      -1,   215,    -1,   216,    -1,   217,    -1,   218,    -1,   431,
     421,    -1,   431,     4,    -1,   387,   387,    -1,   421,   387,
     387,    -1,    -1,   263,   240,   423,   424,     6,     6,     6,
       6,    -1,    -1,    71,    -1,    -1,   263,   260,     6,   426,
       4,     4,    -1,    -1,   263,   166,   428,   429,    -1,   430,
      -1,   429,   430,    -1,     4,     6,    -1,     4,     3,    -1,
       4,     4,    -1,   263,    41,    -1,    -1,   263,    40,   433,
       4,   434,   313,    -1,   387,    -1,     6,     6,    -1,   436,
     387,   313,    -1,   263,    38,    -1,   263,    38,   387,   313,
      -1,   263,    36,    -1,   263,    37,    -1,   263,    39,    -1,
     263,    34,     6,    -1,    96,    31,    -1,   440,   441,   496,
      -1,    43,     6,   261,    -1,    -1,   441,   442,    -1,   443,
     455,   261,    -1,   444,    -1,    -1,   262,   445,   446,    -1,
      -1,     4,   447,   449,    -1,    -1,    45,   264,     4,   448,
       4,   265,    -1,    -1,   449,   450,    -1,    -1,   264,     4,
     451,     4,   454,   265,    -1,    -1,   264,   266,   452,     4,
     454,   265,    -1,    -1,   264,   125,   453,     4,   454,   265,
      -1,    -1,   367,    -1,   263,   126,    -1,    -1,   455,   456,
      -1,    -1,   263,   479,   457,   480,    -1,   263,    33,   470,
      -1,   263,   222,    -1,    -1,   263,   224,   458,     6,    -1,
      -1,   263,    46,   459,     4,    -1,   263,    49,   525,    -1,
     263,    34,     6,    -1,   263,   124,     6,    -1,   263,    51,
       6,    -1,   263,    47,   343,    -1,   263,    48,     6,    -1,
      -1,   263,    59,   460,     4,    -1,   471,    -1,    -1,   263,
     182,   461,     4,    -1,    -1,    -1,   263,   183,   462,   463,
     480,    -1,    -1,    -1,    -1,   263,   123,   464,     4,   465,
     671,   466,   675,    -1,    -1,   263,   166,   467,   468,    -1,
     367,    -1,   469,    -1,   468,   469,    -1,     4,     6,    -1,
       4,     3,    -1,     4,     4,    -1,   215,    -1,   216,    -1,
     217,    -1,   218,    -1,   176,    -1,    -1,   473,   475,   387,
     387,   472,   477,    -1,    -1,   263,   122,   474,     4,    -1,
      -1,    -1,    28,   476,     4,    -1,    -1,   478,   387,   313,
      -1,    39,    -1,    36,    -1,    37,    -1,    36,    -1,    37,
      -1,    52,    -1,   483,    -1,   480,   481,    -1,    -1,    53,
     482,   483,    -1,    -1,    -1,     4,   484,   489,   488,   485,
     486,    -1,    -1,   486,   487,    -1,     4,    -1,     4,   313,
      -1,     4,    25,     6,    26,     6,    27,     6,     6,    -1,
     488,    -1,   264,     6,     6,   265,    -1,   264,   266,     6,
     265,    -1,   264,     6,   266,   265,    -1,   264,   266,   266,
     265,    -1,   264,     6,     6,     6,   265,    -1,   264,   266,
       6,     6,   265,    -1,   264,     6,   266,     6,   265,    -1,
     264,   266,   266,     6,   265,    -1,    -1,   489,   490,    -1,
     493,    -1,   491,    -1,   157,    -1,    -1,   158,   492,     4,
      -1,    48,     6,    -1,    -1,   494,   495,    -1,   263,    55,
     497,    -1,   263,    48,     6,    -1,    96,    43,    -1,   200,
      -1,   201,    -1,   202,    -1,   203,    -1,   204,    -1,   205,
      -1,   206,    -1,   257,    -1,   230,    -1,   207,    -1,   208,
      -1,   209,    -1,   533,   499,   534,    -1,    -1,   499,   500,
      -1,   443,   501,   261,    -1,    -1,   501,   502,    -1,   514,
      -1,   516,    -1,   518,    -1,   503,    -1,   263,   479,    -1,
      -1,   263,   479,   504,   526,    -1,    -1,    -1,   263,   181,
     505,     4,   506,   512,    -1,    -1,    -1,   263,   250,   507,
       4,   508,   384,   385,   385,   386,    -1,    -1,   263,    30,
     509,     4,   387,   387,    -1,   263,    33,   419,    -1,   263,
     222,    -1,   263,   224,     6,    -1,    -1,   263,    46,   510,
       4,    -1,   263,    49,   525,    -1,   263,    34,     6,    -1,
     263,    51,     6,    -1,   263,    47,   343,    -1,   263,    48,
       6,    -1,    -1,   263,   166,   511,   521,    -1,   367,    -1,
      -1,    -1,   513,   526,    -1,    -1,   263,    56,   515,     4,
       6,    -1,    -1,   263,    57,   517,     4,    -1,    -1,    -1,
     263,    58,   519,     4,     6,   520,   523,    -1,   522,    -1,
     521,   522,    -1,     4,     6,    -1,     4,     3,    -1,     4,
       4,    -1,    -1,   146,     6,     6,    -1,    -1,   146,     6,
       6,    -1,   219,    -1,   220,    -1,   221,    -1,    91,    -1,
     529,    -1,   526,   527,    -1,    -1,    53,   528,   529,    -1,
      -1,    -1,     4,   530,   532,   494,   488,   531,   486,    -1,
       6,    -1,    54,     6,   261,    -1,    96,    54,    -1,   536,
     537,   552,    -1,    69,     6,   261,    -1,    -1,   537,   538,
      -1,   539,   541,   543,   261,    -1,    -1,   262,   540,     4,
      -1,    -1,   541,   542,    -1,     4,    -1,    -1,   543,   544,
      -1,   263,    71,   550,    -1,    -1,   263,   166,   545,   548,
      -1,    -1,   263,    41,   546,   547,    -1,   367,    -1,   387,
     387,    -1,     4,    -1,    -1,   548,   549,    -1,     4,     6,
      -1,     4,     3,    -1,     4,     4,    -1,    -1,   550,   551,
      -1,    72,     6,    -1,    73,     6,    -1,    74,     6,    -1,
      96,    69,    -1,   555,   557,   570,    -1,   556,   557,   569,
      -1,    93,     6,   261,    -1,    75,     6,   261,    -1,    -1,
     557,   558,    -1,   559,    -1,   564,    -1,   262,   560,   567,
     261,    -1,    -1,    76,   561,     4,    -1,    -1,    77,   562,
       4,     4,     4,     4,    -1,    78,   264,   563,   265,    -1,
      79,   264,   563,   265,    -1,   560,    -1,   563,   560,    -1,
     563,   267,   560,    -1,    -1,   262,    91,   565,     4,   566,
      92,     6,   261,    -1,    -1,   263,    -1,    -1,   567,   568,
      -1,   263,    87,     6,    -1,   263,    88,     6,    -1,   263,
      89,     6,    -1,   263,    90,     6,    -1,    96,    75,    -1,
      96,    93,    -1,   572,   573,   596,    -1,    80,     6,   261,
      -1,    -1,   573,   574,    -1,   575,   577,   261,    -1,    -1,
     262,   576,     4,    -1,    -1,   577,   579,    -1,    -1,     4,
      -1,    -1,   263,    81,   580,     4,   578,    -1,    -1,   263,
      82,   581,   587,    -1,    -1,   263,    83,   582,   591,    -1,
      -1,   263,    84,   583,     4,   578,    -1,    -1,   263,   177,
     584,   586,    -1,    -1,   263,   227,   585,     4,   595,    -1,
     367,    -1,    -1,   264,     4,     4,   265,    -1,   264,     4,
       4,   265,   264,     4,     4,   265,    -1,    -1,   587,   588,
      -1,    -1,     4,   589,   590,    -1,    -1,   264,     4,     4,
     265,    -1,   264,     4,     4,   265,   264,     4,     4,   265,
      -1,   264,     4,     4,   265,   264,     4,     4,   265,   264,
       4,     4,   265,    -1,    -1,   591,   592,    -1,    -1,     4,
     593,   594,    -1,    -1,   264,     4,     4,   265,    -1,   264,
       4,     4,   265,   264,     4,     4,   265,    -1,   264,     4,
       4,   265,   264,     4,     4,   265,   264,     4,     4,   265,
      -1,    -1,   226,     6,    -1,    96,    80,    -1,   598,   599,
     614,    -1,    97,     6,   261,    -1,    -1,   599,   600,    -1,
     601,   603,   261,    -1,    -1,   262,   264,   602,     4,     4,
     265,    -1,    -1,   603,   604,    -1,   263,   613,   100,     6,
       6,    -1,   263,   613,   101,     6,     6,    -1,   263,   102,
       6,    -1,    -1,    -1,   263,   103,   605,     4,   606,   607,
      -1,   367,    -1,    -1,    -1,   610,   105,   608,     4,   609,
     612,    -1,    -1,    -1,   104,   611,     4,    -1,    -1,   106,
       6,    -1,    98,    -1,    99,    -1,    96,    97,    -1,   616,
     617,    96,   147,    -1,   147,     6,   261,    -1,    -1,   617,
     618,    -1,    -1,    -1,   262,   619,     4,   621,   620,   622,
     623,   261,    -1,   148,    -1,   149,    -1,   150,    -1,   152,
       6,    -1,   151,     6,    -1,   153,     6,    -1,    -1,   623,
     624,    -1,    -1,   263,   154,   625,   627,    -1,    -1,   263,
     155,   626,   627,    -1,    -1,   627,   628,    -1,    -1,   264,
     156,   629,     4,   265,    -1,    -1,   264,    31,   630,     4,
     265,    -1,   632,   633,   643,    -1,   107,     6,   261,    -1,
      -1,   633,   634,    -1,    -1,    -1,   262,   104,   635,     4,
       4,   105,   636,     4,     4,   261,    -1,    -1,   262,   108,
     637,     4,     4,   261,    -1,    -1,   262,   109,   638,     4,
     639,   261,    -1,   262,   180,   261,    -1,    -1,    -1,   104,
     640,     4,   105,   641,     4,    -1,    -1,   108,   642,     4,
      -1,    96,   107,    -1,   645,   646,   670,    -1,   110,     6,
     261,    -1,    -1,   646,   647,    -1,   648,   653,   261,    -1,
      -1,   262,   649,     4,   650,    -1,    -1,   111,   651,   652,
      -1,    -1,   118,    -1,   119,    -1,    -1,   120,    -1,   121,
      -1,    -1,   653,   654,    -1,    -1,   263,   112,   655,     4,
       4,   613,   661,    -1,    -1,   263,   113,   656,     4,     4,
     668,    -1,    -1,   263,   114,   657,     4,   666,    -1,    -1,
     263,   115,   658,     4,     4,   613,   661,    -1,    -1,   263,
     116,   659,     4,     4,   668,    -1,    -1,   263,   117,   660,
       4,   666,    -1,   367,    -1,    -1,   663,    19,   662,   665,
      -1,    -1,   663,   664,    -1,   151,     6,     6,    -1,   152,
       6,     6,    -1,    -1,   665,     4,    -1,    -1,   666,   667,
      -1,    87,     6,    -1,    89,     6,    -1,    88,     6,    -1,
      90,     6,    -1,   669,    -1,   668,   669,    -1,    87,     6,
       6,    -1,    89,     6,     6,    -1,    88,     6,     6,    -1,
      90,     6,     6,    -1,    96,   110,    -1,    -1,   671,   672,
      -1,    -1,   264,   673,     4,     4,   674,   265,    -1,    -1,
     263,   126,    -1,    -1,   675,   676,    -1,    -1,   679,   677,
     480,    -1,    -1,    59,   678,     4,    -1,    36,    -1,    37,
      -1,    52,    -1,   183,    -1,   681,   684,   683,    -1,   175,
       6,   682,    -1,    -1,   261,    -1,    96,   175,    -1,    -1,
     684,   685,    -1,    -1,    -1,   262,   686,     4,     4,   687,
     688,   261,    -1,    -1,   688,   689,    -1,    -1,   263,   166,
     690,   691,    -1,    -1,   691,   692,    -1,     4,     6,    -1,
       4,     3,    -1,     4,     4,    -1,   694,   696,   695,    -1,
     210,     6,   261,    -1,    96,   210,    -1,    -1,   696,   697,
      -1,   698,   709,   708,   261,    -1,    -1,    -1,   262,    28,
     699,     4,   700,   702,    -1,    -1,   262,   211,   701,   706,
      -1,    -1,   702,   703,    -1,   263,    58,     6,    -1,   263,
     237,     6,    -1,   704,    -1,    -1,   263,   174,   705,     4,
      -1,   263,   212,    -1,   263,   213,    -1,   263,   214,    -1,
     263,   256,    -1,    -1,    -1,   263,   174,   707,     4,    -1,
     263,   214,    -1,   263,    71,    -1,   263,   259,     6,    -1,
      -1,   708,   709,    -1,    30,   387,   387,    -1,    -1,   250,
     710,   384,   385,   385,   386,    -1,   712,   714,   713,    -1,
     212,     6,   261,    -1,    96,   212,    -1,    -1,   714,   715,
      -1,   716,   719,   261,    -1,    -1,    -1,   262,    28,   717,
       4,   718,   720,    -1,    -1,   719,   720,    -1,    30,   387,
     387,    -1,    -1,   250,   721,   384,   385,   385,   386,    -1,
     723,   725,   724,    -1,   213,     6,   261,    -1,    96,   213,
      -1,    -1,   725,   726,    -1,   729,   732,   261,    -1,    -1,
      -1,   262,   253,   727,     4,   728,   736,   384,   386,   261,
      -1,    -1,    -1,   262,    28,   730,     4,   731,   735,   733,
      -1,    -1,   732,   733,    -1,    30,   387,   387,    -1,    -1,
     250,   734,   384,   385,   385,   386,    -1,    -1,   263,   258,
      -1,    -1,   263,   258,    -1,   738,   741,   740,   739,    -1,
      60,     6,   261,    -1,    96,    60,    -1,    -1,   740,   741,
      -1,    -1,    -1,   262,   742,     4,   743,   744,   261,    -1,
      -1,   744,   745,    -1,   263,   242,    -1,    -1,    -1,   263,
      28,   746,     4,    56,     6,   747,   751,    -1,    -1,   263,
     253,   748,     4,    -1,    -1,   263,   254,   749,     4,    -1,
      -1,   263,   244,   750,     4,     6,    -1,   753,    -1,    -1,
     751,   752,    -1,   238,     6,    -1,    58,     6,    -1,   255,
       6,    -1,    -1,   263,   166,   754,   755,    -1,    -1,   755,
     756,    -1,     4,     6,    -1,     4,     3,    -1,     4,     4,
      -1,   758,   760,   759,    -1,   249,     6,   261,    -1,    96,
     249,    -1,    -1,   760,   761,    -1,    -1,   262,    48,     6,
     762,   384,   385,   386,   261,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   355,   355,   358,   359,   359,   385,   386,   399,   417,
     418,   419,   422,   422,   422,   422,   423,   423,   423,   424,
     424,   424,   425,   425,   425,   426,   426,   426,   427,   427,
     427,   427,   428,   431,   431,   431,   431,   432,   432,   432,
     433,   433,   433,   434,   434,   434,   435,   435,   435,   435,
     435,   438,   438,   445,   494,   494,   500,   500,   506,   506,
     512,   519,   518,   534,   535,   538,   538,   547,   547,   556,
     556,   565,   565,   574,   574,   583,   583,   592,   592,   601,
     601,   611,   610,   621,   620,   641,   643,   643,   648,   648,
     654,   659,   664,   670,   671,   674,   682,   689,   696,   696,
     709,   709,   721,   721,   734,   735,   736,   737,   738,   739,
     740,   741,   744,   743,   760,   763,   775,   776,   779,   790,
     793,   796,   802,   803,   806,   807,   808,   806,   820,   821,
     823,   829,   835,   841,   862,   862,   882,   882,   902,   906,
     927,   928,   927,   946,   947,   946,   986,   986,  1010,  1021,
    1039,  1057,  1075,  1093,  1093,  1111,  1111,  1130,  1148,  1148,
    1166,  1183,  1184,  1207,  1231,  1232,  1255,  1279,  1284,  1289,
    1294,  1300,  1302,  1304,  1306,  1308,  1310,  1312,  1314,  1319,
    1320,  1320,  1323,  1329,  1331,  1329,  1345,  1355,  1393,  1396,
    1404,  1405,  1408,  1408,  1412,  1413,  1416,  1431,  1440,  1451,
    1450,  1484,  1489,  1491,  1494,  1495,  1495,  1498,  1499,  1502,
    1508,  1537,  1543,  1546,  1549,  1555,  1556,  1559,  1560,  1559,
    1571,  1572,  1575,  1575,  1580,  1581,  1580,  1608,  1608,  1618,
    1620,  1618,  1643,  1644,  1650,  1662,  1674,  1686,  1686,  1699,
    1702,  1705,  1706,  1709,  1716,  1722,  1728,  1734,  1740,  1746,
    1752,  1753,  1756,  1757,  1756,  1766,  1769,  1774,  1775,  1778,
    1778,  1781,  1796,  1797,  1800,  1815,  1824,  1834,  1836,  1839,
    1842,  1849,  1850,  1853,  1859,  1872,  1872,  1880,  1881,  1886,
    1892,  1893,  1896,  1896,  1896,  1896,  1897,  1897,  1897,  1897,
    1898,  1898,  1898,  1901,  1908,  1908,  1914,  1914,  1922,  1923,
    1926,  1932,  1934,  1936,  1938,  1943,  1945,  1951,  1962,  1975,
    1974,  1996,  1997,  2017,  2017,  2036,  2036,  2040,  2041,  2044,
    2059,  2068,  2078,  2081,  2081,  2096,  2098,  2101,  2108,  2115,
    2135,  2137,  2139,  2142,  2148,  2154,  2157,  2164,  2165,  2168,
    2179,  2183,  2183,  2186,  2185,  2194,  2194,  2202,  2203,  2206,
    2206,  2220,  2220,  2227,  2227,  2236,  2237,  2244,  2248,  2249,
    2253,  2252,  2268,  2271,  2288,  2288,  2306,  2306,  2309,  2312,
    2315,  2318,  2321,  2324,  2327,  2327,  2338,  2340,  2340,  2343,
    2344,  2343,  2381,  2387,  2397,  2380,  2411,  2411,  2415,  2421,
    2422,  2425,  2440,  2449,  2459,  2461,  2463,  2465,  2467,  2471,
    2470,  2481,  2481,  2484,  2485,  2485,  2488,  2489,  2492,  2494,
    2496,  2499,  2501,  2503,  2507,  2511,  2514,  2514,  2520,  2540,
    2519,  2549,  2550,  2554,  2564,  2571,  2599,  2608,  2616,  2623,
    2630,  2636,  2644,  2652,  2660,  2668,  2669,  2671,  2672,  2675,
    2679,  2679,  2684,  2703,  2704,  2708,  2712,  2732,  2738,  2740,
    2742,  2744,  2746,  2748,  2750,  2752,  2768,  2770,  2772,  2774,
    2777,  2780,  2781,  2784,  2787,  2788,  2791,  2791,  2792,  2792,
    2795,  2805,  2804,  2823,  2824,  2823,  2827,  2828,  2827,  2867,
    2867,  2896,  2899,  2902,  2905,  2905,  2908,  2911,  2914,  2923,
    2926,  2929,  2929,  2933,  2937,  2946,  2946,  2988,  2988,  2997,
    2997,  3015,  3016,  3015,  3023,  3024,  3027,  3042,  3051,  3061,
    3062,  3067,  3068,  3071,  3073,  3075,  3077,  3081,  3096,  3099,
    3099,  3117,  3124,  3116,  3131,  3136,  3143,  3150,  3153,  3159,
    3160,  3163,  3169,  3169,  3180,  3181,  3184,  3191,  3192,  3195,
    3197,  3197,  3200,  3200,  3202,  3208,  3218,  3223,  3224,  3227,
    3242,  3251,  3261,  3262,  3265,  3273,  3281,  3290,  3297,  3301,
    3304,  3318,  3332,  3333,  3336,  3337,  3347,  3360,  3360,  3365,
    3365,  3370,  3375,  3381,  3382,  3384,  3386,  3386,  3395,  3396,
    3399,  3400,  3403,  3408,  3413,  3418,  3424,  3435,  3446,  3449,
    3455,  3456,  3459,  3465,  3465,  3472,  3473,  3478,  3479,  3482,
    3482,  3486,  3486,  3489,  3488,  3497,  3497,  3501,  3501,  3503,
    3503,  3521,  3528,  3529,  3538,  3552,  3553,  3557,  3556,  3567,
    3568,  3581,  3602,  3633,  3634,  3638,  3637,  3646,  3647,  3660,
    3681,  3713,  3714,  3717,  3726,  3729,  3740,  3741,  3744,  3750,
    3750,  3756,  3757,  3761,  3766,  3771,  3776,  3777,  3776,  3802,
    3809,  3810,  3808,  3816,  3817,  3817,  3823,  3824,  3830,  3830,
    3832,  3838,  3844,  3850,  3851,  3854,  3855,  3854,  3859,  3861,
    3864,  3866,  3868,  3870,  3873,  3874,  3878,  3877,  3881,  3880,
    3885,  3886,  3888,  3888,  3890,  3890,  3893,  3897,  3904,  3905,
    3908,  3909,  3908,  3917,  3917,  3925,  3925,  3933,  3939,  3940,
    3939,  3945,  3945,  3951,  3958,  3961,  3968,  3969,  3972,  3978,
    3978,  3984,  3985,  3992,  3993,  3995,  3999,  4000,  4002,  4005,
    4006,  4009,  4009,  4015,  4015,  4021,  4021,  4027,  4027,  4033,
    4033,  4039,  4039,  4044,  4052,  4051,  4055,  4056,  4059,  4064,
    4070,  4071,  4074,  4075,  4077,  4079,  4081,  4083,  4087,  4088,
    4091,  4094,  4097,  4100,  4104,  4108,  4109,  4112,  4112,  4121,
    4122,  4125,  4126,  4129,  4128,  4141,  4141,  4144,  4146,  4148,
    4150,  4153,  4155,  4161,  4162,  4165,  4169,  4170,  4173,  4174,
    4173,  4183,  4184,  4186,  4186,  4190,  4191,  4194,  4209,  4218,
    4228,  4230,  4234,  4238,  4239,  4242,  4251,  4252,  4251,  4269,
    4268,  4284,  4285,  4288,  4314,  4336,  4340,  4340,  4356,  4371,
    4386,  4401,  4430,  4432,  4432,  4447,  4462,  4490,  4519,  4520,
    4523,  4529,  4528,  4558,  4560,  4564,  4568,  4569,  4572,  4580,
    4581,  4580,  4588,  4589,  4592,  4598,  4597,  4614,  4616,  4620,
    4624,  4625,  4628,  4635,  4636,  4635,  4656,  4657,  4656,  4664,
    4665,  4668,  4674,  4673,  4697,  4698,  4718,  4719,  4739,  4742,
    4760,  4764,  4765,  4768,  4769,  4768,  4779,  4780,  4783,  4788,
    4790,  4788,  4797,  4797,  4803,  4803,  4809,  4809,  4815,  4818,
    4819,  4822,  4828,  4834,  4842,  4842,  4846,  4847,  4850,  4861,
    4870,  4881,  4883,  4900,  4904,  4905,  4909,  4908
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "QSTRING", "T_STRING", "SITE_PATTERN",
  "NUMBER", "K_HISTORY", "K_NAMESCASESENSITIVE", "K_DESIGN", "K_VIAS",
  "K_TECH", "K_UNITS", "K_ARRAY", "K_FLOORPLAN", "K_SITE", "K_CANPLACE",
  "K_CANNOTOCCUPY", "K_DIEAREA", "K_PINS", "K_DEFAULTCAP", "K_MINPINS",
  "K_WIRECAP", "K_TRACKS", "K_GCELLGRID", "K_DO", "K_BY", "K_STEP",
  "K_LAYER", "K_ROW", "K_RECT", "K_COMPS", "K_COMP_GEN", "K_SOURCE",
  "K_WEIGHT", "K_EEQMASTER", "K_FIXED", "K_COVER", "K_UNPLACED",
  "K_PLACED", "K_FOREIGN", "K_REGION", "K_REGIONS", "K_NETS",
  "K_START_NET", "K_MUSTJOIN", "K_ORIGINAL", "K_USE", "K_STYLE",
  "K_PATTERN", "K_PATTERNNAME", "K_ESTCAP", "K_ROUTED", "K_NEW", "K_SNETS",
  "K_SHAPE", "K_WIDTH", "K_VOLTAGE", "K_SPACING", "K_NONDEFAULTRULE",
  "K_NONDEFAULTRULES", "K_N", "K_S", "K_E", "K_W", "K_FN", "K_FE", "K_FS",
  "K_FW", "K_GROUPS", "K_GROUP", "K_SOFT", "K_MAXX", "K_MAXY",
  "K_MAXHALFPERIMETER", "K_CONSTRAINTS", "K_NET", "K_PATH", "K_SUM",
  "K_DIFF", "K_SCANCHAINS", "K_START", "K_FLOATING", "K_ORDERED", "K_STOP",
  "K_IN", "K_OUT", "K_RISEMIN", "K_RISEMAX", "K_FALLMIN", "K_FALLMAX",
  "K_WIREDLOGIC", "K_MAXDIST", "K_ASSERTIONS", "K_DISTANCE", "K_MICRONS",
  "K_END", "K_IOTIMINGS", "K_RISE", "K_FALL", "K_VARIABLE", "K_SLEWRATE",
  "K_CAPACITANCE", "K_DRIVECELL", "K_FROMPIN", "K_TOPIN", "K_PARALLEL",
  "K_TIMINGDISABLES", "K_THRUPIN", "K_MACRO", "K_PARTITIONS", "K_TURNOFF",
  "K_FROMCLOCKPIN", "K_FROMCOMPPIN", "K_FROMIOPIN", "K_TOCLOCKPIN",
  "K_TOCOMPPIN", "K_TOIOPIN", "K_SETUPRISE", "K_SETUPFALL", "K_HOLDRISE",
  "K_HOLDFALL", "K_VPIN", "K_SUBNET", "K_XTALK", "K_PIN", "K_SYNTHESIZED",
  "K_DEFINE", "K_DEFINES", "K_DEFINEB", "K_IF", "K_THEN", "K_ELSE",
  "K_FALSE", "K_TRUE", "K_EQ", "K_NE", "K_LE", "K_LT", "K_GE", "K_GT",
  "K_OR", "K_AND", "K_NOT", "K_SPECIAL", "K_DIRECTION", "K_RANGE", "K_FPC",
  "K_HORIZONTAL", "K_VERTICAL", "K_ALIGN", "K_MIN", "K_MAX", "K_EQUAL",
  "K_BOTTOMLEFT", "K_TOPRIGHT", "K_ROWS", "K_TAPER", "K_TAPERRULE",
  "K_VERSION", "K_DIVIDERCHAR", "K_BUSBITCHARS", "K_PROPERTYDEFINITIONS",
  "K_STRING", "K_REAL", "K_INTEGER", "K_PROPERTY", "K_BEGINEXT",
  "K_ENDEXT", "K_NAMEMAPSTRING", "K_ON", "K_OFF", "K_X", "K_Y",
  "K_COMPONENT", "K_PINPROPERTIES", "K_TEST", "K_COMMONSCANPINS", "K_SNET",
  "K_COMPONENTPIN", "K_REENTRANTPATHS", "K_SHIELD", "K_SHIELDNET",
  "K_NOSHIELD", "K_ANTENNAPINPARTIALMETALAREA",
  "K_ANTENNAPINPARTIALMETALSIDEAREA", "K_ANTENNAPINGATEAREA",
  "K_ANTENNAPINDIFFAREA", "K_ANTENNAPINMAXAREACAR",
  "K_ANTENNAPINMAXSIDEAREACAR", "K_ANTENNAPINPARTIALCUTAREA",
  "K_ANTENNAPINMAXCUTCAR", "K_SIGNAL", "K_POWER", "K_GROUND", "K_CLOCK",
  "K_TIEOFF", "K_ANALOG", "K_SCAN", "K_RESET", "K_RING", "K_STRIPE",
  "K_FOLLOWPIN", "K_IOWIRE", "K_COREWIRE", "K_BLOCKWIRE", "K_FILLWIRE",
  "K_BLOCKAGEWIRE", "K_PADRING", "K_BLOCKRING", "K_BLOCKAGES",
  "K_PLACEMENT", "K_SLOTS", "K_FILLS", "K_PUSHDOWN", "K_NETLIST", "K_DIST",
  "K_USER", "K_TIMING", "K_BALANCED", "K_STEINER", "K_TRUNK",
  "K_FIXEDBUMP", "K_FENCE", "K_FREQUENCY", "K_GUIDE", "K_MAXBITS",
  "K_PARTITION", "K_TYPE", "K_ANTENNAMODEL", "K_DRCFILL", "K_OXIDE1",
  "K_OXIDE2", "K_OXIDE3", "K_OXIDE4", "K_CUTSIZE", "K_CUTSPACING",
  "K_DESIGNRULEWIDTH", "K_DIAGWIDTH", "K_ENCLOSURE", "K_HALO",
  "K_GROUNDSENSITIVITY", "K_HARDSPACING", "K_LAYERS", "K_MINCUTS",
  "K_NETEXPR", "K_OFFSET", "K_ORIGIN", "K_ROWCOL", "K_STYLES", "K_POLYGON",
  "K_PORT", "K_SUPPLYSENSITIVITY", "K_VIA", "K_VIARULE", "K_WIREEXT",
  "K_EXCEPTPGNET", "K_FILLWIREOPC", "K_OPC", "K_PARTIAL", "K_ROUTEHALO",
  "';'", "'-'", "'+'", "'('", "')'", "'*'", "','", "$accept", "def_file",
  "version_stmt", "$@1", "case_sens_stmt", "rules", "rule",
  "design_section", "design_name", "$@2", "end_design", "tech_name", "$@3",
  "array_name", "$@4", "floorplan_name", "$@5", "history",
  "prop_def_section", "$@6", "property_defs", "property_def", "$@7", "$@8",
  "$@9", "$@10", "$@11", "$@12", "$@13", "$@14", "$@15", "$@16",
  "property_type_and_val", "$@17", "$@18", "opt_num_val", "units",
  "divider_char", "bus_bit_chars", "site", "$@19", "canplace", "$@20",
  "cannotoccupy", "$@21", "orient", "die_area", "$@22", "pin_cap_rule",
  "start_def_cap", "pin_caps", "pin_cap", "end_def_cap", "pin_rule",
  "start_pins", "pins", "pin", "$@23", "$@24", "$@25", "pin_options",
  "pin_option", "$@26", "$@27", "$@28", "$@29", "$@30", "$@31", "$@32",
  "$@33", "$@34", "$@35", "pin_layer_spacing_opt", "pin_poly_spacing_opt",
  "pin_oxide", "use_type", "pin_layer_opt", "$@36", "end_pins", "row_rule",
  "$@37", "$@38", "row_do_option", "row_step_option", "row_options",
  "row_option", "$@39", "row_prop_list", "row_prop", "tracks_rule", "$@40",
  "track_start", "track_type", "track_layer_statement", "$@41",
  "track_layers", "track_layer", "gcellgrid", "extension_section",
  "extension_stmt", "via_section", "via", "via_declarations",
  "via_declaration", "$@42", "$@43", "layer_stmts", "layer_stmt", "$@44",
  "$@45", "$@46", "$@47", "$@48", "$@49", "layer_viarule_opts", "$@50",
  "firstPt", "nextPt", "otherPts", "pt", "via_end", "regions_section",
  "regions_start", "regions_stmts", "regions_stmt", "$@51", "$@52",
  "rect_list", "region_options", "region_option", "$@53",
  "region_prop_list", "region_prop", "region_type", "comps_section",
  "start_comps", "comps_rule", "comp", "comp_start", "comp_id_and_name",
  "$@54", "comp_net_list", "comp_options", "comp_option",
  "comp_extension_stmt", "comp_eeq", "$@55", "comp_generate", "$@56",
  "opt_pattern", "comp_source", "source_type", "comp_region",
  "comp_pnt_list", "comp_halo", "$@57", "halo_soft", "comp_routehalo",
  "$@58", "comp_property", "$@59", "comp_prop_list", "comp_prop",
  "comp_region_start", "comp_foreign", "$@60", "opt_paren", "comp_type",
  "placement_status", "weight", "end_comps", "nets_section", "start_nets",
  "net_rules", "one_net", "net_and_connections", "net_start", "$@61",
  "net_name", "$@62", "$@63", "net_connections", "net_connection", "$@64",
  "$@65", "$@66", "conn_opt", "net_options", "net_option", "$@67", "$@68",
  "$@69", "$@70", "$@71", "$@72", "$@73", "$@74", "$@75", "$@76", "$@77",
  "net_prop_list", "net_prop", "netsource_type", "vpin_stmt", "$@78",
  "vpin_begin", "$@79", "vpin_layer_opt", "$@80", "vpin_options",
  "vpin_status", "net_type", "paths", "new_path", "$@81", "path", "$@82",
  "$@83", "path_item_list", "path_item", "path_pt", "opt_taper_style_s",
  "opt_taper_style", "opt_taper", "$@84", "opt_style", "opt_spaths",
  "opt_shape_style", "end_nets", "shape_type", "snets_section",
  "snet_rules", "snet_rule", "snet_options", "snet_option",
  "snet_other_option", "$@85", "$@86", "$@87", "$@88", "$@89", "$@90",
  "$@91", "$@92", "shield_layer", "$@93", "snet_width", "$@94",
  "snet_voltage", "$@95", "snet_spacing", "$@96", "$@97", "snet_prop_list",
  "snet_prop", "opt_snet_range", "opt_range", "pattern_type", "spaths",
  "snew_path", "$@98", "spath", "$@99", "$@100", "width", "start_snets",
  "end_snets", "groups_section", "groups_start", "group_rules",
  "group_rule", "start_group", "$@101", "group_members", "group_member",
  "group_options", "group_option", "$@102", "$@103", "group_region",
  "group_prop_list", "group_prop", "group_soft_options",
  "group_soft_option", "groups_end", "assertions_section",
  "constraint_section", "assertions_start", "constraints_start",
  "constraint_rules", "constraint_rule", "operand_rule", "operand",
  "$@104", "$@105", "operand_list", "wiredlogic_rule", "$@106", "opt_plus",
  "delay_specs", "delay_spec", "constraints_end", "assertions_end",
  "scanchains_section", "scanchain_start", "scanchain_rules", "scan_rule",
  "start_scan", "$@107", "scan_members", "opt_pin", "scan_member", "$@108",
  "$@109", "$@110", "$@111", "$@112", "$@113", "opt_common_pins",
  "floating_inst_list", "one_floating_inst", "$@114", "floating_pins",
  "ordered_inst_list", "one_ordered_inst", "$@115", "ordered_pins",
  "partition_maxbits", "scanchain_end", "iotiming_section",
  "iotiming_start", "iotiming_rules", "iotiming_rule", "start_iotiming",
  "$@116", "iotiming_members", "iotiming_member", "$@117", "$@118",
  "iotiming_drivecell_opt", "$@119", "$@120", "iotiming_frompin", "$@121",
  "iotiming_parallel", "risefall", "iotiming_end",
  "floorplan_contraints_section", "fp_start", "fp_stmts", "fp_stmt",
  "$@122", "$@123", "h_or_v", "constraint_type", "constrain_what_list",
  "constrain_what", "$@124", "$@125", "row_or_comp_list", "row_or_comp",
  "$@126", "$@127", "timingdisables_section", "timingdisables_start",
  "timingdisables_rules", "timingdisables_rule", "$@128", "$@129", "$@130",
  "$@131", "td_macro_option", "$@132", "$@133", "$@134",
  "timingdisables_end", "partitions_section", "partitions_start",
  "partition_rules", "partition_rule", "start_partition", "$@135",
  "turnoff", "turnoff_setup", "turnoff_hold", "partition_members",
  "partition_member", "$@136", "$@137", "$@138", "$@139", "$@140", "$@141",
  "minmaxpins", "$@142", "min_or_max_list", "min_or_max_member",
  "pin_list", "risefallminmax1_list", "risefallminmax1",
  "risefallminmax2_list", "risefallminmax2", "partitions_end",
  "comp_names", "comp_name", "$@143", "subnet_opt_syn", "subnet_options",
  "subnet_option", "$@144", "$@145", "subnet_type", "pin_props_section",
  "begin_pin_props", "opt_semi", "end_pin_props", "pin_prop_list",
  "pin_prop_terminal", "$@146", "$@147", "pin_prop_options", "pin_prop",
  "$@148", "pin_prop_name_value_list", "pin_prop_name_value",
  "blockage_section", "blockage_start", "blockage_end", "blockage_defs",
  "blockage_def", "blockage_rule", "$@149", "$@150", "$@151",
  "layer_blockage_rules", "layer_blockage_rule", "comp_blockage_rule",
  "$@152", "placement_comp_rule", "$@153", "rectPoly_blockage_rules",
  "rectPoly_blockage", "$@154", "slot_section", "slot_start", "slot_end",
  "slot_defs", "slot_def", "slot_rule", "$@155", "$@156",
  "geom_slot_rules", "geom_slot", "$@157", "fill_section", "fill_start",
  "fill_end", "fill_defs", "fill_def", "$@158", "$@159", "fill_rule",
  "$@160", "$@161", "geom_fill_rules", "geom_fill", "$@162",
  "fill_layer_opc", "fill_via_opc", "nondefaultrule_section",
  "nondefault_start", "nondefault_end", "nondefault_defs",
  "nondefault_def", "$@163", "$@164", "nondefault_options",
  "nondefault_option", "$@165", "$@166", "$@167", "$@168", "$@169",
  "nondefault_layer_options", "nondefault_layer_option",
  "nondefault_prop_opt", "$@170", "nondefault_prop_list",
  "nondefault_prop", "styles_section", "styles_start", "styles_end",
  "styles_rules", "styles_rule", "$@171", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,   389,   390,   391,   392,   393,   394,
     395,   396,   397,   398,   399,   400,   401,   402,   403,   404,
     405,   406,   407,   408,   409,   410,   411,   412,   413,   414,
     415,   416,   417,   418,   419,   420,   421,   422,   423,   424,
     425,   426,   427,   428,   429,   430,   431,   432,   433,   434,
     435,   436,   437,   438,   439,   440,   441,   442,   443,   444,
     445,   446,   447,   448,   449,   450,   451,   452,   453,   454,
     455,   456,   457,   458,   459,   460,   461,   462,   463,   464,
     465,   466,   467,   468,   469,   470,   471,   472,   473,   474,
     475,   476,   477,   478,   479,   480,   481,   482,   483,   484,
     485,   486,   487,   488,   489,   490,   491,   492,   493,   494,
     495,   496,   497,   498,   499,   500,   501,   502,   503,   504,
     505,   506,   507,   508,   509,   510,   511,   512,   513,   514,
     515,    59,    45,    43,    40,    41,    42,    44
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint16 yyr1[] =
{
       0,   268,   269,   270,   271,   270,   272,   272,   272,   273,
     273,   273,   274,   274,   274,   274,   274,   274,   274,   274,
     274,   274,   274,   274,   274,   274,   274,   274,   274,   274,
     274,   274,   274,   275,   275,   275,   275,   275,   275,   275,
     275,   275,   275,   275,   275,   275,   275,   275,   275,   275,
     275,   277,   276,   278,   280,   279,   282,   281,   284,   283,
     285,   287,   286,   288,   288,   290,   289,   291,   289,   292,
     289,   293,   289,   294,   289,   295,   289,   296,   289,   297,
     289,   298,   289,   299,   289,   289,   301,   300,   302,   300,
     300,   300,   300,   303,   303,   304,   305,   306,   308,   307,
     310,   309,   312,   311,   313,   313,   313,   313,   313,   313,
     313,   313,   315,   314,   316,   317,   318,   318,   319,   320,
     321,   322,   323,   323,   325,   326,   327,   324,   328,   328,
     329,   329,   329,   329,   330,   329,   331,   329,   329,   329,
     332,   333,   329,   334,   335,   329,   336,   329,   329,   329,
     329,   329,   329,   337,   329,   338,   329,   329,   339,   329,
     329,   340,   340,   340,   341,   341,   341,   342,   342,   342,
     342,   343,   343,   343,   343,   343,   343,   343,   343,   344,
     345,   344,   346,   348,   349,   347,   350,   350,   351,   351,
     352,   352,   354,   353,   355,   355,   356,   356,   356,   358,
     357,   359,   360,   360,   361,   362,   361,   363,   363,   364,
     365,   366,   367,   368,   369,   370,   370,   372,   373,   371,
     374,   374,   376,   375,   377,   378,   375,   379,   375,   380,
     381,   375,   375,   375,   382,   382,   382,   383,   382,   384,
     385,   386,   386,   387,   387,   387,   387,   388,   389,   390,
     391,   391,   393,   394,   392,   395,   395,   396,   396,   398,
     397,   397,   399,   399,   400,   400,   400,   401,   401,   402,
     403,   404,   404,   405,   406,   408,   407,   409,   409,   409,
     410,   410,   411,   411,   411,   411,   411,   411,   411,   411,
     411,   411,   411,   412,   414,   413,   416,   415,   417,   417,
     418,   419,   419,   419,   419,   420,   420,   421,   421,   423,
     422,   424,   424,   426,   425,   428,   427,   429,   429,   430,
     430,   430,   431,   433,   432,   434,   434,   435,   435,   435,
     436,   436,   436,   437,   438,   439,   440,   441,   441,   442,
     443,   445,   444,   447,   446,   448,   446,   449,   449,   451,
     450,   452,   450,   453,   450,   454,   454,   454,   455,   455,
     457,   456,   456,   456,   458,   456,   459,   456,   456,   456,
     456,   456,   456,   456,   460,   456,   456,   461,   456,   462,
     463,   456,   464,   465,   466,   456,   467,   456,   456,   468,
     468,   469,   469,   469,   470,   470,   470,   470,   470,   472,
     471,   474,   473,   475,   476,   475,   477,   477,   478,   478,
     478,   479,   479,   479,   480,   480,   482,   481,   484,   485,
     483,   486,   486,   487,   487,   487,   487,   488,   488,   488,
     488,   488,   488,   488,   488,   489,   489,   490,   490,   491,
     492,   491,   493,   494,   494,   495,   495,   496,   497,   497,
     497,   497,   497,   497,   497,   497,   497,   497,   497,   497,
     498,   499,   499,   500,   501,   501,   502,   502,   502,   502,
     503,   504,   503,   505,   506,   503,   507,   508,   503,   509,
     503,   503,   503,   503,   510,   503,   503,   503,   503,   503,
     503,   511,   503,   503,   512,   513,   512,   515,   514,   517,
     516,   519,   520,   518,   521,   521,   522,   522,   522,   523,
     523,   524,   524,   525,   525,   525,   525,   526,   526,   528,
     527,   530,   531,   529,   532,   533,   534,   535,   536,   537,
     537,   538,   540,   539,   541,   541,   542,   543,   543,   544,
     545,   544,   546,   544,   544,   547,   547,   548,   548,   549,
     549,   549,   550,   550,   551,   551,   551,   552,   553,   554,
     555,   556,   557,   557,   558,   558,   559,   561,   560,   562,
     560,   560,   560,   563,   563,   563,   565,   564,   566,   566,
     567,   567,   568,   568,   568,   568,   569,   570,   571,   572,
     573,   573,   574,   576,   575,   577,   577,   578,   578,   580,
     579,   581,   579,   582,   579,   583,   579,   584,   579,   585,
     579,   579,   586,   586,   586,   587,   587,   589,   588,   590,
     590,   590,   590,   591,   591,   593,   592,   594,   594,   594,
     594,   595,   595,   596,   597,   598,   599,   599,   600,   602,
     601,   603,   603,   604,   604,   604,   605,   606,   604,   604,
     608,   609,   607,   610,   611,   610,   612,   612,   613,   613,
     614,   615,   616,   617,   617,   619,   620,   618,   621,   621,
     622,   622,   622,   622,   623,   623,   625,   624,   626,   624,
     627,   627,   629,   628,   630,   628,   631,   632,   633,   633,
     635,   636,   634,   637,   634,   638,   634,   634,   640,   641,
     639,   642,   639,   643,   644,   645,   646,   646,   647,   649,
     648,   650,   650,   651,   651,   651,   652,   652,   652,   653,
     653,   655,   654,   656,   654,   657,   654,   658,   654,   659,
     654,   660,   654,   654,   662,   661,   663,   663,   664,   664,
     665,   665,   666,   666,   667,   667,   667,   667,   668,   668,
     669,   669,   669,   669,   670,   671,   671,   673,   672,   674,
     674,   675,   675,   677,   676,   678,   676,   679,   679,   679,
     679,   680,   681,   682,   682,   683,   684,   684,   686,   687,
     685,   688,   688,   690,   689,   691,   691,   692,   692,   692,
     693,   694,   695,   696,   696,   697,   699,   700,   698,   701,
     698,   702,   702,   703,   703,   703,   705,   704,   704,   704,
     704,   704,   706,   707,   706,   706,   706,   706,   708,   708,
     709,   710,   709,   711,   712,   713,   714,   714,   715,   717,
     718,   716,   719,   719,   720,   721,   720,   722,   723,   724,
     725,   725,   726,   727,   728,   726,   730,   731,   729,   732,
     732,   733,   734,   733,   735,   735,   736,   736,   737,   738,
     739,   740,   740,   742,   743,   741,   744,   744,   745,   746,
     747,   745,   748,   745,   749,   745,   750,   745,   745,   751,
     751,   752,   752,   752,   754,   753,   755,   755,   756,   756,
     756,   757,   758,   759,   760,   760,   762,   761
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     4,     0,     0,     4,     0,     3,     3,     0,
       2,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     0,     4,     2,     0,     4,     0,     4,     0,     4,
       1,     0,     5,     0,     2,     0,     5,     0,     5,     0,
       5,     0,     5,     0,     5,     0,     5,     0,     5,     0,
       5,     0,     5,     0,     5,     2,     0,     4,     0,     4,
       1,     2,     2,     0,     1,     5,     3,     3,     0,    14,
       0,    14,     0,    14,     1,     1,     1,     1,     1,     1,
       1,     1,     0,     6,     3,     2,     0,     2,     5,     2,
       3,     3,     0,     2,     0,     0,     0,    10,     0,     2,
       2,     1,     3,     3,     0,     4,     0,     4,     3,     2,
       0,     0,     8,     0,     0,    10,     0,     8,     3,     4,
       4,     4,     4,     0,     6,     0,     6,     4,     0,     6,
       3,     0,     2,     2,     0,     2,     2,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     0,
       0,     3,     2,     0,     0,    11,     0,     5,     0,     3,
       0,     2,     0,     4,     0,     2,     2,     2,     2,     0,
       9,     2,     1,     1,     0,     0,     4,     0,     2,     1,
       8,     1,     2,     3,     3,     0,     2,     0,     0,     6,
       0,     2,     0,     6,     0,     0,     9,     0,     4,     0,
       0,    24,     1,     1,     4,     4,     6,     0,     4,     1,
       1,     0,     2,     4,     4,     4,     4,     2,     4,     3,
       0,     2,     0,     0,     7,     2,     3,     0,     2,     0,
       4,     3,     0,     2,     2,     2,     2,     1,     1,     3,
       3,     0,     2,     3,     2,     0,     4,     0,     2,     2,
       0,     2,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     0,     4,     0,     5,     0,     1,
       3,     1,     1,     1,     1,     2,     2,     2,     3,     0,
       8,     0,     1,     0,     6,     0,     4,     1,     2,     2,
       2,     2,     2,     0,     6,     1,     2,     3,     2,     4,
       2,     2,     2,     3,     2,     3,     3,     0,     2,     3,
       1,     0,     3,     0,     3,     0,     6,     0,     2,     0,
       6,     0,     6,     0,     6,     0,     1,     2,     0,     2,
       0,     4,     3,     2,     0,     4,     0,     4,     3,     3,
       3,     3,     3,     3,     0,     4,     1,     0,     4,     0,
       0,     5,     0,     0,     0,     8,     0,     4,     1,     1,
       2,     2,     2,     2,     1,     1,     1,     1,     1,     0,
       6,     0,     4,     0,     0,     3,     0,     3,     1,     1,
       1,     1,     1,     1,     1,     2,     0,     3,     0,     0,
       6,     0,     2,     1,     2,     8,     1,     4,     4,     4,
       4,     5,     5,     5,     5,     0,     2,     1,     1,     1,
       0,     3,     2,     0,     2,     3,     3,     2,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       3,     0,     2,     3,     0,     2,     1,     1,     1,     1,
       2,     0,     4,     0,     0,     6,     0,     0,     9,     0,
       6,     3,     2,     3,     0,     4,     3,     3,     3,     3,
       3,     0,     4,     1,     0,     0,     2,     0,     5,     0,
       4,     0,     0,     7,     1,     2,     2,     2,     2,     0,
       3,     0,     3,     1,     1,     1,     1,     1,     2,     0,
       3,     0,     0,     7,     1,     3,     2,     3,     3,     0,
       2,     4,     0,     3,     0,     2,     1,     0,     2,     3,
       0,     4,     0,     4,     1,     2,     1,     0,     2,     2,
       2,     2,     0,     2,     2,     2,     2,     2,     3,     3,
       3,     3,     0,     2,     1,     1,     4,     0,     3,     0,
       6,     4,     4,     1,     2,     3,     0,     8,     0,     1,
       0,     2,     3,     3,     3,     3,     2,     2,     3,     3,
       0,     2,     3,     0,     3,     0,     2,     0,     1,     0,
       5,     0,     4,     0,     4,     0,     5,     0,     4,     0,
       5,     1,     0,     4,     8,     0,     2,     0,     3,     0,
       4,     8,    12,     0,     2,     0,     3,     0,     4,     8,
      12,     0,     2,     2,     3,     3,     0,     2,     3,     0,
       6,     0,     2,     5,     5,     3,     0,     0,     6,     1,
       0,     0,     6,     0,     0,     3,     0,     2,     1,     1,
       2,     4,     3,     0,     2,     0,     0,     8,     1,     1,
       1,     2,     2,     2,     0,     2,     0,     4,     0,     4,
       0,     2,     0,     5,     0,     5,     3,     3,     0,     2,
       0,     0,    10,     0,     6,     0,     6,     3,     0,     0,
       6,     0,     3,     2,     3,     3,     0,     2,     3,     0,
       4,     0,     3,     0,     1,     1,     0,     1,     1,     0,
       2,     0,     7,     0,     6,     0,     5,     0,     7,     0,
       6,     0,     5,     1,     0,     4,     0,     2,     3,     3,
       0,     2,     0,     2,     2,     2,     2,     2,     1,     2,
       3,     3,     3,     3,     2,     0,     2,     0,     6,     0,
       2,     0,     2,     0,     3,     0,     3,     1,     1,     1,
       1,     3,     3,     0,     1,     2,     0,     2,     0,     0,
       7,     0,     2,     0,     4,     0,     2,     2,     2,     2,
       3,     3,     2,     0,     2,     4,     0,     0,     6,     0,
       4,     0,     2,     3,     3,     1,     0,     4,     2,     2,
       2,     2,     0,     0,     4,     2,     2,     3,     0,     2,
       3,     0,     6,     3,     3,     2,     0,     2,     3,     0,
       0,     6,     0,     2,     3,     0,     6,     3,     3,     2,
       0,     2,     3,     0,     0,     9,     0,     0,     7,     0,
       2,     3,     0,     6,     0,     2,     0,     2,     4,     3,
       2,     0,     2,     0,     0,     6,     0,     2,     2,     0,
       0,     8,     0,     4,     0,     4,     0,     5,     1,     0,
       2,     2,     2,     2,     0,     4,     0,     2,     2,     2,
       2,     3,     3,     2,     0,     2,     0,     8
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
       3,     4,     0,     6,     0,     1,     0,     0,     0,     0,
       0,    11,     0,     5,     7,     8,    60,    51,     0,    54,
       0,    56,    58,    98,   100,   102,   112,     0,     0,     0,
       0,   183,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    61,   211,
       0,     0,     0,     0,     0,    10,    12,    37,     2,    48,
      33,    40,    42,    45,    50,    39,    34,    47,    35,    36,
      38,    43,   116,    44,   122,    46,    49,     0,    41,    17,
      32,   215,    26,   250,    15,   271,    22,   337,    29,   461,
      20,   529,    13,    16,   562,   562,    27,   590,    21,   636,
      19,   663,    31,   688,    24,   706,    25,   776,    14,   793,
      28,   826,    18,   840,    23,     0,    30,   894,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   115,
     202,   203,   201,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    53,     0,     0,     0,     0,     0,
       0,    63,   773,     0,     0,     0,     0,     0,     0,   199,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   863,   861,     0,
       0,   214,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   239,   121,     0,     0,   270,   249,   336,   525,   859,
     528,   561,   589,   560,   635,   687,   705,   662,    96,    97,
       0,   774,   772,   791,   824,   838,   892,     0,     0,   117,
     114,     0,   124,   123,   120,     0,     0,   217,   216,   213,
       0,   252,   251,     0,   275,   272,   280,   277,   269,     0,
     341,   338,   358,   340,   335,     0,   464,   462,   460,     0,
     532,   530,   534,   527,     0,     0,   563,   564,   565,   558,
       0,   559,     0,   593,   591,   595,   588,     0,     0,   637,
     641,   634,     0,   665,   664,     0,     0,   689,   686,     0,
     709,   707,   719,   704,     0,   778,   771,   777,     0,     0,
     790,   794,     0,     0,     0,   823,   827,   832,     0,     0,
     837,   841,   849,     0,     0,     0,     0,   891,   895,    52,
      55,     0,    57,    59,     0,     0,     0,     0,     0,   241,
     240,     0,     0,     0,    65,    77,    71,    83,    73,    67,
       0,    79,    75,    69,    81,    64,     0,   119,   182,     0,
       0,   247,     0,   248,     0,   334,     0,     0,   274,   447,
       0,     0,   526,     0,   557,     0,   537,   587,   567,   569,
       0,     0,   576,   580,   586,   633,     0,     0,   660,   639,
       0,   661,     0,   703,   690,   693,   695,     0,   754,     0,
       0,   775,     0,   792,   796,   799,     0,   821,   818,   825,
     829,     0,   839,   846,   843,     0,   864,     0,   858,   862,
     893,     0,    95,     0,     0,     0,     0,     0,     0,     0,
     241,     0,     0,     0,    85,     0,     0,     0,     0,     0,
       0,    62,     0,     0,     0,     0,     0,     0,     0,   218,
     253,     0,   273,     0,   293,   281,   292,   288,   282,   283,
     287,   289,   290,   291,     0,   286,   284,     0,   285,   279,
     278,   343,     0,   342,   339,     0,   388,   359,   376,   403,
     463,     0,   493,   465,   469,   466,   467,   468,   533,   536,
     535,     0,     0,     0,     0,     0,     0,     0,   594,   592,
       0,   611,   596,     0,   638,     0,   649,   642,     0,     0,
       0,     0,   697,   711,   708,     0,   733,   720,     0,     0,
     812,     0,     0,     0,     0,     0,   835,   828,   833,     0,
       0,     0,   852,   842,   850,   866,   860,   896,   104,   106,
     107,   105,   108,   111,   110,   109,     0,     0,     0,   243,
     245,   244,   246,   242,   113,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     220,     0,   276,   296,     0,     0,   294,   330,   331,   328,
     332,   323,   322,   315,   212,   309,     0,   306,     0,   305,
       0,   347,     0,     0,     0,   411,   412,   366,     0,     0,
       0,     0,   413,   374,   401,   382,     0,   386,   377,   379,
     363,   364,   360,   404,     0,   479,     0,     0,   484,     0,
       0,     0,     0,   497,   499,   501,   491,   473,   482,     0,
     476,   470,   531,     0,   544,   538,   568,     0,   573,     0,
       0,   578,   566,     0,   581,   599,   601,   603,   605,   607,
     609,     0,   658,   659,     0,   646,     0,   668,   669,   666,
       0,     0,     0,   713,   710,   721,   723,   725,   727,   729,
     731,   779,   797,     0,   800,   820,     0,   795,   819,   830,
       0,     0,   847,   844,     0,     0,     0,     0,     0,     0,
       0,     0,   184,    90,    88,    86,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   118,   125,   204,
       0,     0,   257,     0,   301,   302,   303,   304,   300,   333,
       0,     0,     0,     0,   311,   313,   307,     0,   327,   344,
     345,   398,   394,   395,   396,   397,   362,   369,     0,   171,
     172,   173,   174,   175,   176,   177,   178,   372,   373,   516,
     513,   514,   515,   368,   371,     0,     0,     0,   370,     0,
       0,   380,     0,     0,     0,     0,     0,   481,   487,     0,
     489,   490,   486,   488,     0,     0,     0,     0,     0,   483,
       0,     0,   542,   552,   540,     0,   571,     0,   574,   572,
     579,     0,     0,     0,     0,     0,     0,   615,   623,     0,
     612,     0,     0,   645,     0,     0,     0,     0,     0,     0,
     698,   701,     0,   714,   715,   716,     0,     0,     0,     0,
       0,     0,   781,   801,   816,   813,   815,     0,     0,     0,
     834,     0,   854,   856,   851,     0,   865,     0,   867,   878,
       0,     0,     0,     0,   210,   186,    91,   511,   511,    92,
      66,    78,    72,    84,    74,    68,    80,    76,    70,    82,
       0,   205,     0,   219,     0,   233,   221,   232,   255,     0,
       0,   298,   295,   329,     0,     0,   316,   317,   312,     0,
       0,   308,     0,   348,     0,   367,   375,   402,   383,     0,
     387,   389,   378,     0,   365,   418,   361,   414,   405,   399,
       0,   485,     0,   500,     0,     0,   492,   504,   474,   477,
     521,   472,   517,     0,   539,   547,     0,   575,     0,   582,
     583,   584,   585,   597,   602,   604,   597,     0,   608,   631,
     640,   647,     0,     0,   670,     0,     0,     0,   674,   691,
     694,     0,     0,   696,   717,   718,   712,     0,     0,   742,
       0,     0,   742,     0,   798,     0,   817,   241,   831,     0,
       0,     0,     0,     0,     0,   869,   884,   868,   876,   872,
     874,   241,     0,     0,     0,     0,   190,     0,    93,    93,
     126,     0,   200,   222,   237,   227,     0,     0,     0,   224,
     229,   256,   254,     0,   258,   299,   297,     0,   325,     0,
     320,   321,   319,   318,     0,     0,   349,   353,   351,     0,
     755,   392,   393,   391,   390,   381,   435,   416,   415,   406,
       0,   498,   502,   507,   508,   506,   505,   494,     0,     0,
     519,   518,   546,     0,   543,     0,     0,     0,   553,   541,
     570,     0,   598,   600,   617,   616,   625,   624,   606,     0,
       0,   610,   653,   643,   644,   672,   671,   673,     0,     0,
       0,   702,     0,     0,   726,     0,     0,   732,   780,     0,
     782,     0,   802,   805,   814,   822,   241,   855,   848,   857,
     241,   241,     0,   886,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    94,    89,    87,   128,   209,   207,
       0,     0,     0,     0,     0,     0,     0,     0,   259,     0,
     326,   324,     0,   314,     0,     0,     0,   346,   384,     0,
       0,   409,   410,   408,   400,     0,   480,   509,   475,     0,
       0,   524,   443,     0,   545,   554,   555,   556,     0,   548,
     577,   619,   627,     0,   632,   654,   648,     0,   667,     0,
     675,     0,   699,   736,     0,     0,     0,     0,   724,   748,
       0,     0,     0,     0,   743,   736,   730,   783,     0,   806,
     808,   809,   810,     0,   811,   836,     0,   853,     0,   885,
       0,   873,   875,   897,     0,     0,     0,     0,   185,     0,
     191,   512,     0,   206,   207,     0,   238,   228,     0,   235,
     234,   225,     0,   262,   267,   268,   261,     0,   355,   355,
     355,   757,   761,   756,     0,   439,   440,     0,   419,   436,
     438,   437,   417,     0,     0,   503,   496,     0,     0,   520,
     550,   551,   549,     0,   618,     0,   626,   613,     0,   650,
     676,   678,     0,     0,   722,     0,     0,     0,     0,     0,
     749,   744,   746,   745,   747,   728,   785,   803,     0,   804,
     845,     0,     0,   887,   877,     0,     0,     0,   188,   192,
     127,     0,   129,   131,     0,   208,     0,     0,     0,     0,
     260,   310,     0,   356,     0,     0,     0,     0,   385,   442,
       0,     0,     0,   421,   407,     0,   241,     0,   522,   444,
       0,     0,     0,   655,     0,   680,   680,   692,   700,   734,
       0,     0,   737,   750,   752,   751,   753,   784,   807,   870,
     889,   890,   888,     0,     0,     0,     0,   187,   194,   140,
       0,   130,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   136,     0,   143,   139,   134,   146,     0,   223,
     236,     0,     0,     0,   263,   357,   350,   354,   352,     0,
     767,   768,   769,   765,   770,   762,   763,   441,     0,     0,
       0,     0,   420,   510,   478,     0,     0,   421,     0,     0,
       0,   651,   677,   679,   740,     0,     0,     0,   786,   879,
      99,   101,   103,     0,   193,     0,   138,   132,   179,   179,
     179,   179,     0,     0,   179,     0,   167,   168,   169,   170,
     160,     0,   133,     0,     0,     0,   148,     0,     0,   265,
     266,   264,   759,     0,     0,     0,   427,     0,   429,     0,
     428,     0,   430,   423,   422,   426,   446,   448,   449,   450,
     451,   452,   453,   454,   457,   458,   459,   456,   455,   445,
     523,   620,   628,     0,   656,     0,   681,   735,   738,   739,
     788,   789,   787,   871,   189,     0,   195,   141,   180,   149,
     150,   151,   152,   153,   155,   157,   158,   137,   144,   135,
       0,   241,     0,     0,     0,   766,   764,   431,   433,   432,
     434,     0,   424,     0,     0,   614,     0,   652,   684,   682,
     741,     0,     0,     0,   880,   197,   198,   196,   161,     0,
       0,     0,     0,   164,     0,   226,     0,   760,   758,     0,
       0,     0,   657,     0,     0,   882,   881,   883,     0,     0,
       0,   181,   154,   156,   159,     0,     0,     0,     0,   230,
       0,     0,     0,     0,     0,   162,   163,     0,   165,   166,
       0,     0,     0,     0,   621,   629,   685,   683,   142,     0,
     147,     0,     0,     0,     0,   241,     0,     0,     0,     0,
     145,     0,   425,     0,     0,     0,   622,   630,     0,     0,
       0,     0,     0,     0,     0,     0,   231
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,     3,     4,     7,    12,    55,    56,    57,   118,
      58,    59,   120,    60,   122,    61,   123,    62,    63,   151,
     210,   335,   415,   420,   424,   417,   419,   423,   416,   422,
     425,   418,   677,   828,   827,  1075,    64,    65,    66,    67,
     124,    68,   125,    69,   126,   526,    70,   127,    71,    72,
     157,   219,   220,    73,    74,   158,   223,   339,   840,  1077,
    1172,  1252,  1394,  1391,  1375,  1488,  1393,  1493,  1395,  1490,
    1491,  1492,  1510,  1517,  1390,   727,  1449,  1489,   224,    75,
     134,   825,   956,  1307,  1072,  1170,  1308,  1374,  1446,    76,
     225,    77,   132,   842,   961,  1173,  1174,    78,    79,  1263,
      80,    81,   160,   228,   342,   550,   690,   846,  1080,  1086,
    1258,  1082,  1087,  1532,   847,  1081,   190,   410,   411,   320,
     229,    82,    83,   161,   232,   344,   551,   692,   850,   974,
    1183,  1260,  1334,  1186,    84,    85,   162,   235,   236,   237,
     346,   348,   347,   435,   436,   437,   700,   438,   693,   976,
     439,   698,   440,   569,   441,   704,   859,   442,   860,   443,
     703,   856,   857,   444,   445,   702,   979,   446,   447,   448,
     238,    86,    87,   163,   241,   242,   243,   350,   453,   571,
     864,   709,   863,  1094,  1096,  1095,  1264,   351,   457,   743,
     742,   718,   735,   740,   741,   873,   737,   990,  1192,   739,
     870,   871,   716,   458,   999,   459,   736,   594,   744,  1104,
    1105,   592,   876,   998,  1100,   877,   996,  1273,  1352,  1414,
    1415,  1099,  1199,  1200,  1270,  1201,  1208,  1279,   244,  1429,
      88,   164,   247,   353,   463,   464,   761,   758,  1007,   760,
    1008,   746,   749,   757,  1108,  1109,   465,   754,   466,   755,
     467,   756,  1107,   886,   887,  1205,   958,   733,   891,  1011,
    1113,   892,  1009,  1357,  1112,    89,   248,    90,    91,   165,
     251,   252,   355,   356,   470,   471,   615,   895,   893,  1014,
    1019,  1119,   894,  1018,   253,    92,    93,    94,    95,   166,
     256,   257,   618,   472,   473,   619,   258,   476,   771,   477,
     624,   261,   259,    96,    97,   168,   264,   265,   366,   367,
    1023,   482,   776,   777,   778,   779,   780,   781,   908,   904,
    1025,  1121,  1214,   905,  1027,  1122,  1216,  1031,   266,    98,
      99,   169,   269,   270,   483,   370,   487,   784,  1032,  1126,
    1284,  1434,  1127,  1218,  1477,   636,   271,   100,   101,   170,
     274,   372,   787,   639,   918,  1038,  1130,  1285,  1286,  1362,
    1436,  1504,  1503,   102,   103,   171,   277,   489,  1039,   490,
     491,   792,   921,  1223,   922,   278,   104,   105,   172,   281,
     282,   379,   644,   795,   926,   380,   497,   796,   797,   798,
     799,   800,   801,  1224,  1364,  1225,  1292,  1437,  1044,  1144,
    1138,  1139,   283,  1098,  1193,  1267,  1464,  1268,  1345,  1404,
    1403,  1346,   106,   107,   212,   286,   173,   287,   382,   802,
     933,  1050,  1236,  1297,  1368,   108,   109,   290,   174,   291,
     292,   499,   803,   500,   934,  1052,  1053,  1238,   654,   935,
     503,   388,   502,   110,   111,   295,   175,   296,   297,   504,
     809,   391,   508,   661,   112,   113,   300,   176,   301,   510,
     813,   302,   509,   812,   395,   514,   665,   941,   943,   114,
     115,   398,   304,   178,   303,   515,   666,   818,  1062,  1369,
    1065,  1066,  1064,  1443,  1484,   819,  1063,  1159,  1243,   116,
     117,   307,   179,   308,   667
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -1146
static const yytype_int16 yypact[] =
{
     -75, -1146,    99,   104,   119, -1146,    -6,    44,  -134,  -130,
    -126, -1146,   312, -1146, -1146, -1146, -1146, -1146,   132, -1146,
      59, -1146, -1146, -1146, -1146, -1146, -1146,   137,   155,    20,
      20, -1146,   163,   171,   194,   207,   221,   231,   240,   244,
     255,   200,   261,   274,   292,   301,   311,   353, -1146, -1146,
     328,   380,   387,   401,   410, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146,   433, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146,   222, -1146, -1146,   314,   204,
     466,   399,   502,   506,   510,   527,   539,   287,   294, -1146,
   -1146, -1146, -1146,   540,   566,   316,   321,   327,   330,   335,
     339,   340,   358,   360, -1146,   361,   363,   364,   365,   367,
     368, -1146,   369,   370,   381,   382,   383,    60,   -55, -1146,
     -52,   -46,   -44,    19,    33,    38,    48,    49,    50,    53,
      71,    75,    76,    77,    78,    80,    85, -1146, -1146,    88,
     384, -1146,   385,   567,   389,   392,   642,   643,   648,     8,
     287, -1146, -1146,   630,   652, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
     179, -1146, -1146, -1146, -1146, -1146, -1146,   651,   638, -1146,
   -1146,   640, -1146, -1146, -1146,   646,   662, -1146, -1146, -1146,
     631, -1146, -1146,   644, -1146, -1146, -1146, -1146, -1146,   633,
   -1146, -1146, -1146, -1146, -1146,   620, -1146, -1146, -1146,   610,
   -1146, -1146, -1146, -1146,   587,   291, -1146, -1146, -1146, -1146,
     606, -1146,   602, -1146, -1146, -1146, -1146,   586,   420, -1146,
   -1146, -1146,   542, -1146, -1146,   578,    -2, -1146, -1146,   576,
   -1146, -1146, -1146, -1146,   512, -1146, -1146, -1146,   478,    37,
   -1146, -1146,    -3,   479,   665, -1146, -1146, -1146,   477,     9,
   -1146, -1146, -1146,   690,    89,   446,   649, -1146, -1146, -1146,
   -1146,   435, -1146, -1146,   692,   693,   694,    10,    11,   287,
   -1146,   695,   696,   442, -1146, -1146, -1146, -1146, -1146, -1146,
     544, -1146, -1146, -1146, -1146, -1146,   685, -1146, -1146,   704,
     703, -1146,   706, -1146,   707, -1146,   708,  -221,     5, -1146,
      72,  -143, -1146,  -111, -1146,   709,   710, -1146, -1146, -1146,
     451,   452, -1146, -1146, -1146, -1146,   713,   -49, -1146, -1146,
     110, -1146,   715, -1146, -1146, -1146, -1146,   462, -1146,   720,
     122, -1146,   721, -1146, -1146, -1146,   287, -1146, -1146, -1146,
   -1146,   -17, -1146, -1146, -1146,   -10, -1146,   666, -1146, -1146,
   -1146,   722, -1146,   549,   549,   549,   464,   465,   467,   469,
     287,   470,   711,   730, -1146,   733,   735,   736,   737,   738,
     739, -1146,   740,   741,   742,   743,   744,   485,   725, -1146,
   -1146,   745, -1146,    56, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146,    14, -1146, -1146,   287, -1146, -1146,
   -1146, -1146,   490, -1146, -1146,   538, -1146, -1146, -1146,   727,
   -1146,   511, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146,   160,   753,   754,   336,   336,   759,   177, -1146, -1146,
     281, -1146, -1146,   760, -1146,    23, -1146, -1146,    94,   761,
     763,   764, -1146,   658, -1146,   282, -1146, -1146,   766,   767,
     513,   287,   287,    -9,   768,   287, -1146, -1146, -1146,   769,
     770,   287, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146,   750,   752,   755, -1146,
   -1146, -1146, -1146, -1146, -1146,   772,   549,   237,   237,   237,
     237,   237,   237,   237,   237,   237,   237,   518,   705,   776,
   -1146,   287, -1146, -1146,   173,   777, -1146, -1146, -1146,   287,
   -1146, -1146, -1146, -1146, -1146, -1146,   778, -1146,   287,   287,
     549, -1146,   781,   -21,   780, -1146, -1146, -1146,   471,   782,
     140,   783, -1146, -1146, -1146, -1146,   784, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146,   287, -1146,   173,   785, -1146,   471,
     786,   140,   787, -1146, -1146, -1146, -1146, -1146, -1146,   788,
   -1146,   791, -1146,    62, -1146, -1146, -1146,   792, -1146,    -7,
       1,   524, -1146,   338, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146,   793, -1146, -1146,   794, -1146,   276, -1146, -1146, -1146,
     795,   797,     6,   285, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146,   -35, -1146, -1146,   287, -1146, -1146, -1146,
     287,   287, -1146, -1146,   287,   287,   182,   287,   796,   798,
     799,   537, -1146,   803, -1146, -1146,   804,   548,   550,   551,
     552,   553,   554,   556,   557,   558,   559, -1146, -1146,   779,
     203,   287,   287,   806, -1146, -1146, -1146, -1146, -1146, -1146,
     817,   549,   818,   819,   756, -1146, -1146,   287, -1146,   560,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,   821, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146,   822,   824,   825, -1146,   826,
     828, -1146,   827,   830,   831,   287,   832, -1146, -1146,   833,
   -1146, -1146, -1146, -1146,   834,   835,   836,   837,   838, -1146,
     839,   840, -1146, -1146, -1146,   841, -1146,   336, -1146, -1146,
   -1146,   757,   842,   844,   845,   846,   843, -1146, -1146,   849,
     582,   850,   590, -1146,   852,   851,   853,   284,   758,   597,
   -1146, -1146,   599, -1146, -1146,   326,   857,   858,   860,   861,
     862,   864, -1146, -1146, -1146, -1146, -1146,   863,   287,     3,
   -1146,   287,   607,   608, -1146,   287, -1146,    55, -1146, -1146,
     287,   848,   854,   856, -1146,   847, -1146,   729,   729, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
     872, -1146,   616, -1146,    98, -1146, -1146, -1146, -1146,   287,
     208,   874, -1146, -1146,    28,   183,   819, -1146, -1146,   873,
     879, -1146,     7, -1146,   880, -1146, -1146, -1146, -1146,   414,
     826, -1146, -1146,   830, -1146, -1146,   855, -1146, -1146, -1146,
     287, -1146,   881, -1146,   882,   482,   837, -1146, -1146, -1146,
   -1146,   859, -1146,    15,   357, -1146,   885, -1146,   884, -1146,
   -1146, -1146, -1146,   887,   888,   889,   887,   890, -1146,   659,
   -1146, -1146,   891,   892, -1146,   893,   894,   895, -1146, -1146,
   -1146,   898,   899, -1146, -1146, -1146, -1146,   900,   901, -1146,
     902,   903, -1146,   230,   632,   905, -1146,   287, -1146,   287,
     653,    13,   655,   287,   287, -1146, -1146, -1146, -1146, -1146,
   -1146,   287,   904,   908,   909,   910, -1146,   911,   912,   912,
   -1146,   915, -1146, -1146, -1146, -1146,   914,   917,   918, -1146,
   -1146, -1146, -1146,  -117, -1146, -1146, -1146,   919, -1146,   549,
   -1146, -1146, -1146, -1146,   920,   923, -1146, -1146, -1146,   656,
   -1146, -1146, -1146, -1146, -1146,   855, -1146, -1146, -1146,   233,
     287, -1146, -1146, -1146, -1146, -1146, -1146,   925,   287,   924,
   -1146, -1146, -1146,   287, -1146,   926,   927,   928, -1146,   931,
   -1146,   635, -1146, -1146, -1146, -1146, -1146, -1146, -1146,   932,
     933, -1146,   865, -1146, -1146, -1146, -1146, -1146,   293,   934,
     866, -1146,   437,   373,   388,   437,   373,   388, -1146,   765,
   -1146,   -11, -1146, -1146, -1146, -1146,   287, -1146, -1146, -1146,
     287,   287,   936, -1146,   937,   938,   939,   676,   921,   922,
     929,   935,   315,   940, -1146, -1146, -1146, -1146, -1146,   915,
     941,   943,   946,   945,   947,   948,   951,   953, -1146,    63,
   -1146, -1146,   952, -1146,   955,   956,   958, -1146,   680,     0,
     830, -1146, -1146, -1146, -1146,   287, -1146,   820, -1146,   840,
     287, -1146, -1146,   840, -1146, -1146, -1146, -1146,   486, -1146,
   -1146,   688,   699,   700, -1146, -1146, -1146,   867, -1146,   438,
   -1146,   960, -1146, -1146,   961,   962,   964,   967,   373, -1146,
     968,   969,   970,   971, -1146, -1146,   373, -1146,   973, -1146,
   -1146, -1146, -1146,   974, -1146, -1146,   723, -1146,   930,   977,
     976, -1146, -1146, -1146,   979,   981,   982,   983, -1146,   829,
   -1146, -1146,   320, -1146,   915,   287, -1146, -1146,   984, -1146,
   -1146, -1146,   728, -1146, -1146, -1146, -1146,   986,   731,   731,
     731, -1146, -1146, -1146,   987, -1146, -1146,    16, -1146, -1146,
   -1146, -1146, -1146,   549,   990, -1146,   859,   287,   331, -1146,
   -1146, -1146, -1146,   993, -1146,   994, -1146,   719,   995, -1146,
   -1146, -1146,   746,   996, -1146,    47,   997,   998,   999,  1000,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,  1004, -1146,
   -1146,  1003,   509, -1146, -1146,  1005,  1006,  1008,   975, -1146,
   -1146,   266, -1146, -1146,   287, -1146,   287,  1009,   287,   775,
    1012, -1146,   -25, -1146,   762,   771,   773,  1013,   123, -1146,
    1016,    17,    18, -1146, -1146,  1015,   287,    61, -1146, -1146,
    1018,  1019,  1020, -1146,  1021, -1146, -1146, -1146, -1146, -1146,
    1022,  1023, -1146, -1146, -1146, -1146, -1146,  1026, -1146, -1146,
   -1146, -1146, -1146,   774,   789,   790,  1025, -1146, -1146, -1146,
     471, -1146,  1028,  1027,  1031,  1033,  1034,  1035,  1036,  1037,
    1038,   249, -1146,  1042, -1146, -1146, -1146, -1146,   549, -1146,
   -1146,   287,  1040,   517, -1146, -1146, -1146, -1146, -1146,  1030,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,    22,    24,
      25,    26,    21, -1146, -1146,  1041,   296, -1146,   800,   801,
    1045, -1146,   805,   805, -1146,  1047,  1048,   526, -1146, -1146,
   -1146, -1146, -1146,  1049,  1052,  1053, -1146, -1146,  1032,  1032,
    1032,  1032,  1039,  1043,  1032,  1044, -1146, -1146, -1146, -1146,
   -1146,  1054, -1146,  1055,  1057,  1058, -1146,   287,  1062, -1146,
   -1146, -1146,   807,  1059,   830,   808, -1146,   809, -1146,   810,
   -1146,   811, -1146,   541, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
      21,   813,   814,   815,   978,    74, -1146,  1060, -1146, -1146,
   -1146, -1146, -1146,   -23, -1146,   546, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
     823,   287,   816,   875,   868, -1146,   855, -1146, -1146, -1146,
   -1146,  1075, -1146,  1078,  1079, -1146,  1080, -1146, -1146, -1146,
   -1146,  1082,  1083,  1084, -1146, -1146, -1146, -1146,   -20,  1081,
    1087,  1088,  1089,   -12,  1090, -1146,   869, -1146, -1146,  1068,
    1091,  1093, -1146,  1094,  1095, -1146, -1146, -1146,  1096,  1097,
     287, -1146, -1146, -1146, -1146,  1098,  1099,   287,  1100, -1146,
    1101,   870,   876,   877,   883, -1146, -1146,   287, -1146, -1146,
     287,   886,  1104,  1073,   896,   897, -1146, -1146, -1146,   287,
   -1146,  1105,  1107,  1106,  1110,   287,  1111,  1112,  1113,  1115,
   -1146,   871, -1146,   906,   907,   913, -1146, -1146,  1114,  1116,
     916,   942,  1117,  1118,  1119,  1120, -1146
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146,    95, -1146, -1146,    67, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146,  -400, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146,  -592, -1145, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146,  1071, -1146, -1146,   -63,   167, -1146, -1146,  -341,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146,  -499,  -188,  -409,  -127,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146,   520, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146,   265, -1146, -1146, -1146, -1146, -1146,   -42, -1146,
   -1146, -1146, -1146, -1146, -1146,   980, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146,  -591, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146,   262, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146,   677,  -865, -1146, -1146,    39, -1146, -1146,  -220, -1146,
   -1025, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146,   254, -1146,   317,   545,    41, -1146,
   -1146,    34, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,  1061,
   -1146, -1146,  -240, -1146, -1146,   678, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
     246, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146,  -960, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,  -132,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146,    12, -1146, -1146, -1146, -1146,   223, -1146,
     112, -1038, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146,   660, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146,   350, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146,   224, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146,   878, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146, -1146,
   -1146, -1146, -1146, -1146, -1146
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -496
static const yytype_int16 yytable[] =
{
     191,   533,   319,   656,   527,   528,   434,   750,   995,   449,
     456,   986,   462,   505,   317,   363,   406,   408,   567,  1012,
     511,   386,  1271,  1348,  1350,  1413,   481,   386,  1405,   486,
    1407,  1409,  1411,   505,   977,  1481,   804,   393,  1508,   496,
     432,   221,   433,   511,   226,    11,  1515,  1148,  1194,  1088,
     230,    -9,   233,    -9,    -9,    -9,    -9,    -9,    -9,    -9,
      -9,    -9,    -9,    -9,    -9,   384,  1289,    -9,    -9,   358,
     359,   360,   361,    -9,  1198,    -9,   451,   358,   359,   360,
     361,   217,  1133,   945,     1,  1145,    -9,    -9,   553,   554,
     555,   556,   557,   558,   559,   560,   561,   562,    -9,     5,
    1230,  1335,   374,   762,    -9,  1478,   375,   376,  1230,  1355,
     790,  1089,     6,    -9,   791,   239,  1356,   452,   454,    -9,
     455,   632,   633,     8,    -9,   634,   635,    13,   963,   245,
     614,    14,   987,   763,   249,    15,   672,    -9,   119,   805,
      -9,    -9,   564,   128,   254,   260,   262,   964,   965,   267,
     460,    -9,   461,   121,    -9,   711,   218,  1195,  1196,  1340,
    1341,   129,   811,  1149,     9,    10,   815,   272,   820,   135,
     708,   275,   279,   284,   288,  1342,   293,   136,   377,   806,
     323,   298,  1343,  1278,   305,   397,   980,   981,   324,   982,
     564,    -9,   130,   131,   712,   713,   714,   715,  1290,  1291,
     137,  1150,  1151,  1152,    -9,    -9,    -9,   222,   325,   144,
     227,    -9,   479,   138,   480,  1482,   231,  1509,   234,    -9,
     326,   946,   563,   564,   807,  1516,  1153,   139,   764,   564,
    1479,   729,  1483,   506,  1450,  1451,  1452,   140,   327,  1455,
     512,   387,   637,   638,   507,  1154,   141,   387,   385,   328,
     142,   513,   657,   506,    -9,   329,    -9,    -9,   766,   501,
     767,   143,   394,   512,  1197,   564,   769,   145,   767,  1101,
    1102,   450,  1103,   988,   318,   330,   407,   409,   189,   189,
     146,   240,  1272,  1349,  1351,  1197,  1184,  1406,  1185,  1408,
    1410,  1412,   189,    -9,  1309,   240,   565,   947,   147,   948,
     250,   853,   557,   558,   331,   560,  1344,   148,   949,   950,
     255,   255,   263,  1310,   149,   268,   566,   568,   180,    16,
     570,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,   273,   152,    29,    30,   276,   280,   285,
     289,    31,   294,    32,   966,   967,   968,   299,   969,   845,
     306,   177,   970,   332,    33,    34,   150,   333,   334,   730,
     731,   732,   625,   626,   627,   628,    35,   358,   359,   360,
     361,   484,    36,   485,   655,   191,   785,   786,   660,   768,
     768,    37,   362,   494,   664,   495,   153,    38,   694,   695,
     696,   697,    39,   154,   645,   646,   647,   648,   649,   650,
     673,   674,   675,   793,   794,    40,   676,   155,    41,    42,
    1311,  1312,   358,   359,   360,   361,   156,   991,   992,    43,
     993,   612,    44,   613,   691,   772,   773,   774,   775,  1015,
    1016,  1017,   701,   564,   914,   915,   916,   917,   622,   159,
     623,   706,   707,   816,  1060,   817,   924,   925,   564,   564,
    1313,  1314,  1315,  1316,  1317,  1318,  1319,  1320,   629,    45,
    1134,  1135,  1136,  1137,   843,   181,   844,   745,   808,   972,
     182,   973,    46,    47,    48,  1140,  1141,  1142,  1143,    49,
    1386,  1387,  1388,  1389,   177,  1003,  1004,    50,  1005,  1210,
    1211,  1048,  1212,  1049,   183,  1321,  1417,  1418,  1419,  1420,
    1421,  1422,  1423,  1424,  1425,  1426,   184,  1322,   630,  1110,
     185,  1323,  1300,  1301,   186,  1302,  1324,  1325,  1326,  1327,
    1399,  1400,    51,  1401,    52,    53,  1427,   897,  1055,  1440,
    1441,   187,  1442,   810,   191,   632,   633,   814,   191,  1466,
     191,   595,  1067,   188,   596,   597,   193,   575,   576,  1485,
    1486,   189,  1487,  1428,  1128,   192,  1129,   598,   599,   600,
     601,    54,   602,   582,   848,   849,  1471,   603,   604,   605,
     194,   573,   574,   311,   575,   576,  1168,   195,  1169,  1091,
     861,  1250,   196,  1251,   577,   578,   579,   580,   197,   581,
     582,   198,  1220,  1221,  1277,  1197,   199,   583,  1265,  1266,
     200,   201,   518,   519,   520,   521,   522,   523,   524,   525,
     518,   519,   520,   521,   522,   523,   524,   525,   879,   202,
     937,   203,   204,   939,   205,   206,   207,   944,   208,   209,
     211,   213,   951,   678,   679,   680,   681,   682,   683,   684,
     685,   686,   214,   215,   216,   309,   310,  1155,   314,   315,
     312,  1156,  1157,   313,   316,   321,   322,   336,   337,   338,
     584,   585,   586,   719,   720,   721,   722,   723,   724,   725,
     726,   340,   341,   343,   352,   345,   349,   606,   564,   354,
     357,   364,   365,   368,   369,   373,   378,   381,   383,   371,
     392,   389,   607,   390,   396,   400,   402,   401,   403,   404,
     405,   412,   413,   414,   587,   564,   421,   426,   427,   428,
     429,   430,   431,   468,   469,   474,   475,   478,  1376,   488,
     588,   589,   971,   492,   493,   498,   516,   978,   517,   529,
     530,   534,   531,   608,   532,   609,   536,   537,   535,   538,
     539,   540,   541,   542,   543,   544,   545,   546,   548,   552,
     547,  1056,   549,  1000,   572,   593,  1061,   616,   617,  1331,
     590,   610,   591,   621,   631,   640,  1013,   641,   642,   643,
     651,   652,   659,   662,   663,   668,   653,   669,   671,   687,
     670,   688,   689,   699,   705,   710,   717,   770,   728,   734,
     738,   748,   751,   753,   759,  -471,   765,   782,   824,   788,
     783,   789,   821,  1274,   822,   823,   826,   841,   829,   830,
     851,   831,   832,   833,   834,   835,   191,   836,   837,   838,
     839,   852,   854,   855,   862,   865,   866,   858,   867,   868,
     869,  1253,   872,   874,   875,   878,   880,   881,   882,   883,
     884,   885,   888,   889,   890,   896,   907,   903,   899,   898,
     900,   901,   902,   906,   909,   910,   911,   912,   920,   913,
     923,   927,   928,   919,   929,   930,   931,  1354,   932,   936,
     940,   942,   955,  1106,   952,   957,   960,   962,   975,   984,
     953,   191,   954,   985,   989,  1030,  1114,  1001,  1002,  1020,
    1021,  1022,  1024,  1026,  1029,  1051,  1120,  1033,  1034,  1035,
    1036,  1037,  1040,  1041,  1042,  1043,  1045,  1046,   997,  1054,
    1068,  1057,  1010,  1059,  1069,  1070,  1071,  1073,  1074,  1078,
    1083,  1097,  1207,  1084,  1085,  1090,  1092,  1093,  1396,  -495,
    1111,  1147,  1115,  1116,  1117,  1118,  1123,  1163,  1131,  1124,
    1158,  1160,  1161,  1162,  1191,  1175,  1171,  1176,  1164,  1165,
    1177,  1178,  1213,  1179,  1180,  1181,  1166,  1182,  1187,  1188,
    1189,  1167,  1190,  1215,  1222,  1217,  1204,  1226,  1227,  1125,
    1228,  1132,  1219,  1229,  1231,  1232,  1233,  1234,  1203,  1237,
    1239,  1242,  1244,  1282,  1240,  1245,  1241,  1246,  1247,  1248,
    1257,  1259,  1261,  1269,  1262,  1249,  1275,  1280,  1281,  1283,
    1288,  1497,  1306,  1293,  1294,  1295,  1296,  1287,  1298,  1299,
    1332,  1303,  1304,  1472,  1305,  1330,  1333,  1339,  1530,  1276,
    1347,  1353,  1358,  1359,  1360,  1361,  1076,  1336,  1365,  1366,
    1367,  1373,  1377,  1378,  1402,  1370,  1337,  1379,  1338,  1380,
    1381,  1382,  1383,  1384,  1385,  1392,  1398,  1416,  1256,  1433,
    1371,  1372,  1495,  1438,  1439,  1444,  1445,  1447,  1457,  1458,
    1448,  1459,  1460,  1465,  1480,  1431,  1432,  1453,  1462,  1435,
    1463,  1454,  1456,  1467,  1468,  1469,  1470,  1473,  1474,  1496,
    1475,  1499,  1500,  1501,  1476,  1511,  1502,  1494,  1505,  1506,
    1507,  1512,  1513,  1514,  1520,  1521,  1518,  1522,  1523,  1524,
    1542,   133,  1525,  1526,  1528,  1529,  1531,  1533,  1541,  1546,
    1548,  1255,  1519,  1547,  1549,  1551,   747,  1553,  1552,  1554,
    1559,   983,  1560,  1563,  1564,  1565,  1566,  1328,  1079,  1329,
    1254,   191,   994,  1498,  1555,  1534,  1550,  1430,   611,  1202,
    1006,  1535,  1536,  1397,   246,   959,   752,  1209,  1537,  1558,
    1206,  1540,  1028,   620,  1363,  1047,   167,  1235,  1146,   938,
    1543,  1544,     0,   658,     0,  1058,     0,     0,     0,     0,
       0,  1556,  1557,     0,     0,     0,     0,     0,     0,  1561,
       0,  1562,   399,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,  1461,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,  1539,     0,     0,     0,     0,     0,     0,     0,
       0,  1545,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,  1527,     0,     0,     0,     0,     0,     0,
     191,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    1538
};

static const yytype_int16 yycheck[] =
{
     127,   410,   190,   502,   404,   405,   347,   599,   873,     4,
     351,     4,   353,    30,     6,   255,     6,     6,     4,     4,
      30,    30,     6,     6,     6,     4,   367,    30,     6,   370,
       6,     6,     6,    30,     6,    58,    71,    28,    58,   380,
     261,    96,   263,    30,    96,     1,    58,    58,    48,   166,
      96,     7,    96,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    28,    19,    23,    24,    76,
      77,    78,    79,    29,  1099,    31,     4,    76,    77,    78,
      79,    21,  1042,    28,   159,  1045,    42,    43,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    54,     0,
    1138,   126,   104,    41,    60,    31,   108,   109,  1146,    48,
     104,   228,     8,    69,   108,    96,    55,    45,   261,    75,
     263,    98,    99,     4,    80,   102,   103,   261,    30,    96,
     471,   261,   125,    71,    96,   261,   536,    93,     6,   174,
      96,    97,   167,     6,    96,    96,    96,    49,    50,    96,
     261,   107,   263,    94,   110,   176,    96,   157,   158,    36,
      37,     6,   661,   174,   170,   171,   665,    96,   667,     6,
     570,    96,    96,    96,    96,    52,    96,     6,   180,   214,
       1,    96,    59,  1208,    96,    96,     3,     4,     9,     6,
     167,   147,   172,   173,   215,   216,   217,   218,   151,   152,
       6,   212,   213,   214,   160,   161,   162,   262,    29,     9,
     262,   167,   261,     6,   263,   238,   262,   237,   262,   175,
      41,   166,   166,   167,   259,   237,   237,     6,   166,   167,
     156,    91,   255,   250,  1379,  1380,  1381,     6,    59,  1384,
     250,   250,   148,   149,   261,   256,     6,   250,   211,    70,
       6,   261,   261,   250,   210,    76,   212,   213,   265,   386,
     267,     6,   253,   250,   264,   167,   265,     6,   267,    36,
      37,   266,    39,   266,   266,    96,   266,   266,   264,   264,
       6,   262,   266,   266,   266,   264,   223,   265,   225,   265,
     265,   265,   264,   249,    28,   262,   240,   242,     6,   244,
     262,   701,    36,    37,   125,    39,   183,     6,   253,   254,
     262,   262,   262,    47,     3,   262,   260,   444,     4,     7,
     447,     9,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,   262,     6,    23,    24,   262,   262,   262,
     262,    29,   262,    31,   246,   247,   248,   262,   250,   690,
     262,   262,   254,   174,    42,    43,     3,   178,   179,   219,
     220,   221,    81,    82,    83,    84,    54,    76,    77,    78,
      79,   261,    60,   263,   501,   502,   100,   101,   505,   619,
     620,    69,    91,   261,   511,   263,     6,    75,   215,   216,
     217,   218,    80,     6,   112,   113,   114,   115,   116,   117,
     163,   164,   165,   118,   119,    93,   169,     6,    96,    97,
     144,   145,    76,    77,    78,    79,     6,     3,     4,   107,
       6,   261,   110,   263,   551,    87,    88,    89,    90,    72,
      73,    74,   559,   167,   150,   151,   152,   153,   261,     6,
     263,   568,   569,   261,   943,   263,   120,   121,   167,   167,
     184,   185,   186,   187,   188,   189,   190,   191,   177,   147,
      87,    88,    89,    90,   261,   261,   263,   594,   656,   261,
       4,   263,   160,   161,   162,    87,    88,    89,    90,   167,
     231,   232,   233,   234,   262,     3,     4,   175,     6,     3,
       4,   261,     6,   263,    95,   229,   200,   201,   202,   203,
     204,   205,   206,   207,   208,   209,     4,   241,   227,  1008,
       4,   245,     3,     4,     4,     6,   250,   251,   252,   253,
       3,     4,   210,     6,   212,   213,   230,   767,   937,     3,
       4,     4,     6,   660,   661,    98,    99,   664,   665,  1404,
     667,    30,   951,     4,    33,    34,     6,    36,    37,     3,
       4,   264,     6,   257,   261,   261,   263,    46,    47,    48,
      49,   249,    51,    52,   691,   692,    25,    56,    57,    58,
       4,    33,    34,     6,    36,    37,   261,   261,   263,   979,
     707,   261,   261,   263,    46,    47,    48,    49,   261,    51,
      52,   261,   154,   155,   263,   264,   261,    59,  1189,  1190,
     261,   261,    61,    62,    63,    64,    65,    66,    67,    68,
      61,    62,    63,    64,    65,    66,    67,    68,   745,   261,
     808,   261,   261,   811,   261,   261,   261,   815,   261,   261,
     261,   261,   820,   538,   539,   540,   541,   542,   543,   544,
     545,   546,   261,   261,   261,   261,   261,  1056,     6,     6,
     261,  1060,  1061,   261,     6,    25,     4,     6,    20,    19,
     122,   123,   124,   192,   193,   194,   195,   196,   197,   198,
     199,    25,    10,    42,    54,    31,    43,   166,   167,    69,
      93,    75,    80,    97,   264,   107,   110,   175,   210,   147,
     213,   212,   181,    28,     4,   249,   261,    48,     6,     6,
       6,     6,     6,   261,   166,   167,   162,    22,     4,     6,
       4,     4,     4,     4,     4,   264,   264,     4,  1310,     4,
     182,   183,   849,   261,     4,     4,    60,   854,     6,   265,
     265,   261,   265,   222,   265,   224,     6,     4,    27,     4,
       4,     4,     4,     4,     4,     4,     4,     4,   263,     4,
       6,   939,    27,   880,   264,    28,   944,     4,     4,  1258,
     222,   250,   224,     4,     4,     4,   893,     4,     4,   111,
       4,     4,     4,     4,     4,    25,   263,    25,     6,   261,
      25,    76,     6,     6,     6,     4,     6,   263,     6,     6,
       6,     6,     6,     6,     6,     4,     4,     4,   261,     4,
       6,     4,     6,  1203,     6,     6,     3,    28,     4,   261,
       4,   261,   261,   261,   261,   261,   943,   261,   261,   261,
     261,     4,     4,     4,   264,     4,     4,    71,     4,     4,
       4,  1172,     4,     6,     4,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,   264,     4,     6,    92,
       6,     6,     6,     4,     4,   265,     4,     6,   261,     6,
     261,     4,     4,   105,     4,     4,     4,  1276,     4,     6,
     263,   263,    25,  1000,    26,   146,     4,   261,     4,     6,
      26,  1008,    26,     4,     4,   226,  1013,     6,     6,     4,
       6,     4,     4,     4,     4,   263,   261,     6,     6,     6,
       6,     6,     4,     4,     4,     4,     4,     4,    53,     4,
       6,   258,    53,   258,     6,     6,     6,     6,     6,     4,
       6,   265,  1110,     6,     6,     6,     6,     4,  1328,     4,
       6,   166,     6,     6,     6,     4,     4,   261,     4,     6,
       4,     4,     4,     4,   264,     4,     6,     4,    27,    27,
       4,     6,   264,     6,     6,     4,    27,     4,     6,     4,
       4,    26,     4,   264,     4,   265,   146,     6,     6,   104,
       6,   105,   105,     6,     6,     6,     6,     6,  1105,     6,
       6,     4,     6,   264,   261,     6,    56,     6,     6,     6,
       6,   263,     6,     6,   263,   166,     6,     4,     4,     4,
       4,   126,    27,     6,     6,     6,     6,   261,     4,     6,
     235,     6,     6,  1413,     6,     6,     4,     4,  1517,  1207,
       4,     6,     4,     4,     4,     4,   959,   265,     6,     6,
       4,     6,     4,     6,     4,   261,   265,     6,   265,     6,
       6,     6,     6,     6,     6,     3,     6,     6,  1175,     4,
     261,   261,  1461,     6,     6,     6,     4,     4,     4,     4,
      28,     4,     4,     4,     4,   265,   265,    28,     6,   264,
     263,    28,    28,   265,   265,   265,   265,   264,   264,   263,
     265,     6,     4,     4,   106,     4,     6,   264,     6,     6,
       6,     4,     4,     4,    26,     4,     6,     4,     4,     4,
      27,    30,     6,     6,     6,     6,     6,     6,     4,     4,
       4,  1174,   243,     6,     4,     4,   596,     4,     6,     4,
       6,   856,     6,     6,     6,     6,     6,  1254,   961,  1256,
    1172,  1258,   870,   265,   263,   265,  1545,  1357,   461,  1100,
     886,   265,   265,  1331,   164,   828,   601,  1113,   265,   236,
    1109,   265,   906,   475,  1286,   932,    95,  1145,  1046,   809,
     264,   264,    -1,   503,    -1,   941,    -1,    -1,    -1,    -1,
      -1,   265,   265,    -1,    -1,    -1,    -1,    -1,    -1,   263,
      -1,   239,   304,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  1397,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  1530,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,  1539,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,  1510,    -1,    -1,    -1,    -1,    -1,    -1,
    1517,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    1527
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint16 yystos[] =
{
       0,   159,   269,   270,   271,     0,     8,   272,     4,   170,
     171,     1,   273,   261,   261,   261,     7,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,    23,
      24,    29,    31,    42,    43,    54,    60,    69,    75,    80,
      93,    96,    97,   107,   110,   147,   160,   161,   162,   167,
     175,   210,   212,   213,   249,   274,   275,   276,   278,   279,
     281,   283,   285,   286,   304,   305,   306,   307,   309,   311,
     314,   316,   317,   321,   322,   347,   357,   359,   365,   366,
     368,   369,   389,   390,   402,   403,   439,   440,   498,   533,
     535,   536,   553,   554,   555,   556,   571,   572,   597,   598,
     615,   616,   631,   632,   644,   645,   680,   681,   693,   694,
     711,   712,   722,   723,   737,   738,   757,   758,   277,     6,
     280,    94,   282,   284,   308,   310,   312,   315,     6,     6,
     172,   173,   360,   360,   348,     6,     6,     6,     6,     6,
       6,     6,     6,     6,     9,     6,     6,     6,     6,     3,
       3,   287,     6,     6,     6,     6,     6,   318,   323,     6,
     370,   391,   404,   441,   499,   537,   557,   557,   573,   599,
     617,   633,   646,   684,   696,   714,   725,   262,   741,   760,
       4,   261,     4,    95,     4,     4,     4,     4,     4,   264,
     384,   387,   261,     6,     4,   261,   261,   261,   261,   261,
     261,   261,   261,   261,   261,   261,   261,   261,   261,   261,
     288,   261,   682,   261,   261,   261,   261,    21,    96,   319,
     320,    96,   262,   324,   346,   358,    96,   262,   371,   388,
      96,   262,   392,    96,   262,   405,   406,   407,   438,    96,
     262,   442,   443,   444,   496,    96,   443,   500,   534,    96,
     262,   538,   539,   552,    96,   262,   558,   559,   564,   570,
      96,   569,    96,   262,   574,   575,   596,    96,   262,   600,
     601,   614,    96,   262,   618,    96,   262,   634,   643,    96,
     262,   647,   648,   670,    96,   262,   683,   685,    96,   262,
     695,   697,   698,    96,   262,   713,   715,   716,    96,   262,
     724,   726,   729,   742,   740,    96,   262,   759,   761,   261,
     261,     6,   261,   261,     6,     6,     6,     6,   266,   385,
     387,    25,     4,     1,     9,    29,    41,    59,    70,    76,
      96,   125,   174,   178,   179,   289,     6,    20,    19,   325,
      25,    10,   372,    42,   393,    31,   408,   410,   409,    43,
     445,   455,    54,   501,    69,   540,   541,    93,    76,    77,
      78,    79,    91,   560,    75,    80,   576,   577,    97,   264,
     603,   147,   619,   107,   104,   108,   109,   180,   110,   649,
     653,   175,   686,   210,    28,   211,    30,   250,   709,   212,
      28,   719,   213,    28,   253,   732,     4,    96,   739,   741,
     249,    48,   261,     6,     6,     6,     6,   266,     6,   266,
     385,   386,     6,     6,   261,   290,   296,   293,   299,   294,
     291,   162,   297,   295,   292,   298,    22,     4,     6,     4,
       4,     4,   261,   263,   367,   411,   412,   413,   415,   418,
     420,   422,   425,   427,   431,   432,   435,   436,   437,     4,
     266,     4,    45,   446,   261,   263,   367,   456,   471,   473,
     261,   263,   367,   502,   503,   514,   516,   518,     4,     4,
     542,   543,   561,   562,   264,   264,   565,   567,     4,   261,
     263,   367,   579,   602,   261,   263,   367,   604,     4,   635,
     637,   638,   261,     4,   261,   263,   367,   654,     4,   699,
     701,   387,   710,   708,   717,    30,   250,   261,   720,   730,
     727,    30,   250,   261,   733,   743,    60,     6,    61,    62,
      63,    64,    65,    66,    67,    68,   313,   313,   313,   265,
     265,   265,   265,   386,   261,    27,     6,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     6,   263,    27,
     373,   394,     4,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,   166,   167,   240,   260,     4,   387,   421,
     387,   447,   264,    33,    34,    36,    37,    46,    47,    48,
      49,    51,    52,    59,   122,   123,   124,   166,   182,   183,
     222,   224,   479,    28,   475,    30,    33,    34,    46,    47,
      48,    49,    51,    56,    57,    58,   166,   181,   222,   224,
     250,   479,   261,   263,   367,   544,     4,     4,   560,   563,
     563,     4,   261,   263,   568,    81,    82,    83,    84,   177,
     227,     4,    98,    99,   102,   103,   613,   148,   149,   621,
       4,     4,     4,   111,   650,   112,   113,   114,   115,   116,
     117,     4,     4,   263,   706,   387,   384,   261,   709,     4,
     387,   721,     4,     4,   387,   734,   744,   762,    25,    25,
      25,     6,   313,   163,   164,   165,   169,   300,   300,   300,
     300,   300,   300,   300,   300,   300,   300,   261,    76,     6,
     374,   387,   395,   416,   215,   216,   217,   218,   419,     6,
     414,   387,   433,   428,   423,     6,   387,   387,   313,   449,
       4,   176,   215,   216,   217,   218,   470,     6,   459,   192,
     193,   194,   195,   196,   197,   198,   199,   343,     6,    91,
     219,   220,   221,   525,     6,   460,   474,   464,     6,   467,
     461,   462,   458,   457,   476,   387,   509,   419,     6,   510,
     343,     6,   525,     6,   515,   517,   519,   511,   505,     6,
     507,   504,    41,    71,   166,     4,   265,   267,   560,   265,
     263,   566,    87,    88,    89,    90,   580,   581,   582,   583,
     584,   585,     4,     6,   605,   100,   101,   620,     4,     4,
     104,   108,   639,   118,   119,   651,   655,   656,   657,   658,
     659,   660,   687,   700,    71,   174,   214,   259,   385,   718,
     387,   384,   731,   728,   387,   384,   261,   263,   745,   753,
     384,     6,     6,     6,   261,   349,     3,   302,   301,     4,
     261,   261,   261,   261,   261,   261,   261,   261,   261,   261,
     326,    28,   361,   261,   263,   367,   375,   382,   387,   387,
     396,     4,     4,   313,     4,     4,   429,   430,    71,   424,
     426,   387,   264,   450,   448,     4,     4,     4,     4,     4,
     468,   469,     4,   463,     6,     4,   480,   483,     4,   387,
       4,     4,     4,     4,     4,     4,   521,   522,     4,     4,
       4,   526,   529,   546,   550,   545,     4,   560,    92,     6,
       6,     6,     6,     4,   587,   591,     4,   264,   586,     4,
     265,     4,     6,     6,   150,   151,   152,   153,   622,   105,
     261,   640,   642,   261,   120,   121,   652,     4,     4,     4,
       4,     4,     4,   688,   702,   707,     6,   385,   720,   385,
     263,   735,   263,   736,   385,    28,   166,   242,   244,   253,
     254,   385,    26,    26,    26,    25,   350,   146,   524,   524,
       4,   362,   261,    30,    49,    50,   246,   247,   248,   250,
     254,   387,   261,   263,   397,     4,   417,     6,   387,   434,
       3,     4,     6,   430,     6,     4,     4,   125,   266,     4,
     465,     3,     4,     6,   469,   480,   484,    53,   481,   472,
     387,     6,     6,     3,     4,     6,   522,   506,   508,   530,
      53,   527,     4,   387,   547,    72,    73,    74,   551,   548,
       4,     6,     4,   578,     4,   588,     4,   592,   578,     4,
     226,   595,   606,     6,     6,     6,     6,     6,   623,   636,
       4,     4,     4,     4,   666,     4,     4,   666,   261,   263,
     689,   263,   703,   704,     4,   386,   385,   258,   733,   258,
     384,   385,   746,   754,   750,   748,   749,   386,     6,     6,
       6,     6,   352,     6,     6,   303,   303,   327,     4,   364,
     376,   383,   379,     6,     6,     6,   377,   380,   166,   228,
       6,   313,     6,     4,   451,   453,   452,   265,   671,   489,
     482,    36,    37,    39,   477,   478,   387,   520,   512,   513,
     384,     6,   532,   528,   387,     6,     6,     6,     4,   549,
     261,   589,   593,     4,     6,   104,   607,   610,   261,   263,
     624,     4,   105,   613,    87,    88,    89,    90,   668,   669,
      87,    88,    89,    90,   667,   613,   668,   166,    58,   174,
     212,   213,   214,   237,   256,   386,   386,   386,     4,   755,
       4,     4,     4,   261,    27,    27,    27,    26,   261,   263,
     353,     6,   328,   363,   364,     4,     4,     4,     6,     6,
       6,     4,     4,   398,   223,   225,   401,     6,     4,     4,
       4,   264,   466,   672,    48,   157,   158,   264,   488,   490,
     491,   493,   483,   387,   146,   523,   526,   385,   494,   529,
       3,     4,     6,   264,   590,   264,   594,   265,   611,   105,
     154,   155,     4,   641,   661,   663,     6,     6,     6,     6,
     669,     6,     6,     6,     6,   661,   690,     6,   705,     6,
     261,    56,     4,   756,     6,     6,     6,     6,     6,   166,
     261,   263,   329,   367,   436,   363,   387,     6,   378,   263,
     399,     6,   263,   367,   454,   454,   454,   673,   675,     6,
     492,     6,   266,   485,   313,     6,   385,   263,   488,   495,
       4,     4,   264,     4,   608,   625,   626,   261,     4,    19,
     151,   152,   664,     6,     6,     6,     6,   691,     4,     6,
       3,     4,     6,     6,     6,     6,    27,   351,   354,    28,
      47,   144,   145,   184,   185,   186,   187,   188,   189,   190,
     191,   229,   241,   245,   250,   251,   252,   253,   387,   387,
       6,   384,   235,     4,   400,   126,   265,   265,   265,     4,
      36,    37,    52,    59,   183,   676,   679,     4,     6,   266,
       6,   266,   486,     6,   386,    48,    55,   531,     4,     4,
       4,     4,   627,   627,   662,     6,     6,     4,   692,   747,
     261,   261,   261,     6,   355,   332,   343,     4,     6,     6,
       6,     6,     6,     6,     6,     6,   231,   232,   233,   234,
     342,   331,     3,   334,   330,   336,   313,   385,     6,     3,
       4,     6,     4,   678,   677,     6,   265,     6,   265,     6,
     265,     6,   265,     4,   487,   488,     6,   200,   201,   202,
     203,   204,   205,   206,   207,   208,   209,   230,   257,   497,
     486,   265,   265,     4,   609,   264,   628,   665,     6,     6,
       3,     4,     6,   751,     6,     4,   356,     4,    28,   344,
     344,   344,   344,    28,    28,   344,    28,     4,     4,     4,
       4,   385,     6,   263,   674,     4,   480,   265,   265,   265,
     265,    25,   313,   264,   264,   265,   106,   612,    31,   156,
       4,    58,   238,   255,   752,     3,     4,     6,   333,   345,
     337,   338,   339,   335,   264,   386,   263,   126,   265,     6,
       4,     4,     6,   630,   629,     6,     6,     6,    58,   237,
     340,     4,     4,     4,     4,    58,   237,   341,     6,   243,
      26,     4,     4,     4,     4,     6,     6,   387,     6,     6,
     384,     6,   381,     6,   265,   265,   265,   265,   387,   385,
     265,     4,    27,   264,   264,   385,     4,     6,     4,     4,
     386,     4,     6,     4,     4,   263,   265,   265,   236,     6,
       6,   263,   239,     6,     6,     6,     6
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}

/* Prevent warnings from -Wmissing-prototypes.  */
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */


/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*-------------------------.
| yyparse or yypush_parse.  |
`-------------------------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{


    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks thru separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yytoken = 0;
  yyss = yyssa;
  yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */
  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 4:

/* Line 1455 of yacc.c  */
#line 359 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 5:

/* Line 1455 of yacc.c  */
#line 360 "def.y"
    {
        defVersionNum = convert_defname2num((yyvsp[(3) - (4)].string));
        if (defVersionNum > 5.7) {
          char temp[120];
          sprintf(temp,
          "Def parser 5.7 does not support def file with version %s. Parser stops executions.",
                  (yyvsp[(3) - (4)].string));
          defError(6503, temp);
          return 1;
        }
        if (defrReader::get()->getVersionStrCbk()) {
          CALLBACK(defrReader::get()->getVersionStrCbk(), defrVersionStrCbkType, (yyvsp[(3) - (4)].string));
        } else if (defrReader::get()->getVersionCbk()) {
            CALLBACK(defrReader::get()->getVersionCbk(), defrVersionCbkType, defVersionNum);
        }
        if (defVersionNum > 5.3 && defVersionNum < 5.4)
          defIgnoreVersion = 1;
        if (defVersionNum < 5.6)     // default to false before 5.6
          names_case_sensitive = reader_case_sensitive;
        else
          names_case_sensitive = 1;
        hasVer = 1;
        defrReader::get()->setDoneDesign(0);
    ;}
    break;

  case 7:

/* Line 1455 of yacc.c  */
#line 387 "def.y"
    {
        if (defVersionNum < 5.6) {
          names_case_sensitive = 1;
          if (defrReader::get()->getCaseSensitiveCbk())
            CALLBACK(defrReader::get()->getCaseSensitiveCbk(), defrCaseSensitiveCbkType,
                     names_case_sensitive); 
          hasNameCase = 1;
        } else
          if (defrReader::get()->getCaseSensitiveCbk()) /* write error only if cbk is set */
             if (caseSensitiveWarnings++ < defrReader::get()->getCaseSensitiveWarnings())
               defWarning(7011, "NAMESCASESENSITIVE statement is obsolete in version 5.6 and later.\nThe DEF parser will ignore this statement.");
      ;}
    break;

  case 8:

/* Line 1455 of yacc.c  */
#line 400 "def.y"
    {
        if (defVersionNum < 5.6) {
          names_case_sensitive = 0;
          if (defrReader::get()->getCaseSensitiveCbk())
            CALLBACK(defrReader::get()->getCaseSensitiveCbk(), defrCaseSensitiveCbkType,
                     names_case_sensitive);
          hasNameCase = 1;
        } else {
          if (defrReader::get()->getCaseSensitiveCbk()) { /* write error only if cbk is set */
            if (caseSensitiveWarnings++ < defrReader::get()->getCaseSensitiveWarnings()) {
              defError(6504, "Def parser version 5.7 and later does not support NAMESCASESENSITIVE OFF.\nEither remove this optional construct or set it to ON.");
              CHKERR();
            }
          }
        }
      ;}
    break;

  case 51:

/* Line 1455 of yacc.c  */
#line 438 "def.y"
    {dumb_mode = 1; no_num = 1; ;}
    break;

  case 52:

/* Line 1455 of yacc.c  */
#line 439 "def.y"
    {
            if (defrReader::get()->getDesignCbk())
              CALLBACK(defrReader::get()->getDesignCbk(), defrDesignStartCbkType, (yyvsp[(3) - (4)].string));
            hasDes = 1;
          ;}
    break;

  case 53:

/* Line 1455 of yacc.c  */
#line 446 "def.y"
    {
            defrReader::get()->setDoneDesign(1);
            if (defrReader::get()->getDesignEndCbk())
              CALLBACK(defrReader::get()->getDesignEndCbk(), defrDesignEndCbkType, 0);
            // 11/16/2001 - pcr 408334
            // Return 1 if there is any errors during parsing
            if (errors)
                return 1;

            if (!hasVer)
              defWarning(7012, "VERSION is a required statement on DEF file.\nWithout VERSION defined, the DEF file is technically illegal.\nRefer the LEF/DEF Language Reference manual on how to define this statement.");
            if (!hasNameCase && defVersionNum < 5.6)
              defWarning(7013, "NAMESCASESENSITIVE is a required statement on DEF file with version 5.6 and earlier.\nWithout NAMESCASESENSITIVE defined, the DEF file is technically illegal.\nRefer the LEF/DEF 5.5 and earlier Language Referece manual on how to define this statement.");
            if (!hasBusBit && defVersionNum < 5.6)
              defWarning(7014, "BUSBITCHARS is a required statement on DEF file with version 5.6 and earlier.\nWithout BUSBITCHARS defined, the DEF file is technically illegal.\nRefer the LEF/DEF 5.5 and earlier Language Referece manual on how to define this statement.");
            if (!hasDivChar && defVersionNum < 5.6)
              defWarning(7015, "DIVIDERCHAR is a required statement on DEF file with version 5.6 and earlier.\nWithout DIVIDECHAR defined, the DEF file is technically illegal.\nRefer the LEF/DEF 5.5 and earlier Language Referece manual on how to define this statement.");
            if (!hasDes)
              defWarning(7016, "DESIGN is a required statement on DEF file.");
            hasVer = 0;
            hasNameCase = 0;
            hasBusBit = 0;
            hasDivChar = 0;
            hasDes = 0;

            assertionWarnings = 0;
            blockageWarnings = 0;
            caseSensitiveWarnings = 0;
            componentWarnings = 0;
            constraintWarnings = 0;
            defaultCapWarnings = 0;
            gcellGridWarnings = 0;
            iOTimingWarnings = 0;
            netWarnings = 0;
            nonDefaultWarnings = 0;
            pinExtWarnings = 0;
            pinWarnings = 0;
            regionWarnings = 0;
            rowWarnings = 0;
            scanchainWarnings = 0;
            sNetWarnings = 0;
            stylesWarnings = 0;
            trackWarnings = 0;
            unitsWarnings = 0;
            versionWarnings = 0;
            viaWarnings = 0;
          ;}
    break;

  case 54:

/* Line 1455 of yacc.c  */
#line 494 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 55:

/* Line 1455 of yacc.c  */
#line 495 "def.y"
    { 
            if (defrReader::get()->getTechnologyCbk())
              CALLBACK(defrReader::get()->getTechnologyCbk(), defrTechNameCbkType, (yyvsp[(3) - (4)].string));
          ;}
    break;

  case 56:

/* Line 1455 of yacc.c  */
#line 500 "def.y"
    {dumb_mode = 1; no_num = 1;;}
    break;

  case 57:

/* Line 1455 of yacc.c  */
#line 501 "def.y"
    { 
            if (defrReader::get()->getArrayNameCbk())
              CALLBACK(defrReader::get()->getArrayNameCbk(), defrArrayNameCbkType, (yyvsp[(3) - (4)].string));
          ;}
    break;

  case 58:

/* Line 1455 of yacc.c  */
#line 506 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 59:

/* Line 1455 of yacc.c  */
#line 507 "def.y"
    { 
            if (defrReader::get()->getFloorPlanNameCbk())
              CALLBACK(defrReader::get()->getFloorPlanNameCbk(), defrFloorPlanNameCbkType, (yyvsp[(3) - (4)].string));
          ;}
    break;

  case 60:

/* Line 1455 of yacc.c  */
#line 513 "def.y"
    { 
            if (defrReader::get()->getHistoryCbk())
              CALLBACK(defrReader::get()->getHistoryCbk(), defrHistoryCbkType, History_text);
          ;}
    break;

  case 61:

/* Line 1455 of yacc.c  */
#line 519 "def.y"
    {
            parsing_property = 1;
            defInPropDef = 1;     /* set flag as inside propertydefinitions */
            if (defrReader::get()->getPropDefStartCbk())
              CALLBACK(defrReader::get()->getPropDefStartCbk(), defrPropDefStartCbkType, 0);
          ;}
    break;

  case 62:

/* Line 1455 of yacc.c  */
#line 526 "def.y"
    { 
            if (defrReader::get()->getPropDefEndCbk())
              CALLBACK(defrReader::get()->getPropDefEndCbk(), defrPropDefEndCbkType, 0);
            real_num = 0;     /* just want to make sure it is reset */
            parsing_property = 0;
            defInPropDef = 0;     /* reset flag */
          ;}
    break;

  case 64:

/* Line 1455 of yacc.c  */
#line 536 "def.y"
    { ;}
    break;

  case 65:

/* Line 1455 of yacc.c  */
#line 538 "def.y"
    {dumb_mode = 1; no_num = 1; defrReader::get()->getProp().defiProp::clear(); ;}
    break;

  case 66:

/* Line 1455 of yacc.c  */
#line 540 "def.y"
    {
              if (defrReader::get()->getPropCbk()) {
                defrReader::get()->getProp().defiProp::setPropType("design", (yyvsp[(3) - (5)].string));
		CALLBACK(defrReader::get()->getPropCbk(), defrPropCbkType, &defrReader::get()->getProp());
              }
              defrReader::get()->getDesignProp().defiPropType::setPropType((yyvsp[(3) - (5)].string), defPropDefType);
            ;}
    break;

  case 67:

/* Line 1455 of yacc.c  */
#line 547 "def.y"
    { dumb_mode = 1 ; no_num = 1; defrReader::get()->getProp().defiProp::clear(); ;}
    break;

  case 68:

/* Line 1455 of yacc.c  */
#line 549 "def.y"
    {
              if (defrReader::get()->getPropCbk()) {
                defrReader::get()->getProp().defiProp::setPropType("net", (yyvsp[(3) - (5)].string));
		CALLBACK(defrReader::get()->getPropCbk(), defrPropCbkType, &defrReader::get()->getProp());
              }
              defrReader::get()->getNetProp().defiPropType::setPropType((yyvsp[(3) - (5)].string), defPropDefType);
            ;}
    break;

  case 69:

/* Line 1455 of yacc.c  */
#line 556 "def.y"
    { dumb_mode = 1 ; no_num = 1; defrReader::get()->getProp().defiProp::clear(); ;}
    break;

  case 70:

/* Line 1455 of yacc.c  */
#line 558 "def.y"
    {
              if (defrReader::get()->getPropCbk()) {
                defrReader::get()->getProp().defiProp::setPropType("specialnet", (yyvsp[(3) - (5)].string));
		CALLBACK(defrReader::get()->getPropCbk(), defrPropCbkType, &defrReader::get()->getProp());
              }
              defrReader::get()->getSNetProp().defiPropType::setPropType((yyvsp[(3) - (5)].string), defPropDefType);
            ;}
    break;

  case 71:

/* Line 1455 of yacc.c  */
#line 565 "def.y"
    { dumb_mode = 1 ; no_num = 1; defrReader::get()->getProp().defiProp::clear(); ;}
    break;

  case 72:

/* Line 1455 of yacc.c  */
#line 567 "def.y"
    {
              if (defrReader::get()->getPropCbk()) {
                defrReader::get()->getProp().defiProp::setPropType("region", (yyvsp[(3) - (5)].string));
		CALLBACK(defrReader::get()->getPropCbk(), defrPropCbkType, &defrReader::get()->getProp());
              }
              defrReader::get()->getRegionProp().defiPropType::setPropType((yyvsp[(3) - (5)].string), defPropDefType);
            ;}
    break;

  case 73:

/* Line 1455 of yacc.c  */
#line 574 "def.y"
    { dumb_mode = 1 ; no_num = 1; defrReader::get()->getProp().defiProp::clear(); ;}
    break;

  case 74:

/* Line 1455 of yacc.c  */
#line 576 "def.y"
    {
              if (defrReader::get()->getPropCbk()) {
                defrReader::get()->getProp().defiProp::setPropType("group", (yyvsp[(3) - (5)].string));
		CALLBACK(defrReader::get()->getPropCbk(), defrPropCbkType, &defrReader::get()->getProp());
              }
              defrReader::get()->getGroupProp().defiPropType::setPropType((yyvsp[(3) - (5)].string), defPropDefType);
            ;}
    break;

  case 75:

/* Line 1455 of yacc.c  */
#line 583 "def.y"
    { dumb_mode = 1 ; no_num = 1; defrReader::get()->getProp().defiProp::clear(); ;}
    break;

  case 76:

/* Line 1455 of yacc.c  */
#line 585 "def.y"
    {
              if (defrReader::get()->getPropCbk()) {
                defrReader::get()->getProp().defiProp::setPropType("component", (yyvsp[(3) - (5)].string));
		CALLBACK(defrReader::get()->getPropCbk(), defrPropCbkType, &defrReader::get()->getProp());
              }
              defrReader::get()->getCompProp().defiPropType::setPropType((yyvsp[(3) - (5)].string), defPropDefType);
            ;}
    break;

  case 77:

/* Line 1455 of yacc.c  */
#line 592 "def.y"
    { dumb_mode = 1 ; no_num = 1; defrReader::get()->getProp().defiProp::clear(); ;}
    break;

  case 78:

/* Line 1455 of yacc.c  */
#line 594 "def.y"
    {
              if (defrReader::get()->getPropCbk()) {
                defrReader::get()->getProp().defiProp::setPropType("row", (yyvsp[(3) - (5)].string));
		CALLBACK(defrReader::get()->getPropCbk(), defrPropCbkType, &defrReader::get()->getProp());
              }
              defrReader::get()->getRowProp().defiPropType::setPropType((yyvsp[(3) - (5)].string), defPropDefType);
            ;}
    break;

  case 79:

/* Line 1455 of yacc.c  */
#line 601 "def.y"
    { dumb_mode = 1 ; no_num = 1; defrReader::get()->getProp().defiProp::clear(); ;}
    break;

  case 80:

/* Line 1455 of yacc.c  */
#line 603 "def.y"
    {
              if (defrReader::get()->getPropCbk()) {
                defrReader::get()->getProp().defiProp::setPropType("pin", (yyvsp[(3) - (5)].string));
		CALLBACK(defrReader::get()->getPropCbk(), defrPropCbkType, &defrReader::get()->getProp());
              }
              defrReader::get()->getPinDefProp().defiPropType::setPropType((yyvsp[(3) - (5)].string), defPropDefType);
            ;}
    break;

  case 81:

/* Line 1455 of yacc.c  */
#line 611 "def.y"
    { dumb_mode = 1 ; no_num = 1; defrReader::get()->getProp().defiProp::clear(); ;}
    break;

  case 82:

/* Line 1455 of yacc.c  */
#line 613 "def.y"
    {
              if (defrReader::get()->getPropCbk()) {
                defrReader::get()->getProp().defiProp::setPropType("componentpin", (yyvsp[(3) - (5)].string));
		CALLBACK(defrReader::get()->getPropCbk(), defrPropCbkType, &defrReader::get()->getProp());
              }
              defrReader::get()->getCompPinProp().defiPropType::setPropType((yyvsp[(3) - (5)].string), defPropDefType);
            ;}
    break;

  case 83:

/* Line 1455 of yacc.c  */
#line 621 "def.y"
    { dumb_mode = 1 ; no_num = 1; defrReader::get()->getProp().defiProp::clear(); ;}
    break;

  case 84:

/* Line 1455 of yacc.c  */
#line 623 "def.y"
    {
              if (defVersionNum < 5.6) {
                if (nonDefaultWarnings++ < defrReader::get()->getNonDefaultWarnings()) {
                  defMsg = (char*)defMalloc(1000); 
                  sprintf (defMsg,
                     "NONDEFAULTRULE statement is a version 5.6 and later syntax.\nYour def file is defined with version %g.", defVersionNum);
                  defError(6505, defMsg);
                  defFree(defMsg);
                  CHKERR();
                }
              } else {
                if (defrReader::get()->getPropCbk()) {
                  defrReader::get()->getProp().defiProp::setPropType("nondefaultrule", (yyvsp[(3) - (5)].string));
		  CALLBACK(defrReader::get()->getPropCbk(), defrPropCbkType, &defrReader::get()->getProp());
                }
                defrReader::get()->getNDefProp().defiPropType::setPropType((yyvsp[(3) - (5)].string), defPropDefType);
              }
            ;}
    break;

  case 85:

/* Line 1455 of yacc.c  */
#line 641 "def.y"
    { yyerrok; yyclearin;;}
    break;

  case 86:

/* Line 1455 of yacc.c  */
#line 643 "def.y"
    { real_num = 0 ;}
    break;

  case 87:

/* Line 1455 of yacc.c  */
#line 644 "def.y"
    {
              if (defrReader::get()->getPropCbk()) defrReader::get()->getProp().defiProp::setPropInteger();
              defPropDefType = 'I';
            ;}
    break;

  case 88:

/* Line 1455 of yacc.c  */
#line 648 "def.y"
    { real_num = 1 ;}
    break;

  case 89:

/* Line 1455 of yacc.c  */
#line 649 "def.y"
    {
              if (defrReader::get()->getPropCbk()) defrReader::get()->getProp().defiProp::setPropReal();
              defPropDefType = 'R';
              real_num = 0;
            ;}
    break;

  case 90:

/* Line 1455 of yacc.c  */
#line 655 "def.y"
    {
              if (defrReader::get()->getPropCbk()) defrReader::get()->getProp().defiProp::setPropString();
              defPropDefType = 'S';
            ;}
    break;

  case 91:

/* Line 1455 of yacc.c  */
#line 660 "def.y"
    {
              if (defrReader::get()->getPropCbk()) defrReader::get()->getProp().defiProp::setPropQString((yyvsp[(2) - (2)].string));
              defPropDefType = 'Q';
            ;}
    break;

  case 92:

/* Line 1455 of yacc.c  */
#line 665 "def.y"
    {
              if (defrReader::get()->getPropCbk()) defrReader::get()->getProp().defiProp::setPropNameMapString((yyvsp[(2) - (2)].string));
              defPropDefType = 'S';
            ;}
    break;

  case 94:

/* Line 1455 of yacc.c  */
#line 672 "def.y"
    { if (defrReader::get()->getPropCbk()) defrReader::get()->getProp().defiProp::setNumber((yyvsp[(1) - (1)].dval)); ;}
    break;

  case 95:

/* Line 1455 of yacc.c  */
#line 675 "def.y"
    {
            if (defrReader::get()->getUnitsCbk()) {
              if (defValidNum((int)(yyvsp[(4) - (5)].dval)))
                CALLBACK(defrReader::get()->getUnitsCbk(),  defrUnitsCbkType, (yyvsp[(4) - (5)].dval));
            }
          ;}
    break;

  case 96:

/* Line 1455 of yacc.c  */
#line 683 "def.y"
    {
            if (defrReader::get()->getDividerCbk())
              CALLBACK(defrReader::get()->getDividerCbk(), defrDividerCbkType, (yyvsp[(2) - (3)].string));
            hasDivChar = 1;
          ;}
    break;

  case 97:

/* Line 1455 of yacc.c  */
#line 690 "def.y"
    { 
            if (defrReader::get()->getBusBitCbk())
              CALLBACK(defrReader::get()->getBusBitCbk(), defrBusBitCbkType, (yyvsp[(2) - (3)].string));
            hasBusBit = 1;
          ;}
    break;

  case 98:

/* Line 1455 of yacc.c  */
#line 696 "def.y"
    { dumb_mode = 1; no_num = 1; defrReader::get()->getSite().defiSite::clear(); ;}
    break;

  case 99:

/* Line 1455 of yacc.c  */
#line 699 "def.y"
    {
            if (defrReader::get()->getSiteCbk()) {
              defrReader::get()->getSite().defiSite::setName((yyvsp[(3) - (14)].string));
              defrReader::get()->getSite().defiSite::setLocation((yyvsp[(4) - (14)].dval),(yyvsp[(5) - (14)].dval));
              defrReader::get()->getSite().defiSite::setOrient((yyvsp[(6) - (14)].integer));
              defrReader::get()->getSite().defiSite::setDo((yyvsp[(8) - (14)].dval),(yyvsp[(10) - (14)].dval),(yyvsp[(12) - (14)].dval),(yyvsp[(13) - (14)].dval));
	      CALLBACK(defrReader::get()->getSiteCbk(), defrSiteCbkType, &(defrReader::get()->getSite()));
            }
	  ;}
    break;

  case 100:

/* Line 1455 of yacc.c  */
#line 709 "def.y"
    {dumb_mode = 1;no_num = 1; ;}
    break;

  case 101:

/* Line 1455 of yacc.c  */
#line 711 "def.y"
    {
              if (defrReader::get()->getCanplaceCbk()) {
                defrReader::get()->getCanplace().defiSite::setName((yyvsp[(3) - (14)].string));
                defrReader::get()->getCanplace().defiSite::setLocation((yyvsp[(4) - (14)].dval),(yyvsp[(5) - (14)].dval));
                defrReader::get()->getCanplace().defiSite::setOrient((yyvsp[(6) - (14)].integer));
                defrReader::get()->getCanplace().defiSite::setDo((yyvsp[(8) - (14)].dval),(yyvsp[(10) - (14)].dval),(yyvsp[(12) - (14)].dval),(yyvsp[(13) - (14)].dval));
		CALLBACK(defrReader::get()->getCanplaceCbk(), defrCanplaceCbkType,
		&(defrReader::get()->getCanplace()));
              }
            ;}
    break;

  case 102:

/* Line 1455 of yacc.c  */
#line 721 "def.y"
    {dumb_mode = 1;no_num = 1; ;}
    break;

  case 103:

/* Line 1455 of yacc.c  */
#line 723 "def.y"
    {
              if (defrReader::get()->getCannotOccupyCbk()) {
                defrReader::get()->getCannotOccupy().defiSite::setName((yyvsp[(3) - (14)].string));
                defrReader::get()->getCannotOccupy().defiSite::setLocation((yyvsp[(4) - (14)].dval),(yyvsp[(5) - (14)].dval));
                defrReader::get()->getCannotOccupy().defiSite::setOrient((yyvsp[(6) - (14)].integer));
                defrReader::get()->getCannotOccupy().defiSite::setDo((yyvsp[(8) - (14)].dval),(yyvsp[(10) - (14)].dval),(yyvsp[(12) - (14)].dval),(yyvsp[(13) - (14)].dval));
		CALLBACK(defrReader::get()->getCannotOccupyCbk(), defrCannotOccupyCbkType,
                        &(defrReader::get()->getCannotOccupy()));
              }
            ;}
    break;

  case 104:

/* Line 1455 of yacc.c  */
#line 734 "def.y"
    {(yyval.integer) = 0;;}
    break;

  case 105:

/* Line 1455 of yacc.c  */
#line 735 "def.y"
    {(yyval.integer) = 1;;}
    break;

  case 106:

/* Line 1455 of yacc.c  */
#line 736 "def.y"
    {(yyval.integer) = 2;;}
    break;

  case 107:

/* Line 1455 of yacc.c  */
#line 737 "def.y"
    {(yyval.integer) = 3;;}
    break;

  case 108:

/* Line 1455 of yacc.c  */
#line 738 "def.y"
    {(yyval.integer) = 4;;}
    break;

  case 109:

/* Line 1455 of yacc.c  */
#line 739 "def.y"
    {(yyval.integer) = 5;;}
    break;

  case 110:

/* Line 1455 of yacc.c  */
#line 740 "def.y"
    {(yyval.integer) = 6;;}
    break;

  case 111:

/* Line 1455 of yacc.c  */
#line 741 "def.y"
    {(yyval.integer) = 7;;}
    break;

  case 112:

/* Line 1455 of yacc.c  */
#line 744 "def.y"
    {
            if (!defrReader::get()->getGeomPtr()) {  // set up for more than 4 points
              defrReader::get()->setGeomPtr((defiGeometries*)defMalloc(sizeof(defiGeometries)));
              defrReader::get()->getGeomPtr()->defiGeometries::Init();
            } else    // Just reset the number of points
              defrReader::get()->getGeomPtr()->defiGeometries::Reset();
	  ;}
    break;

  case 113:

/* Line 1455 of yacc.c  */
#line 752 "def.y"
    {
	    if (defrReader::get()->getDieAreaCbk()) {
               defrReader::get()->getDieArea().defiBox::addPoint(defrReader::get()->getGeomPtr());
               CALLBACK(defrReader::get()->getDieAreaCbk(), defrDieAreaCbkType, &(defrReader::get()->getDieArea()));
            }
          ;}
    break;

  case 114:

/* Line 1455 of yacc.c  */
#line 761 "def.y"
    { ;}
    break;

  case 115:

/* Line 1455 of yacc.c  */
#line 764 "def.y"
    {
          if (defVersionNum < 5.4) {
             if (defrReader::get()->getDefaultCapCbk())
                CALLBACK(defrReader::get()->getDefaultCapCbk(), defrDefaultCapCbkType, ROUND((yyvsp[(2) - (2)].dval)));
          } else {
             if (defrReader::get()->getDefaultCapCbk()) /* write error only if cbk is set */
                if (defaultCapWarnings++ < defrReader::get()->getDefaultCapWarnings())
                   defWarning(7017, "DEFAULTCAP statement is obsolete in version 5.4 and earlier.\nThe DEF parser will ignore this statement.");
          }
        ;}
    break;

  case 118:

/* Line 1455 of yacc.c  */
#line 780 "def.y"
    {
            if (defVersionNum < 5.4) {
	      if (defrReader::get()->getPinCapCbk()) {
	        defrReader::get()->getPinCap().defiPinCap::setPin(ROUND((yyvsp[(2) - (5)].dval)));
	        defrReader::get()->getPinCap().defiPinCap::setCap((yyvsp[(4) - (5)].dval));
	        CALLBACK(defrReader::get()->getPinCapCbk(), defrPinCapCbkType, &(defrReader::get()->getPinCap()));
	      }
            }
	  ;}
    break;

  case 119:

/* Line 1455 of yacc.c  */
#line 791 "def.y"
    { ;}
    break;

  case 120:

/* Line 1455 of yacc.c  */
#line 794 "def.y"
    { ;}
    break;

  case 121:

/* Line 1455 of yacc.c  */
#line 797 "def.y"
    { 
            if (defrReader::get()->getStartPinsCbk())
              CALLBACK(defrReader::get()->getStartPinsCbk(), defrStartPinsCbkType, ROUND((yyvsp[(2) - (3)].dval)));
          ;}
    break;

  case 124:

/* Line 1455 of yacc.c  */
#line 806 "def.y"
    {dumb_mode = 1; no_num = 1; ;}
    break;

  case 125:

/* Line 1455 of yacc.c  */
#line 807 "def.y"
    {dumb_mode = 1; no_num = 1; ;}
    break;

  case 126:

/* Line 1455 of yacc.c  */
#line 808 "def.y"
    {
            if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
              defrReader::get()->getPin().defiPin::Setup((yyvsp[(3) - (7)].string), (yyvsp[(7) - (7)].string));
            }
            hasPort = 0;
          ;}
    break;

  case 127:

/* Line 1455 of yacc.c  */
#line 815 "def.y"
    { 
            if (defrReader::get()->getPinCbk())
              CALLBACK(defrReader::get()->getPinCbk(), defrPinCbkType, &defrReader::get()->getPin());
          ;}
    break;

  case 130:

/* Line 1455 of yacc.c  */
#line 824 "def.y"
    {
            if (defrReader::get()->getPinCbk())
              defrReader::get()->getPin().defiPin::setSpecial();
          ;}
    break;

  case 131:

/* Line 1455 of yacc.c  */
#line 830 "def.y"
    { 
            if (defrReader::get()->getPinExtCbk())
              CALLBACK(defrReader::get()->getPinExtCbk(), defrPinExtCbkType, History_text);
          ;}
    break;

  case 132:

/* Line 1455 of yacc.c  */
#line 836 "def.y"
    {
            if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk())
              defrReader::get()->getPin().defiPin::setDirection((yyvsp[(3) - (3)].string));
          ;}
    break;

  case 133:

/* Line 1455 of yacc.c  */
#line 842 "def.y"
    {
            if (defVersionNum < 5.6) {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if ((pinWarnings++ < defrReader::get()->getPinWarnings()) &&
                    (pinWarnings++ < defrReader::get()->getPinExtWarnings())) {
                  defMsg = (char*)defMalloc(1000);
                  sprintf (defMsg,
                     "NETEXPR statement is a version 5.6 and later syntax.\nYour def file is defined with version %g.", defVersionNum);
                  defError(6506, defMsg);
                  defFree(defMsg);
                  CHKERR();
                }
              }
            } else {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk())
                defrReader::get()->getPin().defiPin::setNetExpr((yyvsp[(3) - (3)].string));

            }
          ;}
    break;

  case 134:

/* Line 1455 of yacc.c  */
#line 862 "def.y"
    { dumb_mode = 1; ;}
    break;

  case 135:

/* Line 1455 of yacc.c  */
#line 863 "def.y"
    {
            if (defVersionNum < 5.6) {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if ((pinWarnings++ < defrReader::get()->getPinWarnings()) &&
                    (pinWarnings++ < defrReader::get()->getPinExtWarnings())) {
                  defMsg = (char*)defMalloc(1000);
                  sprintf (defMsg,
                     "SUPPLYSENSITIVITY statement is a version 5.6 and later syntax.\nYour def file is defined with version %g.", defVersionNum);
                  defError(6507, defMsg);
                  defFree(defMsg);
                  CHKERR();
                }
              }
            } else {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk())
                defrReader::get()->getPin().defiPin::setSupplySens((yyvsp[(4) - (4)].string));
            }
          ;}
    break;

  case 136:

/* Line 1455 of yacc.c  */
#line 882 "def.y"
    { dumb_mode = 1; ;}
    break;

  case 137:

/* Line 1455 of yacc.c  */
#line 883 "def.y"
    {
            if (defVersionNum < 5.6) {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if ((pinWarnings++ < defrReader::get()->getPinWarnings()) &&
                    (pinWarnings++ < defrReader::get()->getPinExtWarnings())) {
                  defMsg = (char*)defMalloc(1000);
                  sprintf (defMsg,
                     "GROUNDSENSITIVITY statement is a version 5.6 and later syntax.\nYour def file is defined with version %g.", defVersionNum);
                  defError(6508, defMsg);
                  defFree(defMsg);
                  CHKERR();
                }
              }
            } else {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk())
                defrReader::get()->getPin().defiPin::setGroundSens((yyvsp[(4) - (4)].string));
            }
          ;}
    break;

  case 138:

/* Line 1455 of yacc.c  */
#line 903 "def.y"
    {
            if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) defrReader::get()->getPin().defiPin::setUse((yyvsp[(3) - (3)].string));
          ;}
    break;

  case 139:

/* Line 1455 of yacc.c  */
#line 907 "def.y"
    {
            if (defVersionNum < 5.7) {
               if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                 if ((pinWarnings++ < defrReader::get()->getPinWarnings()) &&
                     (pinWarnings++ < defrReader::get()->getPinExtWarnings())) {
                   defMsg = (char*)defMalloc(10000);
                   sprintf (defMsg,
                     "PORT in PINS is a version 5.7 or later syntax.\nYour def file is defined with version %g.", defVersionNum);
                   defError(6555, defMsg);
                   defFree(defMsg);
                   CHKERR();
                 }
               }
            } else {
               if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk())
                 defrReader::get()->getPin().defiPin::addPort();
               hasPort = 1;
            }
          ;}
    break;

  case 140:

/* Line 1455 of yacc.c  */
#line 927 "def.y"
    { dumb_mode = 1; ;}
    break;

  case 141:

/* Line 1455 of yacc.c  */
#line 928 "def.y"
    {
            if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
              if (hasPort)
                 defrReader::get()->getPin().defiPin::addPortLayer((yyvsp[(4) - (4)].string));
              else
                 defrReader::get()->getPin().defiPin::addLayer((yyvsp[(4) - (4)].string));
            }
          ;}
    break;

  case 142:

/* Line 1455 of yacc.c  */
#line 937 "def.y"
    {
            if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
              if (hasPort)
                 defrReader::get()->getPin().defiPin::addPortLayerPts((yyvsp[(7) - (8)].pt).x, (yyvsp[(7) - (8)].pt).y, (yyvsp[(8) - (8)].pt).x, (yyvsp[(8) - (8)].pt).y);
              else
                 defrReader::get()->getPin().defiPin::addLayerPts((yyvsp[(7) - (8)].pt).x, (yyvsp[(7) - (8)].pt).y, (yyvsp[(8) - (8)].pt).x, (yyvsp[(8) - (8)].pt).y);
            }
          ;}
    break;

  case 143:

/* Line 1455 of yacc.c  */
#line 946 "def.y"
    { dumb_mode = 1; ;}
    break;

  case 144:

/* Line 1455 of yacc.c  */
#line 947 "def.y"
    {
            if (defVersionNum < 5.6) {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if ((pinWarnings++ < defrReader::get()->getPinWarnings()) &&
                    (pinWarnings++ < defrReader::get()->getPinExtWarnings())) {
                  defMsg = (char*)defMalloc(1000);
                  sprintf (defMsg,
                     "POLYGON statement is a version 5.6 and later syntax.\nYour def file is defined with version %g.", defVersionNum);
                  defError(6509, defMsg);
                  defFree(defMsg);
                  CHKERR();
                }
              }
            } else {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if (hasPort)
                   defrReader::get()->getPin().defiPin::addPortPolygon((yyvsp[(4) - (4)].string));
                else
                   defrReader::get()->getPin().defiPin::addPolygon((yyvsp[(4) - (4)].string));
              }
            }
            if (!defrReader::get()->getGeomPtr()) {
              defrReader::get()->setGeomPtr((defiGeometries*)defMalloc(sizeof(defiGeometries)));
              defrReader::get()->getGeomPtr()->defiGeometries::Init();
            } else {  // Just reset the number of points
              defrReader::get()->getGeomPtr()->defiGeometries::Reset();
            }
          ;}
    break;

  case 145:

/* Line 1455 of yacc.c  */
#line 976 "def.y"
    {
            if (defVersionNum >= 5.6) {  // only add if 5.6 or beyond
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if (hasPort)
                   defrReader::get()->getPin().defiPin::addPortPolygonPts(defrReader::get()->getGeomPtr());
                else
                   defrReader::get()->getPin().defiPin::addPolygonPts(defrReader::get()->getGeomPtr());
              }
            }
          ;}
    break;

  case 146:

/* Line 1455 of yacc.c  */
#line 986 "def.y"
    { dumb_mode = 1; ;}
    break;

  case 147:

/* Line 1455 of yacc.c  */
#line 987 "def.y"
    {
            if (defVersionNum < 5.7) {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if ((pinWarnings++ < defrReader::get()->getPinWarnings()) &&
                    (pinWarnings++ < defrReader::get()->getPinExtWarnings())) {
                  defMsg = (char*)defMalloc(1000);
                  sprintf (defMsg,
                     "PIN VIA statement is a version 5.7 and later syntax.\nYour def file is defined with version %g.", defVersionNum);
                  defError(6556, defMsg);
                  defFree(defMsg);
                  CHKERR();
                }
              }
            } else {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if (hasPort)
                   defrReader::get()->getPin().defiPin::addPortVia((yyvsp[(4) - (8)].string), (yyvsp[(6) - (8)].dval), (yyvsp[(7) - (8)].dval));
                else
                   defrReader::get()->getPin().defiPin::addVia((yyvsp[(4) - (8)].string), (yyvsp[(6) - (8)].dval), (yyvsp[(7) - (8)].dval));
              }
            }
          ;}
    break;

  case 148:

/* Line 1455 of yacc.c  */
#line 1011 "def.y"
    {
            if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
              if (hasPort)
                 defrReader::get()->getPin().defiPin::setPortPlacement((yyvsp[(1) - (3)].integer), (yyvsp[(2) - (3)].pt).x, (yyvsp[(2) - (3)].pt).y, (yyvsp[(3) - (3)].integer));
              else
                 defrReader::get()->getPin().defiPin::setPlacement((yyvsp[(1) - (3)].integer), (yyvsp[(2) - (3)].pt).x, (yyvsp[(2) - (3)].pt).y, (yyvsp[(3) - (3)].integer));
            }
          ;}
    break;

  case 149:

/* Line 1455 of yacc.c  */
#line 1022 "def.y"
    {
            if (defVersionNum <= 5.3) {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if ((pinWarnings++ < defrReader::get()->getPinWarnings()) &&
                    (pinWarnings++ < defrReader::get()->getPinExtWarnings())) {
                  defMsg = (char*)defMalloc(1000);
                  sprintf (defMsg,
                     "ANTENNAPINPARTIALMETALAREA statement is a version 5.4 and later syntax.\nYour def file is defined with version %g.", defVersionNum);
                  defError(6510, defMsg);
                  defFree(defMsg);
                  CHKERR();
                }
              }
            }
            if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk())
              defrReader::get()->getPin().defiPin::addAPinPartialMetalArea((int)(yyvsp[(3) - (4)].dval), (yyvsp[(4) - (4)].string)); 
          ;}
    break;

  case 150:

/* Line 1455 of yacc.c  */
#line 1040 "def.y"
    {
            if (defVersionNum <= 5.3) {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if ((pinWarnings++ < defrReader::get()->getPinWarnings()) &&
                    (pinWarnings++ < defrReader::get()->getPinExtWarnings())) {
                  defMsg = (char*)defMalloc(1000);
                  sprintf (defMsg,
                     "ANTENNAPINPARTIALMETALSIDEAREA statement is a version 5.4 and later syntax.\nYour def file is defined with version %g", defVersionNum);
                  defError(6511, defMsg);
                  defFree(defMsg);
                  CHKERR();
                }
              }
            }
            if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk())
              defrReader::get()->getPin().defiPin::addAPinPartialMetalSideArea((int)(yyvsp[(3) - (4)].dval), (yyvsp[(4) - (4)].string)); 
          ;}
    break;

  case 151:

/* Line 1455 of yacc.c  */
#line 1058 "def.y"
    {
            if (defVersionNum <= 5.3) {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if ((pinWarnings++ < defrReader::get()->getPinWarnings()) &&
                    (pinWarnings++ < defrReader::get()->getPinExtWarnings())) {
                  defMsg = (char*)defMalloc(1000);
                  sprintf (defMsg,
                     "ANTENNAPINGATEAREA statement is a version 5.4 and later syntax.\nYour def file is defined with version %g", defVersionNum);
                  defError(6512, defMsg);
                  defFree(defMsg);
                  CHKERR();
                }
              }
            }
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk())
                defrReader::get()->getPin().defiPin::addAPinGateArea((int)(yyvsp[(3) - (4)].dval), (yyvsp[(4) - (4)].string)); 
            ;}
    break;

  case 152:

/* Line 1455 of yacc.c  */
#line 1076 "def.y"
    {
            if (defVersionNum <= 5.3) {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if ((pinWarnings++ < defrReader::get()->getPinWarnings()) &&
                    (pinWarnings++ < defrReader::get()->getPinExtWarnings())) {
                  defMsg = (char*)defMalloc(1000);
                  sprintf (defMsg,
                     "ANTENNAPINDIFFAREA statement is a version 5.4 and later syntax.\nYour def file is defined with version %g", defVersionNum);
                  defError(6513, defMsg);
                  defFree(defMsg);
                  CHKERR();
                }
              }
            }
            if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk())
              defrReader::get()->getPin().defiPin::addAPinDiffArea((int)(yyvsp[(3) - (4)].dval), (yyvsp[(4) - (4)].string)); 
          ;}
    break;

  case 153:

/* Line 1455 of yacc.c  */
#line 1093 "def.y"
    {dumb_mode=1;;}
    break;

  case 154:

/* Line 1455 of yacc.c  */
#line 1094 "def.y"
    {
            if (defVersionNum <= 5.3) {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if ((pinWarnings++ < defrReader::get()->getPinWarnings()) &&
                    (pinWarnings++ < defrReader::get()->getPinExtWarnings())) {
                  defMsg = (char*)defMalloc(1000);
                  sprintf (defMsg,
                     "ANTENNAPINMAXAREACAR statement is a version 5.4 and later syntax.\nYour def file is defined with version %g", defVersionNum);
                  defError(6514, defMsg);
                  defFree(defMsg);
                  CHKERR();
                }
              }
            }
            if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk())
              defrReader::get()->getPin().defiPin::addAPinMaxAreaCar((int)(yyvsp[(3) - (6)].dval), (yyvsp[(6) - (6)].string)); 
          ;}
    break;

  case 155:

/* Line 1455 of yacc.c  */
#line 1111 "def.y"
    {dumb_mode=1;;}
    break;

  case 156:

/* Line 1455 of yacc.c  */
#line 1113 "def.y"
    {
            if (defVersionNum <= 5.3) {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if ((pinWarnings++ < defrReader::get()->getPinWarnings()) &&
                    (pinWarnings++ < defrReader::get()->getPinExtWarnings())) {
                  defMsg = (char*)defMalloc(1000);
                  sprintf (defMsg,
                     "ANTENNAPINMAXSIDEAREACAR statement is a version 5.4 and later syntax.\nYour def file is defined with version %g", defVersionNum);
                  defError(6515, defMsg);
                  defFree(defMsg);
                  CHKERR();
                }
              }
            }
            if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk())
              defrReader::get()->getPin().defiPin::addAPinMaxSideAreaCar((int)(yyvsp[(3) - (6)].dval), (yyvsp[(6) - (6)].string)); 
          ;}
    break;

  case 157:

/* Line 1455 of yacc.c  */
#line 1131 "def.y"
    {
            if (defVersionNum <= 5.3) {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if ((pinWarnings++ < defrReader::get()->getPinWarnings()) &&
                    (pinWarnings++ < defrReader::get()->getPinExtWarnings())) {
                  defMsg = (char*)defMalloc(1000);
                  sprintf (defMsg,
                     "ANTENNAPINPARTIALCUTAREA statement is a version 5.4 and later syntax.\nYour def file is defined with version %g", defVersionNum);
                  defError(6516, defMsg);
                  defFree(defMsg);
                  CHKERR();
                }
              }
            }
            if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk())
              defrReader::get()->getPin().defiPin::addAPinPartialCutArea((int)(yyvsp[(3) - (4)].dval), (yyvsp[(4) - (4)].string)); 
          ;}
    break;

  case 158:

/* Line 1455 of yacc.c  */
#line 1148 "def.y"
    {dumb_mode=1;;}
    break;

  case 159:

/* Line 1455 of yacc.c  */
#line 1149 "def.y"
    {
            if (defVersionNum <= 5.3) {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if ((pinWarnings++ < defrReader::get()->getPinWarnings()) &&
                    (pinWarnings++ < defrReader::get()->getPinExtWarnings())) {
                  defMsg = (char*)defMalloc(1000);
                  sprintf (defMsg,
                     "ANTENNAPINMAXCUTCAR statement is a version 5.4 and later syntax.\nYour def file is defined with version %g", defVersionNum);
                  defError(6517, defMsg);
                  defFree(defMsg);
                  CHKERR();
                }
              }
            }
            if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk())
              defrReader::get()->getPin().defiPin::addAPinMaxCutCar((int)(yyvsp[(3) - (6)].dval), (yyvsp[(6) - (6)].string)); 
          ;}
    break;

  case 160:

/* Line 1455 of yacc.c  */
#line 1167 "def.y"
    {  /* 5.5 syntax */
            if (defVersionNum < 5.5) {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if ((pinWarnings++ < defrReader::get()->getPinWarnings()) &&
                    (pinWarnings++ < defrReader::get()->getPinExtWarnings())) {
                  defMsg = (char*)defMalloc(1000);
                  sprintf (defMsg,
                     "ANTENNAMODEL statement is a version 5.5 and later syntax.\nYour def file is defined with version %g", defVersionNum);
                  defError(6518, defMsg);
                  defFree(defMsg);
                  CHKERR();
                }
              }
            }
          ;}
    break;

  case 162:

/* Line 1455 of yacc.c  */
#line 1185 "def.y"
    {
            if (defVersionNum < 5.6) {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if ((pinWarnings++ < defrReader::get()->getPinWarnings()) &&
                    (pinWarnings++ < defrReader::get()->getPinExtWarnings())) {
                  defMsg = (char*)defMalloc(1000);
                  sprintf (defMsg,
                     "SPACING statement is a version 5.6 and later syntax.\nYour def file is defined with version %g", defVersionNum);
                  defError(6519, defMsg);
                  defFree(defMsg);
                  CHKERR();
                }
              }
            } else {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if (hasPort)
                   defrReader::get()->getPin().defiPin::addPortLayerSpacing((int)(yyvsp[(2) - (2)].dval));
                else
                   defrReader::get()->getPin().defiPin::addLayerSpacing((int)(yyvsp[(2) - (2)].dval));
              }
            }
          ;}
    break;

  case 163:

/* Line 1455 of yacc.c  */
#line 1208 "def.y"
    {
            if (defVersionNum < 5.6) {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if ((pinWarnings++ < defrReader::get()->getPinWarnings()) &&
                    (pinWarnings++ < defrReader::get()->getPinExtWarnings())) {
                  defMsg = (char*)defMalloc(1000);
                  sprintf (defMsg,
                     "DESIGNRULEWIDTH statement is a version 5.6 and later syntax.\nYour def file is defined with version %g", defVersionNum);
                  defError(6520, defMsg);
                  defFree(defMsg);
                  CHKERR();
                }
              }
            } else {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if (hasPort)
                   defrReader::get()->getPin().defiPin::addPortLayerDesignRuleWidth((int)(yyvsp[(2) - (2)].dval));
                else
                   defrReader::get()->getPin().defiPin::addLayerDesignRuleWidth((int)(yyvsp[(2) - (2)].dval));
              }
            }
          ;}
    break;

  case 165:

/* Line 1455 of yacc.c  */
#line 1233 "def.y"
    {
            if (defVersionNum < 5.6) {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if ((pinWarnings++ < defrReader::get()->getPinWarnings()) &&
                    (pinWarnings++ < defrReader::get()->getPinExtWarnings())) {
                  defMsg = (char*)defMalloc(1000);
                  sprintf (defMsg,
                     "SPACING statement is a version 5.6 and later syntax.\nYour def file is defined with version %g", defVersionNum);
                  defError(6521, defMsg);
                  defFree(defMsg);
                  CHKERR();
                }
              }
            } else {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if (hasPort)
                   defrReader::get()->getPin().defiPin::addPortPolySpacing((int)(yyvsp[(2) - (2)].dval));
                else
                   defrReader::get()->getPin().defiPin::addPolySpacing((int)(yyvsp[(2) - (2)].dval));
              }
            }
          ;}
    break;

  case 166:

/* Line 1455 of yacc.c  */
#line 1256 "def.y"
    {
            if (defVersionNum < 5.6) {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if ((pinWarnings++ < defrReader::get()->getPinWarnings()) &&
                    (pinWarnings++ < defrReader::get()->getPinExtWarnings())) {
                  defMsg = (char*)defMalloc(1000);
                  sprintf (defMsg,
                     "DESIGNRULEWIDTH statement is a version 5.6 and later syntax.\nYour def file is defined with version %g", defVersionNum);
                  defError(6522, defMsg);
                  defFree(defMsg);
                  CHKERR();
                }
              }
            } else {
              if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk()) {
                if (hasPort)
                   defrReader::get()->getPin().defiPin::addPortPolyDesignRuleWidth((int)(yyvsp[(2) - (2)].dval));
                else
                   defrReader::get()->getPin().defiPin::addPolyDesignRuleWidth((int)(yyvsp[(2) - (2)].dval));
              }
            }
          ;}
    break;

  case 167:

/* Line 1455 of yacc.c  */
#line 1280 "def.y"
    { aOxide = 1;
            if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk())
              defrReader::get()->getPin().defiPin::addAntennaModel(aOxide);
          ;}
    break;

  case 168:

/* Line 1455 of yacc.c  */
#line 1285 "def.y"
    { aOxide = 2;
            if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk())
              defrReader::get()->getPin().defiPin::addAntennaModel(aOxide);
          ;}
    break;

  case 169:

/* Line 1455 of yacc.c  */
#line 1290 "def.y"
    { aOxide = 3;
            if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk())
              defrReader::get()->getPin().defiPin::addAntennaModel(aOxide);
          ;}
    break;

  case 170:

/* Line 1455 of yacc.c  */
#line 1295 "def.y"
    { aOxide = 4;
            if (defrReader::get()->getPinCbk() || defrReader::get()->getPinExtCbk())
              defrReader::get()->getPin().defiPin::addAntennaModel(aOxide);
          ;}
    break;

  case 171:

/* Line 1455 of yacc.c  */
#line 1301 "def.y"
    { (yyval.string) = (char*)"SIGNAL"; ;}
    break;

  case 172:

/* Line 1455 of yacc.c  */
#line 1303 "def.y"
    { (yyval.string) = (char*)"POWER"; ;}
    break;

  case 173:

/* Line 1455 of yacc.c  */
#line 1305 "def.y"
    { (yyval.string) = (char*)"GROUND"; ;}
    break;

  case 174:

/* Line 1455 of yacc.c  */
#line 1307 "def.y"
    { (yyval.string) = (char*)"CLOCK"; ;}
    break;

  case 175:

/* Line 1455 of yacc.c  */
#line 1309 "def.y"
    { (yyval.string) = (char*)"TIEOFF"; ;}
    break;

  case 176:

/* Line 1455 of yacc.c  */
#line 1311 "def.y"
    { (yyval.string) = (char*)"ANALOG"; ;}
    break;

  case 177:

/* Line 1455 of yacc.c  */
#line 1313 "def.y"
    { (yyval.string) = (char*)"SCAN"; ;}
    break;

  case 178:

/* Line 1455 of yacc.c  */
#line 1315 "def.y"
    { (yyval.string) = (char*)"RESET"; ;}
    break;

  case 179:

/* Line 1455 of yacc.c  */
#line 1319 "def.y"
    { (yyval.string) = (char*)""; ;}
    break;

  case 180:

/* Line 1455 of yacc.c  */
#line 1320 "def.y"
    {dumb_mode=1;;}
    break;

  case 181:

/* Line 1455 of yacc.c  */
#line 1321 "def.y"
    { (yyval.string) = (yyvsp[(3) - (3)].string); ;}
    break;

  case 182:

/* Line 1455 of yacc.c  */
#line 1324 "def.y"
    { 
          if (defrReader::get()->getPinEndCbk())
            CALLBACK(defrReader::get()->getPinEndCbk(), defrPinEndCbkType, 0);
        ;}
    break;

  case 183:

/* Line 1455 of yacc.c  */
#line 1329 "def.y"
    {dumb_mode = 2; no_num = 2; ;}
    break;

  case 184:

/* Line 1455 of yacc.c  */
#line 1331 "def.y"
    {
          if (defrReader::get()->getRowCbk()) {
            rowName = (yyvsp[(3) - (7)].string);
            defrReader::get()->getRow().defiRow::setup((yyvsp[(3) - (7)].string), (yyvsp[(4) - (7)].string), (yyvsp[(5) - (7)].dval), (yyvsp[(6) - (7)].dval), (yyvsp[(7) - (7)].integer));
          }
        ;}
    break;

  case 185:

/* Line 1455 of yacc.c  */
#line 1339 "def.y"
    {
          if (defrReader::get()->getRowCbk()) 
	    CALLBACK(defrReader::get()->getRowCbk(), defrRowCbkType, &defrReader::get()->getRow());
        ;}
    break;

  case 186:

/* Line 1455 of yacc.c  */
#line 1345 "def.y"
    {
          if (defVersionNum < 5.6) {
            if (defrReader::get()->getRowCbk()) {
              if (rowWarnings++ < defrReader::get()->getRowWarnings()) {
                defError(6523, "The DO statement which is required in ROW statement is missing.");
                CHKERR();
              }
            }
          }
        ;}
    break;

  case 187:

/* Line 1455 of yacc.c  */
#line 1356 "def.y"
    {
          /* 06/05/2002 - pcr 448455 */
          /* Check for 1 and 0 in the correct position */
          /* 07/26/2002 - Commented out due to pcr 459218 */
          if (hasDoStep) {
            /* 04/29/2004 - pcr 695535 */
            /* changed the testing */
            if ((((yyvsp[(4) - (5)].dval) == 1) && (yStep == 0)) ||
                (((yyvsp[(2) - (5)].dval) == 1) && (xStep == 0))) {
              /* do nothing */
            } else 
              if (defVersionNum < 5.6) {
                if (defrReader::get()->getRowCbk()) {
                  if (rowWarnings++ < defrReader::get()->getRowWarnings()) {
                    defMsg = (char*)defMalloc(1000);
                    sprintf(defMsg,
                            "ROW %s: Both spaceX and spaceY values were specified. The correct syntax for ROW DO statement is \"DO numX BY 1 STEP spaceX 0 | DO 1 BY numY STEP 0 spaceY\". Only the space direction for the appropriate row orientation was retained.", rowName);
                    defWarning(7018, defMsg);
                    defFree(defMsg);
                    }
                  }
              }
          }
          /* pcr 459218 - Error if at least numX or numY does not equal 1 */
          if (((yyvsp[(2) - (5)].dval) != 1) && ((yyvsp[(4) - (5)].dval) != 1)) {
            if (defrReader::get()->getRowCbk()) {
              if (rowWarnings++ < defrReader::get()->getRowWarnings()) {
                defError(6524, "Syntax error, correct syntax is either \"DO 1 BY num... or DO numX BY 1...\"");
                CHKERR();
              }
            }
          }
          if (defrReader::get()->getRowCbk())
            defrReader::get()->getRow().defiRow::setDo(ROUND((yyvsp[(2) - (5)].dval)), ROUND((yyvsp[(4) - (5)].dval)), xStep, yStep);
        ;}
    break;

  case 188:

/* Line 1455 of yacc.c  */
#line 1393 "def.y"
    {
          hasDoStep = 0;
        ;}
    break;

  case 189:

/* Line 1455 of yacc.c  */
#line 1397 "def.y"
    {
          hasDoStep = 1;
          defrReader::get()->getRow().defiRow::setHasDoStep();
          xStep = (yyvsp[(2) - (3)].dval);
          yStep = (yyvsp[(3) - (3)].dval);
        ;}
    break;

  case 192:

/* Line 1455 of yacc.c  */
#line 1408 "def.y"
    { dumb_mode = 10000000; parsing_property = 1;;}
    break;

  case 193:

/* Line 1455 of yacc.c  */
#line 1410 "def.y"
    { dumb_mode = 0; parsing_property = 0; ;}
    break;

  case 196:

/* Line 1455 of yacc.c  */
#line 1417 "def.y"
    {
          if (defrReader::get()->getRowCbk()) {
             char propTp;
             char* str = ringCopy("                       ");
             propTp =  defrReader::get()->getRowProp().defiPropType::propType((yyvsp[(1) - (2)].string));
             CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "ROW");
             /* For backword compatibility, also set the string value */
             /* We will use a temporary string to store the number.
              * The string space is borrowed from the ring buffer
              * in the lexer. */
             sprintf(str, "%g", (yyvsp[(2) - (2)].dval));
             defrReader::get()->getRow().defiRow::addNumProperty((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].dval), str, propTp);
          }
        ;}
    break;

  case 197:

/* Line 1455 of yacc.c  */
#line 1432 "def.y"
    {
          if (defrReader::get()->getRowCbk()) {
             char propTp;
             propTp =  defrReader::get()->getRowProp().defiPropType::propType((yyvsp[(1) - (2)].string));
             CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "ROW");
             defrReader::get()->getRow().defiRow::addProperty((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].string), propTp);
          }
        ;}
    break;

  case 198:

/* Line 1455 of yacc.c  */
#line 1441 "def.y"
    {
          if (defrReader::get()->getRowCbk()) {
             char propTp;
             propTp =  defrReader::get()->getRowProp().defiPropType::propType((yyvsp[(1) - (2)].string));
             CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "ROW");
             defrReader::get()->getRow().defiRow::addProperty((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].string), propTp);
          }
        ;}
    break;

  case 199:

/* Line 1455 of yacc.c  */
#line 1451 "def.y"
    {
          if (defrReader::get()->getTrackCbk()) {
            defrReader::get()->getTrack().defiTrack::setup((yyvsp[(1) - (2)].string));
          }
        ;}
    break;

  case 200:

/* Line 1455 of yacc.c  */
#line 1457 "def.y"
    {
          if (((yyvsp[(5) - (9)].dval) <= 0) && (defVersionNum >= 5.4)) {
            if (defrReader::get()->getTrackCbk())
              if (trackWarnings++ < defrReader::get()->getTrackWarnings()) {
                defMsg = (char*)defMalloc(1000);
                sprintf (defMsg,
                   "The DO number %g in TRACK is invalid.\nThe number value has to be greater than 0", (yyvsp[(5) - (9)].dval));
                defError(6525, defMsg);
                defFree(defMsg);
              }
          }
          if ((yyvsp[(7) - (9)].dval) < 0) {
            if (defrReader::get()->getTrackCbk())
              if (trackWarnings++ < defrReader::get()->getTrackWarnings()) {
                defMsg = (char*)defMalloc(1000);
                sprintf (defMsg,
                   "The STEP number %g in TRACK is invalid.\nThe number value has to be greater than 0", (yyvsp[(7) - (9)].dval));
                defError(6526, defMsg);
                defFree(defMsg);
              }
          }
          if (defrReader::get()->getTrackCbk()) {
            defrReader::get()->getTrack().defiTrack::setDo(ROUND((yyvsp[(2) - (9)].dval)), ROUND((yyvsp[(5) - (9)].dval)), (yyvsp[(7) - (9)].dval));
	    CALLBACK(defrReader::get()->getTrackCbk(), defrTrackCbkType, &defrReader::get()->getTrack());
          }
        ;}
    break;

  case 201:

/* Line 1455 of yacc.c  */
#line 1485 "def.y"
    {
          (yyval.string) = (yyvsp[(2) - (2)].string);
        ;}
    break;

  case 202:

/* Line 1455 of yacc.c  */
#line 1490 "def.y"
    { (yyval.string) = (char*)"X";;}
    break;

  case 203:

/* Line 1455 of yacc.c  */
#line 1492 "def.y"
    { (yyval.string) = (char*)"Y";;}
    break;

  case 205:

/* Line 1455 of yacc.c  */
#line 1495 "def.y"
    { dumb_mode = 1000; ;}
    break;

  case 206:

/* Line 1455 of yacc.c  */
#line 1496 "def.y"
    { dumb_mode = 0; ;}
    break;

  case 209:

/* Line 1455 of yacc.c  */
#line 1503 "def.y"
    {
          if (defrReader::get()->getTrackCbk())
            defrReader::get()->getTrack().defiTrack::addLayer((yyvsp[(1) - (1)].string));
        ;}
    break;

  case 210:

/* Line 1455 of yacc.c  */
#line 1510 "def.y"
    {
          if ((yyvsp[(5) - (8)].dval) <= 0) {
            if (defrReader::get()->getGcellGridCbk())
              if (gcellGridWarnings++ < defrReader::get()->getGcellGridWarnings()) {
                defMsg = (char*)defMalloc(1000);
                sprintf (defMsg,
                   "The DO number %g in GCELLGRID is invalid.\nThe number value has to be greater than 0", (yyvsp[(5) - (8)].dval));
                defError(6527, defMsg);
                defFree(defMsg);
              }
          }
          if ((yyvsp[(7) - (8)].dval) < 0) {
            if (defrReader::get()->getGcellGridCbk())
              if (gcellGridWarnings++ < defrReader::get()->getGcellGridWarnings()) {
                defMsg = (char*)defMalloc(1000);
                sprintf (defMsg,
                   "The STEP number %g in GCELLGRID is invalid.\nThe number value has to be greater than 0", (yyvsp[(7) - (8)].dval));
                defError(6528, defMsg);
                defFree(defMsg);
              }
          }
	  if (defrReader::get()->getGcellGridCbk()) {
	    defrReader::get()->getGcellGrid().defiGcellGrid::setup((yyvsp[(2) - (8)].string), ROUND((yyvsp[(3) - (8)].dval)), ROUND((yyvsp[(5) - (8)].dval)), (yyvsp[(7) - (8)].dval));
	    CALLBACK(defrReader::get()->getGcellGridCbk(), defrGcellGridCbkType, &defrReader::get()->getGcellGrid());
	  }
	;}
    break;

  case 211:

/* Line 1455 of yacc.c  */
#line 1538 "def.y"
    {
          if (defrReader::get()->getExtensionCbk())
             CALLBACK(defrReader::get()->getExtensionCbk(), defrExtensionCbkType, History_text);
        ;}
    break;

  case 212:

/* Line 1455 of yacc.c  */
#line 1544 "def.y"
    { ;}
    break;

  case 214:

/* Line 1455 of yacc.c  */
#line 1550 "def.y"
    {
          if (defrReader::get()->getViaStartCbk())
            CALLBACK(defrReader::get()->getViaStartCbk(), defrViaStartCbkType, ROUND((yyvsp[(2) - (3)].dval)));
        ;}
    break;

  case 217:

/* Line 1455 of yacc.c  */
#line 1559 "def.y"
    {dumb_mode = 1;no_num = 1; ;}
    break;

  case 218:

/* Line 1455 of yacc.c  */
#line 1560 "def.y"
    {
              if (defrReader::get()->getViaCbk()) defrReader::get()->getVia().defiVia::setup((yyvsp[(3) - (3)].string));
              viaRule = 0;
            ;}
    break;

  case 219:

/* Line 1455 of yacc.c  */
#line 1565 "def.y"
    {
              if (defrReader::get()->getViaCbk())
                CALLBACK(defrReader::get()->getViaCbk(), defrViaCbkType, &defrReader::get()->getVia());
              defrReader::get()->getVia().defiVia::clear();
            ;}
    break;

  case 222:

/* Line 1455 of yacc.c  */
#line 1575 "def.y"
    {dumb_mode = 1;no_num = 1; ;}
    break;

  case 223:

/* Line 1455 of yacc.c  */
#line 1576 "def.y"
    {
              if (defrReader::get()->getViaCbk())
                defrReader::get()->getVia().defiVia::addLayer((yyvsp[(4) - (6)].string), (yyvsp[(5) - (6)].pt).x, (yyvsp[(5) - (6)].pt).y, (yyvsp[(6) - (6)].pt).x, (yyvsp[(6) - (6)].pt).y);
            ;}
    break;

  case 224:

/* Line 1455 of yacc.c  */
#line 1580 "def.y"
    { dumb_mode = 1; ;}
    break;

  case 225:

/* Line 1455 of yacc.c  */
#line 1581 "def.y"
    {
              if (defVersionNum < 5.6) {
                if (defrReader::get()->getViaCbk()) {
                  if (viaWarnings++ < defrReader::get()->getViaWarnings()) {
                    defMsg = (char*)defMalloc(1000);
                    sprintf (defMsg,
                       "POLYGON statement is a version 5.6 and later syntax.\nYour def file is defined with version %g", defVersionNum);
                    defError(6529, defMsg);
                    defFree(defMsg);
                    CHKERR();
                  }
                }
              }
              if (!defrReader::get()->getGeomPtr()) {
                defrReader::get()->setGeomPtr((defiGeometries*)defMalloc(sizeof(defiGeometries)));
                defrReader::get()->getGeomPtr()->defiGeometries::Init();
              } else {  // Just reset the number of points
                defrReader::get()->getGeomPtr()->defiGeometries::Reset();
              }
            ;}
    break;

  case 226:

/* Line 1455 of yacc.c  */
#line 1602 "def.y"
    {
              if (defVersionNum >= 5.6) {  // only add if 5.6 or beyond
                if (defrReader::get()->getViaCbk())
                  defrReader::get()->getVia().defiVia::addPolygon((yyvsp[(4) - (9)].string), defrReader::get()->getGeomPtr());
              }
            ;}
    break;

  case 227:

/* Line 1455 of yacc.c  */
#line 1608 "def.y"
    {dumb_mode = 1;no_num = 1; ;}
    break;

  case 228:

/* Line 1455 of yacc.c  */
#line 1609 "def.y"
    {
              if (defVersionNum < 5.6) {
                if (defrReader::get()->getViaCbk())
                  defrReader::get()->getVia().defiVia::addPattern((yyvsp[(4) - (4)].string));
              } else
                if (defrReader::get()->getViaCbk())
                  if (viaWarnings++ < defrReader::get()->getViaWarnings())
                    defWarning(7019, "PATTERNNAME statement is obsolete in version 5.6 and later.\nDEF parser will ignore this statement."); 
            ;}
    break;

  case 229:

/* Line 1455 of yacc.c  */
#line 1618 "def.y"
    {dumb_mode = 1;no_num = 1; ;}
    break;

  case 230:

/* Line 1455 of yacc.c  */
#line 1620 "def.y"
    {dumb_mode = 3;no_num = 1; ;}
    break;

  case 231:

/* Line 1455 of yacc.c  */
#line 1623 "def.y"
    {
               viaRule = 1;
               if (defVersionNum < 5.6) {
                if (defrReader::get()->getViaCbk()) {
                  if (viaWarnings++ < defrReader::get()->getViaWarnings()) {
                    defMsg = (char*)defMalloc(1000);
                    sprintf (defMsg,
                       "VIARULE statement is a version 5.6 and later syntax.\nYour def file is defined with version %g", defVersionNum);
                    defError(6529, defMsg);
                    defFree(defMsg);
                    CHKERR();
                  }
                }
              } else {
                if (defrReader::get()->getViaCbk())
                   defrReader::get()->getVia().defiVia::addViaRule((yyvsp[(4) - (24)].string), (int)(yyvsp[(7) - (24)].dval), (int)(yyvsp[(8) - (24)].dval), (yyvsp[(12) - (24)].string), (yyvsp[(13) - (24)].string),
                                             (yyvsp[(14) - (24)].string), (int)(yyvsp[(17) - (24)].dval), (int)(yyvsp[(18) - (24)].dval), (int)(yyvsp[(21) - (24)].dval),
                                             (int)(yyvsp[(22) - (24)].dval), (int)(yyvsp[(23) - (24)].dval), (int)(yyvsp[(24) - (24)].dval)); 
              }
            ;}
    break;

  case 233:

/* Line 1455 of yacc.c  */
#line 1645 "def.y"
    { 
            if (defrReader::get()->getViaExtCbk())
              CALLBACK(defrReader::get()->getViaExtCbk(), defrViaExtCbkType, History_text);
          ;}
    break;

  case 234:

/* Line 1455 of yacc.c  */
#line 1651 "def.y"
    {
              if (!viaRule) {
                if (defrReader::get()->getViaCbk()) {
                  if (viaWarnings++ < defrReader::get()->getViaWarnings()) {
                    defError (6530, "The ROWCOL statement has to be located in a VIARULE statement");
                    CHKERR();
                  }
                }
              } else if (defrReader::get()->getViaCbk())
                 defrReader::get()->getVia().defiVia::addRowCol((int)(yyvsp[(3) - (4)].dval), (int)(yyvsp[(4) - (4)].dval));
            ;}
    break;

  case 235:

/* Line 1455 of yacc.c  */
#line 1663 "def.y"
    {
              if (!viaRule) {
                if (defrReader::get()->getViaCbk()) {
                  if (viaWarnings++ < defrReader::get()->getViaWarnings()) {
                    defError (6531, "The ORIGIN statement has to  be located in a VIARULE statement");
                    CHKERR();
                  }
                }
              } else if (defrReader::get()->getViaCbk())
                 defrReader::get()->getVia().defiVia::addOrigin((int)(yyvsp[(3) - (4)].dval), (int)(yyvsp[(4) - (4)].dval));
            ;}
    break;

  case 236:

/* Line 1455 of yacc.c  */
#line 1675 "def.y"
    {
              if (!viaRule) {
                if (defrReader::get()->getViaCbk()) {
                  if (viaWarnings++ < defrReader::get()->getViaWarnings()) {
                    defError (6532, " The OFFSET statement has to be located in a VIARULE statement");
                    CHKERR();
                  }
                }
              } else if (defrReader::get()->getViaCbk())
                 defrReader::get()->getVia().defiVia::addOffset((int)(yyvsp[(3) - (6)].dval), (int)(yyvsp[(4) - (6)].dval), (int)(yyvsp[(5) - (6)].dval), (int)(yyvsp[(6) - (6)].dval));
            ;}
    break;

  case 237:

/* Line 1455 of yacc.c  */
#line 1686 "def.y"
    {dumb_mode = 1;no_num = 1; ;}
    break;

  case 238:

/* Line 1455 of yacc.c  */
#line 1687 "def.y"
    {
              if (!viaRule) {
                if (defrReader::get()->getViaCbk()) {
                  if (viaWarnings++ < defrReader::get()->getViaWarnings()) {
                    defError (6533, " The PATTERN statement has to be located in a VIARULE statement");
                    CHKERR();
                  }
                }
              } else if (defrReader::get()->getViaCbk())
                 defrReader::get()->getVia().defiVia::addCutPattern((yyvsp[(4) - (4)].string));
            ;}
    break;

  case 239:

/* Line 1455 of yacc.c  */
#line 1700 "def.y"
    { defrReader::get()->getGeomPtr()->defiGeometries::startList((yyvsp[(1) - (1)].pt).x, (yyvsp[(1) - (1)].pt).y); ;}
    break;

  case 240:

/* Line 1455 of yacc.c  */
#line 1703 "def.y"
    { defrReader::get()->getGeomPtr()->defiGeometries::addToList((yyvsp[(1) - (1)].pt).x, (yyvsp[(1) - (1)].pt).y); ;}
    break;

  case 243:

/* Line 1455 of yacc.c  */
#line 1710 "def.y"
    {
            save_x = (yyvsp[(2) - (4)].dval);
            save_y = (yyvsp[(3) - (4)].dval);
            (yyval.pt).x = ROUND((yyvsp[(2) - (4)].dval));
            (yyval.pt).y = ROUND((yyvsp[(3) - (4)].dval));
          ;}
    break;

  case 244:

/* Line 1455 of yacc.c  */
#line 1717 "def.y"
    {
            save_y = (yyvsp[(3) - (4)].dval);
            (yyval.pt).x = ROUND(save_x);
            (yyval.pt).y = ROUND((yyvsp[(3) - (4)].dval));
          ;}
    break;

  case 245:

/* Line 1455 of yacc.c  */
#line 1723 "def.y"
    {
            save_x = (yyvsp[(2) - (4)].dval);
            (yyval.pt).x = ROUND((yyvsp[(2) - (4)].dval));
            (yyval.pt).y = ROUND(save_y);
          ;}
    break;

  case 246:

/* Line 1455 of yacc.c  */
#line 1729 "def.y"
    {
            (yyval.pt).x = ROUND(save_x);
            (yyval.pt).y = ROUND(save_y);
          ;}
    break;

  case 247:

/* Line 1455 of yacc.c  */
#line 1735 "def.y"
    { 
          if (defrReader::get()->getViaEndCbk())
            CALLBACK(defrReader::get()->getViaEndCbk(), defrViaEndCbkType, 0);
        ;}
    break;

  case 248:

/* Line 1455 of yacc.c  */
#line 1741 "def.y"
    {
          if (defrReader::get()->getRegionEndCbk())
            CALLBACK(defrReader::get()->getRegionEndCbk(), defrRegionEndCbkType, 0);
        ;}
    break;

  case 249:

/* Line 1455 of yacc.c  */
#line 1747 "def.y"
    {
          if (defrReader::get()->getRegionStartCbk())
            CALLBACK(defrReader::get()->getRegionStartCbk(), defrRegionStartCbkType, ROUND((yyvsp[(2) - (3)].dval)));
        ;}
    break;

  case 251:

/* Line 1455 of yacc.c  */
#line 1754 "def.y"
    {;}
    break;

  case 252:

/* Line 1455 of yacc.c  */
#line 1756 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 253:

/* Line 1455 of yacc.c  */
#line 1757 "def.y"
    {
          if (defrReader::get()->getRegionCbk())
             defrReader::get()->getRegion().defiRegion::setup((yyvsp[(3) - (3)].string));
          regTypeDef = 0;
        ;}
    break;

  case 254:

/* Line 1455 of yacc.c  */
#line 1763 "def.y"
    { CALLBACK(defrReader::get()->getRegionCbk(), defrRegionCbkType, &defrReader::get()->getRegion()); ;}
    break;

  case 255:

/* Line 1455 of yacc.c  */
#line 1767 "def.y"
    { if (defrReader::get()->getRegionCbk())
	  defrReader::get()->getRegion().defiRegion::addRect((yyvsp[(1) - (2)].pt).x, (yyvsp[(1) - (2)].pt).y, (yyvsp[(2) - (2)].pt).x, (yyvsp[(2) - (2)].pt).y); ;}
    break;

  case 256:

/* Line 1455 of yacc.c  */
#line 1770 "def.y"
    { if (defrReader::get()->getRegionCbk())
	  defrReader::get()->getRegion().defiRegion::addRect((yyvsp[(2) - (3)].pt).x, (yyvsp[(2) - (3)].pt).y, (yyvsp[(3) - (3)].pt).x, (yyvsp[(3) - (3)].pt).y); ;}
    break;

  case 259:

/* Line 1455 of yacc.c  */
#line 1778 "def.y"
    { dumb_mode = 10000000; parsing_property = 1; ;}
    break;

  case 260:

/* Line 1455 of yacc.c  */
#line 1780 "def.y"
    { dumb_mode = 0; parsing_property = 0; ;}
    break;

  case 261:

/* Line 1455 of yacc.c  */
#line 1782 "def.y"
    {
           if (regTypeDef) {
              if (defrReader::get()->getRegionCbk()) {
                if (regionWarnings++ < defrReader::get()->getRegionWarnings()) {
                  defError(6534, "The TYPE statement has already defined in the REGION statement");
                  CHKERR();
                }
              }
           }
           if (defrReader::get()->getRegionCbk()) defrReader::get()->getRegion().defiRegion::setType((yyvsp[(3) - (3)].string));
           regTypeDef = 1;
         ;}
    break;

  case 264:

/* Line 1455 of yacc.c  */
#line 1801 "def.y"
    {
          if (defrReader::get()->getRegionCbk()) {
             char propTp;
             char* str = ringCopy("                       ");
             propTp = defrReader::get()->getRegionProp().defiPropType::propType((yyvsp[(1) - (2)].string));
             CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "REGION");
             /* For backword compatibility, also set the string value */
             /* We will use a temporary string to store the number.
              * The string space is borrowed from the ring buffer
              * in the lexer. */
             sprintf(str, "%g", (yyvsp[(2) - (2)].dval));
             defrReader::get()->getRegion().defiRegion::addNumProperty((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].dval), str, propTp);
          }
        ;}
    break;

  case 265:

/* Line 1455 of yacc.c  */
#line 1816 "def.y"
    {
          if (defrReader::get()->getRegionCbk()) {
             char propTp;
             propTp = defrReader::get()->getRegionProp().defiPropType::propType((yyvsp[(1) - (2)].string));
             CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "REGION");
             defrReader::get()->getRegion().defiRegion::addProperty((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].string), propTp);
          }
        ;}
    break;

  case 266:

/* Line 1455 of yacc.c  */
#line 1825 "def.y"
    {
          if (defrReader::get()->getRegionCbk()) {
             char propTp;
             propTp = defrReader::get()->getRegionProp().defiPropType::propType((yyvsp[(1) - (2)].string));
             CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "REGION");
             defrReader::get()->getRegion().defiRegion::addProperty((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].string), propTp);
          }
        ;}
    break;

  case 267:

/* Line 1455 of yacc.c  */
#line 1835 "def.y"
    { (yyval.string) = (char*)"FENCE"; ;}
    break;

  case 268:

/* Line 1455 of yacc.c  */
#line 1837 "def.y"
    { (yyval.string) = (char*)"GUIDE"; ;}
    break;

  case 270:

/* Line 1455 of yacc.c  */
#line 1843 "def.y"
    { 
            if (defrReader::get()->getComponentStartCbk())
              CALLBACK(defrReader::get()->getComponentStartCbk(), defrComponentStartCbkType,
                       ROUND((yyvsp[(2) - (3)].dval)));
         ;}
    break;

  case 273:

/* Line 1455 of yacc.c  */
#line 1854 "def.y"
    {
            if (defrReader::get()->getComponentCbk())
              CALLBACK(defrReader::get()->getComponentCbk(), defrComponentCbkType, &defrReader::get()->getComponent());
         ;}
    break;

  case 274:

/* Line 1455 of yacc.c  */
#line 1860 "def.y"
    {
            dumb_mode = 0;
            no_num = 0;
            /* The net connections were added to the array in
             * reverse order so fix the order now */
            /* Do not need to reverse the order since the storing is
               in sequence now  08/07/2001
            if (defrReader::get()->getComponentCbk())
              defrReader::get()->getComponent().defiComponent::reverseNetOrder();
            */
         ;}
    break;

  case 275:

/* Line 1455 of yacc.c  */
#line 1872 "def.y"
    {dumb_mode = 1000000000; no_num = 10000000; ;}
    break;

  case 276:

/* Line 1455 of yacc.c  */
#line 1874 "def.y"
    {
            if (defrReader::get()->getComponentCbk())
              defrReader::get()->getComponent().defiComponent::IdAndName((yyvsp[(3) - (4)].string), (yyvsp[(4) - (4)].string));
         ;}
    break;

  case 277:

/* Line 1455 of yacc.c  */
#line 1880 "def.y"
    { ;}
    break;

  case 278:

/* Line 1455 of yacc.c  */
#line 1882 "def.y"
    {
              if (defrReader::get()->getComponentCbk())
                defrReader::get()->getComponent().defiComponent::addNet("*");
            ;}
    break;

  case 279:

/* Line 1455 of yacc.c  */
#line 1887 "def.y"
    {
              if (defrReader::get()->getComponentCbk())
                defrReader::get()->getComponent().defiComponent::addNet((yyvsp[(2) - (2)].string));
            ;}
    break;

  case 293:

/* Line 1455 of yacc.c  */
#line 1902 "def.y"
    {
          if (defrReader::get()->getComponentCbk())
            CALLBACK(defrReader::get()->getComponentExtCbk(), defrComponentExtCbkType,
                     History_text);
        ;}
    break;

  case 294:

/* Line 1455 of yacc.c  */
#line 1908 "def.y"
    {dumb_mode=1; no_num = 1; ;}
    break;

  case 295:

/* Line 1455 of yacc.c  */
#line 1909 "def.y"
    {
          if (defrReader::get()->getComponentCbk())
            defrReader::get()->getComponent().defiComponent::setEEQ((yyvsp[(4) - (4)].string));
        ;}
    break;

  case 296:

/* Line 1455 of yacc.c  */
#line 1914 "def.y"
    { dumb_mode = 2;  no_num = 2; ;}
    break;

  case 297:

/* Line 1455 of yacc.c  */
#line 1916 "def.y"
    {
          if (defrReader::get()->getComponentCbk())
             defrReader::get()->getComponent().defiComponent::setGenerate((yyvsp[(4) - (5)].string), (yyvsp[(5) - (5)].string));
        ;}
    break;

  case 298:

/* Line 1455 of yacc.c  */
#line 1922 "def.y"
    { (yyval.string) = (char*)""; ;}
    break;

  case 299:

/* Line 1455 of yacc.c  */
#line 1924 "def.y"
    { (yyval.string) = (yyvsp[(1) - (1)].string); ;}
    break;

  case 300:

/* Line 1455 of yacc.c  */
#line 1927 "def.y"
    {
          if (defrReader::get()->getComponentCbk())
            defrReader::get()->getComponent().defiComponent::setSource((yyvsp[(3) - (3)].string));
        ;}
    break;

  case 301:

/* Line 1455 of yacc.c  */
#line 1933 "def.y"
    { (yyval.string) = (char*)"NETLIST"; ;}
    break;

  case 302:

/* Line 1455 of yacc.c  */
#line 1935 "def.y"
    { (yyval.string) = (char*)"DIST"; ;}
    break;

  case 303:

/* Line 1455 of yacc.c  */
#line 1937 "def.y"
    { (yyval.string) = (char*)"USER"; ;}
    break;

  case 304:

/* Line 1455 of yacc.c  */
#line 1939 "def.y"
    { (yyval.string) = (char*)"TIMING"; ;}
    break;

  case 305:

/* Line 1455 of yacc.c  */
#line 1944 "def.y"
    { ;}
    break;

  case 306:

/* Line 1455 of yacc.c  */
#line 1946 "def.y"
    {
	  if (defrReader::get()->getComponentCbk())
	    defrReader::get()->getComponent().defiComponent::setRegionName((yyvsp[(2) - (2)].string));
	;}
    break;

  case 307:

/* Line 1455 of yacc.c  */
#line 1952 "def.y"
    { 
          /* 11/12/2002 - this is obsolete in 5.5, & will be ignored */
          if (defVersionNum < 5.5) {
            if (defrReader::get()->getComponentCbk())
	       defrReader::get()->getComponent().defiComponent::setRegionBounds((yyvsp[(1) - (2)].pt).x, (yyvsp[(1) - (2)].pt).y, 
                                                            (yyvsp[(2) - (2)].pt).x, (yyvsp[(2) - (2)].pt).y);
          }
          else
            defWarning(7020, "REGION pt pt statement is obsolete in version 5.5 and later.\nThe DEF parser will ignore this statement.");
	;}
    break;

  case 308:

/* Line 1455 of yacc.c  */
#line 1963 "def.y"
    { 
          /* 11/12/2002 - this is obsolete in 5.5, & will be ignored */
          if (defVersionNum < 5.5) {
	    if (defrReader::get()->getComponentCbk())
	       defrReader::get()->getComponent().defiComponent::setRegionBounds((yyvsp[(2) - (3)].pt).x, (yyvsp[(2) - (3)].pt).y,
                                                            (yyvsp[(3) - (3)].pt).x, (yyvsp[(3) - (3)].pt).y);
          }
          else
            defWarning(7020, "REGION pt pt statement is obsolete in version 5.5 and later.\nThe DEF parser will ignore this statement.");
	;}
    break;

  case 309:

/* Line 1455 of yacc.c  */
#line 1975 "def.y"
    {
          if (defVersionNum < 5.6) {
             if (defrReader::get()->getComponentCbk()) {
               if (componentWarnings++ < defrReader::get()->getComponentWarnings()) {
                 defMsg = (char*)defMalloc(1000);
                 sprintf (defMsg,
                    "HALO statement is a version 5.6 and later syntax.\nYour def file is defined with version %g", defVersionNum);
                 defError(6529, defMsg);
                 defFree(defMsg);
                 CHKERR();
               }
             }
          }
        ;}
    break;

  case 310:

/* Line 1455 of yacc.c  */
#line 1990 "def.y"
    {
          if (defrReader::get()->getComponentCbk())
            defrReader::get()->getComponent().defiComponent::setHalo((int)(yyvsp[(5) - (8)].dval), (int)(yyvsp[(6) - (8)].dval),
                                                 (int)(yyvsp[(7) - (8)].dval), (int)(yyvsp[(8) - (8)].dval));
        ;}
    break;

  case 312:

/* Line 1455 of yacc.c  */
#line 1998 "def.y"
    {
        if (defVersionNum < 5.7) {
           if (defrReader::get()->getComponentCbk()) {
             if (componentWarnings++ < defrReader::get()->getComponentWarnings()) {
                defMsg = (char*)defMalloc(10000);
                sprintf (defMsg,
                  "HALO SOFT is a version 5.7 or later syntax.\nYour def file is defined with version %g.", defVersionNum);
                defError(6550, defMsg);
                defFree(defMsg);
                CHKERR();
             }
           }
        } else {
           if (defrReader::get()->getComponentCbk())
             defrReader::get()->getComponent().defiComponent::setHaloSoft();
        }
      ;}
    break;

  case 313:

/* Line 1455 of yacc.c  */
#line 2017 "def.y"
    { dumb_mode = 2; no_num = 2; ;}
    break;

  case 314:

/* Line 1455 of yacc.c  */
#line 2018 "def.y"
    {
        if (defVersionNum < 5.7) {
           if (defrReader::get()->getComponentCbk()) {
             if (componentWarnings++ < defrReader::get()->getComponentWarnings()) {
                defMsg = (char*)defMalloc(10000);
                sprintf (defMsg,
                  "ROUTEHALO is a version 5.7 or later syntax.\nYour def file is defined with version %g.", defVersionNum);
                defError(6551, defMsg);
                defFree(defMsg);
                CHKERR();
             }
           }
        } else {
           if (defrReader::get()->getComponentCbk())
             defrReader::get()->getComponent().defiComponent::setRouteHalo((yyvsp[(3) - (6)].dval), (yyvsp[(5) - (6)].string), (yyvsp[(6) - (6)].string));
        }
      ;}
    break;

  case 315:

/* Line 1455 of yacc.c  */
#line 2036 "def.y"
    { dumb_mode = 10000000; parsing_property = 1; ;}
    break;

  case 316:

/* Line 1455 of yacc.c  */
#line 2038 "def.y"
    { dumb_mode = 0; parsing_property = 0; ;}
    break;

  case 319:

/* Line 1455 of yacc.c  */
#line 2045 "def.y"
    {
          if (defrReader::get()->getComponentCbk()) {
            char propTp;
            char* str = ringCopy("                       ");
            propTp = defrReader::get()->getCompProp().defiPropType::propType((yyvsp[(1) - (2)].string));
            CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "COMPONENT");
            /* For backword compatibility, also set the string value */
            /* We will use a temporary string to store the number.
             * The string space is borrowed from the ring buffer
             * in the lexer. */
            sprintf(str, "%g", (yyvsp[(2) - (2)].dval));
            defrReader::get()->getComponent().defiComponent::addNumProperty((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].dval), str, propTp);
          }
        ;}
    break;

  case 320:

/* Line 1455 of yacc.c  */
#line 2060 "def.y"
    {
          if (defrReader::get()->getComponentCbk()) {
            char propTp;
            propTp = defrReader::get()->getCompProp().defiPropType::propType((yyvsp[(1) - (2)].string));
            CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "COMPONENT");
            defrReader::get()->getComponent().defiComponent::addProperty((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].string), propTp);
          }
        ;}
    break;

  case 321:

/* Line 1455 of yacc.c  */
#line 2069 "def.y"
    {
          if (defrReader::get()->getComponentCbk()) {
            char propTp;
            propTp = defrReader::get()->getCompProp().defiPropType::propType((yyvsp[(1) - (2)].string));
            CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "COMPONENT");
            defrReader::get()->getComponent().defiComponent::addProperty((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].string), propTp);
          }
        ;}
    break;

  case 322:

/* Line 1455 of yacc.c  */
#line 2079 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 323:

/* Line 1455 of yacc.c  */
#line 2081 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 324:

/* Line 1455 of yacc.c  */
#line 2083 "def.y"
    { 
          if (defVersionNum < 5.6) {
            if (defrReader::get()->getComponentCbk()) {
              defrReader::get()->getComponent().defiComponent::setForeignName((yyvsp[(4) - (6)].string));
              defrReader::get()->getComponent().defiComponent::setForeignLocation((yyvsp[(5) - (6)].pt).x, (yyvsp[(5) - (6)].pt).y, (yyvsp[(6) - (6)].integer));
            }
         } else
            if (defrReader::get()->getComponentCbk())
              if (componentWarnings++ < defrReader::get()->getComponentWarnings())
                defWarning(7021, "FOREIGN statement is obsolete in version 5.6 and later.\nThe DEF parser will ignore this statement.");
         ;}
    break;

  case 325:

/* Line 1455 of yacc.c  */
#line 2097 "def.y"
    { (yyval.pt) = (yyvsp[(1) - (1)].pt); ;}
    break;

  case 326:

/* Line 1455 of yacc.c  */
#line 2099 "def.y"
    { (yyval.pt).x = ROUND((yyvsp[(1) - (2)].dval)); (yyval.pt).y = ROUND((yyvsp[(2) - (2)].dval)); ;}
    break;

  case 327:

/* Line 1455 of yacc.c  */
#line 2102 "def.y"
    {
          if (defrReader::get()->getComponentCbk()) {
            defrReader::get()->getComponent().defiComponent::setPlacementStatus((yyvsp[(1) - (3)].integer));
            defrReader::get()->getComponent().defiComponent::setPlacementLocation((yyvsp[(2) - (3)].pt).x, (yyvsp[(2) - (3)].pt).y, (yyvsp[(3) - (3)].integer));
          }
        ;}
    break;

  case 328:

/* Line 1455 of yacc.c  */
#line 2109 "def.y"
    {
          if (defrReader::get()->getComponentCbk())
            defrReader::get()->getComponent().defiComponent::setPlacementStatus(
                                         DEFI_COMPONENT_UNPLACED);
            defrReader::get()->getComponent().defiComponent::setPlacementLocation(-1, -1, -1);
        ;}
    break;

  case 329:

/* Line 1455 of yacc.c  */
#line 2116 "def.y"
    {
          if (defVersionNum < 5.4) {   /* PCR 495463 */
            if (defrReader::get()->getComponentCbk()) {
              defrReader::get()->getComponent().defiComponent::setPlacementStatus(
                                          DEFI_COMPONENT_UNPLACED);
              defrReader::get()->getComponent().defiComponent::setPlacementLocation((yyvsp[(3) - (4)].pt).x, (yyvsp[(3) - (4)].pt).y, (yyvsp[(4) - (4)].integer));
            }
          } else {
            if (componentWarnings++ < defrReader::get()->getComponentWarnings())
               defWarning(7022, "In the COMPONENT UNPLACED statement, point & orient are invalid in version 5.4 and later.\nDEF parser will ignore this statement.");
            /* Should not be called.  Def parser should ignore the statement
            if (defrReader::get()->getComponentCbk())
              defrReader::get()->getComponent().defiComponent::setPlacementStatus(
                                           DEFI_COMPONENT_UNPLACED);
              defrReader::get()->getComponent().defiComponent::setPlacementLocation($3.x, $3.y, $4);
            */
          }
        ;}
    break;

  case 330:

/* Line 1455 of yacc.c  */
#line 2136 "def.y"
    { (yyval.integer) = DEFI_COMPONENT_FIXED; ;}
    break;

  case 331:

/* Line 1455 of yacc.c  */
#line 2138 "def.y"
    { (yyval.integer) = DEFI_COMPONENT_COVER; ;}
    break;

  case 332:

/* Line 1455 of yacc.c  */
#line 2140 "def.y"
    { (yyval.integer) = DEFI_COMPONENT_PLACED; ;}
    break;

  case 333:

/* Line 1455 of yacc.c  */
#line 2143 "def.y"
    {
          if (defrReader::get()->getComponentCbk())
            defrReader::get()->getComponent().defiComponent::setWeight(ROUND((yyvsp[(3) - (3)].dval)));
        ;}
    break;

  case 334:

/* Line 1455 of yacc.c  */
#line 2149 "def.y"
    { 
          if (defrReader::get()->getComponentCbk())
            CALLBACK(defrReader::get()->getComponentEndCbk(), defrComponentEndCbkType, 0);
        ;}
    break;

  case 336:

/* Line 1455 of yacc.c  */
#line 2158 "def.y"
    { 
          if (defrReader::get()->getNetStartCbk())
            CALLBACK(defrReader::get()->getNetStartCbk(), defrNetStartCbkType, ROUND((yyvsp[(2) - (3)].dval)));
          netOsnet = 1;
        ;}
    break;

  case 339:

/* Line 1455 of yacc.c  */
#line 2169 "def.y"
    { 
          if (defrReader::get()->getNetCbk())
            CALLBACK(defrReader::get()->getNetCbk(), defrNetCbkType, &defrReader::get()->getNet());
        ;}
    break;

  case 340:

/* Line 1455 of yacc.c  */
#line 2180 "def.y"
    {dumb_mode = 0; no_num = 0; ;}
    break;

  case 341:

/* Line 1455 of yacc.c  */
#line 2183 "def.y"
    {dumb_mode = 1000000000; no_num = 10000000; nondef_is_keyword = TRUE; mustjoin_is_keyword = TRUE;;}
    break;

  case 343:

/* Line 1455 of yacc.c  */
#line 2186 "def.y"
    {
          /* 9/22/1999 */
          /* this is shared by both net and special net */
          if ((defrReader::get()->getNetCbk() && (netOsnet==1)) || (defrReader::get()->getSNetCbk() && (netOsnet==2)))
            defrReader::get()->getNet().defiNet::setName((yyvsp[(1) - (1)].string));
          if (defrReader::get()->getNetNameCbk())
            CALLBACK(defrReader::get()->getNetNameCbk(), defrNetNameCbkType, (yyvsp[(1) - (1)].string));
        ;}
    break;

  case 345:

/* Line 1455 of yacc.c  */
#line 2194 "def.y"
    {dumb_mode = 1; no_num = 1;;}
    break;

  case 346:

/* Line 1455 of yacc.c  */
#line 2195 "def.y"
    {
          if ((defrReader::get()->getNetCbk() && (netOsnet==1)) || (defrReader::get()->getSNetCbk() && (netOsnet==2)))
            defrReader::get()->getNet().defiNet::addMustPin((yyvsp[(3) - (6)].string), (yyvsp[(5) - (6)].string), 0);
          dumb_mode = 3;
          no_num = 3;
        ;}
    break;

  case 349:

/* Line 1455 of yacc.c  */
#line 2206 "def.y"
    {dumb_mode = 1000000000; no_num = 10000000;;}
    break;

  case 350:

/* Line 1455 of yacc.c  */
#line 2208 "def.y"
    {
          /* 9/22/1999 */
          /* since the code is shared by both net & special net, */
          /* need to check on both flags */
          if ((defrReader::get()->getNetCbk() && (netOsnet==1)) || (defrReader::get()->getSNetCbk() && (netOsnet==2)))
            defrReader::get()->getNet().defiNet::addPin((yyvsp[(2) - (6)].string), (yyvsp[(4) - (6)].string), (yyvsp[(5) - (6)].integer));
          /* 1/14/2000 - pcr 289156 */
          /* reset dumb_mode & no_num to 3 , just in case */
          /* the next statement is another net_connection */
          dumb_mode = 3;
          no_num = 3;
        ;}
    break;

  case 351:

/* Line 1455 of yacc.c  */
#line 2220 "def.y"
    {dumb_mode = 1; no_num = 1;;}
    break;

  case 352:

/* Line 1455 of yacc.c  */
#line 2221 "def.y"
    {
          if ((defrReader::get()->getNetCbk() && (netOsnet==1)) || (defrReader::get()->getSNetCbk() && (netOsnet==2)))
            defrReader::get()->getNet().defiNet::addPin("*", (yyvsp[(4) - (6)].string), (yyvsp[(5) - (6)].integer));
          dumb_mode = 3;
          no_num = 3;
        ;}
    break;

  case 353:

/* Line 1455 of yacc.c  */
#line 2227 "def.y"
    {dumb_mode = 1; no_num = 1;;}
    break;

  case 354:

/* Line 1455 of yacc.c  */
#line 2228 "def.y"
    {
          if ((defrReader::get()->getNetCbk() && (netOsnet==1)) || (defrReader::get()->getSNetCbk() && (netOsnet==2)))
            defrReader::get()->getNet().defiNet::addPin("PIN", (yyvsp[(4) - (6)].string), (yyvsp[(5) - (6)].integer));
          dumb_mode = 3;
          no_num = 3;
        ;}
    break;

  case 355:

/* Line 1455 of yacc.c  */
#line 2236 "def.y"
    { (yyval.integer) = 0; ;}
    break;

  case 356:

/* Line 1455 of yacc.c  */
#line 2238 "def.y"
    {
          if (defrReader::get()->getNetConnectionExtCbk())
	    CALLBACK(defrReader::get()->getNetConnectionExtCbk(), defrNetConnectionExtCbkType,
              History_text);
          (yyval.integer) = 0;
        ;}
    break;

  case 357:

/* Line 1455 of yacc.c  */
#line 2245 "def.y"
    { (yyval.integer) = 1; ;}
    break;

  case 360:

/* Line 1455 of yacc.c  */
#line 2253 "def.y"
    {  
          if (defrReader::get()->getNetCbk()) defrReader::get()->getNet().defiNet::addWire((yyvsp[(2) - (2)].string), NULL);
        ;}
    break;

  case 361:

/* Line 1455 of yacc.c  */
#line 2257 "def.y"
    {
          by_is_keyword = FALSE;
          do_is_keyword = FALSE;
          new_is_keyword = FALSE;
          nondef_is_keyword = FALSE;
          mustjoin_is_keyword = FALSE;
          step_is_keyword = FALSE;
          orient_is_keyword = FALSE;
          needNPCbk = 0;
        ;}
    break;

  case 362:

/* Line 1455 of yacc.c  */
#line 2269 "def.y"
    { if (defrReader::get()->getNetCbk()) defrReader::get()->getNet().defiNet::setSource((yyvsp[(3) - (3)].string)); ;}
    break;

  case 363:

/* Line 1455 of yacc.c  */
#line 2272 "def.y"
    {
          if (defVersionNum < 5.5) {
            if (defrReader::get()->getNetCbk()) {
              if (netWarnings++ < defrReader::get()->getNetWarnings()) {
                 defMsg = (char*)defMalloc(1000);
                 sprintf (defMsg,
                    "FIXEDBUMP statement is a version 5.5 and later syntax.\nYour def file is defined with version %g", defVersionNum);
                 defError(6530, defMsg);
                 defFree(defMsg);
                 CHKERR();
              }
            }
          }
          if (defrReader::get()->getNetCbk()) defrReader::get()->getNet().defiNet::setFixedbump();
        ;}
    break;

  case 364:

/* Line 1455 of yacc.c  */
#line 2288 "def.y"
    { real_num = 1; ;}
    break;

  case 365:

/* Line 1455 of yacc.c  */
#line 2289 "def.y"
    {
          if (defVersionNum < 5.5) {
            if (defrReader::get()->getNetCbk()) {
              if (netWarnings++ < defrReader::get()->getNetWarnings()) {
                 defMsg = (char*)defMalloc(1000);
                 sprintf (defMsg,
                    "FREQUENCY statement is a version 5.5 and later syntax.\nYour def file is defined with version %g", defVersionNum);
                 defError(6530, defMsg);
                 defFree(defMsg);
                 CHKERR();
              }
            }
          }
          if (defrReader::get()->getNetCbk()) defrReader::get()->getNet().defiNet::setFrequency((yyvsp[(4) - (4)].dval));
          real_num = 0;
        ;}
    break;

  case 366:

/* Line 1455 of yacc.c  */
#line 2306 "def.y"
    {dumb_mode = 1; no_num = 1;;}
    break;

  case 367:

/* Line 1455 of yacc.c  */
#line 2307 "def.y"
    { if (defrReader::get()->getNetCbk()) defrReader::get()->getNet().defiNet::setOriginal((yyvsp[(4) - (4)].string)); ;}
    break;

  case 368:

/* Line 1455 of yacc.c  */
#line 2310 "def.y"
    { if (defrReader::get()->getNetCbk()) defrReader::get()->getNet().defiNet::setPattern((yyvsp[(3) - (3)].string)); ;}
    break;

  case 369:

/* Line 1455 of yacc.c  */
#line 2313 "def.y"
    { if (defrReader::get()->getNetCbk()) defrReader::get()->getNet().defiNet::setWeight(ROUND((yyvsp[(3) - (3)].dval))); ;}
    break;

  case 370:

/* Line 1455 of yacc.c  */
#line 2316 "def.y"
    { if (defrReader::get()->getNetCbk()) defrReader::get()->getNet().defiNet::setXTalk(ROUND((yyvsp[(3) - (3)].dval))); ;}
    break;

  case 371:

/* Line 1455 of yacc.c  */
#line 2319 "def.y"
    { if (defrReader::get()->getNetCbk()) defrReader::get()->getNet().defiNet::setCap((yyvsp[(3) - (3)].dval)); ;}
    break;

  case 372:

/* Line 1455 of yacc.c  */
#line 2322 "def.y"
    { if (defrReader::get()->getNetCbk()) defrReader::get()->getNet().defiNet::setUse((yyvsp[(3) - (3)].string)); ;}
    break;

  case 373:

/* Line 1455 of yacc.c  */
#line 2325 "def.y"
    { if (defrReader::get()->getNetCbk()) defrReader::get()->getNet().defiNet::setStyle((int)(yyvsp[(3) - (3)].dval)); ;}
    break;

  case 374:

/* Line 1455 of yacc.c  */
#line 2327 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 375:

/* Line 1455 of yacc.c  */
#line 2328 "def.y"
    { 
          if (defrReader::get()->getNetCbk() && defrReader::get()->getNetNonDefaultRuleCbk()) {
             /* User wants a callback on nondefaultrule */
             CALLBACK(defrReader::get()->getNetNonDefaultRuleCbk(),
                      defrNetNonDefaultRuleCbkType, (yyvsp[(4) - (4)].string));
          }
          /* Still save data in the class */
          if (defrReader::get()->getNetCbk()) defrReader::get()->getNet().defiNet::setNonDefaultRule((yyvsp[(4) - (4)].string));
        ;}
    break;

  case 377:

/* Line 1455 of yacc.c  */
#line 2340 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 378:

/* Line 1455 of yacc.c  */
#line 2341 "def.y"
    { if (defrReader::get()->getNetCbk()) defrReader::get()->getNet().defiNet::addShieldNet((yyvsp[(4) - (4)].string)); ;}
    break;

  case 379:

/* Line 1455 of yacc.c  */
#line 2343 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 380:

/* Line 1455 of yacc.c  */
#line 2344 "def.y"
    { /* since the parser still support 5.3 and earlier, can't */
          /* move NOSHIELD in net_type */
          if (defVersionNum < 5.4) {   /* PCR 445209 */
            if (defrReader::get()->getNetCbk()) defrReader::get()->getNet().defiNet::addNoShield("");
            by_is_keyword = FALSE;
            do_is_keyword = FALSE;
            new_is_keyword = FALSE;
            step_is_keyword = FALSE;
            orient_is_keyword = FALSE;
            shield = TRUE;    /* save the path info in the shield paths */
          } else
            if (defrReader::get()->getNetCbk()) defrReader::get()->getNet().defiNet::addWire("NOSHIELD", NULL);
        ;}
    break;

  case 381:

/* Line 1455 of yacc.c  */
#line 2358 "def.y"
    {
          if (defVersionNum < 5.4) {   /* PCR 445209 */
            shield = FALSE;
            by_is_keyword = FALSE;
            do_is_keyword = FALSE;
            new_is_keyword = FALSE;
            step_is_keyword = FALSE;
            nondef_is_keyword = FALSE;
            mustjoin_is_keyword = FALSE;
            orient_is_keyword = FALSE;
          } else {
            by_is_keyword = FALSE;
            do_is_keyword = FALSE;
            new_is_keyword = FALSE;
            step_is_keyword = FALSE;
            nondef_is_keyword = FALSE;
            mustjoin_is_keyword = FALSE;
            orient_is_keyword = FALSE;
          }
          needNPCbk = 0;
        ;}
    break;

  case 382:

/* Line 1455 of yacc.c  */
#line 2381 "def.y"
    { dumb_mode = 1; no_num = 1;
          if (defrReader::get()->getNetCbk()) {
            defrReader::get()->setSubnet((defiSubnet*)defMalloc(sizeof(defiSubnet)));
            defrReader::get()->getSubnet()->defiSubnet::Init();
          }
        ;}
    break;

  case 383:

/* Line 1455 of yacc.c  */
#line 2387 "def.y"
    {
          if (defrReader::get()->getNetCbk() && defrReader::get()->getNetSubnetNameCbk()) {
            /* User wants a callback on Net subnetName */
            CALLBACK(defrReader::get()->getNetSubnetNameCbk(), defrNetSubnetNameCbkType, (yyvsp[(4) - (4)].string));
          }
          /* Still save the subnet name in the class */
          if (defrReader::get()->getNetCbk()) {
            defrReader::get()->getSubnet()->defiSubnet::setName((yyvsp[(4) - (4)].string));
          }
        ;}
    break;

  case 384:

/* Line 1455 of yacc.c  */
#line 2397 "def.y"
    {
          routed_is_keyword = TRUE;
          fixed_is_keyword = TRUE;
          cover_is_keyword = TRUE;
        ;}
    break;

  case 385:

/* Line 1455 of yacc.c  */
#line 2401 "def.y"
    {
          if (defrReader::get()->getNetCbk()) {
            defrReader::get()->getNet().defiNet::addSubnet(defrReader::get()->getSubnet());
            defrReader::get()->setSubnet(0);
            routed_is_keyword = FALSE;
            fixed_is_keyword = FALSE;
            cover_is_keyword = FALSE;
          }
        ;}
    break;

  case 386:

/* Line 1455 of yacc.c  */
#line 2411 "def.y"
    {dumb_mode = 10000000; parsing_property = 1; ;}
    break;

  case 387:

/* Line 1455 of yacc.c  */
#line 2413 "def.y"
    { dumb_mode = 0; parsing_property = 0; ;}
    break;

  case 388:

/* Line 1455 of yacc.c  */
#line 2416 "def.y"
    { 
          if (defrReader::get()->getNetExtCbk())
            CALLBACK(defrReader::get()->getNetExtCbk(), defrNetExtCbkType, History_text);
        ;}
    break;

  case 391:

/* Line 1455 of yacc.c  */
#line 2426 "def.y"
    {
          if (defrReader::get()->getNetCbk()) {
            char propTp;
            char* str = ringCopy("                       ");
            propTp = defrReader::get()->getNetProp().defiPropType::propType((yyvsp[(1) - (2)].string));
            CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "NET");
            /* For backword compatibility, also set the string value */
            /* We will use a temporary string to store the number.
             * The string space is borrowed from the ring buffer
             * in the lexer. */
            sprintf(str, "%g", (yyvsp[(2) - (2)].dval));
            defrReader::get()->getNet().defiNet::addNumProp((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].dval), str, propTp);
          }
        ;}
    break;

  case 392:

/* Line 1455 of yacc.c  */
#line 2441 "def.y"
    {
          if (defrReader::get()->getNetCbk()) {
            char propTp;
            propTp = defrReader::get()->getNetProp().defiPropType::propType((yyvsp[(1) - (2)].string));
            CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "NET");
            defrReader::get()->getNet().defiNet::addProp((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].string), propTp);
          }
        ;}
    break;

  case 393:

/* Line 1455 of yacc.c  */
#line 2450 "def.y"
    {
          if (defrReader::get()->getNetCbk()) {
            char propTp;
            propTp = defrReader::get()->getNetProp().defiPropType::propType((yyvsp[(1) - (2)].string));
            CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "NET");
            defrReader::get()->getNet().defiNet::addProp((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].string), propTp);
          }
        ;}
    break;

  case 394:

/* Line 1455 of yacc.c  */
#line 2460 "def.y"
    { (yyval.string) = (char*)"NETLIST"; ;}
    break;

  case 395:

/* Line 1455 of yacc.c  */
#line 2462 "def.y"
    { (yyval.string) = (char*)"DIST"; ;}
    break;

  case 396:

/* Line 1455 of yacc.c  */
#line 2464 "def.y"
    { (yyval.string) = (char*)"USER"; ;}
    break;

  case 397:

/* Line 1455 of yacc.c  */
#line 2466 "def.y"
    { (yyval.string) = (char*)"TIMING"; ;}
    break;

  case 398:

/* Line 1455 of yacc.c  */
#line 2468 "def.y"
    { (yyval.string) = (char*)"TEST"; ;}
    break;

  case 399:

/* Line 1455 of yacc.c  */
#line 2471 "def.y"
    {
          /* vpin_options may have to deal with orient */
          orient_is_keyword = TRUE;
        ;}
    break;

  case 400:

/* Line 1455 of yacc.c  */
#line 2476 "def.y"
    { if (defrReader::get()->getNetCbk())
            defrReader::get()->getNet().defiNet::addVpinBounds((yyvsp[(3) - (6)].pt).x, (yyvsp[(3) - (6)].pt).y, (yyvsp[(4) - (6)].pt).x, (yyvsp[(4) - (6)].pt).y);
          orient_is_keyword = FALSE;
        ;}
    break;

  case 401:

/* Line 1455 of yacc.c  */
#line 2481 "def.y"
    {dumb_mode = 1; no_num = 1;;}
    break;

  case 402:

/* Line 1455 of yacc.c  */
#line 2482 "def.y"
    { if (defrReader::get()->getNetCbk()) defrReader::get()->getNet().defiNet::addVpin((yyvsp[(4) - (4)].string)); ;}
    break;

  case 404:

/* Line 1455 of yacc.c  */
#line 2485 "def.y"
    {dumb_mode=1;;}
    break;

  case 405:

/* Line 1455 of yacc.c  */
#line 2486 "def.y"
    { if (defrReader::get()->getNetCbk()) defrReader::get()->getNet().defiNet::addVpinLayer((yyvsp[(3) - (3)].string)); ;}
    break;

  case 407:

/* Line 1455 of yacc.c  */
#line 2490 "def.y"
    { if (defrReader::get()->getNetCbk()) defrReader::get()->getNet().defiNet::addVpinLoc((yyvsp[(1) - (3)].string), (yyvsp[(2) - (3)].pt).x, (yyvsp[(2) - (3)].pt).y, (yyvsp[(3) - (3)].integer)); ;}
    break;

  case 408:

/* Line 1455 of yacc.c  */
#line 2493 "def.y"
    { (yyval.string) = (char*)"PLACED"; ;}
    break;

  case 409:

/* Line 1455 of yacc.c  */
#line 2495 "def.y"
    { (yyval.string) = (char*)"FIXED"; ;}
    break;

  case 410:

/* Line 1455 of yacc.c  */
#line 2497 "def.y"
    { (yyval.string) = (char*)"COVER"; ;}
    break;

  case 411:

/* Line 1455 of yacc.c  */
#line 2500 "def.y"
    { (yyval.string) = (char*)"FIXED"; dumb_mode = 1; ;}
    break;

  case 412:

/* Line 1455 of yacc.c  */
#line 2502 "def.y"
    { (yyval.string) = (char*)"COVER"; dumb_mode = 1; ;}
    break;

  case 413:

/* Line 1455 of yacc.c  */
#line 2504 "def.y"
    { (yyval.string) = (char*)"ROUTED"; dumb_mode = 1; ;}
    break;

  case 414:

/* Line 1455 of yacc.c  */
#line 2508 "def.y"
    { if (defrReader::get()->needPathData() && defrReader::get()->getNetCbk())
          defrReader::get()->pathIsDone(shield, 0, netOsnet, &needNPCbk);
      ;}
    break;

  case 415:

/* Line 1455 of yacc.c  */
#line 2512 "def.y"
    { ;}
    break;

  case 416:

/* Line 1455 of yacc.c  */
#line 2514 "def.y"
    { dumb_mode = 1; ;}
    break;

  case 417:

/* Line 1455 of yacc.c  */
#line 2515 "def.y"
    { if (defrReader::get()->needPathData() && defrReader::get()->getNetCbk())
          defrReader::get()->pathIsDone(shield, 0, netOsnet, &needNPCbk);
      ;}
    break;

  case 418:

/* Line 1455 of yacc.c  */
#line 2520 "def.y"
    {
        /* 9/26/2002 - pcr 449514
        Check if $1 is equal to TAPER or TAPERRULE, layername
        is missing */
        if ((strcmp((yyvsp[(1) - (1)].string), "TAPER") == 0) || (strcmp((yyvsp[(1) - (1)].string), "TAPERRULE") == 0)) {
          if (defrReader::get()->needPathData()) {
            if (netWarnings++ < defrReader::get()->getNetWarnings()) {
              defError(6531, "layerName which is required in path is missing");
              CHKERR();
            }
          }
          /* Since there is already error, the next token is insignificant */
          dumb_mode = 1; no_num = 1;
        } else {
          if (defrReader::get()->needPathData())
              defrReader::get()->getPath()->defiPath::addLayer((yyvsp[(1) - (1)].string));
          dumb_mode = 0; no_num = 0;
        }
      ;}
    break;

  case 419:

/* Line 1455 of yacc.c  */
#line 2540 "def.y"
    { dumb_mode = 1000000000; by_is_keyword = TRUE; do_is_keyword = TRUE;
/*
       dumb_mode = 1; by_is_keyword = TRUE; do_is_keyword = TRUE;
*/
        new_is_keyword = TRUE; step_is_keyword = TRUE;
        orient_is_keyword = TRUE; ;}
    break;

  case 420:

/* Line 1455 of yacc.c  */
#line 2547 "def.y"
    { dumb_mode = 0; ;}
    break;

  case 423:

/* Line 1455 of yacc.c  */
#line 2555 "def.y"
    {
        if (defrReader::get()->needPathData() && ((defrReader::get()->getNetCbk() && (netOsnet==1)) ||
            (defrReader::get()->getSNetCbk() && (netOsnet==2)))) {
          if (strcmp((yyvsp[(1) - (1)].string), "TAPER") == 0)
            defrReader::get()->getPath()->defiPath::setTaper();
          else 
            defrReader::get()->getPath()->defiPath::addVia((yyvsp[(1) - (1)].string));
        }
      ;}
    break;

  case 424:

/* Line 1455 of yacc.c  */
#line 2565 "def.y"
    { if (defrReader::get()->needPathData() && ((defrReader::get()->getNetCbk() && (netOsnet==1)) ||
            (defrReader::get()->getSNetCbk() && (netOsnet==2)))) {
            defrReader::get()->getPath()->defiPath::addVia((yyvsp[(1) - (2)].string));
            defrReader::get()->getPath()->defiPath::addViaRotation((yyvsp[(2) - (2)].integer));
        }
      ;}
    break;

  case 425:

/* Line 1455 of yacc.c  */
#line 2572 "def.y"
    {
        if (defVersionNum < 5.5) {
          if (defrReader::get()->needPathData()) {
            if (netWarnings++ < defrReader::get()->getNetWarnings()) {
              defMsg = (char*)defMalloc(1000);
              sprintf (defMsg,
                 "VIA DO statement is a version 5.5 and later syntax.\nYour def file is defined with version %g", defVersionNum);
              defError(6532, defMsg);
              defFree(defMsg);
              CHKERR();
            }
          }
        }
        if (((yyvsp[(3) - (8)].dval) == 0) || ((yyvsp[(5) - (8)].dval) == 0)) {
          if (defrReader::get()->needPathData()) {
            if (netWarnings++ < defrReader::get()->getNetWarnings()) {
              defError(6533, "Either the numX or numY in the VIA DO statement has the value 0.\nThis is illegal.");
              CHKERR();
            }
          }
        }
        if (defrReader::get()->needPathData() && ((defrReader::get()->getNetCbk() && (netOsnet==1)) ||
            (defrReader::get()->getSNetCbk() && (netOsnet==2)))) {
            defrReader::get()->getPath()->defiPath::addVia((yyvsp[(1) - (8)].string));
            defrReader::get()->getPath()->defiPath::addViaData((int)(yyvsp[(3) - (8)].dval), (int)(yyvsp[(5) - (8)].dval), (int)(yyvsp[(7) - (8)].dval), (int)(yyvsp[(8) - (8)].dval));
        }
      ;}
    break;

  case 426:

/* Line 1455 of yacc.c  */
#line 2600 "def.y"
    { // reset dumb_mode to 1 just incase the next token is a via of the path
        // 2/5/2004 - pcr 686781
        dumb_mode = 100000; by_is_keyword = TRUE; do_is_keyword = TRUE;
        new_is_keyword = TRUE; step_is_keyword = TRUE;
        orient_is_keyword = TRUE; ;}
    break;

  case 427:

/* Line 1455 of yacc.c  */
#line 2609 "def.y"
    {
	if (defrReader::get()->needPathData() && ((defrReader::get()->getNetCbk() && (netOsnet==1)) ||
          (defrReader::get()->getSNetCbk() && (netOsnet==2))))
	  defrReader::get()->getPath()->defiPath::addPoint(ROUND((yyvsp[(2) - (4)].dval)), ROUND((yyvsp[(3) - (4)].dval))); 
	save_x = (yyvsp[(2) - (4)].dval);
	save_y = (yyvsp[(3) - (4)].dval);
      ;}
    break;

  case 428:

/* Line 1455 of yacc.c  */
#line 2617 "def.y"
    {
	if (defrReader::get()->needPathData() && ((defrReader::get()->getNetCbk() && (netOsnet==1)) ||
          (defrReader::get()->getSNetCbk() && (netOsnet==2))))
	  defrReader::get()->getPath()->defiPath::addPoint(ROUND(save_x), ROUND((yyvsp[(3) - (4)].dval))); 
	save_y = (yyvsp[(3) - (4)].dval);
      ;}
    break;

  case 429:

/* Line 1455 of yacc.c  */
#line 2624 "def.y"
    {
	if (defrReader::get()->needPathData() && ((defrReader::get()->getNetCbk() && (netOsnet==1)) ||
          (defrReader::get()->getSNetCbk() && (netOsnet==2))))
	  defrReader::get()->getPath()->defiPath::addPoint(ROUND((yyvsp[(2) - (4)].dval)), ROUND(save_y)); 
	save_x = (yyvsp[(2) - (4)].dval);
      ;}
    break;

  case 430:

/* Line 1455 of yacc.c  */
#line 2631 "def.y"
    {
	if (defrReader::get()->needPathData() && ((defrReader::get()->getNetCbk() && (netOsnet==1)) ||
            (defrReader::get()->getSNetCbk() && (netOsnet==2))))
	  defrReader::get()->getPath()->defiPath::addPoint(ROUND(save_x), ROUND(save_y)); 
      ;}
    break;

  case 431:

/* Line 1455 of yacc.c  */
#line 2637 "def.y"
    {
	if (defrReader::get()->needPathData() && ((defrReader::get()->getNetCbk() && (netOsnet==1)) ||
            (defrReader::get()->getSNetCbk() && (netOsnet==2))))
	  defrReader::get()->getPath()->defiPath::addFlushPoint(ROUND((yyvsp[(2) - (5)].dval)), ROUND((yyvsp[(3) - (5)].dval)), ROUND((yyvsp[(4) - (5)].dval))); 
	save_x = (yyvsp[(2) - (5)].dval);
	save_y = (yyvsp[(3) - (5)].dval);
      ;}
    break;

  case 432:

/* Line 1455 of yacc.c  */
#line 2645 "def.y"
    {
	if (defrReader::get()->needPathData() && ((defrReader::get()->getNetCbk() && (netOsnet==1)) ||
          (defrReader::get()->getSNetCbk() && (netOsnet==2))))
	  defrReader::get()->getPath()->defiPath::addFlushPoint(ROUND(save_x), ROUND((yyvsp[(3) - (5)].dval)),
	  ROUND((yyvsp[(4) - (5)].dval))); 
	save_y = (yyvsp[(3) - (5)].dval);
      ;}
    break;

  case 433:

/* Line 1455 of yacc.c  */
#line 2653 "def.y"
    {
	if (defrReader::get()->needPathData() && ((defrReader::get()->getNetCbk() && (netOsnet==1)) ||
          (defrReader::get()->getSNetCbk() && (netOsnet==2))))
	  defrReader::get()->getPath()->defiPath::addFlushPoint(ROUND((yyvsp[(2) - (5)].dval)), ROUND(save_y),
	  ROUND((yyvsp[(4) - (5)].dval))); 
	save_x = (yyvsp[(2) - (5)].dval);
      ;}
    break;

  case 434:

/* Line 1455 of yacc.c  */
#line 2661 "def.y"
    {
	if (defrReader::get()->needPathData() && ((defrReader::get()->getNetCbk() && (netOsnet==1)) ||
          (defrReader::get()->getSNetCbk() && (netOsnet==2))))
	  defrReader::get()->getPath()->defiPath::addFlushPoint(ROUND(save_x), ROUND(save_y),
	  ROUND((yyvsp[(4) - (5)].dval))); 
      ;}
    break;

  case 439:

/* Line 1455 of yacc.c  */
#line 2676 "def.y"
    { if (defrReader::get()->needPathData() && ((defrReader::get()->getNetCbk() && (netOsnet==1)) ||
          (defrReader::get()->getSNetCbk() && (netOsnet==2))))
          defrReader::get()->getPath()->defiPath::setTaper(); ;}
    break;

  case 440:

/* Line 1455 of yacc.c  */
#line 2679 "def.y"
    { dumb_mode = 1; ;}
    break;

  case 441:

/* Line 1455 of yacc.c  */
#line 2680 "def.y"
    { if (defrReader::get()->needPathData() && ((defrReader::get()->getNetCbk() && (netOsnet==1)) ||
          (defrReader::get()->getSNetCbk() && (netOsnet==2))))
          defrReader::get()->getPath()->defiPath::addTaperRule((yyvsp[(3) - (3)].string)); ;}
    break;

  case 442:

/* Line 1455 of yacc.c  */
#line 2685 "def.y"
    { 
        if (defVersionNum < 5.6) {
           if (defrReader::get()->needPathData()) {
             if (netWarnings++ < defrReader::get()->getNetWarnings()) {
               defMsg = (char*)defMalloc(1000);
               sprintf (defMsg,
                  "STYLE statement is a version 5.6 and later syntax.\nYour def file is defined with version %g", defVersionNum);
               defError(6534, defMsg);
               defFree(defMsg);
               CHKERR();
             }
           }
        } else
           if (defrReader::get()->needPathData() && ((defrReader::get()->getNetCbk() && (netOsnet==1)) ||
             (defrReader::get()->getSNetCbk() && (netOsnet==2))))
             defrReader::get()->getPath()->defiPath::addStyle((int)(yyvsp[(2) - (2)].dval));
      ;}
    break;

  case 445:

/* Line 1455 of yacc.c  */
#line 2709 "def.y"
    { if (defrReader::get()->needPathData() && ((defrReader::get()->getNetCbk() && (netOsnet==1)) ||
          (defrReader::get()->getSNetCbk() && (netOsnet==2))))
          defrReader::get()->getPath()->defiPath::addShape((yyvsp[(3) - (3)].string)); ;}
    break;

  case 446:

/* Line 1455 of yacc.c  */
#line 2713 "def.y"
    { if (defVersionNum < 5.6) {
          if (defrReader::get()->needPathData() && ((defrReader::get()->getNetCbk() && (netOsnet==1)) ||
            (defrReader::get()->getSNetCbk() && (netOsnet==2)))) {
            if (netWarnings++ < defrReader::get()->getNetWarnings()) {
              defMsg = (char*)defMalloc(1000);
              sprintf (defMsg,
                 "STYLE statement is a version 5.6 and later syntax.\nYour def file is defined with version %g", defVersionNum);
              defError(6534, defMsg);
              defFree(defMsg);
              CHKERR();
            }
          }
        } else {
          if (defrReader::get()->needPathData() && ((defrReader::get()->getNetCbk() && (netOsnet==1)) ||
            (defrReader::get()->getSNetCbk() && (netOsnet==2))))
            defrReader::get()->getPath()->defiPath::addStyle((int)(yyvsp[(3) - (3)].dval));
        }
      ;}
    break;

  case 447:

/* Line 1455 of yacc.c  */
#line 2733 "def.y"
    { 
            CALLBACK(defrReader::get()->getNetEndCbk(), defrNetEndCbkType, 0);
            netOsnet = 0;
          ;}
    break;

  case 448:

/* Line 1455 of yacc.c  */
#line 2739 "def.y"
    { (yyval.string) = (char*)"RING"; ;}
    break;

  case 449:

/* Line 1455 of yacc.c  */
#line 2741 "def.y"
    { (yyval.string) = (char*)"STRIPE"; ;}
    break;

  case 450:

/* Line 1455 of yacc.c  */
#line 2743 "def.y"
    { (yyval.string) = (char*)"FOLLOWPIN"; ;}
    break;

  case 451:

/* Line 1455 of yacc.c  */
#line 2745 "def.y"
    { (yyval.string) = (char*)"IOWIRE"; ;}
    break;

  case 452:

/* Line 1455 of yacc.c  */
#line 2747 "def.y"
    { (yyval.string) = (char*)"COREWIRE"; ;}
    break;

  case 453:

/* Line 1455 of yacc.c  */
#line 2749 "def.y"
    { (yyval.string) = (char*)"BLOCKWIRE"; ;}
    break;

  case 454:

/* Line 1455 of yacc.c  */
#line 2751 "def.y"
    { (yyval.string) = (char*)"FILLWIRE"; ;}
    break;

  case 455:

/* Line 1455 of yacc.c  */
#line 2753 "def.y"
    {
              if (defVersionNum < 5.7) {
                 if (defrReader::get()->needPathData()) {
                   if (fillWarnings++ < defrReader::get()->getFillWarnings()) {
                     defMsg = (char*)defMalloc(10000);
                     sprintf (defMsg,
                       "FILLWIREOPC is a version 5.7 or later syntax.\nYour def file is defined with version %g.", defVersionNum);
                     defError(6552, defMsg);
                     defFree(defMsg);
                     CHKERR();
                  }
                }
              }
              (yyval.string) = (char*)"FILLWIREOPC";
            ;}
    break;

  case 456:

/* Line 1455 of yacc.c  */
#line 2769 "def.y"
    { (yyval.string) = (char*)"DRCFILL"; ;}
    break;

  case 457:

/* Line 1455 of yacc.c  */
#line 2771 "def.y"
    { (yyval.string) = (char*)"BLOCKAGEWIRE"; ;}
    break;

  case 458:

/* Line 1455 of yacc.c  */
#line 2773 "def.y"
    { (yyval.string) = (char*)"PADRING"; ;}
    break;

  case 459:

/* Line 1455 of yacc.c  */
#line 2775 "def.y"
    { (yyval.string) = (char*)"BLOCKRING"; ;}
    break;

  case 463:

/* Line 1455 of yacc.c  */
#line 2785 "def.y"
    { CALLBACK(defrReader::get()->getSNetCbk(), defrSNetCbkType, &defrReader::get()->getNet()); ;}
    break;

  case 470:

/* Line 1455 of yacc.c  */
#line 2796 "def.y"
    {
             if (defrReader::get()->getSNetCbk()) {   /* PCR 902306 */
               defMsg = (char*)defMalloc(1024);
               sprintf(defMsg, "The SPECIAL NET statement, with type %s, does not have any net statement defined.\nThe DEF parser will ignore this statemnet.", (yyvsp[(2) - (2)].string));
               defWarning(7023, defMsg);
               defFree(defMsg);
             }
            ;}
    break;

  case 471:

/* Line 1455 of yacc.c  */
#line 2805 "def.y"
    {
            if (defrReader::get()->getSNetCbk()) defrReader::get()->getNet().defiNet::addWire((yyvsp[(2) - (2)].string), NULL);
            ;}
    break;

  case 472:

/* Line 1455 of yacc.c  */
#line 2809 "def.y"
    {
            // 7/17/2003 - Fix for pcr 604848, add a callback for each wire
            if (defrReader::get()->getSNetWireCbk()) {
               CALLBACK(defrReader::get()->getSNetWireCbk(), defrSNetWireCbkType, &defrReader::get()->getNet());
               defrReader::get()->getNet().defiNet::freeWire();
            }
            by_is_keyword = FALSE;
            do_is_keyword = FALSE;
            new_is_keyword = FALSE;
            step_is_keyword = FALSE;
            orient_is_keyword = FALSE;
            needSNPCbk = 0;
            ;}
    break;

  case 473:

/* Line 1455 of yacc.c  */
#line 2823 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 474:

/* Line 1455 of yacc.c  */
#line 2824 "def.y"
    { shieldName = (yyvsp[(4) - (4)].string); ;}
    break;

  case 476:

/* Line 1455 of yacc.c  */
#line 2827 "def.y"
    { dumb_mode = 1; ;}
    break;

  case 477:

/* Line 1455 of yacc.c  */
#line 2828 "def.y"
    {
            if (defVersionNum < 5.6) {
              if (defrReader::get()->getSNetCbk()) {
                if (sNetWarnings++ < defrReader::get()->getSNetWarnings()) {
                  defMsg = (char*)defMalloc(1000);
                  sprintf (defMsg,
                     "POLYGON statement is a version 5.6 and later syntax.\nYour def file is defined with version %g", defVersionNum);
                  defError(6535, defMsg);
                  defFree(defMsg);
                  CHKERR();
                }
              }
            }
            if (!defrReader::get()->getGeomPtr()) {
              defrReader::get()->setGeomPtr((defiGeometries*)defMalloc(sizeof(defiGeometries)));
              defrReader::get()->getGeomPtr()->defiGeometries::Init();
            } else {  // Just reset the number of points
              defrReader::get()->getGeomPtr()->defiGeometries::Reset();
            }
          ;}
    break;

  case 478:

/* Line 1455 of yacc.c  */
#line 2849 "def.y"
    {
            if (defVersionNum >= 5.6) {  // only add if 5.6 or beyond
              if (defrReader::get()->getSNetCbk()) {
                // needSNPCbk will indicate that it has reach the max
                // memory and if user has set partialPathCBk, def parser
                // will make the callback.
                // This will improve performance
                // This construct is only in specialnet
                defrReader::get()->getNet().defiNet::addPolygon((yyvsp[(4) - (9)].string), defrReader::get()->getGeomPtr(), &needSNPCbk);
                if (needSNPCbk && defrReader::get()->getSNetPartialPathCbk()) {
                   CALLBACK(defrReader::get()->getSNetPartialPathCbk(), defrSNetPartialPathCbkType,
                            &defrReader::get()->getNet());
                   defrReader::get()->getNet().defiNet::clearRectPolyNPath();
                }
              }
            }
          ;}
    break;

  case 479:

/* Line 1455 of yacc.c  */
#line 2867 "def.y"
    { dumb_mode = 1; ;}
    break;

  case 480:

/* Line 1455 of yacc.c  */
#line 2868 "def.y"
    {
            if (defVersionNum < 5.6) {
              if (defrReader::get()->getSNetCbk()) {
                if (sNetWarnings++ < defrReader::get()->getSNetWarnings()) {
                  defMsg = (char*)defMalloc(1000);
                  sprintf (defMsg,
                     "RECT statement is a version 5.6 and later syntax.\nYour def file is defined with version %g", defVersionNum);
                  defError(6536, defMsg);
                  defFree(defMsg);
                  CHKERR();
                }
              }
            }
            if (defrReader::get()->getSNetCbk()) {
              // needSNPCbk will indicate that it has reach the max
              // memory and if user has set partialPathCBk, def parser
              // will make the callback.
              // This will improve performance
              // This construct is only in specialnet
              defrReader::get()->getNet().defiNet::addRect((yyvsp[(4) - (6)].string), (yyvsp[(5) - (6)].pt).x, (yyvsp[(5) - (6)].pt).y, (yyvsp[(6) - (6)].pt).x, (yyvsp[(6) - (6)].pt).y, &needSNPCbk);
              if (needSNPCbk && defrReader::get()->getSNetPartialPathCbk()) {
                 CALLBACK(defrReader::get()->getSNetPartialPathCbk(), defrSNetPartialPathCbkType,
                          &defrReader::get()->getNet());
                 defrReader::get()->getNet().defiNet::clearRectPolyNPath();
              }
            }
          ;}
    break;

  case 481:

/* Line 1455 of yacc.c  */
#line 2897 "def.y"
    { if (defrReader::get()->getSNetCbk()) defrReader::get()->getNet().defiNet::setSource((yyvsp[(3) - (3)].string)); ;}
    break;

  case 482:

/* Line 1455 of yacc.c  */
#line 2900 "def.y"
    { if (defrReader::get()->getSNetCbk()) defrReader::get()->getNet().defiNet::setFixedbump(); ;}
    break;

  case 483:

/* Line 1455 of yacc.c  */
#line 2903 "def.y"
    { if (defrReader::get()->getSNetCbk()) defrReader::get()->getNet().defiNet::setFrequency((yyvsp[(3) - (3)].dval)); ;}
    break;

  case 484:

/* Line 1455 of yacc.c  */
#line 2905 "def.y"
    {dumb_mode = 1; no_num = 1;;}
    break;

  case 485:

/* Line 1455 of yacc.c  */
#line 2906 "def.y"
    { if (defrReader::get()->getSNetCbk()) defrReader::get()->getNet().defiNet::setOriginal((yyvsp[(4) - (4)].string)); ;}
    break;

  case 486:

/* Line 1455 of yacc.c  */
#line 2909 "def.y"
    { if (defrReader::get()->getSNetCbk()) defrReader::get()->getNet().defiNet::setPattern((yyvsp[(3) - (3)].string)); ;}
    break;

  case 487:

/* Line 1455 of yacc.c  */
#line 2912 "def.y"
    { if (defrReader::get()->getSNetCbk()) defrReader::get()->getNet().defiNet::setWeight(ROUND((yyvsp[(3) - (3)].dval))); ;}
    break;

  case 488:

/* Line 1455 of yacc.c  */
#line 2915 "def.y"
    { 
              /* 11/12/2002 - this is obsolete in 5.5, & will be ignored */
              if (defVersionNum < 5.5)
                 if (defrReader::get()->getSNetCbk()) defrReader::get()->getNet().defiNet::setCap((yyvsp[(3) - (3)].dval));
              else
                 defWarning(7024, "ESTCAP statement is obsolete in version 5.5 and later.\nThe DEF parser will ignore this statement.");
            ;}
    break;

  case 489:

/* Line 1455 of yacc.c  */
#line 2924 "def.y"
    { if (defrReader::get()->getSNetCbk()) defrReader::get()->getNet().defiNet::setUse((yyvsp[(3) - (3)].string)); ;}
    break;

  case 490:

/* Line 1455 of yacc.c  */
#line 2927 "def.y"
    { if (defrReader::get()->getSNetCbk()) defrReader::get()->getNet().defiNet::setStyle((int)(yyvsp[(3) - (3)].dval)); ;}
    break;

  case 491:

/* Line 1455 of yacc.c  */
#line 2929 "def.y"
    {dumb_mode = 10000000; parsing_property = 1; ;}
    break;

  case 492:

/* Line 1455 of yacc.c  */
#line 2931 "def.y"
    { dumb_mode = 0; parsing_property = 0; ;}
    break;

  case 493:

/* Line 1455 of yacc.c  */
#line 2934 "def.y"
    { CALLBACK(defrReader::get()->getNetExtCbk(), defrNetExtCbkType, History_text); ;}
    break;

  case 494:

/* Line 1455 of yacc.c  */
#line 2937 "def.y"
    {
             if (defrReader::get()->getSNetCbk()) {
               defMsg = (char*)defMalloc(1024);
               sprintf(defMsg, "The SPECIAL NET SHIELD statement, does not have shield net statement defined.\nThe DEF parser will ignore this statemnet.");
               defWarning(7025, defMsg);
               defFree(defMsg);
             }
            ;}
    break;

  case 495:

/* Line 1455 of yacc.c  */
#line 2946 "def.y"
    { /* since the parser still supports 5.3 and earlier, */
              /* can't just move SHIELD in net_type */
              if (defVersionNum < 5.4) { /* PCR 445209 */
                if (defrReader::get()->getSNetCbk()) defrReader::get()->getNet().defiNet::addShield(shieldName);
                by_is_keyword = FALSE;
                do_is_keyword = FALSE;
                new_is_keyword = FALSE;
                step_is_keyword = FALSE;
                orient_is_keyword = FALSE;
                shield = TRUE;   /* save the path info in the shield paths */
              } else
                if (defrReader::get()->getSNetCbk()) defrReader::get()->getNet().defiNet::addWire("SHIELD", shieldName);
            ;}
    break;

  case 496:

/* Line 1455 of yacc.c  */
#line 2960 "def.y"
    {
              // 7/17/2003 - Fix for pcr 604848, add a callback for each wire
              if (defrReader::get()->getSNetWireCbk()) {
                 CALLBACK(defrReader::get()->getSNetWireCbk(), defrSNetWireCbkType, &defrReader::get()->getNet());
                 if (defVersionNum < 5.4)
                   defrReader::get()->getNet().defiNet::freeShield();
                 else
                   defrReader::get()->getNet().defiNet::freeWire();
              }
              if (defVersionNum < 5.4) {  /* PCR 445209 */
                shield = FALSE;
                by_is_keyword = FALSE;
                do_is_keyword = FALSE;
                new_is_keyword = FALSE;
                step_is_keyword = FALSE;
                nondef_is_keyword = FALSE;
                mustjoin_is_keyword = FALSE;
                orient_is_keyword = FALSE;
              } else {
                by_is_keyword = FALSE;
                do_is_keyword = FALSE;
                new_is_keyword = FALSE;
                step_is_keyword = FALSE;
                orient_is_keyword = FALSE;
              }
              needSNPCbk = 0;
            ;}
    break;

  case 497:

/* Line 1455 of yacc.c  */
#line 2988 "def.y"
    { dumb_mode = 1; ;}
    break;

  case 498:

/* Line 1455 of yacc.c  */
#line 2989 "def.y"
    {
              /* 11/12/2002 - this is obsolete in 5.5, & will be ignored */
              if (defVersionNum < 5.5)
                 if (defrReader::get()->getSNetCbk()) defrReader::get()->getNet().defiNet::setWidth((yyvsp[(4) - (5)].string), (yyvsp[(5) - (5)].dval));
              else
                 defWarning(7026, "WIDTH statement is obsolete in version 5.5 and later.\nThe DEF parser will ignore this statement.");
            ;}
    break;

  case 499:

/* Line 1455 of yacc.c  */
#line 2997 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 500:

/* Line 1455 of yacc.c  */
#line 2998 "def.y"
    {
              if (numIsInt((yyvsp[(4) - (4)].string))) {
                 if (defrReader::get()->getSNetCbk()) defrReader::get()->getNet().defiNet::setVoltage(atoi((yyvsp[(4) - (4)].string)));
              } else {
                 if (defrReader::get()->getSNetCbk()) {
                   if (sNetWarnings++ < defrReader::get()->getSNetWarnings()) {
                     defMsg = (char*)defMalloc(1000);
                     sprintf (defMsg,
                        "The value %s for statement VOLTAGE is not an integer.\nThe syntax requires an integer in units of millivolts", (yyvsp[(4) - (4)].string));
                     defError(6537, defMsg);
                     defFree(defMsg);
                     CHKERR();
                   }
                 }
              }
            ;}
    break;

  case 501:

/* Line 1455 of yacc.c  */
#line 3015 "def.y"
    { dumb_mode = 1; ;}
    break;

  case 502:

/* Line 1455 of yacc.c  */
#line 3016 "def.y"
    {
              if (defrReader::get()->getSNetCbk()) defrReader::get()->getNet().defiNet::setSpacing((yyvsp[(4) - (5)].string),(yyvsp[(5) - (5)].dval));
            ;}
    break;

  case 503:

/* Line 1455 of yacc.c  */
#line 3020 "def.y"
    {
            ;}
    break;

  case 506:

/* Line 1455 of yacc.c  */
#line 3028 "def.y"
    {
              if (defrReader::get()->getSNetCbk()) {
                char propTp;
                char* str = ringCopy("                       ");
                propTp = defrReader::get()->getSNetProp().defiPropType::propType((yyvsp[(1) - (2)].string));
                CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "SPECIAL NET");
                /* For backword compatibility, also set the string value */
                /* We will use a temporary string to store the number.
                 * The string space is borrowed from the ring buffer
                 * in the lexer. */
                sprintf(str, "%g", (yyvsp[(2) - (2)].dval));
                defrReader::get()->getNet().defiNet::addNumProp((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].dval), str, propTp);
              }
            ;}
    break;

  case 507:

/* Line 1455 of yacc.c  */
#line 3043 "def.y"
    {
              if (defrReader::get()->getSNetCbk()) {
                char propTp;
                propTp = defrReader::get()->getSNetProp().defiPropType::propType((yyvsp[(1) - (2)].string));
                CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "SPECIAL NET");
                defrReader::get()->getNet().defiNet::addProp((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].string), propTp);
              }
            ;}
    break;

  case 508:

/* Line 1455 of yacc.c  */
#line 3052 "def.y"
    {
              if (defrReader::get()->getSNetCbk()) {
                char propTp;
                propTp = defrReader::get()->getSNetProp().defiPropType::propType((yyvsp[(1) - (2)].string));
                CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "SPECIAL NET");
                defrReader::get()->getNet().defiNet::addProp((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].string), propTp);
              }
            ;}
    break;

  case 510:

/* Line 1455 of yacc.c  */
#line 3063 "def.y"
    {
              if (defrReader::get()->getSNetCbk()) defrReader::get()->getNet().defiNet::setRange((yyvsp[(2) - (3)].dval),(yyvsp[(3) - (3)].dval));
            ;}
    break;

  case 512:

/* Line 1455 of yacc.c  */
#line 3069 "def.y"
    { defrReader::get()->getProp().defiProp::setRange((yyvsp[(2) - (3)].dval), (yyvsp[(3) - (3)].dval)); ;}
    break;

  case 513:

/* Line 1455 of yacc.c  */
#line 3072 "def.y"
    { (yyval.string) = (char*)"BALANCED"; ;}
    break;

  case 514:

/* Line 1455 of yacc.c  */
#line 3074 "def.y"
    { (yyval.string) = (char*)"STEINER"; ;}
    break;

  case 515:

/* Line 1455 of yacc.c  */
#line 3076 "def.y"
    { (yyval.string) = (char*)"TRUNK"; ;}
    break;

  case 516:

/* Line 1455 of yacc.c  */
#line 3078 "def.y"
    { (yyval.string) = (char*)"WIREDLOGIC"; ;}
    break;

  case 517:

/* Line 1455 of yacc.c  */
#line 3082 "def.y"
    { 
        if (defrReader::get()->needPathData() && defrReader::get()->getSNetCbk()) {
           if (needSNPCbk && defrReader::get()->getSNetPartialPathCbk()) { 
              // require a callback before proceed because needSNPCbk must be
              // set to 1 from the previous pathIsDone and user has registered
              // a callback routine.
              CALLBACK(defrReader::get()->getSNetPartialPathCbk(), defrSNetPartialPathCbkType,
                       &defrReader::get()->getNet());
              needSNPCbk = 0;   // reset the flag
              defrReader::get()->pathIsDone(shield, 1, netOsnet, &needSNPCbk);
           } else
              defrReader::get()->pathIsDone(shield, 0, netOsnet, &needSNPCbk);
        }
      ;}
    break;

  case 518:

/* Line 1455 of yacc.c  */
#line 3097 "def.y"
    { ;}
    break;

  case 519:

/* Line 1455 of yacc.c  */
#line 3099 "def.y"
    { dumb_mode = 1; ;}
    break;

  case 520:

/* Line 1455 of yacc.c  */
#line 3100 "def.y"
    { if (defrReader::get()->needPathData() && defrReader::get()->getSNetCbk()) {
           if (needSNPCbk && defrReader::get()->getSNetPartialPathCbk()) {
              // require a callback before proceed because needSNPCbk must be
              // set to 1 from the previous pathIsDone and user has registered
              // a callback routine.
              CALLBACK(defrReader::get()->getSNetPartialPathCbk(), defrSNetPartialPathCbkType,
                       &defrReader::get()->getNet());
              needSNPCbk = 0;   // reset the flag
              defrReader::get()->pathIsDone(shield, 1, netOsnet, &needSNPCbk);
              // reset any poly or rect in special wiring statement
              defrReader::get()->getNet().defiNet::clearRectPolyNPath();
           } else
              defrReader::get()->pathIsDone(shield, 0, netOsnet, &needSNPCbk);
        }
      ;}
    break;

  case 521:

/* Line 1455 of yacc.c  */
#line 3117 "def.y"
    { if (defrReader::get()->needPathData() && defrReader::get()->getSNetCbk()) defrReader::get()->getPath()->defiPath::addLayer((yyvsp[(1) - (1)].string));
        dumb_mode = 0; no_num = 0;
      ;}
    break;

  case 522:

/* Line 1455 of yacc.c  */
#line 3124 "def.y"
    { dumb_mode = 1000000000; by_is_keyword = TRUE; do_is_keyword = TRUE;
        new_is_keyword = TRUE; step_is_keyword = TRUE;
        orient_is_keyword = TRUE; ;}
    break;

  case 523:

/* Line 1455 of yacc.c  */
#line 3129 "def.y"
    { dumb_mode = 0; ;}
    break;

  case 524:

/* Line 1455 of yacc.c  */
#line 3132 "def.y"
    { if (defrReader::get()->needPathData() && defrReader::get()->getSNetCbk())
          defrReader::get()->getPath()->defiPath::addWidth(ROUND((yyvsp[(1) - (1)].dval)));
      ;}
    break;

  case 525:

/* Line 1455 of yacc.c  */
#line 3137 "def.y"
    { 
        if (defrReader::get()->getSNetStartCbk())
          CALLBACK(defrReader::get()->getSNetStartCbk(), defrSNetStartCbkType, ROUND((yyvsp[(2) - (3)].dval)));
        netOsnet = 2;
      ;}
    break;

  case 526:

/* Line 1455 of yacc.c  */
#line 3144 "def.y"
    { 
        if (defrReader::get()->getSNetEndCbk())
          CALLBACK(defrReader::get()->getSNetEndCbk(), defrSNetEndCbkType, 0);
        netOsnet = 0;
      ;}
    break;

  case 528:

/* Line 1455 of yacc.c  */
#line 3154 "def.y"
    {
        if (defrReader::get()->getGroupsStartCbk())
           CALLBACK(defrReader::get()->getGroupsStartCbk(), defrGroupsStartCbkType, ROUND((yyvsp[(2) - (3)].dval)));
      ;}
    break;

  case 531:

/* Line 1455 of yacc.c  */
#line 3164 "def.y"
    {
        if (defrReader::get()->getGroupCbk())
           CALLBACK(defrReader::get()->getGroupCbk(), defrGroupCbkType, &defrReader::get()->getGroup());
      ;}
    break;

  case 532:

/* Line 1455 of yacc.c  */
#line 3169 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 533:

/* Line 1455 of yacc.c  */
#line 3170 "def.y"
    {
        dumb_mode = 1000000000;
        no_num = 1000000000;
        /* dumb_mode is automatically turned off at the first
         * + in the options or at the ; at the end of the group */
        if (defrReader::get()->getGroupCbk()) defrReader::get()->getGroup().defiGroup::setup((yyvsp[(3) - (3)].string));
        if (defrReader::get()->getGroupNameCbk())
           CALLBACK(defrReader::get()->getGroupNameCbk(), defrGroupNameCbkType, (yyvsp[(3) - (3)].string));
      ;}
    break;

  case 535:

/* Line 1455 of yacc.c  */
#line 3182 "def.y"
    {  ;}
    break;

  case 536:

/* Line 1455 of yacc.c  */
#line 3185 "def.y"
    {
        /* if (defrReader::get()->getGroupCbk()) defrReader::get()->getGroup().defiGroup::addMember($1); */
        if (defrReader::get()->getGroupMemberCbk())
          CALLBACK(defrReader::get()->getGroupMemberCbk(), defrGroupMemberCbkType, (yyvsp[(1) - (1)].string));
      ;}
    break;

  case 539:

/* Line 1455 of yacc.c  */
#line 3196 "def.y"
    { ;}
    break;

  case 540:

/* Line 1455 of yacc.c  */
#line 3197 "def.y"
    { dumb_mode = 10000000; parsing_property = 1; ;}
    break;

  case 541:

/* Line 1455 of yacc.c  */
#line 3199 "def.y"
    { dumb_mode = 0; parsing_property = 0; ;}
    break;

  case 542:

/* Line 1455 of yacc.c  */
#line 3200 "def.y"
    { dumb_mode = 1;  no_num = 1; ;}
    break;

  case 543:

/* Line 1455 of yacc.c  */
#line 3201 "def.y"
    { ;}
    break;

  case 544:

/* Line 1455 of yacc.c  */
#line 3203 "def.y"
    { 
        if (defrReader::get()->getGroupMemberCbk())
          CALLBACK(defrReader::get()->getGroupExtCbk(), defrGroupExtCbkType, History_text);
      ;}
    break;

  case 545:

/* Line 1455 of yacc.c  */
#line 3209 "def.y"
    {
        /* 11/12/2002 - this is obsolete in 5.5, & will be ignored */
        if (defVersionNum < 5.5) {
          if (defrReader::get()->getGroupCbk())
            defrReader::get()->getGroup().defiGroup::addRegionRect((yyvsp[(1) - (2)].pt).x, (yyvsp[(1) - (2)].pt).y, (yyvsp[(2) - (2)].pt).x, (yyvsp[(2) - (2)].pt).y);
        }
        else
          defWarning(7027, "GROUP REGION pt pt statement is obsolete in version 5.5 and later.\nThe DEF parser will ignore this statement.");
      ;}
    break;

  case 546:

/* Line 1455 of yacc.c  */
#line 3219 "def.y"
    { if (defrReader::get()->getGroupCbk())
          defrReader::get()->getGroup().defiGroup::setRegionName((yyvsp[(1) - (1)].string));
      ;}
    break;

  case 549:

/* Line 1455 of yacc.c  */
#line 3228 "def.y"
    {
        if (defrReader::get()->getGroupCbk()) {
          char propTp;
          char* str = ringCopy("                       ");
          propTp = defrReader::get()->getGroupProp().defiPropType::propType((yyvsp[(1) - (2)].string));
          CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "GROUP");
          /* For backword compatibility, also set the string value */
          /* We will use a temporary string to store the number.
           * The string space is borrowed from the ring buffer
           * in the lexer. */
          sprintf(str, "%g", (yyvsp[(2) - (2)].dval));
          defrReader::get()->getGroup().defiGroup::addNumProperty((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].dval), str, propTp);
        }
      ;}
    break;

  case 550:

/* Line 1455 of yacc.c  */
#line 3243 "def.y"
    {
        if (defrReader::get()->getGroupCbk()) {
          char propTp;
          propTp = defrReader::get()->getGroupProp().defiPropType::propType((yyvsp[(1) - (2)].string));
          CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "GROUP");
          defrReader::get()->getGroup().defiGroup::addProperty((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].string), propTp);
        }
      ;}
    break;

  case 551:

/* Line 1455 of yacc.c  */
#line 3252 "def.y"
    {
        if (defrReader::get()->getGroupCbk()) {
          char propTp;
          propTp = defrReader::get()->getGroupProp().defiPropType::propType((yyvsp[(1) - (2)].string));
          CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "GROUP");
          defrReader::get()->getGroup().defiGroup::addProperty((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].string), propTp);
        }
      ;}
    break;

  case 553:

/* Line 1455 of yacc.c  */
#line 3263 "def.y"
    { ;}
    break;

  case 554:

/* Line 1455 of yacc.c  */
#line 3266 "def.y"
    {
        /* 11/12/2002 - this is obsolete in 5.5, & will be ignored */
        if (defVersionNum < 5.5)
          if (defrReader::get()->getGroupCbk()) defrReader::get()->getGroup().defiGroup::setMaxX(ROUND((yyvsp[(2) - (2)].dval)));
        else
          defWarning(7028, "GROUP SOFT MAXX statement is obsolete in version 5.5 and later.\nThe DEF parser will ignore this statement.");
      ;}
    break;

  case 555:

/* Line 1455 of yacc.c  */
#line 3274 "def.y"
    { 
        /* 11/12/2002 - this is obsolete in 5.5, & will be ignored */
        if (defVersionNum < 5.5)
          if (defrReader::get()->getGroupCbk()) defrReader::get()->getGroup().defiGroup::setMaxY(ROUND((yyvsp[(2) - (2)].dval)));
        else
          defWarning(7029, "GROUP SOFT MAXY statement is obsolete in version 5.5 and later.\nThe DEF parser will ignore this statement.");
      ;}
    break;

  case 556:

/* Line 1455 of yacc.c  */
#line 3282 "def.y"
    { 
        /* 11/12/2002 - this is obsolete in 5.5, & will be ignored */
        if (defVersionNum < 5.5)
          if (defrReader::get()->getGroupCbk()) defrReader::get()->getGroup().defiGroup::setPerim(ROUND((yyvsp[(2) - (2)].dval)));
        else
          defWarning(7030, "GROUP SOFT MAXHALFPERIMETER statement is obsolete in version 5.5 and later.\nThe DEF parser will ignore this statement.");
      ;}
    break;

  case 557:

/* Line 1455 of yacc.c  */
#line 3291 "def.y"
    { 
        if (defrReader::get()->getGroupsEndCbk())
          CALLBACK(defrReader::get()->getGroupsEndCbk(), defrGroupsEndCbkType, 0);
      ;}
    break;

  case 560:

/* Line 1455 of yacc.c  */
#line 3305 "def.y"
    {
        if ((defVersionNum < 5.4) && (defrReader::get()->getAssertionsStartCbk())) {
          CALLBACK(defrReader::get()->getAssertionsStartCbk(), defrAssertionsStartCbkType,
	           ROUND((yyvsp[(2) - (3)].dval)));
        } else {
          if (defrReader::get()->getAssertionCbk())
            if (assertionWarnings++ < defrReader::get()->getAssertionWarnings())
              defWarning(7031, "ASSERTIONS statement is obsolete in version 5.4 and later.\nThe DEF parser will ignore this statement.");
        }
        if (defrReader::get()->getAssertionCbk())
          defrReader::get()->getAssertion().defiAssertion::setAssertionMode();
      ;}
    break;

  case 561:

/* Line 1455 of yacc.c  */
#line 3319 "def.y"
    {
        if ((defVersionNum < 5.4) && (defrReader::get()->getConstraintsStartCbk())) {
          CALLBACK(defrReader::get()->getConstraintsStartCbk(), defrConstraintsStartCbkType,
                   ROUND((yyvsp[(2) - (3)].dval)));
        } else {
          if (defrReader::get()->getConstraintCbk())
            if (constraintWarnings++ < defrReader::get()->getConstraintWarnings())
              defWarning(7032, "CONSTRAINTS statement is obsolete in version 5.4 and later.\nThe DEF parser will ignore this statement.");
        }
        if (defrReader::get()->getConstraintCbk())
          defrReader::get()->getAssertion().defiAssertion::setConstraintMode();
      ;}
    break;

  case 565:

/* Line 1455 of yacc.c  */
#line 3338 "def.y"
    {
        if ((defVersionNum < 5.4) && (defrReader::get()->getConstraintCbk() || defrReader::get()->getAssertionCbk())) {
          if (defrReader::get()->getAssertion().defiAssertion::isConstraint()) 
            CALLBACK(defrReader::get()->getConstraintCbk(), defrConstraintCbkType, &defrReader::get()->getAssertion());
          if (defrReader::get()->getAssertion().defiAssertion::isAssertion()) 
            CALLBACK(defrReader::get()->getAssertionCbk(), defrAssertionCbkType, &defrReader::get()->getAssertion());
        }
      ;}
    break;

  case 566:

/* Line 1455 of yacc.c  */
#line 3348 "def.y"
    { 
        if ((defVersionNum < 5.4) && (defrReader::get()->getConstraintCbk() || defrReader::get()->getAssertionCbk())) {
          if (defrReader::get()->getAssertion().defiAssertion::isConstraint()) 
	    CALLBACK(defrReader::get()->getConstraintCbk(), defrConstraintCbkType, &defrReader::get()->getAssertion());
          if (defrReader::get()->getAssertion().defiAssertion::isAssertion()) 
            CALLBACK(defrReader::get()->getAssertionCbk(), defrAssertionCbkType, &defrReader::get()->getAssertion());
        }
   
        // reset all the flags and everything
        defrReader::get()->getAssertion().defiAssertion::clear();
      ;}
    break;

  case 567:

/* Line 1455 of yacc.c  */
#line 3360 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 568:

/* Line 1455 of yacc.c  */
#line 3361 "def.y"
    {
         if (defrReader::get()->getConstraintCbk() || defrReader::get()->getAssertionCbk())
           defrReader::get()->getAssertion().defiAssertion::addNet((yyvsp[(3) - (3)].string));
      ;}
    break;

  case 569:

/* Line 1455 of yacc.c  */
#line 3365 "def.y"
    {dumb_mode = 4; no_num = 4;;}
    break;

  case 570:

/* Line 1455 of yacc.c  */
#line 3366 "def.y"
    {
         if (defrReader::get()->getConstraintCbk() || defrReader::get()->getAssertionCbk())
           defrReader::get()->getAssertion().defiAssertion::addPath((yyvsp[(3) - (6)].string), (yyvsp[(4) - (6)].string), (yyvsp[(5) - (6)].string), (yyvsp[(6) - (6)].string));
      ;}
    break;

  case 571:

/* Line 1455 of yacc.c  */
#line 3371 "def.y"
    {
        if (defrReader::get()->getConstraintCbk() || defrReader::get()->getAssertionCbk())
           defrReader::get()->getAssertion().defiAssertion::setSum();
      ;}
    break;

  case 572:

/* Line 1455 of yacc.c  */
#line 3376 "def.y"
    {
        if (defrReader::get()->getConstraintCbk() || defrReader::get()->getAssertionCbk())
           defrReader::get()->getAssertion().defiAssertion::setDiff();
      ;}
    break;

  case 574:

/* Line 1455 of yacc.c  */
#line 3383 "def.y"
    { ;}
    break;

  case 576:

/* Line 1455 of yacc.c  */
#line 3386 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 577:

/* Line 1455 of yacc.c  */
#line 3388 "def.y"
    {
        if (defrReader::get()->getConstraintCbk() || defrReader::get()->getAssertionCbk())
          defrReader::get()->getAssertion().defiAssertion::setWiredlogic((yyvsp[(4) - (8)].string), (yyvsp[(7) - (8)].dval));
      ;}
    break;

  case 578:

/* Line 1455 of yacc.c  */
#line 3395 "def.y"
    { (yyval.string) = (char*)""; ;}
    break;

  case 579:

/* Line 1455 of yacc.c  */
#line 3397 "def.y"
    { (yyval.string) = (char*)"+"; ;}
    break;

  case 582:

/* Line 1455 of yacc.c  */
#line 3404 "def.y"
    {
        if (defrReader::get()->getConstraintCbk() || defrReader::get()->getAssertionCbk())
          defrReader::get()->getAssertion().defiAssertion::setRiseMin((yyvsp[(3) - (3)].dval));
      ;}
    break;

  case 583:

/* Line 1455 of yacc.c  */
#line 3409 "def.y"
    {
        if (defrReader::get()->getConstraintCbk() || defrReader::get()->getAssertionCbk())
          defrReader::get()->getAssertion().defiAssertion::setRiseMax((yyvsp[(3) - (3)].dval));
      ;}
    break;

  case 584:

/* Line 1455 of yacc.c  */
#line 3414 "def.y"
    {
        if (defrReader::get()->getConstraintCbk() || defrReader::get()->getAssertionCbk())
          defrReader::get()->getAssertion().defiAssertion::setFallMin((yyvsp[(3) - (3)].dval));
      ;}
    break;

  case 585:

/* Line 1455 of yacc.c  */
#line 3419 "def.y"
    {
        if (defrReader::get()->getConstraintCbk() || defrReader::get()->getAssertionCbk())
          defrReader::get()->getAssertion().defiAssertion::setFallMax((yyvsp[(3) - (3)].dval));
      ;}
    break;

  case 586:

/* Line 1455 of yacc.c  */
#line 3425 "def.y"
    { if ((defVersionNum < 5.4) && defrReader::get()->getConstraintsEndCbk()) {
          CALLBACK(defrReader::get()->getConstraintsEndCbk(), defrConstraintsEndCbkType, 0);
        } else {
          if (defrReader::get()->getConstraintsEndCbk()) {
            if (constraintWarnings++ < defrReader::get()->getConstraintWarnings())
              defWarning(7032, "CONSTRAINTS statement is obsolete in version 5.4 and later.\nThe DEF parser will ignore this statement.");
          }
        }
      ;}
    break;

  case 587:

/* Line 1455 of yacc.c  */
#line 3436 "def.y"
    { if ((defVersionNum < 5.4) && defrReader::get()->getAssertionsEndCbk()) {
          CALLBACK(defrReader::get()->getAssertionsEndCbk(), defrAssertionsEndCbkType, 0);
        } else {
          if (defrReader::get()->getAssertionsEndCbk()) {
            if (assertionWarnings++ < defrReader::get()->getAssertionWarnings())
              defWarning(7031, "ASSERTIONS statement is obsolete in version 5.4 and later.\nThe DEF parser will ignore this statement.");
          }
        }
      ;}
    break;

  case 589:

/* Line 1455 of yacc.c  */
#line 3450 "def.y"
    { if (defrReader::get()->getScanchainsStartCbk())
          CALLBACK(defrReader::get()->getScanchainsStartCbk(), defrScanchainsStartCbkType,
                   ROUND((yyvsp[(2) - (3)].dval)));
      ;}
    break;

  case 591:

/* Line 1455 of yacc.c  */
#line 3457 "def.y"
    {;}
    break;

  case 592:

/* Line 1455 of yacc.c  */
#line 3460 "def.y"
    { 
        if (defrReader::get()->getScanchainCbk())
          CALLBACK(defrReader::get()->getScanchainCbk(), defrScanchainCbkType, &defrReader::get()->getScanchain());
      ;}
    break;

  case 593:

/* Line 1455 of yacc.c  */
#line 3465 "def.y"
    {dumb_mode = 1; no_num = 1;;}
    break;

  case 594:

/* Line 1455 of yacc.c  */
#line 3466 "def.y"
    {
        if (defrReader::get()->getScanchainCbk())
          defrReader::get()->getScanchain().defiScanchain::setName((yyvsp[(3) - (3)].string));
        bit_is_keyword = TRUE;
      ;}
    break;

  case 597:

/* Line 1455 of yacc.c  */
#line 3478 "def.y"
    { (yyval.string) = (char*)""; ;}
    break;

  case 598:

/* Line 1455 of yacc.c  */
#line 3480 "def.y"
    { (yyval.string) = (yyvsp[(1) - (1)].string); ;}
    break;

  case 599:

/* Line 1455 of yacc.c  */
#line 3482 "def.y"
    {dumb_mode = 2; no_num = 2;;}
    break;

  case 600:

/* Line 1455 of yacc.c  */
#line 3483 "def.y"
    { if (defrReader::get()->getScanchainCbk())
          defrReader::get()->getScanchain().defiScanchain::setStart((yyvsp[(4) - (5)].string), (yyvsp[(5) - (5)].string));
      ;}
    break;

  case 601:

/* Line 1455 of yacc.c  */
#line 3486 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 602:

/* Line 1455 of yacc.c  */
#line 3487 "def.y"
    { dumb_mode = 0; no_num = 0; ;}
    break;

  case 603:

/* Line 1455 of yacc.c  */
#line 3489 "def.y"
    {
         dumb_mode = 1;
         no_num = 1;
         if (defrReader::get()->getScanchainCbk())
           defrReader::get()->getScanchain().defiScanchain::addOrderedList();
      ;}
    break;

  case 604:

/* Line 1455 of yacc.c  */
#line 3496 "def.y"
    { dumb_mode = 0; no_num = 0; ;}
    break;

  case 605:

/* Line 1455 of yacc.c  */
#line 3497 "def.y"
    {dumb_mode = 2; no_num = 2; ;}
    break;

  case 606:

/* Line 1455 of yacc.c  */
#line 3498 "def.y"
    { if (defrReader::get()->getScanchainCbk())
          defrReader::get()->getScanchain().defiScanchain::setStop((yyvsp[(4) - (5)].string), (yyvsp[(5) - (5)].string));
      ;}
    break;

  case 607:

/* Line 1455 of yacc.c  */
#line 3501 "def.y"
    { dumb_mode = 10; no_num = 10; ;}
    break;

  case 608:

/* Line 1455 of yacc.c  */
#line 3502 "def.y"
    { dumb_mode = 0;  no_num = 0; ;}
    break;

  case 609:

/* Line 1455 of yacc.c  */
#line 3503 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 610:

/* Line 1455 of yacc.c  */
#line 3505 "def.y"
    {
        if (defVersionNum < 5.5) {
          if (defrReader::get()->getScanchainCbk()) {
            if (scanchainWarnings++ < defrReader::get()->getScanchainWarnings()) {
              defMsg = (char*)defMalloc(1000);
              sprintf (defMsg,
                 "PARTITION statement is a version 5.5 and later syntax.\nYour def file is defined with version %g", defVersionNum);
              defError(6538, defMsg);
              defFree(defMsg);
              CHKERR();
            }
          }
        }
        if (defrReader::get()->getScanchainCbk())
          defrReader::get()->getScanchain().defiScanchain::setPartition((yyvsp[(4) - (5)].string), (yyvsp[(5) - (5)].integer));
      ;}
    break;

  case 611:

/* Line 1455 of yacc.c  */
#line 3522 "def.y"
    {
        if (defrReader::get()->getScanChainExtCbk())
          CALLBACK(defrReader::get()->getScanChainExtCbk(), defrScanChainExtCbkType, History_text);
      ;}
    break;

  case 612:

/* Line 1455 of yacc.c  */
#line 3528 "def.y"
    { ;}
    break;

  case 613:

/* Line 1455 of yacc.c  */
#line 3530 "def.y"
    {
	if (defrReader::get()->getScanchainCbk()) {
	  if (strcmp((yyvsp[(2) - (4)].string), "IN") == 0 || strcmp((yyvsp[(2) - (4)].string), "in") == 0)
	    defrReader::get()->getScanchain().defiScanchain::setCommonIn((yyvsp[(3) - (4)].string));
	  else if (strcmp((yyvsp[(2) - (4)].string), "OUT") == 0 || strcmp((yyvsp[(2) - (4)].string), "out") == 0)
	    defrReader::get()->getScanchain().defiScanchain::setCommonOut((yyvsp[(3) - (4)].string));
	}
      ;}
    break;

  case 614:

/* Line 1455 of yacc.c  */
#line 3539 "def.y"
    {
	if (defrReader::get()->getScanchainCbk()) {
	  if (strcmp((yyvsp[(2) - (8)].string), "IN") == 0 || strcmp((yyvsp[(2) - (8)].string), "in") == 0)
	    defrReader::get()->getScanchain().defiScanchain::setCommonIn((yyvsp[(3) - (8)].string));
	  else if (strcmp((yyvsp[(2) - (8)].string), "OUT") == 0 || strcmp((yyvsp[(2) - (8)].string), "out") == 0)
	    defrReader::get()->getScanchain().defiScanchain::setCommonOut((yyvsp[(3) - (8)].string));
	  if (strcmp((yyvsp[(6) - (8)].string), "IN") == 0 || strcmp((yyvsp[(6) - (8)].string), "in") == 0)
	    defrReader::get()->getScanchain().defiScanchain::setCommonIn((yyvsp[(7) - (8)].string));
	  else if (strcmp((yyvsp[(6) - (8)].string), "OUT") == 0 || strcmp((yyvsp[(6) - (8)].string), "out") == 0)
	    defrReader::get()->getScanchain().defiScanchain::setCommonOut((yyvsp[(7) - (8)].string));
	}
      ;}
    break;

  case 617:

/* Line 1455 of yacc.c  */
#line 3557 "def.y"
    {
	dumb_mode = 1000;
	no_num = 1000;
	if (defrReader::get()->getScanchainCbk())
	  defrReader::get()->getScanchain().defiScanchain::addFloatingInst((yyvsp[(1) - (1)].string));
      ;}
    break;

  case 618:

/* Line 1455 of yacc.c  */
#line 3564 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 619:

/* Line 1455 of yacc.c  */
#line 3567 "def.y"
    { ;}
    break;

  case 620:

/* Line 1455 of yacc.c  */
#line 3569 "def.y"
    {
	if (defrReader::get()->getScanchainCbk()) {
	  if (strcmp((yyvsp[(2) - (4)].string), "IN") == 0 || strcmp((yyvsp[(2) - (4)].string), "in") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addFloatingIn((yyvsp[(3) - (4)].string));
	  else if (strcmp((yyvsp[(2) - (4)].string), "OUT") == 0 || strcmp((yyvsp[(2) - (4)].string), "out") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addFloatingOut((yyvsp[(3) - (4)].string));
          else if (strcmp((yyvsp[(2) - (4)].string), "BITS") == 0 || strcmp((yyvsp[(2) - (4)].string), "bits") == 0) {
            bitsNum = atoi((yyvsp[(3) - (4)].string));
            defrReader::get()->getScanchain().defiScanchain::setFloatingBits(bitsNum);
          }
	}
      ;}
    break;

  case 621:

/* Line 1455 of yacc.c  */
#line 3582 "def.y"
    {
	if (defrReader::get()->getScanchainCbk()) {
	  if (strcmp((yyvsp[(2) - (8)].string), "IN") == 0 || strcmp((yyvsp[(2) - (8)].string), "in") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addFloatingIn((yyvsp[(3) - (8)].string));
	  else if (strcmp((yyvsp[(2) - (8)].string), "OUT") == 0 || strcmp((yyvsp[(2) - (8)].string), "out") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addFloatingOut((yyvsp[(3) - (8)].string));
	  else if (strcmp((yyvsp[(2) - (8)].string), "BITS") == 0 || strcmp((yyvsp[(2) - (8)].string), "bits") == 0) {
            bitsNum = atoi((yyvsp[(3) - (8)].string));
	    defrReader::get()->getScanchain().defiScanchain::setFloatingBits(bitsNum);
          }
	  if (strcmp((yyvsp[(6) - (8)].string), "IN") == 0 || strcmp((yyvsp[(6) - (8)].string), "in") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addFloatingIn((yyvsp[(7) - (8)].string));
	  else if (strcmp((yyvsp[(6) - (8)].string), "OUT") == 0 || strcmp((yyvsp[(6) - (8)].string), "out") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addFloatingOut((yyvsp[(7) - (8)].string));
	  else if (strcmp((yyvsp[(6) - (8)].string), "BITS") == 0 || strcmp((yyvsp[(6) - (8)].string), "bits") == 0) {
            bitsNum = atoi((yyvsp[(7) - (8)].string));
	    defrReader::get()->getScanchain().defiScanchain::setFloatingBits(bitsNum);
          }
	}
      ;}
    break;

  case 622:

/* Line 1455 of yacc.c  */
#line 3604 "def.y"
    {
	if (defrReader::get()->getScanchainCbk()) {
	  if (strcmp((yyvsp[(2) - (12)].string), "IN") == 0 || strcmp((yyvsp[(2) - (12)].string), "in") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addFloatingIn((yyvsp[(3) - (12)].string));
	  else if (strcmp((yyvsp[(2) - (12)].string), "OUT") == 0 || strcmp((yyvsp[(2) - (12)].string), "out") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addFloatingOut((yyvsp[(3) - (12)].string));
	  else if (strcmp((yyvsp[(2) - (12)].string), "BITS") == 0 || strcmp((yyvsp[(2) - (12)].string), "bits") == 0) {
            bitsNum = atoi((yyvsp[(3) - (12)].string));
	    defrReader::get()->getScanchain().defiScanchain::setFloatingBits(bitsNum);
          }
	  if (strcmp((yyvsp[(6) - (12)].string), "IN") == 0 || strcmp((yyvsp[(6) - (12)].string), "in") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addFloatingIn((yyvsp[(7) - (12)].string));
	  else if (strcmp((yyvsp[(6) - (12)].string), "OUT") == 0 || strcmp((yyvsp[(6) - (12)].string), "out") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addFloatingOut((yyvsp[(7) - (12)].string));
	  else if (strcmp((yyvsp[(6) - (12)].string), "BITS") == 0 || strcmp((yyvsp[(6) - (12)].string), "bits") == 0) {
            bitsNum = atoi((yyvsp[(7) - (12)].string));
	    defrReader::get()->getScanchain().defiScanchain::setFloatingBits(bitsNum);
          }
	  if (strcmp((yyvsp[(10) - (12)].string), "IN") == 0 || strcmp((yyvsp[(10) - (12)].string), "in") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addFloatingIn((yyvsp[(11) - (12)].string));
	  else if (strcmp((yyvsp[(10) - (12)].string), "OUT") == 0 || strcmp((yyvsp[(10) - (12)].string), "out") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addFloatingOut((yyvsp[(11) - (12)].string));
	  else if (strcmp((yyvsp[(10) - (12)].string), "BITS") == 0 || strcmp((yyvsp[(10) - (12)].string), "bits") == 0) {
            bitsNum = atoi((yyvsp[(11) - (12)].string));
	    defrReader::get()->getScanchain().defiScanchain::setFloatingBits(bitsNum);
          }
	}
      ;}
    break;

  case 625:

/* Line 1455 of yacc.c  */
#line 3638 "def.y"
    { dumb_mode = 1000; no_num = 1000; 
	if (defrReader::get()->getScanchainCbk())
	  defrReader::get()->getScanchain().defiScanchain::addOrderedInst((yyvsp[(1) - (1)].string));
      ;}
    break;

  case 626:

/* Line 1455 of yacc.c  */
#line 3643 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 627:

/* Line 1455 of yacc.c  */
#line 3646 "def.y"
    { ;}
    break;

  case 628:

/* Line 1455 of yacc.c  */
#line 3648 "def.y"
    {
	if (defrReader::get()->getScanchainCbk()) {
	  if (strcmp((yyvsp[(2) - (4)].string), "IN") == 0 || strcmp((yyvsp[(2) - (4)].string), "in") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addOrderedIn((yyvsp[(3) - (4)].string));
	  else if (strcmp((yyvsp[(2) - (4)].string), "OUT") == 0 || strcmp((yyvsp[(2) - (4)].string), "out") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addOrderedOut((yyvsp[(3) - (4)].string));
          else if (strcmp((yyvsp[(2) - (4)].string), "BITS") == 0 || strcmp((yyvsp[(2) - (4)].string), "bits") == 0) {
            bitsNum = atoi((yyvsp[(3) - (4)].string));
            defrReader::get()->getScanchain().defiScanchain::setOrderedBits(bitsNum);
         }
	}
      ;}
    break;

  case 629:

/* Line 1455 of yacc.c  */
#line 3661 "def.y"
    {
	if (defrReader::get()->getScanchainCbk()) {
	  if (strcmp((yyvsp[(2) - (8)].string), "IN") == 0 || strcmp((yyvsp[(2) - (8)].string), "in") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addOrderedIn((yyvsp[(3) - (8)].string));
	  else if (strcmp((yyvsp[(2) - (8)].string), "OUT") == 0 || strcmp((yyvsp[(2) - (8)].string), "out") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addOrderedOut((yyvsp[(3) - (8)].string));
	  else if (strcmp((yyvsp[(2) - (8)].string), "BITS") == 0 || strcmp((yyvsp[(2) - (8)].string), "bits") == 0) {
            bitsNum = atoi((yyvsp[(3) - (8)].string));
            defrReader::get()->getScanchain().defiScanchain::setOrderedBits(bitsNum);
          }
	  if (strcmp((yyvsp[(6) - (8)].string), "IN") == 0 || strcmp((yyvsp[(6) - (8)].string), "in") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addOrderedIn((yyvsp[(7) - (8)].string));
	  else if (strcmp((yyvsp[(6) - (8)].string), "OUT") == 0 || strcmp((yyvsp[(6) - (8)].string), "out") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addOrderedOut((yyvsp[(7) - (8)].string));
	  else if (strcmp((yyvsp[(6) - (8)].string), "BITS") == 0 || strcmp((yyvsp[(6) - (8)].string), "bits") == 0) {
            bitsNum = atoi((yyvsp[(7) - (8)].string));
            defrReader::get()->getScanchain().defiScanchain::setOrderedBits(bitsNum);
          }
	}
      ;}
    break;

  case 630:

/* Line 1455 of yacc.c  */
#line 3683 "def.y"
    {
	if (defrReader::get()->getScanchainCbk()) {
	  if (strcmp((yyvsp[(2) - (12)].string), "IN") == 0 || strcmp((yyvsp[(2) - (12)].string), "in") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addOrderedIn((yyvsp[(3) - (12)].string));
	  else if (strcmp((yyvsp[(2) - (12)].string), "OUT") == 0 || strcmp((yyvsp[(2) - (12)].string), "out") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addOrderedOut((yyvsp[(3) - (12)].string));
	  else if (strcmp((yyvsp[(2) - (12)].string), "BITS") == 0 || strcmp((yyvsp[(2) - (12)].string), "bits") == 0) {
            bitsNum = atoi((yyvsp[(3) - (12)].string));
	    defrReader::get()->getScanchain().defiScanchain::setOrderedBits(bitsNum);
          }
	  if (strcmp((yyvsp[(6) - (12)].string), "IN") == 0 || strcmp((yyvsp[(6) - (12)].string), "in") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addOrderedIn((yyvsp[(7) - (12)].string));
	  else if (strcmp((yyvsp[(6) - (12)].string), "OUT") == 0 || strcmp((yyvsp[(6) - (12)].string), "out") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addOrderedOut((yyvsp[(7) - (12)].string));
	  else if (strcmp((yyvsp[(6) - (12)].string), "BITS") == 0 || strcmp((yyvsp[(6) - (12)].string), "bits") == 0) {
            bitsNum = atoi((yyvsp[(7) - (12)].string));
	    defrReader::get()->getScanchain().defiScanchain::setOrderedBits(bitsNum);
          }
	  if (strcmp((yyvsp[(10) - (12)].string), "IN") == 0 || strcmp((yyvsp[(10) - (12)].string), "in") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addOrderedIn((yyvsp[(11) - (12)].string));
	  else if (strcmp((yyvsp[(10) - (12)].string), "OUT") == 0 || strcmp((yyvsp[(10) - (12)].string), "out") == 0)
	    defrReader::get()->getScanchain().defiScanchain::addOrderedOut((yyvsp[(11) - (12)].string));
	  else if (strcmp((yyvsp[(10) - (12)].string), "BITS") == 0 || strcmp((yyvsp[(10) - (12)].string), "bits") == 0) {
            bitsNum = atoi((yyvsp[(11) - (12)].string));
	    defrReader::get()->getScanchain().defiScanchain::setOrderedBits(bitsNum);
          }
	}
      ;}
    break;

  case 631:

/* Line 1455 of yacc.c  */
#line 3713 "def.y"
    { (yyval.integer) = -1; ;}
    break;

  case 632:

/* Line 1455 of yacc.c  */
#line 3715 "def.y"
    { (yyval.integer) = ROUND((yyvsp[(2) - (2)].dval)); ;}
    break;

  case 633:

/* Line 1455 of yacc.c  */
#line 3718 "def.y"
    { 
        if (defrReader::get()->getScanchainsEndCbk())
          CALLBACK(defrReader::get()->getScanchainsEndCbk(), defrScanchainsEndCbkType, 0);
        bit_is_keyword = FALSE;
        dumb_mode = 0; no_num = 0;
      ;}
    break;

  case 635:

/* Line 1455 of yacc.c  */
#line 3730 "def.y"
    {
        if (defVersionNum < 5.4 && defrReader::get()->getIOTimingsStartCbk()) {
          CALLBACK(defrReader::get()->getIOTimingsStartCbk(), defrIOTimingsStartCbkType, ROUND((yyvsp[(2) - (3)].dval)));
        } else {
          if (defrReader::get()->getIOTimingsStartCbk())
            if (iOTimingWarnings++ < defrReader::get()->getIOTimingWarnings())
              defWarning(7035, "IOTIMINGS statement is obsolete in version 5.4 and later.\nThe DEF parser will ignore this statement.");
        }
      ;}
    break;

  case 637:

/* Line 1455 of yacc.c  */
#line 3742 "def.y"
    { ;}
    break;

  case 638:

/* Line 1455 of yacc.c  */
#line 3745 "def.y"
    { 
        if (defVersionNum < 5.4 && defrReader::get()->getIOTimingCbk())
          CALLBACK(defrReader::get()->getIOTimingCbk(), defrIOTimingCbkType, &defrReader::get()->getIOTiming());
      ;}
    break;

  case 639:

/* Line 1455 of yacc.c  */
#line 3750 "def.y"
    {dumb_mode = 2; no_num = 2; ;}
    break;

  case 640:

/* Line 1455 of yacc.c  */
#line 3751 "def.y"
    {
        if (defrReader::get()->getIOTimingCbk())
          defrReader::get()->getIOTiming().defiIOTiming::setName((yyvsp[(4) - (6)].string), (yyvsp[(5) - (6)].string));
      ;}
    break;

  case 643:

/* Line 1455 of yacc.c  */
#line 3762 "def.y"
    {
        if (defrReader::get()->getIOTimingCbk()) 
          defrReader::get()->getIOTiming().defiIOTiming::setVariable((yyvsp[(2) - (5)].string), (yyvsp[(4) - (5)].dval), (yyvsp[(5) - (5)].dval));
      ;}
    break;

  case 644:

/* Line 1455 of yacc.c  */
#line 3767 "def.y"
    {
        if (defrReader::get()->getIOTimingCbk()) 
          defrReader::get()->getIOTiming().defiIOTiming::setSlewRate((yyvsp[(2) - (5)].string), (yyvsp[(4) - (5)].dval), (yyvsp[(5) - (5)].dval));
      ;}
    break;

  case 645:

/* Line 1455 of yacc.c  */
#line 3772 "def.y"
    {
        if (defrReader::get()->getIOTimingCbk()) 
          defrReader::get()->getIOTiming().defiIOTiming::setCapacitance((yyvsp[(3) - (3)].dval));
      ;}
    break;

  case 646:

/* Line 1455 of yacc.c  */
#line 3776 "def.y"
    {dumb_mode = 1; no_num = 1; ;}
    break;

  case 647:

/* Line 1455 of yacc.c  */
#line 3777 "def.y"
    {
        if (defrReader::get()->getIOTimingCbk()) 
          defrReader::get()->getIOTiming().defiIOTiming::setDriveCell((yyvsp[(4) - (4)].string));
      ;}
    break;

  case 649:

/* Line 1455 of yacc.c  */
#line 3803 "def.y"
    {
        if (defVersionNum < 5.4 && defrReader::get()->getIoTimingsExtCbk())
          CALLBACK(defrReader::get()->getIoTimingsExtCbk(), defrIoTimingsExtCbkType, History_text);
      ;}
    break;

  case 650:

/* Line 1455 of yacc.c  */
#line 3809 "def.y"
    {dumb_mode = 1; no_num = 1; ;}
    break;

  case 651:

/* Line 1455 of yacc.c  */
#line 3810 "def.y"
    {
        if (defrReader::get()->getIOTimingCbk()) 
          defrReader::get()->getIOTiming().defiIOTiming::setTo((yyvsp[(4) - (4)].string));
      ;}
    break;

  case 654:

/* Line 1455 of yacc.c  */
#line 3817 "def.y"
    {dumb_mode = 1; no_num = 1; ;}
    break;

  case 655:

/* Line 1455 of yacc.c  */
#line 3818 "def.y"
    {
        if (defrReader::get()->getIOTimingCbk())
          defrReader::get()->getIOTiming().defiIOTiming::setFrom((yyvsp[(3) - (3)].string));
      ;}
    break;

  case 657:

/* Line 1455 of yacc.c  */
#line 3825 "def.y"
    {
        if (defrReader::get()->getIOTimingCbk())
          defrReader::get()->getIOTiming().defiIOTiming::setParallel((yyvsp[(2) - (2)].dval));
      ;}
    break;

  case 658:

/* Line 1455 of yacc.c  */
#line 3830 "def.y"
    { (yyval.string) = (char*)"RISE"; ;}
    break;

  case 659:

/* Line 1455 of yacc.c  */
#line 3830 "def.y"
    { (yyval.string) = (char*)"FALL"; ;}
    break;

  case 660:

/* Line 1455 of yacc.c  */
#line 3833 "def.y"
    {
        if (defVersionNum < 5.4 && defrReader::get()->getIOTimingsEndCbk())
          CALLBACK(defrReader::get()->getIOTimingsEndCbk(), defrIOTimingsEndCbkType, 0);
      ;}
    break;

  case 661:

/* Line 1455 of yacc.c  */
#line 3839 "def.y"
    { 
        if (defrReader::get()->getFPCEndCbk())
          CALLBACK(defrReader::get()->getFPCEndCbk(), defrFPCEndCbkType, 0);
      ;}
    break;

  case 662:

/* Line 1455 of yacc.c  */
#line 3845 "def.y"
    {
        if (defrReader::get()->getFPCStartCbk())
          CALLBACK(defrReader::get()->getFPCStartCbk(), defrFPCStartCbkType, ROUND((yyvsp[(2) - (3)].dval)));
      ;}
    break;

  case 664:

/* Line 1455 of yacc.c  */
#line 3852 "def.y"
    {;}
    break;

  case 665:

/* Line 1455 of yacc.c  */
#line 3854 "def.y"
    { dumb_mode = 1; no_num = 1;  ;}
    break;

  case 666:

/* Line 1455 of yacc.c  */
#line 3855 "def.y"
    { if (defrReader::get()->getFPCCbk()) defrReader::get()->getFPC().defiFPC::setName((yyvsp[(3) - (4)].string), (yyvsp[(4) - (4)].string)); ;}
    break;

  case 667:

/* Line 1455 of yacc.c  */
#line 3857 "def.y"
    { if (defrReader::get()->getFPCCbk()) CALLBACK(defrReader::get()->getFPCCbk(), defrFPCCbkType, &defrReader::get()->getFPC()); ;}
    break;

  case 668:

/* Line 1455 of yacc.c  */
#line 3860 "def.y"
    { (yyval.string) = (char*)"HORIZONTAL"; ;}
    break;

  case 669:

/* Line 1455 of yacc.c  */
#line 3862 "def.y"
    { (yyval.string) = (char*)"VERTICAL"; ;}
    break;

  case 670:

/* Line 1455 of yacc.c  */
#line 3865 "def.y"
    { if (defrReader::get()->getFPCCbk()) defrReader::get()->getFPC().defiFPC::setAlign(); ;}
    break;

  case 671:

/* Line 1455 of yacc.c  */
#line 3867 "def.y"
    { if (defrReader::get()->getFPCCbk()) defrReader::get()->getFPC().defiFPC::setMax((yyvsp[(2) - (2)].dval)); ;}
    break;

  case 672:

/* Line 1455 of yacc.c  */
#line 3869 "def.y"
    { if (defrReader::get()->getFPCCbk()) defrReader::get()->getFPC().defiFPC::setMin((yyvsp[(2) - (2)].dval)); ;}
    break;

  case 673:

/* Line 1455 of yacc.c  */
#line 3871 "def.y"
    { if (defrReader::get()->getFPCCbk()) defrReader::get()->getFPC().defiFPC::setEqual((yyvsp[(2) - (2)].dval)); ;}
    break;

  case 676:

/* Line 1455 of yacc.c  */
#line 3878 "def.y"
    { if (defrReader::get()->getFPCCbk()) defrReader::get()->getFPC().defiFPC::setDoingBottomLeft(); ;}
    break;

  case 678:

/* Line 1455 of yacc.c  */
#line 3881 "def.y"
    { if (defrReader::get()->getFPCCbk()) defrReader::get()->getFPC().defiFPC::setDoingTopRight(); ;}
    break;

  case 682:

/* Line 1455 of yacc.c  */
#line 3888 "def.y"
    {dumb_mode = 1; no_num = 1; ;}
    break;

  case 683:

/* Line 1455 of yacc.c  */
#line 3889 "def.y"
    { if (defrReader::get()->getFPCCbk()) defrReader::get()->getFPC().defiFPC::addRow((yyvsp[(4) - (5)].string)); ;}
    break;

  case 684:

/* Line 1455 of yacc.c  */
#line 3890 "def.y"
    {dumb_mode = 1; no_num = 1; ;}
    break;

  case 685:

/* Line 1455 of yacc.c  */
#line 3891 "def.y"
    { if (defrReader::get()->getFPCCbk()) defrReader::get()->getFPC().defiFPC::addComps((yyvsp[(4) - (5)].string)); ;}
    break;

  case 687:

/* Line 1455 of yacc.c  */
#line 3898 "def.y"
    { 
        if (defrReader::get()->getTimingDisablesStartCbk())
          CALLBACK(defrReader::get()->getTimingDisablesStartCbk(), defrTimingDisablesStartCbkType,
                   ROUND((yyvsp[(2) - (3)].dval)));
      ;}
    break;

  case 689:

/* Line 1455 of yacc.c  */
#line 3906 "def.y"
    {;}
    break;

  case 690:

/* Line 1455 of yacc.c  */
#line 3908 "def.y"
    { dumb_mode = 2; no_num = 2;  ;}
    break;

  case 691:

/* Line 1455 of yacc.c  */
#line 3909 "def.y"
    { dumb_mode = 2; no_num = 2;  ;}
    break;

  case 692:

/* Line 1455 of yacc.c  */
#line 3910 "def.y"
    {
        if (defrReader::get()->getTimingDisableCbk()) {
          defrReader::get()->getTimingDisable().defiTimingDisable::setFromTo((yyvsp[(4) - (10)].string), (yyvsp[(5) - (10)].string), (yyvsp[(8) - (10)].string), (yyvsp[(9) - (10)].string));
          CALLBACK(defrReader::get()->getTimingDisableCbk(), defrTimingDisableCbkType,
                &defrReader::get()->getTimingDisable());
        }
      ;}
    break;

  case 693:

/* Line 1455 of yacc.c  */
#line 3917 "def.y"
    {dumb_mode = 2; no_num = 2; ;}
    break;

  case 694:

/* Line 1455 of yacc.c  */
#line 3918 "def.y"
    {
        if (defrReader::get()->getTimingDisableCbk()) {
          defrReader::get()->getTimingDisable().defiTimingDisable::setThru((yyvsp[(4) - (6)].string), (yyvsp[(5) - (6)].string));
          CALLBACK(defrReader::get()->getTimingDisableCbk(), defrTimingDisableCbkType,
                   &defrReader::get()->getTimingDisable());
        }
      ;}
    break;

  case 695:

/* Line 1455 of yacc.c  */
#line 3925 "def.y"
    {dumb_mode = 1; no_num = 1;;}
    break;

  case 696:

/* Line 1455 of yacc.c  */
#line 3926 "def.y"
    {
        if (defrReader::get()->getTimingDisableCbk()) {
          defrReader::get()->getTimingDisable().defiTimingDisable::setMacro((yyvsp[(4) - (6)].string));
          CALLBACK(defrReader::get()->getTimingDisableCbk(), defrTimingDisableCbkType,
                &defrReader::get()->getTimingDisable());
        }
      ;}
    break;

  case 697:

/* Line 1455 of yacc.c  */
#line 3934 "def.y"
    { if (defrReader::get()->getTimingDisableCbk())
          defrReader::get()->getTimingDisable().defiTimingDisable::setReentrantPathsFlag();
      ;}
    break;

  case 698:

/* Line 1455 of yacc.c  */
#line 3939 "def.y"
    {dumb_mode = 1; no_num = 1;;}
    break;

  case 699:

/* Line 1455 of yacc.c  */
#line 3940 "def.y"
    {dumb_mode=1; no_num = 1;;}
    break;

  case 700:

/* Line 1455 of yacc.c  */
#line 3941 "def.y"
    {
        if (defrReader::get()->getTimingDisableCbk())
          defrReader::get()->getTimingDisable().defiTimingDisable::setMacroFromTo((yyvsp[(3) - (6)].string),(yyvsp[(6) - (6)].string));
      ;}
    break;

  case 701:

/* Line 1455 of yacc.c  */
#line 3945 "def.y"
    {dumb_mode=1; no_num = 1;;}
    break;

  case 702:

/* Line 1455 of yacc.c  */
#line 3946 "def.y"
    {
        if (defrReader::get()->getTimingDisableCbk())
          defrReader::get()->getTimingDisable().defiTimingDisable::setMacroThru((yyvsp[(3) - (3)].string));
      ;}
    break;

  case 703:

/* Line 1455 of yacc.c  */
#line 3952 "def.y"
    { 
        if (defrReader::get()->getTimingDisablesEndCbk())
          CALLBACK(defrReader::get()->getTimingDisablesEndCbk(), defrTimingDisablesEndCbkType, 0);
      ;}
    break;

  case 705:

/* Line 1455 of yacc.c  */
#line 3962 "def.y"
    {
        if (defrReader::get()->getPartitionsStartCbk())
          CALLBACK(defrReader::get()->getPartitionsStartCbk(), defrPartitionsStartCbkType,
                   ROUND((yyvsp[(2) - (3)].dval)));
      ;}
    break;

  case 707:

/* Line 1455 of yacc.c  */
#line 3970 "def.y"
    { ;}
    break;

  case 708:

/* Line 1455 of yacc.c  */
#line 3973 "def.y"
    { 
        if (defrReader::get()->getPartitionCbk())
          CALLBACK(defrReader::get()->getPartitionCbk(), defrPartitionCbkType, &defrReader::get()->getPartition());
      ;}
    break;

  case 709:

/* Line 1455 of yacc.c  */
#line 3978 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 710:

/* Line 1455 of yacc.c  */
#line 3979 "def.y"
    {
        if (defrReader::get()->getPartitionCbk())
          defrReader::get()->getPartition().defiPartition::setName((yyvsp[(3) - (4)].string));
      ;}
    break;

  case 712:

/* Line 1455 of yacc.c  */
#line 3986 "def.y"
    {
        if (defrReader::get()->getPartitionCbk())
          defrReader::get()->getPartition().defiPartition::addTurnOff((yyvsp[(2) - (3)].string), (yyvsp[(3) - (3)].string));
      ;}
    break;

  case 713:

/* Line 1455 of yacc.c  */
#line 3992 "def.y"
    { (yyval.string) = (char*)" "; ;}
    break;

  case 714:

/* Line 1455 of yacc.c  */
#line 3994 "def.y"
    { (yyval.string) = (char*)"R"; ;}
    break;

  case 715:

/* Line 1455 of yacc.c  */
#line 3996 "def.y"
    { (yyval.string) = (char*)"F"; ;}
    break;

  case 716:

/* Line 1455 of yacc.c  */
#line 3999 "def.y"
    { (yyval.string) = (char*)" "; ;}
    break;

  case 717:

/* Line 1455 of yacc.c  */
#line 4001 "def.y"
    { (yyval.string) = (char*)"R"; ;}
    break;

  case 718:

/* Line 1455 of yacc.c  */
#line 4003 "def.y"
    { (yyval.string) = (char*)"F"; ;}
    break;

  case 721:

/* Line 1455 of yacc.c  */
#line 4009 "def.y"
    {dumb_mode=2; no_num = 2;;}
    break;

  case 722:

/* Line 1455 of yacc.c  */
#line 4011 "def.y"
    {
        if (defrReader::get()->getPartitionCbk())
          defrReader::get()->getPartition().defiPartition::setFromClockPin((yyvsp[(4) - (7)].string), (yyvsp[(5) - (7)].string));
      ;}
    break;

  case 723:

/* Line 1455 of yacc.c  */
#line 4015 "def.y"
    {dumb_mode=2; no_num = 2; ;}
    break;

  case 724:

/* Line 1455 of yacc.c  */
#line 4017 "def.y"
    {
        if (defrReader::get()->getPartitionCbk())
          defrReader::get()->getPartition().defiPartition::setFromCompPin((yyvsp[(4) - (6)].string), (yyvsp[(5) - (6)].string));
      ;}
    break;

  case 725:

/* Line 1455 of yacc.c  */
#line 4021 "def.y"
    {dumb_mode=1; no_num = 1; ;}
    break;

  case 726:

/* Line 1455 of yacc.c  */
#line 4023 "def.y"
    {
        if (defrReader::get()->getPartitionCbk())
          defrReader::get()->getPartition().defiPartition::setFromIOPin((yyvsp[(4) - (5)].string));
      ;}
    break;

  case 727:

/* Line 1455 of yacc.c  */
#line 4027 "def.y"
    {dumb_mode=2; no_num = 2; ;}
    break;

  case 728:

/* Line 1455 of yacc.c  */
#line 4029 "def.y"
    {
        if (defrReader::get()->getPartitionCbk())
          defrReader::get()->getPartition().defiPartition::setToClockPin((yyvsp[(4) - (7)].string), (yyvsp[(5) - (7)].string));
      ;}
    break;

  case 729:

/* Line 1455 of yacc.c  */
#line 4033 "def.y"
    {dumb_mode=2; no_num = 2; ;}
    break;

  case 730:

/* Line 1455 of yacc.c  */
#line 4035 "def.y"
    {
        if (defrReader::get()->getPartitionCbk())
          defrReader::get()->getPartition().defiPartition::setToCompPin((yyvsp[(4) - (6)].string), (yyvsp[(5) - (6)].string));
      ;}
    break;

  case 731:

/* Line 1455 of yacc.c  */
#line 4039 "def.y"
    {dumb_mode=1; no_num = 2; ;}
    break;

  case 732:

/* Line 1455 of yacc.c  */
#line 4040 "def.y"
    {
        if (defrReader::get()->getPartitionCbk())
          defrReader::get()->getPartition().defiPartition::setToIOPin((yyvsp[(4) - (5)].string));
      ;}
    break;

  case 733:

/* Line 1455 of yacc.c  */
#line 4045 "def.y"
    { 
        if (defrReader::get()->getPartitionsExtCbk())
          CALLBACK(defrReader::get()->getPartitionsExtCbk(), defrPartitionsExtCbkType,
                   History_text);
      ;}
    break;

  case 734:

/* Line 1455 of yacc.c  */
#line 4052 "def.y"
    { dumb_mode = 1000000000; no_num = 10000000; ;}
    break;

  case 735:

/* Line 1455 of yacc.c  */
#line 4053 "def.y"
    { dumb_mode = 0; no_num = 0; ;}
    break;

  case 737:

/* Line 1455 of yacc.c  */
#line 4057 "def.y"
    { ;}
    break;

  case 738:

/* Line 1455 of yacc.c  */
#line 4060 "def.y"
    {
        if (defrReader::get()->getPartitionCbk())
          defrReader::get()->getPartition().defiPartition::setMin((yyvsp[(2) - (3)].dval), (yyvsp[(3) - (3)].dval));
      ;}
    break;

  case 739:

/* Line 1455 of yacc.c  */
#line 4065 "def.y"
    {
        if (defrReader::get()->getPartitionCbk())
          defrReader::get()->getPartition().defiPartition::setMax((yyvsp[(2) - (3)].dval), (yyvsp[(3) - (3)].dval));
      ;}
    break;

  case 741:

/* Line 1455 of yacc.c  */
#line 4072 "def.y"
    { if (defrReader::get()->getPartitionCbk()) defrReader::get()->getPartition().defiPartition::addPin((yyvsp[(2) - (2)].string)); ;}
    break;

  case 744:

/* Line 1455 of yacc.c  */
#line 4078 "def.y"
    { if (defrReader::get()->getPartitionCbk()) defrReader::get()->getPartition().defiPartition::addRiseMin((yyvsp[(2) - (2)].dval)); ;}
    break;

  case 745:

/* Line 1455 of yacc.c  */
#line 4080 "def.y"
    { if (defrReader::get()->getPartitionCbk()) defrReader::get()->getPartition().defiPartition::addFallMin((yyvsp[(2) - (2)].dval)); ;}
    break;

  case 746:

/* Line 1455 of yacc.c  */
#line 4082 "def.y"
    { if (defrReader::get()->getPartitionCbk()) defrReader::get()->getPartition().defiPartition::addRiseMax((yyvsp[(2) - (2)].dval)); ;}
    break;

  case 747:

/* Line 1455 of yacc.c  */
#line 4084 "def.y"
    { if (defrReader::get()->getPartitionCbk()) defrReader::get()->getPartition().defiPartition::addFallMax((yyvsp[(2) - (2)].dval)); ;}
    break;

  case 750:

/* Line 1455 of yacc.c  */
#line 4092 "def.y"
    { if (defrReader::get()->getPartitionCbk())
          defrReader::get()->getPartition().defiPartition::addRiseMinRange((yyvsp[(2) - (3)].dval), (yyvsp[(3) - (3)].dval)); ;}
    break;

  case 751:

/* Line 1455 of yacc.c  */
#line 4095 "def.y"
    { if (defrReader::get()->getPartitionCbk())
          defrReader::get()->getPartition().defiPartition::addFallMinRange((yyvsp[(2) - (3)].dval), (yyvsp[(3) - (3)].dval)); ;}
    break;

  case 752:

/* Line 1455 of yacc.c  */
#line 4098 "def.y"
    { if (defrReader::get()->getPartitionCbk())
          defrReader::get()->getPartition().defiPartition::addRiseMaxRange((yyvsp[(2) - (3)].dval), (yyvsp[(3) - (3)].dval)); ;}
    break;

  case 753:

/* Line 1455 of yacc.c  */
#line 4101 "def.y"
    { if (defrReader::get()->getPartitionCbk())
          defrReader::get()->getPartition().defiPartition::addFallMaxRange((yyvsp[(2) - (3)].dval), (yyvsp[(3) - (3)].dval)); ;}
    break;

  case 754:

/* Line 1455 of yacc.c  */
#line 4105 "def.y"
    { if (defrReader::get()->getPartitionsEndCbk())
          CALLBACK(defrReader::get()->getPartitionsEndCbk(), defrPartitionsEndCbkType, 0); ;}
    break;

  case 756:

/* Line 1455 of yacc.c  */
#line 4110 "def.y"
    { ;}
    break;

  case 757:

/* Line 1455 of yacc.c  */
#line 4112 "def.y"
    {dumb_mode=2; no_num = 2; ;}
    break;

  case 758:

/* Line 1455 of yacc.c  */
#line 4114 "def.y"
    {
        /* note that the first T_STRING could be the keyword VPIN */
        if (defrReader::get()->getNetCbk())
          defrReader::get()->getSubnet()->defiSubnet::addPin((yyvsp[(3) - (6)].string), (yyvsp[(4) - (6)].string), (yyvsp[(5) - (6)].integer));
      ;}
    break;

  case 759:

/* Line 1455 of yacc.c  */
#line 4121 "def.y"
    { (yyval.integer) = 0; ;}
    break;

  case 760:

/* Line 1455 of yacc.c  */
#line 4123 "def.y"
    { (yyval.integer) = 1; ;}
    break;

  case 763:

/* Line 1455 of yacc.c  */
#line 4129 "def.y"
    {  
        if (defrReader::get()->getNetCbk()) defrReader::get()->getSubnet()->defiSubnet::addWire((yyvsp[(1) - (1)].string));
      ;}
    break;

  case 764:

/* Line 1455 of yacc.c  */
#line 4133 "def.y"
    {  
        by_is_keyword = FALSE;
        do_is_keyword = FALSE;
        new_is_keyword = FALSE;
        step_is_keyword = FALSE;
        orient_is_keyword = FALSE;
        needNPCbk = 0;
      ;}
    break;

  case 765:

/* Line 1455 of yacc.c  */
#line 4141 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 766:

/* Line 1455 of yacc.c  */
#line 4142 "def.y"
    { if (defrReader::get()->getNetCbk()) defrReader::get()->getSubnet()->defiSubnet::setNonDefault((yyvsp[(3) - (3)].string)); ;}
    break;

  case 767:

/* Line 1455 of yacc.c  */
#line 4145 "def.y"
    { (yyval.string) = (char*)"FIXED"; dumb_mode = 1; ;}
    break;

  case 768:

/* Line 1455 of yacc.c  */
#line 4147 "def.y"
    { (yyval.string) = (char*)"COVER"; dumb_mode = 1; ;}
    break;

  case 769:

/* Line 1455 of yacc.c  */
#line 4149 "def.y"
    { (yyval.string) = (char*)"ROUTED"; dumb_mode = 1; ;}
    break;

  case 770:

/* Line 1455 of yacc.c  */
#line 4151 "def.y"
    { (yyval.string) = (char*)"NOSHIELD"; dumb_mode = 1; ;}
    break;

  case 772:

/* Line 1455 of yacc.c  */
#line 4156 "def.y"
    { if (defrReader::get()->getPinPropStartCbk())
          CALLBACK(defrReader::get()->getPinPropStartCbk(), defrPinPropStartCbkType, ROUND((yyvsp[(2) - (3)].dval))); ;}
    break;

  case 773:

/* Line 1455 of yacc.c  */
#line 4161 "def.y"
    { ;}
    break;

  case 774:

/* Line 1455 of yacc.c  */
#line 4163 "def.y"
    { ;}
    break;

  case 775:

/* Line 1455 of yacc.c  */
#line 4166 "def.y"
    { if (defrReader::get()->getPinPropEndCbk())
          CALLBACK(defrReader::get()->getPinPropEndCbk(), defrPinPropEndCbkType, 0); ;}
    break;

  case 778:

/* Line 1455 of yacc.c  */
#line 4173 "def.y"
    { dumb_mode = 2; no_num = 2; ;}
    break;

  case 779:

/* Line 1455 of yacc.c  */
#line 4174 "def.y"
    { if (defrReader::get()->getPinPropCbk()) defrReader::get()->getPinProp().defiPinProp::setName((yyvsp[(3) - (4)].string), (yyvsp[(4) - (4)].string)); ;}
    break;

  case 780:

/* Line 1455 of yacc.c  */
#line 4176 "def.y"
    { if (defrReader::get()->getPinPropCbk()) {
          CALLBACK(defrReader::get()->getPinPropCbk(), defrPinPropCbkType, &defrReader::get()->getPinProp());
         // reset the property number
         defrReader::get()->getPinProp().defiPinProp::clear();
        }
      ;}
    break;

  case 783:

/* Line 1455 of yacc.c  */
#line 4186 "def.y"
    { dumb_mode = 10000000; parsing_property = 1; ;}
    break;

  case 784:

/* Line 1455 of yacc.c  */
#line 4188 "def.y"
    { dumb_mode = 0; parsing_property = 0; ;}
    break;

  case 787:

/* Line 1455 of yacc.c  */
#line 4195 "def.y"
    {
        if (defrReader::get()->getPinPropCbk()) {
          char propTp;
          char* str = ringCopy("                       ");
          propTp = defrReader::get()->getCompPinProp().defiPropType::propType((yyvsp[(1) - (2)].string));
          CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "PINPROPERTIES");
          /* For backword compatibility, also set the string value */
          /* We will use a temporary string to store the number.
           * The string space is borrowed from the ring buffer
           * in the lexer. */
          sprintf(str, "%g", (yyvsp[(2) - (2)].dval));
          defrReader::get()->getPinProp().defiPinProp::addNumProperty((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].dval), str, propTp);
        }
      ;}
    break;

  case 788:

/* Line 1455 of yacc.c  */
#line 4210 "def.y"
    {
        if (defrReader::get()->getPinPropCbk()) {
          char propTp;
          propTp = defrReader::get()->getCompPinProp().defiPropType::propType((yyvsp[(1) - (2)].string));
          CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "PINPROPERTIES");
          defrReader::get()->getPinProp().defiPinProp::addProperty((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].string), propTp);
        }
      ;}
    break;

  case 789:

/* Line 1455 of yacc.c  */
#line 4219 "def.y"
    {
        if (defrReader::get()->getPinPropCbk()) {
          char propTp;
          propTp = defrReader::get()->getCompPinProp().defiPropType::propType((yyvsp[(1) - (2)].string));
          CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "PINPROPERTIES");
          defrReader::get()->getPinProp().defiPinProp::addProperty((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].string), propTp);
        }
      ;}
    break;

  case 791:

/* Line 1455 of yacc.c  */
#line 4231 "def.y"
    { if (defrReader::get()->getBlockageStartCbk())
          CALLBACK(defrReader::get()->getBlockageStartCbk(), defrBlockageStartCbkType, ROUND((yyvsp[(2) - (3)].dval))); ;}
    break;

  case 792:

/* Line 1455 of yacc.c  */
#line 4235 "def.y"
    { if (defrReader::get()->getBlockageEndCbk())
          CALLBACK(defrReader::get()->getBlockageEndCbk(), defrBlockageEndCbkType, 0); ;}
    break;

  case 795:

/* Line 1455 of yacc.c  */
#line 4244 "def.y"
    {
        if (defrReader::get()->getBlockageCbk()) {
          CALLBACK(defrReader::get()->getBlockageCbk(), defrBlockageCbkType, &defrReader::get()->getBlockage());
          defrReader::get()->getBlockage().defiBlockage::clear();
        }
      ;}
    break;

  case 796:

/* Line 1455 of yacc.c  */
#line 4251 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 797:

/* Line 1455 of yacc.c  */
#line 4252 "def.y"
    {
        if (defrReader::get()->getBlockageCbk()) {
          if (defrReader::get()->getBlockage().defiBlockage::hasPlacement() != 0) {
            if (blockageWarnings++ < defrReader::get()->getBlockageWarnings()) {
              defError(6539, "The BLOCKAGE statement has both the LAYER & the PLACEMENT statements defined.\nThis is illegal.");
              CHKERR();
            }
          }
          defrReader::get()->getBlockage().defiBlockage::setLayer((yyvsp[(4) - (4)].string));
          defrReader::get()->getBlockage().defiBlockage::clearPoly(); // free poly, if any
        }
        hasBlkLayerComp = 0;
        hasBlkLayerSpac = 0;
      ;}
    break;

  case 799:

/* Line 1455 of yacc.c  */
#line 4269 "def.y"
    {
        if (defrReader::get()->getBlockageCbk()) {
          if (defrReader::get()->getBlockage().defiBlockage::hasLayer() != 0) {
            if (blockageWarnings++ < defrReader::get()->getBlockageWarnings()) {
              defError(6539, "The BLOCKAGE statement has both the LAYER & the PLACEMENT statements defined.");
              CHKERR();
            }
          }
          defrReader::get()->getBlockage().defiBlockage::setPlacement();
          defrReader::get()->getBlockage().defiBlockage::clearPoly(); // free poly, if any
        }
        hasBlkPlaceComp = 0;
      ;}
    break;

  case 803:

/* Line 1455 of yacc.c  */
#line 4289 "def.y"
    {
        if (defVersionNum < 5.6) {
          if (defrReader::get()->getBlockageCbk()) {
            if (blockageWarnings++ < defrReader::get()->getBlockageWarnings()) {
              defMsg = (char*)defMalloc(1000);
              sprintf (defMsg,
                 "SPACING statement is a version 5.6 and later syntax.\nYour def file is defined with version %g", defVersionNum);
              defError(6540, defMsg);
              defFree(defMsg);
              CHKERR();
            }
          }
        } else if (hasBlkLayerSpac) {
          if (defrReader::get()->getBlockageCbk()) {
            if (blockageWarnings++ < defrReader::get()->getBlockageWarnings()) {
              defError(6541, "A SPACING statement is defined in the LAYER statement,\nbut there is already either a SPACING statement or DESIGNRULEWIDTH  statement has defined in the layer.\nThis is illegal");
              CHKERR();
            }
          }
        } else {
          if (defrReader::get()->getBlockageCbk())
            defrReader::get()->getBlockage().defiBlockage::setSpacing((int)(yyvsp[(3) - (3)].dval));
          hasBlkLayerSpac = 1;
        }
      ;}
    break;

  case 804:

/* Line 1455 of yacc.c  */
#line 4315 "def.y"
    {
        if (defVersionNum < 5.6) {
          if (defrReader::get()->getBlockageCbk()) {
            if (blockageWarnings++ < defrReader::get()->getBlockageWarnings()) {
              defError(6541, "A SPACING statement is defined in the LAYER statement,\nbut there is already either a SPACING statement or DESIGNRULEWIDTH  statement has defined in the layer.\nThis is illegal");
              CHKERR();
            }
          }
        } else if (hasBlkLayerSpac) {
          if (defrReader::get()->getBlockageCbk()) {
            if (blockageWarnings++ < defrReader::get()->getBlockageWarnings()) {
              defError(6541, "A SPACING statement is defined in the LAYER statement,\nbut there is already either a SPACING statement or DESIGNRULEWIDTH  statement has defined in the layer.\nThis is illegal");
              CHKERR();
            }
          }
        } else {
          if (defrReader::get()->getBlockageCbk())
            defrReader::get()->getBlockage().defiBlockage::setDesignRuleWidth((int)(yyvsp[(3) - (3)].dval));
          hasBlkLayerSpac = 1;
        }
      ;}
    break;

  case 806:

/* Line 1455 of yacc.c  */
#line 4340 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 807:

/* Line 1455 of yacc.c  */
#line 4341 "def.y"
    {
        if (hasBlkLayerComp) {
          if (defrReader::get()->getBlockageCbk()) {
            if (blockageWarnings++ < defrReader::get()->getBlockageWarnings()) {
              defError(6542, "The BLOCKAGES COMPONENT statement already has either COMPONENT, SLOTS, FILLS, PUSHDOWN or EXCEPTPGNET defined.\nOnly one of these statement is allowed per LAYER.");
              CHKERR();
            }
          }
        } else {
          if (defrReader::get()->getBlockageCbk())
            defrReader::get()->getBlockage().defiBlockage::setComponent((yyvsp[(4) - (4)].string));
          hasBlkLayerComp = 1;
        }
      ;}
    break;

  case 808:

/* Line 1455 of yacc.c  */
#line 4357 "def.y"
    {
        if (hasBlkLayerComp) {
          if (defrReader::get()->getBlockageCbk()) {
            if (blockageWarnings++ < defrReader::get()->getBlockageWarnings()) {
              defError(6542, "The BLOCKAGES COMPONENT statement already has either COMPONENT, SLOTS, FILLS, PUSHDOWN or EXCEPTPGNET defined.\nOnly one of these statement is allowed per LAYER.");
              CHKERR();
            }
          }
        } else {
          if (defrReader::get()->getBlockageCbk())
            defrReader::get()->getBlockage().defiBlockage::setSlots();
          hasBlkLayerComp = 1;
        }
      ;}
    break;

  case 809:

/* Line 1455 of yacc.c  */
#line 4372 "def.y"
    {
        if (hasBlkLayerComp) {
          if (defrReader::get()->getBlockageCbk()) {
            if (blockageWarnings++ < defrReader::get()->getBlockageWarnings()) {
              defError(6542, "The BLOCKAGES COMPONENT statement already has either COMPONENT, SLOTS, FILLS, PUSHDOWN or EXCEPTPGNET defined.\nOnly one of these statement is allowed per LAYER.");
              CHKERR();
            }
          }
        } else {
          if (defrReader::get()->getBlockageCbk())
            defrReader::get()->getBlockage().defiBlockage::setFills();
          hasBlkLayerComp = 1;
        }
      ;}
    break;

  case 810:

/* Line 1455 of yacc.c  */
#line 4387 "def.y"
    {
        if (hasBlkLayerComp) {
          if (defrReader::get()->getBlockageCbk()) {
            if (blockageWarnings++ < defrReader::get()->getBlockageWarnings()) {
              defError(6542, "The BLOCKAGES COMPONENT statement already has either COMPONENT, SLOTS, FILLS, PUSHDOWN or EXCEPTPGNET defined.\nOnly one of these statement is allowed per LAYER.");
              CHKERR();
            }
          }
        } else {
          if (defrReader::get()->getBlockageCbk())
            defrReader::get()->getBlockage().defiBlockage::setPushdown();
          hasBlkLayerComp = 1;
        }
      ;}
    break;

  case 811:

/* Line 1455 of yacc.c  */
#line 4402 "def.y"
    {
        if (defVersionNum < 5.7) {
           if (defrReader::get()->getBlockageCbk()) {
             if (blockageWarnings++ < defrReader::get()->getBlockageWarnings()) {
               defMsg = (char*)defMalloc(10000);
               sprintf (defMsg,
                 "EXCEPTPGNET is a version 5.7 or later syntax.\nYour def file is defined with version %g.", defVersionNum);
               defError(6549, defMsg);
               defFree(defMsg);
               CHKERR();
              }
           }
        } else {
           if (hasBlkLayerComp) {
             if (defrReader::get()->getBlockageCbk()) {
               if (blockageWarnings++ < defrReader::get()->getBlockageWarnings()) {
                 defError(6542, "The BLOCKAGES COMPONENT statement already has either COMPONENT, SLOTS, FILLS, PUSHDOWN or EXCEPTPGNET defined.\nOnly one of these statement is allowed per LAYER.");
                 CHKERR();
               }
             }
           } else {
             if (defrReader::get()->getBlockageCbk())
               defrReader::get()->getBlockage().defiBlockage::setExceptpgnet();
             hasBlkLayerComp = 1;
           }
        }
      ;}
    break;

  case 813:

/* Line 1455 of yacc.c  */
#line 4432 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 814:

/* Line 1455 of yacc.c  */
#line 4433 "def.y"
    {
        if (hasBlkPlaceComp) {
          if (defrReader::get()->getBlockageCbk()) {
            if (blockageWarnings++ < defrReader::get()->getBlockageWarnings()) {
              defError(6543, "The BLOCKAGES PLACEMNET statement already has either COMPONENT, PUSHDOWN, SOFT or PARTIAL defined.\nOnly one of these statement is allowed per LAYER.");
              CHKERR();
            }
          }
        } else {
          if (defrReader::get()->getBlockageCbk())
            defrReader::get()->getBlockage().defiBlockage::setComponent((yyvsp[(4) - (4)].string));
          hasBlkPlaceComp = 1;
        }
      ;}
    break;

  case 815:

/* Line 1455 of yacc.c  */
#line 4448 "def.y"
    {
        if (hasBlkPlaceComp) {
          if (defrReader::get()->getBlockageCbk()) {
            if (blockageWarnings++ < defrReader::get()->getBlockageWarnings()) {
              defError(6543, "The BLOCKAGES PLACEMNET statement already has either COMPONENT, PUSHDOWN, SOFT or PARTIAL defined.\nOnly one of these statement is allowed per LAYER.");
              CHKERR();
            }
          }
        } else {
          if (defrReader::get()->getBlockageCbk())
            defrReader::get()->getBlockage().defiBlockage::setPushdown();
          hasBlkPlaceComp = 1;
        }
      ;}
    break;

  case 816:

/* Line 1455 of yacc.c  */
#line 4463 "def.y"
    {
        if (defVersionNum < 5.7) {
           if (defrReader::get()->getBlockageCbk()) {
             if (blockageWarnings++ < defrReader::get()->getBlockageWarnings()) {
               defMsg = (char*)defMalloc(10000);
               sprintf (defMsg,
                 "PLACEMENT SOFT is a version 5.7 or later syntax.\nYour def file is defined with version %g.", defVersionNum);
               defError(6547, defMsg);
               defFree(defMsg);
               CHKERR();
             }
           }
        } else {
           if (hasBlkPlaceComp) {
             if (defrReader::get()->getBlockageCbk()) {
               if (blockageWarnings++ < defrReader::get()->getBlockageWarnings()) {
                 defError(6543, "The BLOCKAGES PLACEMNET statement already has either COMPONENT, PUSHDOWN, SOFT or PARTIAL defined.\nOnly one of these statement is allowed per LAYER.");
                 CHKERR();
               }
             }
           } else {
             if (defrReader::get()->getBlockageCbk())
               defrReader::get()->getBlockage().defiBlockage::setSoft();
             hasBlkPlaceComp = 1;
           }
        }
      ;}
    break;

  case 817:

/* Line 1455 of yacc.c  */
#line 4491 "def.y"
    {
        if (defVersionNum < 5.7) {
           if (defrReader::get()->getBlockageCbk()) {
             if (blockageWarnings++ < defrReader::get()->getBlockageWarnings()) {
                defMsg = (char*)defMalloc(10000);
                sprintf (defMsg,
                  "PARTIAL is a version 5.7 or later syntax.\nYour def file is defined with version %g.", defVersionNum);
                defError(6548, defMsg);
                defFree(defMsg);
                CHKERR();
             }
           }
        } else {
           if (hasBlkPlaceComp) {
             if (defrReader::get()->getBlockageCbk()) {
               if (blockageWarnings++ < defrReader::get()->getBlockageWarnings()) {
                 defError(6543, "The BLOCKAGES PLACEMNET statement already has either COMPONENT, PUSHDOWN, SOFT or PARTIAL defined.\nOnly one of these statement is allowed per LAYER.");
                 CHKERR();
               }
             }
           } else {
             if (defrReader::get()->getBlockageCbk())
               defrReader::get()->getBlockage().defiBlockage::setPartial((yyvsp[(3) - (3)].dval));
             hasBlkPlaceComp = 1;
           }
         }
      ;}
    break;

  case 820:

/* Line 1455 of yacc.c  */
#line 4524 "def.y"
    {
        if (defrReader::get()->getBlockageCbk())
          defrReader::get()->getBlockage().defiBlockage::addRect((yyvsp[(2) - (3)].pt).x, (yyvsp[(2) - (3)].pt).y, (yyvsp[(3) - (3)].pt).x, (yyvsp[(3) - (3)].pt).y);
      ;}
    break;

  case 821:

/* Line 1455 of yacc.c  */
#line 4529 "def.y"
    {
        if (defrReader::get()->getBlockageCbk()) {
          if (!defrReader::get()->getGeomPtr()) {
            defrReader::get()->setGeomPtr((defiGeometries*)defMalloc(sizeof(defiGeometries)));
            defrReader::get()->getGeomPtr()->defiGeometries::Init();
          } else   // Just reset the number of points
            defrReader::get()->getGeomPtr()->defiGeometries::Reset();
        }
      ;}
    break;

  case 822:

/* Line 1455 of yacc.c  */
#line 4539 "def.y"
    {
        if (defrReader::get()->getBlockageCbk()) {
          if (defVersionNum >= 5.6) {  // only 5.6 and beyond
            if (defrReader::get()->getBlockage().defiBlockage::hasLayer()) {  // only in layer
              if (defrReader::get()->getBlockageCbk())
                defrReader::get()->getBlockage().defiBlockage::addPolygon(defrReader::get()->getGeomPtr());
            } else {
              if (defrReader::get()->getBlockageCbk()) {
                if (blockageWarnings++ < defrReader::get()->getBlockageWarnings()) {
                  defError(6544, "A POLYGON statement is defined in the BLOCKAGE statement,\nbut it is not defined in the BLOCKAGE LAYER statement.\nThis is illegal");
                  CHKERR();
                }
              }
            }
          }
        }
      ;}
    break;

  case 824:

/* Line 1455 of yacc.c  */
#line 4561 "def.y"
    { if (defrReader::get()->getSlotStartCbk())
          CALLBACK(defrReader::get()->getSlotStartCbk(), defrSlotStartCbkType, ROUND((yyvsp[(2) - (3)].dval))); ;}
    break;

  case 825:

/* Line 1455 of yacc.c  */
#line 4565 "def.y"
    { if (defrReader::get()->getSlotEndCbk())
          CALLBACK(defrReader::get()->getSlotEndCbk(), defrSlotEndCbkType, 0); ;}
    break;

  case 828:

/* Line 1455 of yacc.c  */
#line 4573 "def.y"
    {
        if (defrReader::get()->getSlotCbk()) {
          CALLBACK(defrReader::get()->getSlotCbk(), defrSlotCbkType, &defrReader::get()->getSlot());
          defrReader::get()->getSlot().defiSlot::clear();
        }
      ;}
    break;

  case 829:

/* Line 1455 of yacc.c  */
#line 4580 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 830:

/* Line 1455 of yacc.c  */
#line 4581 "def.y"
    {
        if (defrReader::get()->getSlotCbk()) {
          defrReader::get()->getSlot().defiSlot::setLayer((yyvsp[(4) - (4)].string));
          defrReader::get()->getSlot().defiSlot::clearPoly();     // free poly, if any
        }
      ;}
    break;

  case 834:

/* Line 1455 of yacc.c  */
#line 4593 "def.y"
    {
        if (defrReader::get()->getSlotCbk())
          defrReader::get()->getSlot().defiSlot::addRect((yyvsp[(2) - (3)].pt).x, (yyvsp[(2) - (3)].pt).y, (yyvsp[(3) - (3)].pt).x, (yyvsp[(3) - (3)].pt).y);
      ;}
    break;

  case 835:

/* Line 1455 of yacc.c  */
#line 4598 "def.y"
    {
        if (!defrReader::get()->getGeomPtr()) {
          defrReader::get()->setGeomPtr((defiGeometries*)defMalloc(sizeof(defiGeometries)));
          defrReader::get()->getGeomPtr()->defiGeometries::Init();
        } else   // Just reset the number of points
          defrReader::get()->getGeomPtr()->defiGeometries::Reset();
      ;}
    break;

  case 836:

/* Line 1455 of yacc.c  */
#line 4606 "def.y"
    {
        if (defVersionNum >= 5.6) {  // only 5.6 and beyond
          if (defrReader::get()->getSlotCbk())
            defrReader::get()->getSlot().defiSlot::addPolygon(defrReader::get()->getGeomPtr());
        }
      ;}
    break;

  case 838:

/* Line 1455 of yacc.c  */
#line 4617 "def.y"
    { if (defrReader::get()->getFillStartCbk())
          CALLBACK(defrReader::get()->getFillStartCbk(), defrFillStartCbkType, ROUND((yyvsp[(2) - (3)].dval))); ;}
    break;

  case 839:

/* Line 1455 of yacc.c  */
#line 4621 "def.y"
    { if (defrReader::get()->getFillEndCbk())
          CALLBACK(defrReader::get()->getFillEndCbk(), defrFillEndCbkType, 0); ;}
    break;

  case 842:

/* Line 1455 of yacc.c  */
#line 4629 "def.y"
    {
        if (defrReader::get()->getFillCbk()) {
          CALLBACK(defrReader::get()->getFillCbk(), defrFillCbkType, &defrReader::get()->getFill());
          defrReader::get()->getFill().defiFill::clear();
        }
      ;}
    break;

  case 843:

/* Line 1455 of yacc.c  */
#line 4635 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 844:

/* Line 1455 of yacc.c  */
#line 4636 "def.y"
    {
        if (defrReader::get()->getFillCbk()) {
          defrReader::get()->getFill().defiFill::setVia((yyvsp[(4) - (4)].string));
          defrReader::get()->getFill().defiFill::clearPts();
          if (!defrReader::get()->getGeomPtr()) {
            defrReader::get()->setGeomPtr((defiGeometries*)defMalloc(sizeof(defiGeometries)));
            defrReader::get()->getGeomPtr()->defiGeometries::Init();
          } else   // Just reset the number of points
            defrReader::get()->getGeomPtr()->defiGeometries::Reset();
        }
      ;}
    break;

  case 845:

/* Line 1455 of yacc.c  */
#line 4648 "def.y"
    {
        if (defrReader::get()->getFillCbk()) {
          defrReader::get()->getFill().defiFill::addPts(defrReader::get()->getGeomPtr());
          CALLBACK(defrReader::get()->getFillCbk(), defrFillCbkType, &defrReader::get()->getFill());
          defrReader::get()->getFill().defiFill::clear();
        }
      ;}
    break;

  case 846:

/* Line 1455 of yacc.c  */
#line 4656 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 847:

/* Line 1455 of yacc.c  */
#line 4657 "def.y"
    {
        if (defrReader::get()->getFillCbk()) {
          defrReader::get()->getFill().defiFill::setLayer((yyvsp[(4) - (4)].string));
          defrReader::get()->getFill().defiFill::clearPoly();    // free poly, if any
        }
      ;}
    break;

  case 851:

/* Line 1455 of yacc.c  */
#line 4669 "def.y"
    {
        if (defrReader::get()->getFillCbk())
          defrReader::get()->getFill().defiFill::addRect((yyvsp[(2) - (3)].pt).x, (yyvsp[(2) - (3)].pt).y, (yyvsp[(3) - (3)].pt).x, (yyvsp[(3) - (3)].pt).y);
      ;}
    break;

  case 852:

/* Line 1455 of yacc.c  */
#line 4674 "def.y"
    {
        if (!defrReader::get()->getGeomPtr()) {
          defrReader::get()->setGeomPtr((defiGeometries*)defMalloc(sizeof(defiGeometries)));
          defrReader::get()->getGeomPtr()->defiGeometries::Init();
        } else   // Just reset the number of points
          defrReader::get()->getGeomPtr()->defiGeometries::Reset();
      ;}
    break;

  case 853:

/* Line 1455 of yacc.c  */
#line 4682 "def.y"
    {
        if (defVersionNum >= 5.6) {  // only 5.6 and beyond
          if (defrReader::get()->getFillCbk())
            defrReader::get()->getFill().defiFill::addPolygon(defrReader::get()->getGeomPtr());
        } else {
            defMsg = (char*)defMalloc(10000);
            sprintf (defMsg,
              "POLYGON statement in FILLS LAYER is a version 5.6 and later syntax.\nYour def file is defined with version %g", defVersionNum);
            defError(6564, defMsg);
            defFree(defMsg);
            CHKERR();
        }
      ;}
    break;

  case 854:

/* Line 1455 of yacc.c  */
#line 4697 "def.y"
    {;}
    break;

  case 855:

/* Line 1455 of yacc.c  */
#line 4699 "def.y"
    {
        if (defVersionNum < 5.7) {
           if (defrReader::get()->getFillCbk()) {
             if (fillWarnings++ < defrReader::get()->getFillWarnings()) {
               defMsg = (char*)defMalloc(10000);
               sprintf (defMsg,
                 "LAYER OPC is a version 5.7 or later syntax.\nYour def file is defined with version %g.", defVersionNum);
               defError(6553, defMsg);
               defFree(defMsg);
               CHKERR();
             }
           }
        } else {
           if (defrReader::get()->getFillCbk())
             defrReader::get()->getFill().defiFill::setLayerOpc();
        }
      ;}
    break;

  case 856:

/* Line 1455 of yacc.c  */
#line 4718 "def.y"
    {;}
    break;

  case 857:

/* Line 1455 of yacc.c  */
#line 4720 "def.y"
    {
        if (defVersionNum < 5.7) {
           if (defrReader::get()->getFillCbk()) {
             if (fillWarnings++ < defrReader::get()->getFillWarnings()) {
               defMsg = (char*)defMalloc(10000);
               sprintf (defMsg,
                 "VIA OPC is a version 5.7 or later syntax.\nYour def file is defined with version %g.", defVersionNum);
               defError(6554, defMsg);
               defFree(defMsg);
               CHKERR();
             }
           }
        } else {
           if (defrReader::get()->getFillCbk())
             defrReader::get()->getFill().defiFill::setViaOpc();
        }
      ;}
    break;

  case 859:

/* Line 1455 of yacc.c  */
#line 4743 "def.y"
    { 
        if (defVersionNum < 5.6) {
          if (defrReader::get()->getNonDefaultStartCbk()) {
            if (nonDefaultWarnings++ < defrReader::get()->getNonDefaultWarnings()) {
              defMsg = (char*)defMalloc(1000);
              sprintf (defMsg,
                 "NONDEFAULTRULE statement is a version 5.6 and later syntax.\nYour def file is defined with version %g", defVersionNum);
              defError(6545, defMsg);
              defFree(defMsg);
              CHKERR();
            }
          }
        } else if (defrReader::get()->getNonDefaultStartCbk())
          CALLBACK(defrReader::get()->getNonDefaultStartCbk(), defrNonDefaultStartCbkType,
                   ROUND((yyvsp[(2) - (3)].dval)));
      ;}
    break;

  case 860:

/* Line 1455 of yacc.c  */
#line 4761 "def.y"
    { if (defrReader::get()->getNonDefaultEndCbk())
          CALLBACK(defrReader::get()->getNonDefaultEndCbk(), defrNonDefaultEndCbkType, 0); ;}
    break;

  case 863:

/* Line 1455 of yacc.c  */
#line 4768 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 864:

/* Line 1455 of yacc.c  */
#line 4769 "def.y"
    {
        if (defrReader::get()->getNonDefaultCbk()) {
          defrReader::get()->getNonDefault().defiNonDefault::clear(); 
          defrReader::get()->getNonDefault().defiNonDefault::setName((yyvsp[(3) - (3)].string));
        }
      ;}
    break;

  case 865:

/* Line 1455 of yacc.c  */
#line 4776 "def.y"
    { if (defrReader::get()->getNonDefaultCbk())
          CALLBACK(defrReader::get()->getNonDefaultCbk(), defrNonDefaultCbkType, &defrReader::get()->getNonDefault()); ;}
    break;

  case 868:

/* Line 1455 of yacc.c  */
#line 4784 "def.y"
    {
        if (defrReader::get()->getNonDefaultCbk())
          defrReader::get()->getNonDefault().defiNonDefault::setHardspacing();
      ;}
    break;

  case 869:

/* Line 1455 of yacc.c  */
#line 4788 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 870:

/* Line 1455 of yacc.c  */
#line 4790 "def.y"
    {
        if (defrReader::get()->getNonDefaultCbk()) {
          defrReader::get()->getNonDefault().defiNonDefault::addLayer((yyvsp[(4) - (6)].string));
          defrReader::get()->getNonDefault().defiNonDefault::addWidth((yyvsp[(6) - (6)].dval));
        }
      ;}
    break;

  case 872:

/* Line 1455 of yacc.c  */
#line 4797 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 873:

/* Line 1455 of yacc.c  */
#line 4798 "def.y"
    {
        if (defrReader::get()->getNonDefaultCbk()) {
          defrReader::get()->getNonDefault().defiNonDefault::addVia((yyvsp[(4) - (4)].string));
        }
      ;}
    break;

  case 874:

/* Line 1455 of yacc.c  */
#line 4803 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 875:

/* Line 1455 of yacc.c  */
#line 4804 "def.y"
    {
        if (defrReader::get()->getNonDefaultCbk()) {
          defrReader::get()->getNonDefault().defiNonDefault::addViaRule((yyvsp[(4) - (4)].string));
        }
      ;}
    break;

  case 876:

/* Line 1455 of yacc.c  */
#line 4809 "def.y"
    { dumb_mode = 1; no_num = 1; ;}
    break;

  case 877:

/* Line 1455 of yacc.c  */
#line 4810 "def.y"
    {
        if (defrReader::get()->getNonDefaultCbk()) {
          defrReader::get()->getNonDefault().defiNonDefault::addMinCuts((yyvsp[(4) - (5)].string), (int)(yyvsp[(5) - (5)].dval));
        }
      ;}
    break;

  case 881:

/* Line 1455 of yacc.c  */
#line 4823 "def.y"
    {
        if (defrReader::get()->getNonDefaultCbk()) {
          defrReader::get()->getNonDefault().defiNonDefault::addDiagWidth((yyvsp[(2) - (2)].dval));
        }
      ;}
    break;

  case 882:

/* Line 1455 of yacc.c  */
#line 4829 "def.y"
    {
        if (defrReader::get()->getNonDefaultCbk()) {
          defrReader::get()->getNonDefault().defiNonDefault::addSpacing((yyvsp[(2) - (2)].dval));
        }
      ;}
    break;

  case 883:

/* Line 1455 of yacc.c  */
#line 4835 "def.y"
    {
        if (defrReader::get()->getNonDefaultCbk()) {
          defrReader::get()->getNonDefault().defiNonDefault::addWireExt((yyvsp[(2) - (2)].dval));
        }
      ;}
    break;

  case 884:

/* Line 1455 of yacc.c  */
#line 4842 "def.y"
    { dumb_mode = 10000; parsing_property = 1; ;}
    break;

  case 885:

/* Line 1455 of yacc.c  */
#line 4844 "def.y"
    { dumb_mode = 0; parsing_property = 0; ;}
    break;

  case 888:

/* Line 1455 of yacc.c  */
#line 4851 "def.y"
    {
        if (defrReader::get()->getNonDefaultCbk()) {
          char propTp;
          char* str = ringCopy("                       ");
          propTp = defrReader::get()->getNDefProp().defiPropType::propType((yyvsp[(1) - (2)].string));
          CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "NONDEFAULTRULE");
          sprintf(str, "%g", (yyvsp[(2) - (2)].dval));
          defrReader::get()->getNonDefault().defiNonDefault::addNumProperty((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].dval), str, propTp);
        }
      ;}
    break;

  case 889:

/* Line 1455 of yacc.c  */
#line 4862 "def.y"
    {
        if (defrReader::get()->getNonDefaultCbk()) {
          char propTp;
          propTp = defrReader::get()->getNDefProp().defiPropType::propType((yyvsp[(1) - (2)].string));
          CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "NONDEFAULTRULE");
          defrReader::get()->getNonDefault().defiNonDefault::addProperty((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].string), propTp);
        }
      ;}
    break;

  case 890:

/* Line 1455 of yacc.c  */
#line 4871 "def.y"
    {
        if (defrReader::get()->getNonDefaultCbk()) {
          char propTp;
          propTp = defrReader::get()->getNDefProp().defiPropType::propType((yyvsp[(1) - (2)].string));
          CHKPROPTYPE(propTp, (yyvsp[(1) - (2)].string), "NONDEFAULTRULE");
          defrReader::get()->getNonDefault().defiNonDefault::addProperty((yyvsp[(1) - (2)].string), (yyvsp[(2) - (2)].string), propTp);
        }
      ;}
    break;

  case 892:

/* Line 1455 of yacc.c  */
#line 4884 "def.y"
    {
        if (defVersionNum < 5.6) {
          if (defrReader::get()->getStylesStartCbk()) {
            if (stylesWarnings++ < defrReader::get()->getStylesWarnings()) {
              defMsg = (char*)defMalloc(1000);
              sprintf (defMsg,
                 "STYLES statement is a version 5.6 and later syntax.\nYour def file is defined with version %g", defVersionNum);
              defError(6546, defMsg);
              defFree(defMsg);
              CHKERR();
            }
          }
        } else if (defrReader::get()->getStylesStartCbk())
          CALLBACK(defrReader::get()->getStylesStartCbk(), defrStylesStartCbkType, ROUND((yyvsp[(2) - (3)].dval)));
      ;}
    break;

  case 893:

/* Line 1455 of yacc.c  */
#line 4901 "def.y"
    { if (defrReader::get()->getStylesEndCbk())
          CALLBACK(defrReader::get()->getStylesEndCbk(), defrStylesEndCbkType, 0); ;}
    break;

  case 896:

/* Line 1455 of yacc.c  */
#line 4909 "def.y"
    {
        if (defrReader::get()->getStylesCbk()) defrReader::get()->getStyles().defiStyles::setStyle((int)(yyvsp[(3) - (3)].dval));
        if (!defrReader::get()->getGeomPtr()) {
          defrReader::get()->setGeomPtr((defiGeometries*)defMalloc(sizeof(defiGeometries)));
          defrReader::get()->getGeomPtr()->defiGeometries::Init();
        } else   // Just reset the number of points
          defrReader::get()->getGeomPtr()->defiGeometries::Reset();
      ;}
    break;

  case 897:

/* Line 1455 of yacc.c  */
#line 4918 "def.y"
    {
        if (defVersionNum >= 5.6) {  // only 5.6 and beyond will call the callback
          if (defrReader::get()->getStylesCbk()) {
            defrReader::get()->getStyles().defiStyles::setPolygon(defrReader::get()->getGeomPtr());
            CALLBACK(defrReader::get()->getStylesCbk(), defrStylesCbkType, &defrReader::get()->getStyles());
          }
        }
      ;}
    break;



/* Line 1455 of yacc.c  */
#line 10894 "def.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined(yyoverflow) || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



/* Line 1675 of yacc.c  */
#line 4927 "def.y"



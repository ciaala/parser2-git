#include "rwxml.hpp"
#define UNKNOWN 0
#define TDEF 1
#define TLEF 2
#define TXML 3
#include "xmlDef/XmlParser.hpp"

using namespace std;
int file_format ( char* filename )
{
    int length = strlen ( filename );
    char* extension = filename + length - 3;
    if ( strncasecmp ( extension,"def",3 ) ==0 )
    {
        return TDEF;
    }
    else if ( strncasecmp ( extension, "lef",3 ) ==0 )
    {
        return TLEF;
    }
    else if ( strncasecmp ( extension, "xml",3 ) ==0 )
    {
        return TXML;
    }
    return UNKNOWN;
}

int main ( int argc, char **argv )
{
    bool debug = false;
    //TODO modificare controllato la dimensione dell'array.
    if ((argc>=2) && strcmp ( argv[1],"-d" ) == 0 )
    {
        debug = true;
        argv=&argv[1];
        argc--;
    }
    if ( argc == 3 )
    {
        int type1 = file_format ( argv[1] );
        int type2 = file_format ( argv[2] );
        deflef::Document* document;
        if ( type2 == TXML )
        {
            switch ( type1 )
            {
            case TDEF:
                document = new deflef::Def ( argv[1] );
                break;
            case TLEF:
                document = new deflef::Lef ( argv[1] );
                break;
            default:
                std::cout << "Error: "<< argv[1]<< " should be of type and extension def, lef or xml"<< std::endl;
                break;
            }
            document->read();
            if ( debug )
            {
                document->getDocument()->write_to_stream_formatted ( cout );
            }
            else
            {
                document->getDocument()->write_to_file_formatted ( argv[2] );
            }

        }
        else if ( type1 == TXML )
        {
            deflef::XmlParser* parser = new deflef::XmlParser();
            parser->read_file(argv[1]);
            
            parser->write_document(argv[2]);
            delete parser;
        }
        else
        {
            std::cout << "Error: "<< argv[1] << " and " << argv[2] << " should be of type and extension def, lef or xml"<< std::endl;
        }

    }
    else
    {
        cout << "Error: rwxml [-d] <inputfile> <outputfile>" << std::endl;
    }
    return 0;
}

// kate: indent-mode cstyle; space-indent on; indent-width 4; 

#include "document.hpp"
	
namespace deflef{
  
Document::Document(){
	document.set_internal_subset("example_xml_doc", "" ,"example_xml_doc.dtd");
	document.create_root_node("exampleroot", "http:://foo", "foo");
	
	
}

xmlpp::Document* Document::getDocument(){
	return &document;
}
}
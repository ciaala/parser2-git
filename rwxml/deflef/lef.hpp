#ifndef LEF_HPP
#define LEF_HPP
#include "document.hpp"
#include "../common/commonCallbacks.hpp"
#include "../lef/library.hpp"


namespace deflef {
	class Lef: public Document {
	  public:
		Lef(char* file);
		void read();
		void setupCallbacks();
		void write_lef(string file);
		void write_xml(string file);
	};
  
}
#endif

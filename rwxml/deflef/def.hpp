#ifndef DEF_HPP
#define DEF_HPP

#include "document.hpp"
#include "../common/commonCallbacks.hpp"
#include "../def/design_top.hpp"
#include "../def/design_bottom.hpp"
#include "../def/design_net.hpp"

namespace deflef {

  class Def: public Document {
	  public:
		Def(char* file);
		void setupCallbacks();
		void read();
		void write_def(string file);
		void write_xml(string file);
       
	};
	
}
#endif

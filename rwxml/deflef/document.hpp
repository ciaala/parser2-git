
#ifndef DOCUMENT_HPP
#define DOCUMENT_HPP
#include "../include/allin.hpp"

namespace deflef {
  
class Document{
	protected :
	  xmlpp::Document document;
	  string filename;
	public :
	  Document();
	  xmlpp::Document* getDocument();
	  virtual void read()=0;
};
  
}
#endif
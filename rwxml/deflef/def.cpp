#include "def.hpp"


/**
* DefXml is used to read a file containg DEF.
*
*/
namespace deflef
{
void Def::setupCallbacks()
{

    setupCommonHeaderCallbacks();
    setupDefDesignTopCallbacks();
    setupDefDesignBottomCallbacks();
    setupDefDesignNetCallbacks();

}

Def::Def ( char* file )
{
    this->filename = file;
    this->setupCallbacks();
    defrInit();
}

void Def::read()
{
    defrRead ( fopen ( this->filename.c_str(),"r" ),this->filename.c_str(), ( void* ) document.get_root_node(), 0 );
}

void Def::write_def ( string file )
{

}
void Def::write_xml ( string file )
{

}

}
// kate: indent-mode cstyle; space-indent on; indent-width 4; 

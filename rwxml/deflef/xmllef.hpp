#include "document.hpp"
#ifndef XMLLEF_HPP
#define XMLLEF_HPP

// deflef::Document* lef_xml(char* inputfile, char* outputfile);


namespace deflef {
	class XmlLef: public Document {
	  public:
		XmlLef(char* file);
		void read();
	};
  
}
#endif

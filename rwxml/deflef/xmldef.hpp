#include "document.hpp"
#ifndef XMLDEF_HPP
#define XMLDEF_HPP

// deflef::Document* lef_xml(char* inputfile, char* outputfile);


namespace deflef {
	class XmlDef: public Document {
	  public:
		XmlDef(char* file);
		void read();
	};
  
}
#endif

#include "lef.hpp"

namespace deflef{


void Lef::setupCallbacks() {
		setupCommonHeaderCallbacks();
		setupLefLibraryTopCallbacks();
	}

Lef::Lef(char* file){
	this->filename = file;
	this->setupCallbacks();
	lefrInit();
}

void Lef::read(){
		
	lefrRead(fopen(this->filename.c_str(),"r"),this->filename.c_str(), (void*) document.get_root_node());
}

}

#include "Element.hpp"
#include "stdlib.h"
namespace deflef
{

string Element::print()
{
	char buffer[16];
	string output = node->get_name();
	snprintf(buffer, 16, " %d", (int)node->get_attributes().size());
	output.append(buffer);
	output.append("{ ");
	xmlpp::Element::AttributeList attrs =  node->get_attributes();
	for (xmlpp::Element::AttributeList::iterator iterator = attrs.begin(); iterator != attrs.end(); iterator++) {
		output.append((*iterator)->get_name());
		output.append("=");
		output.append((*iterator)->get_value());
		output.append(" ");
	}
	output.append("}");
	return output;
}

Element::Element(xmlpp::Node::NodeList::iterator* iterator)
{
	this->node = new xmlpp::Element((**iterator)->cobj());
}

Element::Element(xmlpp::Node* node)
{
	this->node = new xmlpp::Element(node->cobj());
}

string Element::getString(char* attribute)
{
	return node->get_attribute_value(attribute);
}

string Element::getString(const char* attribute)
{
	return node->get_attribute_value(attribute);
}

string Element::getString(string attribute)
{
	return node->get_attribute_value(attribute);
}

double Element::getDouble(string attribute)
{
	string value = node->get_attribute_value(attribute);
	return atof(value.c_str());
}

int Element::getInt(string attribute)
{
	string value = node->get_attribute_value(attribute);
	return atoi(value.c_str());
}

int Element::create_properties()
{
	xmlpp::Node::NodeList list = node->get_children(PROPERTY);
	for (xmlpp::Node::NodeList::iterator iterator = list.begin(); iterator != list.end(); iterator++) {
		Element* e = new Element(&iterator);
		string name = e->getString(A_name);
		string type = e->getString(A_type);

		if (type.compare(REAL) == 0) {
			CS(defwRealProperty(name.c_str(), e->getDouble(A_value)));
		}  else if (type.compare(INTEGER) == 0) {
			CS(defwIntProperty(name.c_str(), e->getInt(A_value)));
		} else if (type.compare(STRING) == 0) {
			CS(defwStringProperty(name.c_str(), e->getString(A_value).c_str()));
		}
		delete e;
	}
	return DEFW_OK;
}
int Element::countElement(const char* name)
{
	xmlpp::Node::NodeList list =  node->get_children(name);
	return (int) list.size();
}

Element* Element::getSingleElement(string name)
{
	return this->getSingleElement(name.c_str());
}

Element* Element::getSingleElement(const char* name)
{
	xmlpp::Node::NodeList list =  node->get_children(name);
	if (list.size() == 1) {
		Element* e = new Element(list.front());
		free_list_single.push_back(e);
		return e;
	} else if (list.size() == 0) {
		return NULL;
	} else {
		string node_name = node->get_name();
		fprintf(stderr, "Runtime Error: node %s, has %d children of type %s\n", node_name.c_str(), (int)list.size(), name);
		exit(1);
	}
}
list<Element*>* Element::getSubElements(string name)
{
	return getSubElements(name.c_str());
}

list<Element*>* Element::getSubElements(const char* name)
{
	xmlpp::Node::NodeList nlist =  node->get_children(name);
	if (nlist.size() >= 1) {
		list<Element*>* elements = new list<Element*>();
		for (xmlpp::Node::NodeList::iterator iterator = nlist.begin(); iterator != nlist.end(); iterator++) {
			elements->push_back(new Element(&iterator));
		}
		free_list_children.push_back(elements);
		return elements;
	} else {
		return NULL;
	}
}

Element::~Element()
{
	for (list<Element*>::iterator i = free_list_single.begin(); i != free_list_single.end(); i++) {
		delete(*i);
	}
	for (list<list<Element*>*>::iterator i = free_list_children.begin(); i != free_list_children.end(); i++) {
		for (list<Element*>::iterator j = (*i)->begin(); j != (*i)->end(); j++) {
			delete(*j);
		}
		delete(*i);
	}
}


int Element::points(int (*func)(int, double*, double*))
{
	list<Element*>* points = this->getSubElements(POINT);
	if (points != NULL) {
		double* x = new double[points->size()];
		double* y = new double[points->size()];
		int k = 0;
		for (list<Element*>::iterator j = points->begin(); j != points->end(); j++, k++) {
			Element* p = (*j);
			x[k] = p->getDouble(A_x);
			y[k] = p->getDouble(A_y);
		}
		int result = func(points->size(), x, y);
		delete [] x;
		delete [] y;
		if (DEFW_OK != result) {
			defwPrintError(result);
			return result;
		}
	}
	return DEFW_OK;
}

int Element::polygons(int (*func)(int, double*, double*))
{
	Element* e = this->getSingleElement(POLYGONS);
	if (e == NULL) {
		list<Element*>* polygons = e->getSubElements(POLYGON);
		for (list<Element*>::iterator i = polygons->begin(); i != polygons->end(); i++) {
			Element* poly = (*i);
			list<Element*>* points = poly->getSubElements(POINT);
			double* x = new double[points->size()];
			double* y = new double[points->size()];
			int k = 0;
			for (list<Element*>::iterator j = points->begin(); j != points->end(); j++, k++) {
				Element* p = (*j);
				x[k] = p->getDouble(A_x);
				y[k] = p->getDouble(A_y);
			}
			int result = func(points->size(), x, y);
			delete [] x;
			delete [] y;
			if (DEFW_OK != result) {
				defwPrintError(result);
				return result;
			}
		}
	}
	return DEFW_OK;
}

int Element::rectangles(int (*func)(int, int, int, int))
{
	list<Element*>* rectangles;
	Element* e = this->getSingleElement(RECTANGLES);
	if (e == NULL) {
		rectangles = this->getSubElements(RECTANGLE);
	} else {
		rectangles = e->getSubElements(RECTANGLE);
	}
	if (rectangles != NULL) {
		for (list<Element*>::iterator i = rectangles->begin(); i != rectangles->end(); i++) {
			Element* rect = (*i);
			CS(func(rect->getInt(A_xl), rect->getInt(A_yl), rect->getInt(A_xh), rect->getInt(A_yh)));
		}
	}
	return DEFW_OK;
}


int Element::properties(){
	list<Element*>* properties = this->getSubElements(PROPERTY);
	for ( list<Element*>::iterator i = properties->begin(); i != properties->end(); i++){
		Element* p = (*i);
		CS(defwRealProperty(p->getString(A_name).c_str(),p->getDouble(A_value)));
		CS(defwIntProperty(p->getString(A_name).c_str(),p->getInt(A_value)));
		CS(defwStringProperty( p->getString(A_name).c_str(), p->getString(A_value).c_str()));					
	}
	return DEFW_OK;
}

}
// kate: indent-mode cstyle; replace-tabs off; tab-width 4;  replace-tabs off;  replace-tabs off;  replace-tabs off;  replace-tabs off;

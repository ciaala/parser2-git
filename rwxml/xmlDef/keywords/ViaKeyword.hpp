#ifndef VIAKEYWORD_HPP
#define VIAKEYWORD_HPP
#include  "Keyword.hpp"
namespace deflef{
	
	
class ViasKeyword : public Keyword {
	private :
		int handle_viarule(Element* v);
		int handle_rectangles(Element* v);
		int handle_polygons(Element* v);
	public :
		ViasKeyword();
		int handle(Element* e);
};

class NonDefaultRulesKeyword : public Keyword {
	private :
		
	public :
		NonDefaultRulesKeyword();
		int handle(Element* e);
};

}

#endif
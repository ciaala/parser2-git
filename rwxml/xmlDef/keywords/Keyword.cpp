#include "Keyword.hpp"
#include <algorithm>
namespace deflef
{


Keyword::Keyword() {}

Keyword::~Keyword()
{

}

string Keyword::get_keyword()
{
	return keyword;
}


int Keyword::handle(Element* e)
{
	fprintf(stderr, "Error class %s is not implementing handle\n", keyword.c_str());
	return DEFW_UNINITIALIZED;
}



DoubleKeyword::DoubleKeyword(string keyword, twointegerFn callback, string attribute)
{
	this->keyword = keyword;
	this->i_callback = callback;
	this->d_callback = NULL;
	this->attribute = attribute;
}

DoubleKeyword::DoubleKeyword(string keyword, doubleFn callback, string attribute)
{
	this->keyword = keyword;
	this->d_callback = callback;
	this->i_callback = NULL;
	this->attribute = attribute;
}

DoubleKeyword::~DoubleKeyword()
{


}

int DoubleKeyword::handle(Element* e)
{
	if (i_callback != NULL) {
		int first, second;
		string v = e->getString(this->attribute);
		sscanf(v.c_str(), "%d.%d", &first, &second);
		return i_callback(first, second);

	} else if (d_callback != NULL) {
		return d_callback(e->getDouble(this->attribute));
	}
}

StringKeyword::StringKeyword(const char* keyword, stringFn callback, const char* attribute)
{
	this->keyword = string(keyword);
	this->callback = callback;
	this->attribute = string(attribute);
}

StringKeyword::StringKeyword(string keyword, stringFn callback, string attribute)
{
	this->keyword = keyword;
	this->callback = callback;
	this->attribute = attribute;

}
int StringKeyword::handle(Element* e)
{
	return callback(e->getString(this->attribute).c_str());
}

StringKeyword::~StringKeyword()
{

}

IntegerKeyword::IntegerKeyword(string keyword,  integerFn callback , string attribute)
{
	this->keyword = keyword;
	this->callback = callback;
	this->attribute = attribute;
}

int IntegerKeyword::handle(Element* e)
{
	return callback(e->getInt(this->attribute));
}

}
// kate: indent-mode cstyle; replace-tabs off; tab-width 4;  replace-tabs off;  replace-tabs off;

#ifndef COMPLEX_HPP
#define COMPLEX_HPP
#include "Keyword.hpp"


namespace deflef
{
class ComponentsKeyword : public Keyword
{
	public :
		ComponentsKeyword();
		int handle(Element* e);
};

class BlockagesKeyword : public Keyword
{
	public :
		BlockagesKeyword();
		int handle(Element* e);
};

class ScanchainsKeyword : public Keyword
{
	public :
		ScanchainsKeyword();
		int handle(Element* e);
};
class GroupsKeyword : public Keyword{
	public :
		GroupsKeyword();
		int handle(Element* e);
};
}

#endif
// kate: indent-mode cstyle; replace-tabs off; tab-width 4;  replace-tabs off;  replace-tabs off;

#include "ViaKeyword.hpp"
namespace deflef
{

ViasKeyword::ViasKeyword()
{
	this->keyword = VIAS;
}

int ViasKeyword::handle(Element* e)
{
	CS(defwStartVias(e->getInt(A_items)));
	list<Element*>* vias = e->getSubElements(VIA);
	for (list<Element*>::iterator u = vias->begin(); u != vias->end(); u++) {
		Element* v = *u;
		CS(defwViaName((v->getString(A_name)).c_str()));
		if (v->countElement(VIARULE) == 1) {
			CS(this->handle_viarule(v));			
		} else if (v->countElement(RECTANGLES) == 1) {
			CS( this->handle_rectangles(v) );
		//	int result = this->handle_rectangles(v);
		//	return result;
		} else if (v->countElement(POLYGONS) == 1) {
			CS(this->handle_polygons(v));			
		} else {
			fprintf(stderr, "Error in the VIA structure, VIA %s", v->getString(A_name).c_str());
		} 
		if ( v->countElement(ROWCOL) ==1 ){
			Element* t = v->getSingleElement(ROWCOL);
			CS(defwViaViaruleRowCol(t->getInt(A_rows),t->getInt(A_cols)));
		}		
		if ( v->countElement(ORIGIN) ==1 ){
			Element* t = v->getSingleElement(ORIGIN);
			CS(defwViaViaruleOrigin(t->getInt(A_xOffset), t->getInt(A_yOffset)));
		}
		if ( v->countElement(OFFSET) ==1 ){
			Element* t = v->getSingleElement(OFFSET);
			CS(defwViaViaruleOffset( t->getInt( A_xBotOffset),t->getInt(A_yBotOffset),t->getInt(A_xTopOffset),t->getInt(A_yTopOffset)));
		}
		if ( v->countElement(CUTPATTERN) ==1 ){
			Element* t = v->getSingleElement(CUTPATTERN);
			CS(defwViaViarulePattern(t->getString(A_name).c_str()));
		}
		CS(defwOneViaEnd());		
	}
	CS(defwEndVias());
}


int ViasKeyword::handle_viarule(Element* v)
{
	Element* vr = v->getSingleElement(VIARULE);
	return defwViaViarule(vr->getString(A_name).c_str(),
					  vr->getDouble(A_xCutSize), vr->getDouble(A_yCutSize),
					  vr->getString(A_botMetalLayer).c_str(),
					  vr->getString(A_cutLayer).c_str(), 
					  vr->getString(A_topMetalLayer).c_str(),
					  vr->getDouble(A_xCutSpacing), vr->getDouble(A_yCutSpacing),
					  vr->getDouble(A_xBotEnc), vr->getDouble(A_yBotEnc),
					  vr->getDouble(A_xTopEnc), vr->getDouble(A_yTopEnc));
}

int ViasKeyword::handle_rectangles(Element* v)
{
	list<Element*>* rectangles = v->getSingleElement(RECTANGLES)->getSubElements(RECTANGLE);
	for (list<Element*>::iterator i = rectangles->begin(); i != rectangles->end(); i++) {
		Element* rect = (*i);
		CS(defwViaRect(rect->getString(A_name).c_str(), rect->getInt(A_xl), rect->getInt(A_yl), rect->getInt(A_xh), rect->getInt(A_yh)));
	}
	return DEFW_OK;
}


int ViasKeyword::handle_polygons(Element* v)
{
	list<Element*>* polygons = v->getSingleElement(POLYGONS)->getSubElements(POLYGON);
	for (list<Element*>::iterator i = polygons->begin(); i != polygons->end(); i++) {
		Element* poly = (*i);
		list<Element*>* points = poly->getSubElements(POINT);
		double* x = new double[points->size()];
		double* y = new double[points->size()];
		int k = 0;
		for (list<Element*>::iterator j = points->begin(); j != points->end(); j++, k++) {
			Element* p = (*j);
			x[k] = p->getDouble(A_x);
			y[k] = p->getDouble(A_y);
		}
		int result = defwViaPolygon(poly->getString(A_name).c_str(), points->size(), x, y);
		delete [] x;
		delete [] y;
		if (DEFW_OK != result){
			defwPrintError(result);
			return result;
		}
	}
	return DEFW_OK;
}


}
// kate: indent-mode cstyle; replace-tabs off; tab-width 4;  replace-tabs off;  replace-tabs off;  replace-tabs off;  replace-tabs off;  replace-tabs off;  replace-tabs off;  replace-tabs off;  replace-tabs off;

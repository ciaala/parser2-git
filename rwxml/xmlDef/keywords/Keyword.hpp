#ifndef KEYWORD_HPP
#define KEYWORD_HPP

#include "../../include/allin.hpp"
#include "stdlib.h"
#include "../Element.hpp"


typedef int (* twointegerFn)(int, int) ;
typedef int (* doubleFn)(double);
typedef int (* stringFn)(const char*);
typedef int (* integerFn)(int);


namespace deflef
{

class Keyword
{
    protected:
        string keyword;
        string attribute;
    public:
        Keyword();
        ~Keyword();
        string get_keyword();
        virtual int handle(Element* e);	
        string get_attribute_value(xmlpp::Node::NodeList::iterator iterator, string attribute);

		
};

class DoubleKeyword : public Keyword
{
    private:
        twointegerFn i_callback;
        doubleFn    d_callback;
        string attribute;
    public:
        DoubleKeyword(string keyword, twointegerFn callback,  string attribute = "value");
        DoubleKeyword(string keyword, doubleFn callback,  string attribute = "value");
        ~DoubleKeyword();
        int handle(Element* e);
};

class StringKeyword : public Keyword
{
    private :
        stringFn callback;
        string* attribute1;
        string* keyword1;
    public:
        StringKeyword(const char* keyword, stringFn callback, const char* attribute = A_value);
        StringKeyword(string keyword,  stringFn callback, string attribute = A_value);
        int handle(Element* e);
        ~StringKeyword();

};

class IntegerKeyword : public Keyword
{

    private :
        integerFn callback;
        string attribute;
    public :
        IntegerKeyword(string keyword, integerFn callback, string attribute = "value");
        int handle(Element* e);
};

}
#endif
// kate: indent-mode cstyle; replace-tabs off; tab-width 4; 

#ifndef PROPERTY_DEFINITIONS_HPP
#define PROPERTY_DEFINITIONS_HPP
#include "../Element.hpp"
#include "Keyword.hpp"


namespace deflef
{
class PropertyDefinitionsKeyword : public Keyword
{
    private:
    public:  
		PropertyDefinitionsKeyword();
        int handle(Element* e);
      
};


class DieAreaKeyword : public Keyword
{
    private:
    public:    DieAreaKeyword();
        int handle(Element* e);
    
};

class RowKeyword : public Keyword
{
    private:
    public:		
		RowKeyword();
		int handle(Element* e);

};

class TracksKeyword : public Keyword{
	private:
	public:		
		TracksKeyword();
		int handle(Element* e);

};
class GCellGridKeyword : public Keyword{
	private:
	public:		
		GCellGridKeyword();
		int handle(Element* e);

};
class StylesKeyword : public Keyword{
	private :
	public:
		StylesKeyword();
		int handle(Element* e);
	
};
class FillsKeyword : public Keyword{
	private:
	public:
		FillsKeyword();
		int handle(Element* e);
};
class SlotsKeyword : public Keyword{
	private:
	public:
		SlotsKeyword();
		int handle(Element* e);
};
class RegionsKeyword : public Keyword{
	private:
	public:
		RegionsKeyword();
		int handle(Element* e);
};

class BeginExtKeyword : public Keyword{
	private :
	public :
		BeginExtKeyword();
		int handle(Element* e);
};

}
#endif
// kate: indent-mode cstyle; replace-tabs off; tab-width 4; 

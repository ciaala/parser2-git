#include "DefSimpleStatement.hpp"
#include <algorithm>
#include <iostream>

namespace deflef
{



PropertyDefinitionsKeyword::PropertyDefinitionsKeyword()
{
	this->keyword = PROPERTYDEFINITIONS;

}
int PropertyDefinitionsKeyword::handle(Element* e)
{
	CS(defwStartPropDef());

	list<Element*>* properties = e->getSubElements(PROPERTY);
	for (list<Element*>::iterator iterator = properties->begin(); iterator != properties->end(); ++ iterator) {
		Element* p = *iterator;
		string name = p->getString(A_name);
		string type = p->getString(A_type);
		string objType = p->getString(A_objectType);
		std::transform(objType.begin(), objType.end(), objType.begin(), toupper);


		string left = p->getString(A_left);
		string right = p->getString(A_right);
		if (type.compare(REAL) == 0) {
			if (left.length() != 0 && right.length() != 0) {
				CS(defwRealPropDef(objType.c_str(), name.c_str(), p->getDouble("left"), e->getDouble("right"), 0));
			} else {
				CS(defwRealPropDef(objType.c_str(), name.c_str(), 0, 0, p->getDouble(A_value)));
			}
		} else if (type.compare(INTEGER) == 0) {
			if (left.length() != 0 && right.length() != 0) {
				CS(defwIntPropDef(objType.c_str(), name.c_str(), e->getInt(A_left), p->getInt(A_right), 0));
			} else {
				CS(defwIntPropDef(objType.c_str(), name.c_str(), 0, 0, p->getInt(A_value)));
			}
		} else if (type.compare(STRING) == 0) {
			if (p->getString(A_value).length()) {
				CS(defwStringPropDef(objType.c_str(), name.c_str(), 0, 0, p->getString(A_value).c_str()));
			} else {
				CS(defwStringPropDef(objType.c_str(), name.c_str(), 0, 0, NULL));
			}
		}
	}
	return defwEndPropDef();
}


DieAreaKeyword::DieAreaKeyword()
{
	this->keyword = DIEAREA;
}
int DieAreaKeyword::handle(Element* e)
{
	int numPoints;
	int *xVector, *yVector;
	list<Element*>* points = e->getSubElements(POINT);
	numPoints = points->size();
	xVector = new int[numPoints];
	yVector = new int[numPoints];
	list<Element*>::iterator iterator = points->begin();

	//TODO gestire
	for (int i = 0; i < numPoints; iterator++, i++) {
		Element* point = *iterator;
		xVector[i] = point->getInt(A_x);
		yVector[i] = point->getInt(A_y);
	}
	int result = defwDieAreaList(numPoints, xVector, yVector);
	delete[] xVector;
	delete[] yVector;
	return DEFW_OK;
}

RowKeyword::RowKeyword()
{
	this->keyword = ROW;
}
int RowKeyword::handle(Element* e)
{

	int xCount = 0, yCount = 0;
	int xStep = 0, yStep = 0;

	Element* d = e->getSingleElement(DOBYSTEP);
	if (d != NULL) {
		xCount = d->getInt(A_xCount);
		yCount = d->getInt(A_yCount);
		xStep = d->getInt(A_xStep);
		yStep = d->getInt(A_yStep);
	} else {
		Element* d = e->getSingleElement(DOBY);
		if (d != NULL) {
			xCount = d->getInt(A_xCount);
			yCount = d->getInt(A_yCount);
		}
	}
	CS(defwRowStr(e->getString(A_name).c_str(), e->getString(A_macro).c_str(),
				  e->getDouble(A_x), e->getDouble(A_y), e->getString(A_orientation).c_str(),
				  xCount, yCount, xStep, yStep));
	return e->create_properties();
}

TracksKeyword::TracksKeyword()
{
	this->keyword = TRACKS;
}

int TracksKeyword::handle(Element* e)
{
	list<Element*>* layers = e->getSubElements(LAYER);
	const char** layers_array = new const char*[layers->size()];
	list<Element*>::iterator i = layers->begin();
	for (int j = 0; j < layers->size(); i++, j++) {
		layers_array[j] = (*i)->getString(A_name).c_str();
	}
	int result = defwTracks(e->getString(A_macro).c_str(), e->getInt(A_start), e->getInt(A_time), e->getInt(A_step), layers->size() , layers_array);
	delete [] layers_array;
	return result;
}

GCellGridKeyword::GCellGridKeyword()
{
	this->keyword = GCELLGRID;
}

int GCellGridKeyword::handle(Element* e)
{
	return defwGcellGrid(e->getString(A_macro).c_str(), e->getInt(A_start), e->getInt(A_time), e->getInt(A_step));
}



StylesKeyword::StylesKeyword()
{
	this->keyword = STYLES;
}

int StylesKeyword::handle(Element* e)
{
	list<Element*>* styles = e->getSubElements(STYLE);
	CS(defwStartStyles(styles->size()));
	int counter = 0;
	for (list<Element*>::iterator i = styles->begin(); i != styles->end(); i++, counter++) {
		list<Element*>* points = (*i)->getSubElements(POINT);
		int size = points->size();
		double* x = new double[size];
		double* y = new double[size];
		int k = 0;
		for (list<Element*>::iterator p = points->begin(); p != points->end(); p++, k++) {
			x[k] = (*p)->getDouble(A_x);
			y[k] = (*p)->getDouble(A_y);
		}
		int result = defwStyles(counter, points->size(), x, y);
		delete [] x;
		delete [] y;
		if (result != DEFW_OK) {
			return result;
		}
	}
	return defwEndStyles();
}

FillsKeyword::FillsKeyword()
{
	this->keyword = FILLS;
}

int FillsKeyword::handle(Element* e)
{
	list<Element*>* layers = e->getSubElements(LAYER);
	list<Element*>* vias = e->getSubElements(VIA);
	CS(defwStartFills(layers->size()));
	for (list<Element*>::iterator i = layers->begin(); i != layers->end();i++) {
		Element* layer = (*i);
		CS(defwFillLayer(layer->getString(A_name).c_str()));
		if (layer->getString(A_opc).length() > 0) {
			CS(defwFillLayerOPC());
		}

		CS(layer->rectangles(defwFillRect));
		CS(layer->polygons(defwFillPolygon));

	}
	for (list<Element*>::iterator i = vias->begin(); i != vias->end();i++) {
		Element* via = (*i);
		CS(defwFillVia(via->getString(A_name).c_str()));
		if (via->getString(A_opc).length() >= 0) {
			CS(defwFillViaOPC());
		}
		CS(via->points(defwFillPoints));

	}
	CS(defwEndFills());
	return DEFW_OK;
}


SlotsKeyword::SlotsKeyword()
{
	this->keyword = SLOTS;
}
int SlotsKeyword::handle(Element* e)
{
	list<Element*>* layers = e->getSubElements(LAYER);
	CS(defwStartSlots(layers->size()));
	for (list<Element*>::iterator i = layers->begin(); i != layers->end();i++) {
		Element* layer = (*i);
		if (layer->countElement(RECTANGLES) == 1) {
			CS(layer->rectangles(defwSlotRect));
		}
		if (layer->countElement(POLYGONS) == 1) {
			CS(layer->polygons(defwSlotPolygon));
		}
	}
	CS(defwEndSlots());
	return DEFW_OK;
}
RegionsKeyword::RegionsKeyword()
{
	this->keyword = REGIONS;
}
int RegionsKeyword::handle(Element* e)
{
	list<Element*>* regions = e->getSubElements(LAYER);
	CS(defwStartRegions(regions->size()));
	for (list<Element*>::iterator i = regions->begin(); i != regions->end();i++) {
		Element* r = (*i);
		CS(defwRegionName(r->getString(A_name).c_str()));
		CS(r->rectangles(defwRegionPoints));
		CS(defwRegionType(r->getString(A_type).c_str()));
		CS(r->properties());
	}
	CS(defwEndRegions());
	return DEFW_OK;
}
BeginExtKeyword::BeginExtKeyword()
{
	this->keyword = BEGINEXT;
}
int BeginExtKeyword::handle(Element* e)
{
// 	defwStartBeginext();
// 	defwBeginextCreator();
// 	defwBeginextDate();
// 	defwBeginextRevision();
// 	defwBeginextSyntax();
// 	
	return DEFW_OK;
}
}
// kate: indent-mode cstyle; replace-tabs off; tab-width 4;  replace-tabs off;  replace-tabs off;  replace-tabs off;  replace-tabs off;  replace-tabs off;  replace-tabs off;  replace-tabs off;  replace-tabs off;  replace-tabs off;  replace-tabs off;  replace-tabs off;  replace-tabs off;  replace-tabs off;  replace-tabs off;  replace-tabs off;  replace-tabs off;  replace-tabs off;

#ifndef NETS_H
#define NETS_H
#include "Keyword.hpp"
namespace deflef{
class NetsKeyword : public Keyword {
	
	public:
		NetsKeyword();
		int handle(Element* e);
};

class SpecialNetsKeyword : public Keyword {
	
	public:
		SpecialNetsKeyword();
		int handle(Element* e);
};
}
#endif
#ifndef PINS_HPP
#define PINS_HPP
#include "Keyword.hpp"

namespace deflef
{
class PinsKeyword : public Keyword
{
	public :
		PinsKeyword();
		int handle(Element* e);
};
class PinsPropertiesKeyword : public Keyword
{
	public :
		PinsPropertiesKeyword();
		int handle(Element* e);
};
}
#endif
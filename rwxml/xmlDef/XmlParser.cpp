#include "XmlParser.hpp"
#include <list>




namespace deflef
{

XmlParser::XmlParser()
{
//
// DEF
//
    defKeywordsL1.push_back(new DoubleKeyword(VERSION, defwVersion));
    defKeywordsL1.push_back(new StringKeyword(DIVIDERCHAR , defwDividerChar));
    defKeywordsL1.push_back(new StringKeyword(BUSBITCHAR, defwBusBitChars));
    defKeywordsL1.push_back(new StringKeyword(DESIGN, defwDesignName, A_name));
    defKeywordsL1.push_back(new StringKeyword(TECHNOLOGY, defwTechnology, A_name));
    defKeywordsL1.push_back(new IntegerKeyword(UNITS, defwUnits));
    defKeywordsL1.push_back(new StringKeyword(HISTORY, defwHistory));
    defKeywordsL1.push_back(new PropertyDefinitionsKeyword());
    defKeywordsL1.push_back(new DieAreaKeyword());
    defKeywordsL1.push_back(new RowKeyword());
	defKeywordsL1.push_back(new TracksKeyword());
	defKeywordsL1.push_back(new GCellGridKeyword());
	defKeywordsL1.push_back(new ViasKeyword());
	defKeywordsL1.push_back(new StylesKeyword());
	
//	defKeywordsL1.push_back(new NonDefaultRulesKeyword());
	defKeywordsL1.push_back(new RegionsKeyword());
//	defKeywordsL1.push_back(new ComponentsKeyword());
//	defKeywordsL1.push_back(new PinsKeyword());
//	defKeywordsL1.push_back(new PinsKeyword());
//	defKeywordsL1.push_back(new PinsPropertiesKeyword());
//	defKeywordsL1.push_back(new BlockagesKeyword());

	defKeywordsL1.push_back(new SlotsKeyword());
	defKeywordsL1.push_back(new FillsKeyword());
//	defKeywordsL1.push_back(new SpecialNetsKeyword());
//	defKeywordsL1.push_back(new NetsKeyword());
	
//	defKeywordsL1.push_back(new ScanchainsKeyword());
//	defKeywordsL1.push_back(new GroupsKeyword());
//	defKeywordsL1.push_back(new BeginExtKeyword());
//	
// LEF
//
    lefKeywordsL1.push_back(new DoubleKeyword(VERSION, lefwVersion));
    lefKeywordsL1.push_back(new StringKeyword(DIVIDERCHAR, lefwDividerChar));
    lefKeywordsL1.push_back(new StringKeyword(BUSBITCHAR, lefwBusBitChars));
}

xmlpp::Document* XmlParser::read_file(string input_file)
{
    this->input_file = input_file;
}

int XmlParser::write_document(string output_file)
{
	FILE* output = fopen(output_file.c_str(), "w+");
    defwInitCbk(output);
    xmlpp::DomParser domParser;
    domParser.parse_file(input_file);
    xmlpp::Document* document = domParser.get_document();
    // RICONOSCE LE KEYWORD DI L1
    list<Keyword*>* l1Keyword = NULL;
    xmlpp::Element* root = document->get_root_node();
    xmlpp::Node::NodeList nodes = root->get_children(DESIGN);
    if (nodes.empty()) {
        l1Keyword = &lefKeywordsL1;
    } else {
        l1Keyword = &defKeywordsL1;
    }
    for (list<Keyword*>::iterator l1_iter = l1Keyword->begin(); l1_iter != l1Keyword->end(); ++ l1_iter) {
        Keyword* l1 = *l1_iter ;
        //LOOP
        string keyword = l1->get_keyword();

        xmlpp::Node::NodeList list = root->get_children(keyword);
        for (xmlpp::Node::NodeList::iterator iterator = list.begin(); iterator != list.end();++ iterator) {
            // LOOP SUI SINGOLI L1 KEYWORD   { HISTORY , PROPERTY DEFINITIONS, ROWS inventato da me }.
            Element* e = new Element(*iterator);
            int result = l1->handle(e);
            if (result != DEFW_OK) {
                defwPrintError(result);
                fprintf(stderr, "*** Error %d while handling keyword: %s\n", result, keyword.c_str());
				delete e;
				fclose( output);
                return result;
            }
            delete e;
        }
        //END LOOP
    }
    fclose(output);
    return DEFW_OK;
}

XmlParser::~XmlParser(){
	for(list<Keyword*>::iterator i = defKeywordsL1.begin(); i!= defKeywordsL1.end(); i++){
			delete (*i);
	}
}

}
// kate: indent-mode cstyle; replace-tabs off; tab-width 4; 

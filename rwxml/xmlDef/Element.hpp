#ifndef ELEMENT_HPP
#define ELEMENT_HPP
#include "../include/allin.hpp"
namespace deflef
{


class Element
{

	private:
		list<list<Element*>*> free_list_children;
		list<Element*> free_list_single;
		xmlpp::Element* node;
	public:
		Element(xmlpp::Node* node);
		Element(xmlpp::Node::NodeList::iterator* iterator);
		~Element();
		double getDouble(string attribute);
		int getInt(string attribute);
		string getString(const char* attribute);
		string getString(char* attribute);
		string getString(string attribute);
		int create_properties();
		Element* getSingleElement(string name);
		Element* getSingleElement(const char* name);
		list<Element*>* getSubElements(string name);
		list<Element*>* getSubElements(const char* name);
		int countElement(const char* name);
		string print();
		
		
		int properties();
		int rectangles(int (*func)(int,int,int,int));
		int polygons(int (*func)(int,double*,double*));
		int points(int (*func)(int, double*, double*));
		int rectangle(int (*func)(int,int,int,int));
};





}

#endif
// kate: indent-mode cstyle; replace-tabs off; tab-width 4; 

#ifndef XMLPARSER_HPP
#define XMLPARSER_HPP
#include "../include/allin.hpp"
#include "keywords/Keyword.hpp"
#include  "keywords/DefSimpleStatement.hpp"
#include "keywords/ViaKeyword.hpp"
#include "keywords/Nets.hpp"
#include "keywords/Complex.hpp"
#include "keywords/Pins.hpp"

namespace deflef{
using std::list;

class XmlParser{
  private:
	string input_file;
	xmlpp::Document* document;
	void parse_document(xmlpp::Document* document);
	list<Keyword*> defKeywordsL1;
	list<Keyword*> lefKeywordsL1;
	//= list<Keyword>(); 
	
	
  public:
	XmlParser();
	~XmlParser();
	//aggiungere il lancio di una eccezione se non si trova il file.
	xmlpp::Document*  read_file(string input_file);
	int write_document( string output_file);
};

}
#endif

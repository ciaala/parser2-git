#include "utility.hpp"

void set_attribute ( xmlpp::Element* node,string attribute, int value )
{
    char buffer[32];
    snprintf ( buffer, 32, "%d", value );
    node->set_attribute ( attribute, buffer );
}

void set_attribute ( xmlpp::Element* node,string attribute, double value )
{
    char buffer[16];
    snprintf ( buffer, 32, "%g", value );
    node->set_attribute ( attribute, buffer );
}

void set_attribute ( xmlpp::Element* node, string attribute, string value )
{
    if ( value.length() )
    {
        node->set_attribute ( attribute, value );
    }
}


void set_attribute ( xmlpp::Element* node, string attribute, char value )
{
    string temp;
    temp = value;
    node->set_attribute ( attribute, temp );
}

void set_attribute ( xmlpp::Element* node, int xl, int yl, int xh, int yh )
{
    set_attribute ( node,"xl", xl );
    set_attribute ( node,"yl", yl );
    set_attribute ( node,"xh", xh );
    set_attribute ( node,"yh", yh );
}

xmlpp::Element* create_element ( xmlpp::Element* parent, string element, string attribute, int value )
{
    xmlpp::Element* node = parent->add_child ( element );
    char buffer[16];
    snprintf ( buffer, 16, "%d", value );
    node->set_attribute ( attribute, buffer );
    return node;
}


xmlpp::Element* create_element ( xmlpp::Element* parent, string element, string attribute, double value )
{
    xmlpp::Element* node = parent->add_child ( element );
    char buffer[16];
    snprintf ( buffer, 16, "%g", value );
    node->set_attribute ( attribute, buffer );
    return node;
}

xmlpp::Element* create_element ( xmlpp::Element* parent, string element, string attribute, string value )
{
    if ( value.length() )
    {
        xmlpp::Element* node = parent->add_child ( element );
        node->set_attribute ( attribute, value );
        return node;
    }
    else
    {
        fprintf ( stderr,"Error: the value associated with the attribute <%s '%s'> is an empty string\n",element.c_str(), attribute.c_str() );
        return NULL;
    }
}


void set_datatype ( xmlpp::Element* node, char datatype, string attribute )
{
    string subtype;
    switch ( datatype )
    {
    case 'R' :
        subtype = "REAL";
        break;
    case 'I' :
        subtype = "INTEGER";
        break;
    case 'S' :
        subtype = "STRING";
        break;
    case 'Q' :
        subtype = "QUOTESTRING";
        break;
    case 'N' :
        subtype = "NUMBER";
        break;
    }
    node->set_attribute ( attribute ,subtype );

}
xmlpp::Element* create_placement ( xmlpp::Element* parent, int isCover, int isPlaced, int isUnplaced,int isFixed, int placeX, int placeY, string orientation )
{
    xmlpp::Element* node = NULL;
    if ( isPlaced )
    {
        node = parent->add_child ( "PLACED" );
    }
    else if ( isCover )
    {
        node = parent->add_child ( "COVER" );
    }
    else if ( isFixed )
    {
        node = parent->add_child ( "FIXED" );
    }
    if ( node )
    {
        set_attribute ( node,"x",placeX );
        set_attribute ( node,"y",placeY );
        set_attribute ( node,"orientation",orientation );
    }
    return node;
}
xmlpp::Element* create_polygon ( xmlpp::Element* parent, string layerName, int hasSpacing,int spacing, int hasRuleWidth, int ruleWidth,defiPoints points )
{
    xmlpp::Element* polygon = parent->add_child ( "POLYGON" );
    polygon->set_attribute ( "layerName", layerName );
    if ( hasSpacing )
    {
        set_attribute ( polygon,"minSpacing", spacing );
    }
    if ( hasRuleWidth )
    {
        set_attribute ( polygon,"effectiveWidth", ruleWidth );
    }
    create_points ( polygon,points );
    return polygon;
}

xmlpp::Element* create_polygon ( xmlpp::Element* parent, defiPoints points )
{
    xmlpp::Element* polygon = parent->add_child ( "POLYGON" );
    create_points ( polygon,points );
    return polygon;
}

xmlpp::Element* create_layer ( xmlpp::Element* parent, string layerName, int hasSpacing,int spacing, int hasRuleWidth, int ruleWidth, int xl,int yl,int xh,int yh )
{
    xmlpp::Element* layer = parent->add_child ( "LAYER" );
    layer->set_attribute ( "layerName", layerName );
    if ( hasSpacing )
    {
        set_attribute ( layer,"minSpacing", spacing );
    }
    if ( hasRuleWidth )
    {
        set_attribute ( layer,"effectiveWidth", ruleWidth );
    }
    set_attribute ( layer, "xl", xl );
    set_attribute ( layer, "yl", yl );
    set_attribute ( layer, "xh", xh );
    set_attribute ( layer, "xh", yh );
    return layer;
}

void create_points ( xmlpp::Element* parent, defiPoints points )
{
    for ( int i=0; i < points.numPoints; i++ )
    {
        xmlpp::Element* point = create_element ( parent,"POINT","x",points.x[i] );
        set_attribute ( point,"y",points.y[i] );
    }

}

xmlpp::Element* create_rectangle_element ( xmlpp::Element* parent,int xl,int yl,int xh,int yh,string element )
{
    xmlpp::Element* rectangle = parent->add_child ( element );
    set_attribute ( rectangle, A_xl, xl );
    set_attribute ( rectangle, A_yl, yl );
    set_attribute ( rectangle, A_xh, xh );
    set_attribute ( rectangle, A_yh, yh );
    return rectangle;
}


xmlpp::Element* create_property ( xmlpp::Element* parent, string name, string literal, double number, char datatype )
{

    xmlpp::Element* property = create_element(parent,PROPERTY,"name", name);
    set_datatype ( property,datatype );
    test_set ( property, ( datatype == 'R' ) || ( datatype == 'I' ), A_value, number )
    test_set ( property, ( datatype == 'S' ), A_value, literal );

    return property;
}

xmlpp::Element* create_property_definition ( xmlpp::Element* parent, string name, string literal, double number, char datatype, string objType )
{
    xmlpp::Element* property = create_element(parent,PROPERTY,A_name, name);
    set_datatype ( property,datatype );
    set_attribute ( property,A_objectType, objType );
    test_set ( property, (( datatype == 'R' ) || ( datatype == 'I' )), A_value, number )
    test_set ( property, ( datatype == 'S' ), A_value, literal );
    return property;
}


xmlpp::Element* create_doby_step ( xmlpp::Element* parent, int xCount, int yCount, int xStep, int yStep )
{
    xmlpp::Element* do_e = create_element ( parent,"DOBYSTEP", A_xCount, xCount );
    set_attribute ( do_e, A_yCount, yCount );
    set_attribute ( do_e, A_xStep, xStep );
    set_attribute ( do_e, A_yStep, yStep );
    return do_e;
}
xmlpp::Element* create_doby ( xmlpp::Element* parent, int xCount, int yCount){
    xmlpp::Element* do_e = create_element ( parent,"DOBY", A_xCount, xCount );
    set_attribute ( do_e, A_yCount, yCount );
    return do_e;
}


xmlpp::Element* create_path ( xmlpp::Element* parent, defiPath* path )
{
    xmlpp::Element* layer = NULL;
    int type;
    xmlpp::Element* path_e = parent->add_child ( "PATH" );
    path->initTraverse();
    type = ( int ) path->next();

    if ( DEFIPATH_LAYER == type )
    {
        create_element ( path_e,"LAYER", "layerName", path->getLayer() );
    }
    else
    {
        fprintf ( stderr,"Error Path Creation, %d != %d", type, DEFIPATH_LAYER );
        return NULL;
    }
    while ( ( type = ( int ) path->next() ) != DEFIPATH_DONE )
    {

        switch ( type )
        {
        case DEFIPATH_VIA:
            create_element ( layer,"VIA","viaName",path->getVia() );
            break;
        case DEFIPATH_VIAROTATION:
            create_element ( layer,"VIAROTATION","viaRotation", path->getViaRotationStr() );
            break;
        case DEFIPATH_VIADATA:
        {

            int numX, numY, stepX, stepY;
            path->getViaData ( &numX, &numY, &stepX, &stepY );
            create_doby_step ( layer->add_child ( "VIADATA" ), numX, numY, stepX, stepY );

            break;
        }

        case DEFIPATH_WIDTH:
            set_attribute ( layer,"width", path->getWidth() );
            break;
        case DEFIPATH_POINT:
        {
            int x,y;
            path->getPoint ( &x, &y );
            set_attribute ( create_element ( layer,"POINT","x",x ),"y",y );
            break;
        }
        case DEFIPATH_FLUSHPOINT:
        {
            int x,y,z;
            path->getFlushPoint ( &x, &y, &z );
            xmlpp::Element* point = create_element ( layer,"POINT","x",x );
            set_attribute ( point,"y",y );
            set_attribute ( point,"z",z );
            break;
        }
        case DEFIPATH_TAPER:
            set_attribute ( layer, "taper","TAPER" );
            break;
        case DEFIPATH_TAPERRULE:
            set_attribute ( layer, "taperRule", path->getTaperRule() );
            break;
        case DEFIPATH_SHAPE:
            create_element ( layer, "SHAPE", "shape",path->getShape() );
            break;
        case DEFIPATH_STYLE:
            create_element ( layer, "STYLE", "style", path->getStyle() );
            break;
        }

    }
}

xmlpp::Element* create_wire ( xmlpp::Element* parent, defiWire* wire )
{
    xmlpp::Element* wire_e = parent->add_child ( "WIRE" );
    xmlpp::Element* wireType_e = wire_e->add_child ( wire->wireType() );
    if ( strcmp ( wire->wireType(),"SHIELD" ) == 0 )
        wireType_e->set_attribute ( "shieldNetName",wire->wireShieldNetName() );
    if ( wire->numPaths() > 0 )
    {
        xmlpp::Element* paths = create_element ( wire_e,"PATHS","items",wire->numPaths() );
        for ( int i=0, size=wire->numPaths();i < size; i++ )
        {
            defiPath* path = wire->path ( i );
            create_path ( paths, path );
        }
    }

    return wire_e;
}

void setNetParameters ( xmlpp::Element* parent, defiNet* net )
{
    if ( net->hasFixedbump() ) set_attribute ( parent,"fixedBump", "FIXEDBUMP" );
    if ( net->hasFrequency() ) set_attribute ( parent, "frequency", net->frequency() );
    if ( net->hasVoltage() ) set_attribute ( parent, "voltage", net->voltage() );
    if ( net->hasWeight() ) set_attribute ( parent, "weight", net->weight() );
    if ( net->hasCap() ) set_attribute ( parent, "estcap", net->cap() );
    if ( net->hasSource() ) set_attribute ( parent, "source", net->source() );
    if ( net->hasPattern() ) set_attribute ( parent, "pattern", net->pattern() );
    if ( net->hasOriginal() ) set_attribute ( parent, "original", net->original() );
    if ( net->hasUse() ) set_attribute ( parent, "use", net->use() );
}

xmlpp::Element* create_vpin ( xmlpp::Element* parent, defiVpin* vpin )
{
    xmlpp::Element* vpin_e = create_element ( parent,"VPIN","name", vpin->name() );
    test_set ( vpin_e, vpin->layer(),"layer",vpin->layer() );
    set_attribute ( vpin_e, vpin->xl(), vpin->yl(),vpin->xh(), vpin->yh() );
    if ( vpin->status() != ' ' )
    {
        set_attribute ( vpin_e,"status", vpin->status() );
        set_attribute ( vpin_e,"xLoc", vpin->xLoc() );
        set_attribute ( vpin_e,"yLoc", vpin->yLoc() );
    }
    return vpin_e;
}

xmlpp::Element* create_subnet ( xmlpp::Element* parent, defiSubnet* subnet )
{
    xmlpp::Element* subnet_e = create_element ( parent,"SUBNET","name",subnet->name() );
    if ( subnet->numConnections() )
    {
        if ( subnet->pinIsMustJoin ( 0 ) )
        {
            subnet_e->set_attribute ( "mustjoin", "MUSTJOIN" );
        }
        else
        {
            subnet_e->set_attribute ( "name", subnet->name() );
        }
        for ( int j=0; j<subnet->numConnections(); j ++ )
        {
            create_connection ( subnet_e,  subnet->instance ( j ) ,subnet->pin ( j ), subnet->pinIsSynthesized ( j ) );
        }
    }
    if ( subnet->numWires() )
    {
        for ( int j=0, size=subnet->numWires();  j<size; j++ )
        {
            defiWire* wire = subnet->wire ( j );
            create_wire ( subnet_e, wire );
        }
    }


    return subnet_e;
}
xmlpp::Element* create_ordered ( xmlpp::Element* parent, char* instance, char* in, char* out, int bits )
{
    xmlpp::Element* instance_e = create_element ( parent, "ORDERED", "instance", instance );
    test_set ( instance_e, in, "in", in );
    test_set ( instance_e, out, "out", out );
    test_set ( instance_e, bits, "bits", bits );
    return instance_e;
}

xmlpp::Element* create_floating ( xmlpp::Element* parent, char* instance, char* in, char* out, int bits )
{
    xmlpp::Element* instance_e = create_element ( parent, "FLOATING", "instance", instance );
    test_set ( instance_e, in, "in", in );
    test_set ( instance_e, out, "out", out );

    test_set ( instance_e, bits, "bits", bits );
    return instance_e;
}

xmlpp::Element* create_connection ( xmlpp::Element* parent, const char* instance, const char* pin, bool isSynthesized )
{
    xmlpp::Element* connection = parent->add_child ( "CONNECTION" );
    set_attribute ( connection, "instance",instance );
    set_attribute ( connection,"pin",pin );
    test_set ( connection, isSynthesized,"synthetised", "SYNTHETISED" );
    return connection;
}


// kate: indent-mode cstyle; space-indent on; indent-width 4; 

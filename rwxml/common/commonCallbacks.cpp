#include "commonCallbacks.hpp"

namespace deflef
{
int readDefVersionCallback ( defrCallbackType_e c, double num, xmlpp::Element* parent )
{
    create_element ( parent,VERSION,"value", num );
    return 0;
}

int readDefDividercharCallback ( defrCallbackType_e c, const char* string, xmlpp::Element* parent )
{
    parent->add_child ( DIVIDERCHAR )->set_attribute ( "value", string );
    return 0;
}

int readDefBusbitCharsCallback ( defrCallbackType_e c, const char* string, xmlpp::Element* parent )
{
    parent->add_child ( BUSBITCHAR )->set_attribute ( "value", string );
    return 0;
}

// Note: doesn't not support the version value as a string i think it's deprecated in 5.7
void setupCommonHeaderCallbacks()
{
    defrSetVersionCbk ( ( defrDoubleCbkFnType ) readDefVersionCallback );
    defrSetDividerCbk ( ( defrStringCbkFnType ) readDefDividercharCallback );
    defrSetBusBitCbk ( ( defrStringCbkFnType ) readDefBusbitCharsCallback );
}

void setupWriteDefVersion()
{

}

void setupWriteLefVersion()
{

}
int readDefCommonStartCallback ( defrCallbackType_e c, void* data, xmlpp::Element* parent )
{   
    string element;
    switch(c){
        /* design_top */
        case defrViaStartCbkType :      element = "VIAS"; break;
        case defrPropDefStartCbkType     :      element =   PROPERTYDEFINITIONS; break;
        //
        case defrStartPinsCbkType :     element = "PINS"; break;
        case defrPinPropStartCbkType :  element = "PINPROPERTIES"; break;
        case defrBlockageStartCbkType : element = "BLOCKAGES"; break;
        case defrFillStartCbkType :     element = "FILLS"; break;
        case defrSlotStartCbkType :     element = "SLOTS"; break;
        case defrSNetStartCbkType :     element = "SPECIALNETS"; break;
        case defrNetStartCbkType :      element = "NETS"; break;
        case defrScanchainsStartCbkType: element = "SCANCHAINS"; break;
        case defrGroupsStartCbkType:     element = "GROUP"; break;
      
        default :   fprintf(stderr,"Error:missing <StartCallbackType> for [%c]\n",c); break;
    } 
   
    defrSetUserData ( create_element(parent,element,"items",(int) intptr_t(data)) );
    return 0;
}

int readDefCommonNameCallback ( defrCallbackType_e c, char* name, xmlpp::Element* parent){
    string element;
    switch ( c ){
        case defrNetNameCbkType:   element = "NET"; break;
        case defrGroupNameCbkType: element = "GROUP"; break;
        
    }
    defrSetUserData( create_element(parent, element, "name",name));
    return 0;
}

int readDefCommonEndCallback ( defrCallbackType_e c, void* data, xmlpp::Element* parent )
{
    defrSetUserData ( parent->get_parent() );
    return 0;
}
    
}
// kate: indent-mode cstyle; space-indent on; indent-width 4; 

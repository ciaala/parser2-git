#ifndef VERSION_H
#define VERSION_H

#include "../include/allin.hpp"
#include "utility.hpp"

namespace deflef
{
void setupCommonHeaderCallbacks();

int readDefCommonStartCallback ( defrCallbackType_e c, void* data, xmlpp::Element* parent );
int readDefCommonEndCallback ( defrCallbackType_e c, void* data, xmlpp::Element* parent );
int readDefCommonNameCallback ( defrCallbackType_e c, char* name, xmlpp::Element* parent);

}


#endif
// kate: indent-mode cstyle; space-indent on; indent-width 4; 

#ifndef UTILITY_HPP
#define UTILITY_HPP
#include "../include/allin.hpp"

#define test_create(node,condition,element,attribute,value) if (condition) { create_element( node,element,attribute,value);}
#define test_set(node, condition, attribute, value) if (condition) { set_attribute( node, attribute, value);}
//
void set_attribute ( xmlpp::Element* node,string attribute, int value );
void set_attribute ( xmlpp::Element* node,string attribute, double value );
void set_attribute ( xmlpp::Element* node,string attribute, string value );
void set_attribute ( xmlpp::Element* node,string attribute, char c );
void set_attribute ( xmlpp::Element* node, int xl, int yl, int xh, int yh );

void set_datatype ( xmlpp::Element* node, char datatype, string attribute = "type" );


xmlpp::Element* create_element ( xmlpp::Element* parent, string element, string attribute, string value );
xmlpp::Element* create_element ( xmlpp::Element* parent, string element, string attribute, double value );
xmlpp::Element* create_element ( xmlpp::Element* parent, string element, string attribute, int value );


xmlpp::Element* create_placement ( xmlpp::Element* parent, int isCover, int isPlaced, int isUnplaced,int isFixed, int placeX, int placeY, string orientation );




xmlpp::Element* create_polygon ( xmlpp::Element* parent, defiPoints points );
xmlpp::Element* create_polygon( xmlpp::Element* parent, string layerName, int hasSpacing,int spacing, int hasRuleWidth, int ruleWidth,defiPoints points);
xmlpp::Element* create_layer ( xmlpp::Element* parent, string layerName, int hasSpacing,int spacing, int hasRuleWidth, int ruleWidth, int xl,int yl,int xh,int yh );
void create_points(xmlpp::Element* parent, defiPoints points);

xmlpp::Element* create_rectangle_element ( xmlpp::Element* parent,int xl,int yl,int xh,int yh,string element_name = "RECTANGLE" );

xmlpp::Element* create_property_definition( xmlpp::Element* parent, string name, string literal, double number, char datatype, string objType );
xmlpp::Element* create_property ( xmlpp::Element* parent, string name, string literal, double number, char datatype );

xmlpp::Element* create_wire ( xmlpp::Element* parent, defiWire* wire );
xmlpp::Element* create_doby_step ( xmlpp::Element* parent, int xCount, int yCount, int xStep, int yStep );
xmlpp::Element* create_doby (xmlpp::Element* parent, int xCount, int yCount);
xmlpp::Element* create_path ( xmlpp::Element* parent, defiPath* path );

void setNetParameters( xmlpp::Element* snet_e, defiNet* net);
xmlpp::Element* create_vpin( xmlpp::Element* parent, defiVpin* vpin);
xmlpp::Element* create_subnet( xmlpp::Element* parent, defiSubnet* subnet);
xmlpp::Element* create_floating(xmlpp::Element* parent, char* instance, char* in, char* out, int bits);
xmlpp::Element* create_ordered(xmlpp::Element* parent, char* instance, char* in, char* out, int bits);
xmlpp::Element* create_connection(xmlpp::Element* parent, const char* instance, const char* pin, bool isSynthesized);

#endif
// kate: indent-mode cstyle; space-indent on; indent-width 4; 

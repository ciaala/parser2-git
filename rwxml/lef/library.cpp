#include "library.hpp"

namespace deflef
{

int readLefUnitsCallback ( lefrCallbackType_e c, lefiUnits* unit,xmlpp::Element* parent )
{

    xmlpp::Element* node = parent->add_child ( "UNITS" );

    if ( unit->hasDatabase() )
    {
        xmlpp::Element* temp = node->add_child ( "DATABASE" );
        set_attribute ( temp,"microns", unit->databaseNumber() );
    }
    if ( unit->hasCapacitance() )
    {
        xmlpp::Element* temp = node->add_child ( "CAPACITANCE" );
        set_attribute ( temp, "farads", unit->capacitance() );
    }
    if ( unit->hasResistance() )
    {
        xmlpp::Element* temp = node->add_child ( "RESISTANCE" );
        set_attribute ( temp, "ohms", unit->resistance() );
    }
    if ( unit->hasPower() )
    {
        xmlpp::Element* temp = node->add_child ( "POWER" );
        set_attribute ( temp, "milliwatts", unit->power() );
    }
    if ( unit->hasCurrent() )
    {
        xmlpp::Element* temp = node->add_child ( "CURRENT" );
        set_attribute ( temp, "milliamps", unit->current() );
    }
    if ( unit->hasVoltage() )
    {
        xmlpp::Element* temp = node->add_child ( "VOLTAGE" );
        set_attribute ( temp, "volts", unit->voltage() );
    }
    if ( unit->hasFrequency() )
    {
        xmlpp::Element* temp = node->add_child ( "FREQUENCY" );
        set_attribute ( temp, "megahertz", unit->frequency() );
    }
    if ( unit->hasTime() )
    {
        xmlpp::Element* temp = node->add_child ( "TIME" );
        set_attribute ( temp, "nanoseconds", unit->time() );
    }
    return 0;
}
int readLefClearanceMeasureCallback ( lefrCallbackType_e c, const char* name,xmlpp::Element* parent )
{
    xmlpp::Element* node = parent->add_child ( "CLEARANCEMEASURE" );
    node->set_attribute ( "name", name );
    return 0;
}

int readLefManufacturingCallback ( lefrCallbackType_e c, double num,xmlpp::Element* parent )
{
    xmlpp::Element* node = parent->add_child ( "MANUFACTURINGGRID" );
    set_attribute ( node, "value", num );
    return 0;
}

int readLefMaxStackViaCallback ( lefrCallbackType_e c, lefiMaxStackVia* maxStack,xmlpp::Element* parent )
{
    xmlpp::Element* maxviaStack = parent->add_child ( "MAXVIASTACK" );
    set_attribute ( maxviaStack, "value", maxStack->maxStackVia() );
    xmlpp::Element* temp = maxviaStack->add_child ( "RANGE" );
    temp->set_attribute ( "bottomlayer", maxStack->maxStackViaBottomLayer() );
    temp->set_attribute ( "toplayer", maxStack->maxStackViaTopLayer() );
    return 0;
}
int readLefNonDefaultCallback ( lefrCallbackType_e c, lefiNonDefault* nondefault,xmlpp::Element* parent )
{
    xmlpp::Element* nondefaultnode = parent->add_child ( "NONDEFAULTRULE" );
    nondefaultnode->set_attribute ( "rulename", nondefault->name() );
    if ( nondefault->hasHardspacing() )
    {
        xmlpp::Element* hardspacing = nondefaultnode->add_child ( "HARDSPACING" );
    }
    for ( int i = 0; i < nondefault->numLayers(); i++ )
    {
        //xmlpp::Element* hardspacing = nondefaultnode->add_child ( "HARDSPACING" );
        xmlpp::Element* layernode = parent->add_child ( "LAYER" );
        layernode->set_attribute ( "name", nondefault->layerName ( i ) );

        if ( nondefault->hasLayerWidth ( i ) )
        {
            xmlpp::Element* width = parent->add_child ( "WIDTH" );
            set_attribute ( width, "width", nondefault->layerWidth ( i ) );
        }
        if ( nondefault->hasLayerSpacing ( i ) )
        {
            xmlpp::Element* spacing = parent->add_child ( "SPACING" );
            set_attribute ( spacing, "spacing", nondefault->layerSpacing ( i ) );
        }
        if ( nondefault->hasLayerDiagWidth ( i ) )
        {
            xmlpp::Element* diagWidth = parent->add_child ( "DIAGWIDTH" );
            set_attribute ( diagWidth, "diagwith", nondefault->layerDiagWidth ( i ) );
        }
        if ( nondefault->hasLayerWireExtension ( i ) )
        {
            xmlpp::Element* wireextension = parent->add_child ( "WIREEXTENSION" );
            set_attribute ( wireextension, "wireextension", nondefault->layerWireExtension ( i ) );
        }
        if ( nondefault->hasLayerResistance ( i ) )
        {
            xmlpp::Element* resistance = parent->add_child ( "RESISTANCE" );
            set_attribute ( resistance, "persq", nondefault->layerResistance ( i ) );
        }
        if ( nondefault->hasLayerCapacitance ( i ) )
        {
            xmlpp::Element* capacitance = parent->add_child ( "CAPACITANCECPERSQDIST" );
            set_attribute ( capacitance, "srsqdist", nondefault->layerCapacitance ( i ) );
        }
        if ( nondefault->hasLayerEdgeCap ( i ) )
        {
            xmlpp::Element* spacing = parent->add_child ( "SPACING" );
            set_attribute ( spacing, "spacing", nondefault->layerEdgeCap ( i ) );
        }
    }
    /*for (int i = 0; i < nondefault->numVias(); i++) {
    xmlpp::Element* temp = node->add_child("VIA");
    temp->set_attribute ( "name", nondefault->viaRule(i) );
    }*/ // da fare regole VIARULES
    return 0;
}

int readLefExtensionCallback ( lefrCallbackType_e c, const char* extsn,xmlpp::Element* parent )
{
    xmlpp::Element* node = parent->add_child ( "BEGINEXT" );
    node->set_attribute ( "name", extsn );
    return 0;
}

int readLefPropCbkCallback ( lefrCallbackType_e c, lefiProp* property,xmlpp::Element* parent )
{
    xmlpp::Element* propertyDefinitions = parent->add_child ( "PROPERTYDEFINITIONS" );
    xmlpp::Element* propType = propertyDefinitions->add_child ( "PROPTYPE" );
    propertyDefinitions->set_attribute ( "proptype", property->propType() );
    propertyDefinitions->set_attribute ( "propname", property->propName() );
    set_datatype(propertyDefinitions,property->dataType());
    if ( property->hasNumber() )
    {
        set_attribute ( propertyDefinitions, "value", property->number() );
    }
    if ( property->hasString() )
    {
        propertyDefinitions->set_attribute ( "name", property->string() );
    }
    if ( property->hasRange() )
    {
        xmlpp::Element* range = propertyDefinitions->add_child ( "RANGE" );
        set_attribute ( range, "left", property->left() );
        set_attribute ( range, "right", property->right() );
    }
    return 0;
}

int readLefSpacingCallback ( lefrCallbackType_e c, lefiSpacing* spacing,xmlpp::Element* parent )
{
    xmlpp::Element* node = parent->add_child ( "SPACING" );
    node->set_attribute ( "spacingname1", spacing->name1() );
    node->set_attribute ( "spacingname2", spacing->name2() );
    set_attribute ( node,"distance", spacing->distance() );
    if ( spacing->hasStack() )
    {
        xmlpp::Element* temp = node->add_child ( "STACK" );
    }

    return 0;
}

int readLefSiteCallback ( lefrCallbackType_e c, lefiSite* site,xmlpp::Element* parent )
{
    xmlpp::Element* sitenode = parent->add_child ( "SITE" );
    sitenode->set_attribute ( "sitename", site->name() );
    xmlpp::Element* classnode = sitenode->add_child ( "CLASS" );

    if ( site->hasClass() )
    {
        classnode->set_attribute ( "typeclass", site->siteClass() );
    }
    if ( site->hasXSymmetry() )
    {
        xmlpp::Element* symmetryx = classnode->add_child ( "SYMMETRYX" );
    }
    if ( site->hasYSymmetry() )
    {
        xmlpp::Element* symmetryy = classnode->add_child ( "SYMMETRYY" );
    }
    else
    {
        xmlpp::Element* y = classnode->add_child ( "Y" );
    }

    return 0;
}

int readLefUseMinSpacingCallback ( lefrCallbackType_e c, lefiUseMinSpacing* spacing,xmlpp::Element* parent )
{
    xmlpp::Element* useminspacing = parent->add_child ( "USEMINSPACING" );
    useminspacing->set_attribute ( "name", spacing->name() );
    if (spacing->value()) {
        xmlpp::Element* on = useminspacing->add_child ( "ON" );
    }
    else {
        xmlpp::Element* off = useminspacing->add_child ( "OFF" );
    }
    return 0;
}

int readLefViaRuleCallback ( lefrCallbackType_e c, lefiViaRule* viaRule ,xmlpp::Element* parent )
{
    int numLayers, numVias, i;
    lefiViaRuleLayer* vLayer;
    
    xmlpp::Element* viaRuleNode = parent->add_child ( "VIARULE" );
    viaRuleNode->set_attribute ( "name", viaRule->name() );

  if (viaRule->hasGenerate()){
     xmlpp::Element* viaRuleNode = parent->add_child ( "VIARULE" );
     xmlpp::Element* node = viaRuleNode->add_child ( "GENERATE" );
  }
  if (viaRule->hasDefault()) {
     xmlpp::Element* viaRuleNode = parent->add_child ( "VIARULE" );
     xmlpp::Element* node = viaRuleNode->add_child ( "DEFAULT" );
  
  }

  numLayers = viaRule->numLayers();
  for ( i = 0; i < numLayers; i++) {
     vLayer = viaRule->layer(i); 
  } 
   
  if (numLayers == 2 && !(viaRule->hasGenerate())) {
     numVias = viaRule->numVias();
     if (numVias == 0) {
         xmlpp::Element* viaRuleNode = parent->add_child ( "VIARULE" );
         viaRuleNode->set_attribute ( "numVias0", "Should have via names in VIARULE" );
     }   
     else {
        for (i = 0; i < numVias; i++) {
            xmlpp::Element* viaRuleNode = parent->add_child ( "VIARULE" );
            xmlpp::Element* via = viaRuleNode->add_child ( "VIA" );
            via->set_attribute ( "name", viaRule->viaName(i) );
        }   
     }
  } 
    if (viaRule->numProps() > 0) {
     xmlpp::Element* viaRuleNode = parent->add_child ( "VIARULE" );
     xmlpp::Element* via = viaRuleNode->add_child ( "PROPERTY" );
     for (i = 0; i < viaRule->numProps(); i++) {
         xmlpp::Element* viaRuleNode = parent->add_child ( "VIARULE" );
         xmlpp::Element* via = viaRuleNode->add_child ( "PROPERTY" );
         viaRuleNode->set_attribute ( "name", viaRule->propName(i) );
        if (viaRule->propValue(i)){
         xmlpp::Element* viaRuleNode = parent->add_child ( "VIARULE" );
         xmlpp::Element* via = viaRuleNode->add_child ( "PROPERTY" );
         viaRuleNode->set_attribute ( "value", viaRule->propValue(i) );
        }
        string subtype;
        switch (viaRule->propType(i)) {
           case 'R': subtype = "REAL";
                     break;
           case 'I': subtype = "INTEGER";
                     break;
           case 'S': subtype = "STRING";
                     break;
           case 'Q': subtype = "QUOTESRTING";
                     break;
           case 'N': subtype = "NUMBER";
                     break;
        }
        viaRuleNode->set_attribute ( "type",subtype );
     }
  } 
   
    return 0;
}

int readLefLayerCallback ( lefrCallbackType_e c,  lefiLayer* layer ,xmlpp::Element* parent )
{
   int i, j, k;
  int numPoints, propNum;
  double *widths, *current;
  lefiLayerDensity* density;
  lefiAntennaPWL* pwl;
  lefiSpacingTable* spTable;
  lefiInfluence* influence;
  lefiParallel* parallel;
  lefiTwoWidths* twoWidths;
  char pType;
  int numMinCut, numMinenclosed;
  lefiAntennaModel* aModel;
  lefiOrthogonal*   ortho; 
  
  xmlpp::Element* layerNode = parent->add_child ( "LAYER" );
  layerNode->set_attribute ( "name", layer->name() );
  
  if (layer->hasType()){
      xmlpp::Element* type = layerNode->add_child ( "TYPE" );
      type->set_attribute ( "type", layer->type() );
  }
  if (layer->hasPitch()){
      xmlpp::Element* pitch = layerNode->add_child ( "PITCH" );
      set_attribute ( pitch,"type", layer->pitch() );
  }
  
  else if (layer->hasXYPitch()){
      xmlpp::Element* pitch = layerNode->add_child ( "PITCHXY" );
      set_attribute ( pitch,"x", layer->pitchX() );
      set_attribute ( pitch,"y", layer->pitchY() );
  }           
  if (layer->hasOffset()){
      xmlpp::Element* offset = layerNode->add_child ( "OFFSET" );
      set_attribute ( offset,"offset", layer->offset() );
  }   
  else if (layer->hasXYOffset()){
      xmlpp::Element* offset = layerNode->add_child ( "OFFSETXY" );
      set_attribute ( offset,"x", layer->offsetX() ); 
      set_attribute ( offset,"y", layer->offsetY() );
  }           
  if (layer->hasDiagPitch()){
      xmlpp::Element* diagPitch = layerNode->add_child ( "DIAGPITCH" );
      set_attribute ( diagPitch,"pitch", layer->diagPitch() ); 
  }   
  else if (layer->lefiLayer::hasXYDiagPitch()){
      xmlpp::Element* diagPitch = layerNode->add_child ( "DIAGPITCHXY" );
      set_attribute ( diagPitch,"x", layer->diagPitchX() ); 
      set_attribute ( diagPitch,"y", layer->diagPitchY() ); 
  }           
  if (layer->hasDiagWidth()){
     xmlpp::Element* diagWidth = layerNode->add_child ( "DIAGWIDTH" );
      set_attribute ( diagWidth,"value", layer->diagWidth() );  
  }   
  if (layer->hasDiagSpacing()){
     xmlpp::Element* diagSpacing = layerNode->add_child ( "DIAGSPACING" );
      set_attribute ( diagSpacing,"value", layer->diagSpacing() );  
  }   
  if (layer->hasWidth()){
      xmlpp::Element* with = layerNode->add_child ( "WIDTH" );
      set_attribute ( with,"value", layer->width() );  
  }   
  if (layer->hasArea()){
     xmlpp::Element* area = layerNode->add_child ( "AREA" );
      set_attribute ( area,"value", layer->area() );   
  }   
  if (layer->hasSlotWireWidth()){
     xmlpp::Element* slotWireWidth = layerNode->add_child ( "SLOTWIREWIDTH" );
      set_attribute ( slotWireWidth,"value", layer->slotWireWidth() );  
  }   
  if (layer->hasSlotWireLength()){
     xmlpp::Element* slotWireLength = layerNode->add_child ( "SLOTWIRELENGTH" );
      set_attribute ( slotWireLength,"value", layer->slotWireLength() );  
  }           
  if (layer->hasSlotWidth()){
      xmlpp::Element* slotWidth = layerNode->add_child ( "SLOTWIDTH" );
      set_attribute ( slotWidth,"value", layer->slotWidth() );  
  }   
  if (layer->hasSlotLength()){
     xmlpp::Element* slotLength = layerNode->add_child ( "SLOTLENGTH" );
      set_attribute ( slotLength,"value", layer->slotLength() );  
  }   
  if (layer->hasMaxAdjacentSlotSpacing()){
     xmlpp::Element* maxAdjacentSlotSpacing = layerNode->add_child ( "MAXADJACENTSLOTSPACING" );
      set_attribute ( maxAdjacentSlotSpacing,"value", layer->maxAdjacentSlotSpacing() );  
  }           
  if (layer->hasMaxCoaxialSlotSpacing()){
     xmlpp::Element* maxCoaxialSlotSpacing = layerNode->add_child ( "MAXCOAXIALSLOTSPACING" );
      set_attribute ( maxCoaxialSlotSpacing,"value", layer->maxCoaxialSlotSpacing() );  
  }           
  if (layer->hasMaxEdgeSlotSpacing()){
     xmlpp::Element* maxEdgeSlotSpacing = layerNode->add_child ( "MAXEDGESLOTSPACING" );
      set_attribute ( maxEdgeSlotSpacing,"value", layer->maxEdgeSlotSpacing() );  
  }           
  if (layer->hasMaxFloatingArea()){          // 5.7
     xmlpp::Element* maxFloatingArea = layerNode->add_child ( "MAXFLOATINGAREA" );
      set_attribute ( maxFloatingArea,"value", layer->maxFloatingArea() );  
  }           
  if (layer->hasArraySpacing()) {           // 5.7
     xmlpp::Element* arraySpacing = layerNode->add_child ( "ARRAYSPACING" );
     if (layer->hasLongArray()){
         xmlpp::Element* longArray = arraySpacing->add_child ( "LONGARRAY" );
     }   
     if (layer->hasViaWidth()){
         xmlpp::Element* width = arraySpacing->add_child ( "WIDTH" );
         set_attribute ( width,"value", layer->viaWidth() );  
         xmlpp::Element* cutSpacing = arraySpacing->add_child ( "CUTSPACING" );
         set_attribute ( cutSpacing,"value", layer->viaWidth() );  
     }
     for (i = 0; i < layer->numArrayCuts(); i++) {
         xmlpp::Element* arrayCuts = arraySpacing->add_child ( "ARRAYCUTS" );
         set_attribute ( arrayCuts,"value", layer->arrayCuts(i) );  
         xmlpp::Element* spacing = arraySpacing->add_child ( "SPACING" );
         set_attribute ( spacing,"value", layer->arraySpacing(i) );  
      }
  }
  if (layer->hasSplitWireWidth()){
      xmlpp::Element* splitWireWidth = layerNode->add_child ( "SPLITWIREWIDTH" );
      set_attribute ( splitWireWidth,"value", layer->splitWireWidth() );
  }           
  if (layer->hasMinimumDensity()){
      xmlpp::Element* minimumDensity = layerNode->add_child ( "MINIMUMDENSITY" );
      set_attribute ( minimumDensity,"value", layer->minimumDensity() );
  }           
  if (layer->hasMaximumDensity()){
     xmlpp::Element* maximumDensity = layerNode->add_child ( "MAXIMUMDENSITY" );
      set_attribute ( maximumDensity,"value", layer->maximumDensity() );
  }           
  if (layer->hasDensityCheckWindow()){
     xmlpp::Element* densityCheckWindow = layerNode->add_child ( "DENSITYCHECKWINDOW" );
      set_attribute ( densityCheckWindow,"value", layer->densityCheckWindowLength() );
      set_attribute ( densityCheckWindow,"value", layer->densityCheckWindowWidth() );
  }           
  if (layer->hasDensityCheckStep()){
     xmlpp::Element* densityCheckStep = layerNode->add_child ( "DENSITYCHECKSTEP" );
      set_attribute ( densityCheckStep,"value", layer->densityCheckStep() );
  }           
  if (layer->lefiLayer::hasFillActiveSpacing()){
      xmlpp::Element* fillActiveSpacing = layerNode->add_child ( "FILLACTIVESPACING" );
      set_attribute ( fillActiveSpacing,"value", layer->fillActiveSpacing() );
  }
  
  numMinCut = layer->numMinimumcut();
  if (numMinCut > 0) {
     for (i = 0; i < numMinCut; i++) {
         xmlpp::Element* minimumcut = layerNode->add_child ( "MINIMUMCUT" );
         set_attribute ( minimumcut,"value", layer->minimumcut(i) );
         xmlpp::Element* minimumcutWidth = layerNode->add_child ( "WIDTH" );
         set_attribute ( minimumcut,"value", layer->minimumcutWidth(i) );
         if (layer->hasMinimumcutWithin(i)){
           xmlpp::Element* within = layerNode->add_child ( "WITHIN" );
           set_attribute ( within,"value", layer->minimumcutWithin(i) );
         }   
         if (layer->hasMinimumcutConnection(i)){
         xmlpp::Element* connection = layerNode->add_child ( "CONNECTION" );
         connection->set_attribute ( "value", layer->minimumcutConnection(i) );
         }   
         if (layer->hasMinimumcutNumCuts(i)){
         xmlpp::Element* minimumcutNum = layerNode->add_child ( "MINIMUMCUTNUM" );
         xmlpp::Element* cutLenght = minimumcutNum->add_child ( "LENGTH" );
         set_attribute ( cutLenght,"value", layer->minimumcutLength(i) );
         xmlpp::Element* distance = minimumcutNum->add_child ( "DISTANCE" );
         set_attribute ( distance,"value", layer->minimumcutDistance(i) );
         }
     }
  }
  // 5.4.1
  if (layer->hasMaxwidth()) {
     xmlpp::Element* maxWidth = layerNode->add_child ( "MAXWIDTH" );
      set_attribute ( maxWidth,"value", layer->maxwidth() ); 
  }
  // 5.5
  if (layer->hasMinwidth()) {
     xmlpp::Element* minWidth = layerNode->add_child ( "MINWIDTH" );
      set_attribute ( minWidth,"value", layer->minwidth() );
  }
  // 5.5
  numMinenclosed = layer->numMinenclosedarea();
  if (numMinenclosed > 0) {
     for (i = 0; i < numMinenclosed; i++) {
         xmlpp::Element* minenclosedarea = layerNode->add_child ( "MINCLOSEDAREA" );
         set_attribute ( minenclosedarea,"value", layer->minenclosedarea(i) );
         if (layer->hasMinenclosedareaWidth(i)){
             xmlpp::Element* minenclosedareaWidth = layerNode->add_child ( "MINCLOSEDAREAWIDTH" );
             set_attribute ( minenclosedareaWidth,"value", layer->minenclosedareaWidth(i) );
         }             
     }
  }
  // 5.4.1 & 5.6
  if (layer->hasMinstep()) {
     for (i = 0; i < layer->numMinstep(); i++) {
        xmlpp::Element* minstep = layerNode->add_child ( "MINSTEP" );
         set_attribute ( minstep,"value", layer->minstep(i) );
        if (layer->hasMinstepType(i)){
           xmlpp::Element* steptype = layerNode->add_child ( "STEPTYPE" );
          steptype->set_attribute ("type", layer->minstepType(i) );
        }   
        if (layer->hasMinstepLengthsum(i)){
           xmlpp::Element* lengthsum = layerNode->add_child ( "LENGTHSUM" );
         set_attribute ( lengthsum,"value", layer->minstepLengthsum(i) );
        }           
        if (layer->hasMinstepMaxedges(i)){
            xmlpp::Element* maxedges = layerNode->add_child ( "MAXEDGES" );
         set_attribute ( maxedges,"value", layer->minstepMaxedges(i) );
        }
     }
  }
  // 5.4.1
  if (layer->lefiLayer::hasProtrusion()) {
      xmlpp::Element* protrusionWidth = layerNode->add_child ( "PROTRUSIONWIDTH" );
         set_attribute ( protrusionWidth,"value", layer->protrusionWidth1() );
         xmlpp::Element* protlength = layerNode->add_child ( "LENGTH" );
         set_attribute ( protlength,"value", layer->protrusionLength() );
         xmlpp::Element* protwidth = layerNode->add_child ( "WIDTH" );
         set_attribute ( protwidth,"value", layer->protrusionWidth2() );
  } 
  if (layer->hasSpacingNumber()) {
     for (i = 0; i < layer->numSpacing(); i++) {
         xmlpp::Element* spacingnode = layerNode->add_child ( "SPACING" );
         set_attribute ( spacingnode,"value", layer->spacing(i) );  
       if (layer->hasSpacingName(i)){
          xmlpp::Element* layernode = spacingnode->add_child ( "LAYER" );
          layernode->set_attribute ( "value", layer->spacingName(i) );  
       }
       if (layer->hasSpacingLayerStack(i)){
          xmlpp::Element* spacing = spacingnode->add_child ( "STACK" );
       }   
       if (layer->hasSpacingAdjacent(i)){
          xmlpp::Element* adjacent = spacingnode->add_child ( "ADJACENTCUTS" );
          set_attribute ( adjacent,"value", layer->spacingAdjacentCuts(i) );  
          xmlpp::Element* within = spacingnode->add_child ( "WITHIN" );
          set_attribute ( within,"value", layer->spacingAdjacentWithin(i) ); 
       }           
       if (layer->hasSpacingAdjacentExcept(i)){    // 5.7
          xmlpp::Element* except = spacingnode->add_child ( "EXCEPTSAMEPGNET" );
       }   
       if (layer->hasSpacingCenterToCenter(i)){
          xmlpp::Element* center = spacingnode->add_child ( "CENTERTOCENTER" );
       }   
       if (layer->hasSpacingSamenet(i)) {          // 5.7
          xmlpp::Element* samenet = spacingnode->add_child ( "SAMENET" );
       }   
           if (layer->hasSpacingSamenetPGonly(i)){ // 5.7
              xmlpp::Element* pgonly = spacingnode->add_child ( "PGONLY" );
           }
       if (layer->hasSpacingArea(i)){              // 5.7
          xmlpp::Element* area = spacingnode->add_child ( "AREA" );
          set_attribute ( area,"value", layer->spacingArea(i) );  
       }   
       if (layer->hasSpacingRange(i)) {
           xmlpp::Element* range = spacingnode->add_child ( "RANGE" );
          set_attribute ( range,"min", layer->spacingRangeMin(i) );  
          set_attribute ( range,"max", layer->spacingRangeMax(i) );  
          if (layer->hasSpacingRangeUseLengthThreshold(i)){
             xmlpp::Element* threshold = range->add_child ( "USELENGTHTHRESHOLD" );
          }   
          else if (layer->hasSpacingRangeInfluence(i)) {
              xmlpp::Element* influence = range->add_child ( "INFLUENCE" );
              set_attribute ( influence,"value", layer->spacingRangeInfluence(i) );  
              if (layer->hasSpacingRangeInfluenceRange(i)){
                  xmlpp::Element* influencerange = influence->add_child ( "INFLUENCERANGE" );
                 set_attribute ( influencerange,"min", layer->spacingRangeInfluenceMin(i) );  
                 set_attribute ( influencerange,"max", layer->spacingRangeInfluenceMax(i) );   
              }      
              } else if (layer->hasSpacingRangeRange(i)){
                      xmlpp::Element* rangerange = range->add_child ( "RANGE" );
                      set_attribute ( rangerange,"min", layer->spacingRangeRangeMin(i) );  
                      set_attribute ( rangerange,"max", layer->spacingRangeRangeMax(i) );   
             }    
       } else if (layer->hasSpacingLengthThreshold(i)) {
           xmlpp::Element* lengthreshold = spacingnode->add_child ( "LENGTHTHRESHOLD" );
          set_attribute ( lengthreshold,"value", layer->spacingLengthThreshold(i) );      
           if (layer->hasSpacingLengthThresholdRange(i)){
              xmlpp::Element* rangethreshold = spacingnode->add_child ( "THRESHOLDRANGE" );
          set_attribute ( rangethreshold,"min", layer->spacingLengthThresholdRangeMin(i) );      
          set_attribute ( rangethreshold,"max", layer->spacingLengthThresholdRangeMax(i) );      
           }     
       } else if (layer->hasSpacingNotchLength(i)) { // 5.7
           // INIZIO NUOVA SINTASSI DI COSTRUTTI
          create_element(spacingnode,"NOTCHLENGTH","min", layer->spacingNotchLength(i));
       } else if (layer->hasSpacingEndOfNotchWidth(i)) { // 5.7
           create_element(spacingnode,"ENDOFNOTCHWIDTH","value", layer->spacingEndOfNotchWidth(i));
           create_element(spacingnode,"NOTCHSPACING","value", layer->spacingEndOfNotchSpacing(i));
           create_element(spacingnode,"NOTCHLENGTH","value", layer->spacingEndOfNotchLength(i));
          }         

       if (layer->hasSpacingParallelOverlap(i)) {  // 5.7
          xmlpp::Element* parallelOverlap = spacingnode->add_child ( "PARALLELOVERLAP" );
       }   
       if (layer->hasSpacingEndOfLine(i)) {       // 5.7
           xmlpp::Element * eof = create_element(spacingnode,"ENDOFLINE","eolWidth", layer->spacingEolWidth(i));
           set_attribute(eof, "eolWithin",layer->spacingEolWithin(i));
          if (layer->lefiLayer::hasSpacingParellelEdge(i)) {
            create_element(eof,"PARALLELEDGE","value", layer->spacingParSpace(i));
            set_attribute(eof, "eolWithin",layer->spacingParWithin(i));
             if (layer->hasSpacingTwoEdges(i)) {
              set_attribute(eof, "twoEdges","TWOEDGES");
             }
          }
       }
     }
  }
  
  if (layer->hasSpacingTableOrtho()) {            // 5.7
      ortho = layer->orthogonal();
      xmlpp::Element* spacingTable = layerNode->add_child ( "SPACINGTABLEORTHOGONAL" );
     for (i = 0; i < ortho->numOrthogonal(); i++) {
         create_element(spacingTable,"WITHIN","value", ortho->cutWithin(i));
         create_element(spacingTable,"SPACING","value", ortho->orthoSpacing(i));
     }
  }
  for (i = 0; i < layer->numEnclosure(); i++) {
     xmlpp::Element* enclosure = layerNode->add_child ( "ENCLOSURE" );
     if (layer->hasEnclosureRule(i)){
         create_element(enclosure,"RULE","value", layer->enclosureRule(i));
         create_element(enclosure,"OVER1","value", layer->enclosureOverhang1(i));
         create_element(enclosure,"OVER2","value", layer->enclosureOverhang2(i));
     }    
     if (layer->hasEnclosureWidth(i)){
         create_element(enclosure,"WIDTH","value", layer->enclosureMinWidth(i));
     }    
     if (layer->hasEnclosureExceptExtraCut(i)){
        create_element(enclosure,"EXEPTEXTRACUT","value", layer->enclosureExceptExtraCut(i));
     }   
     if (layer->hasEnclosureMinLength(i)){
         create_element(enclosure,"LENGTH","value", layer->enclosureMinLength(i));
     }   
  }
  for (i = 0; i < layer->numPreferEnclosure(); i++) {
      xmlpp::Element* preferenclosure = layerNode->add_child ( "PREFERENCLOSURE" );
     if (layer->lefiLayer::hasPreferEnclosureRule(i)){
        create_element(preferenclosure,"RULE","name", layer->preferEnclosureRule(i)); 
        create_element(preferenclosure,"OVER1","value", layer->preferEnclosureOverhang1(i)); 
        create_element(preferenclosure,"OVER2","value", layer->preferEnclosureOverhang2(i)); 
     }                        
     if (layer->hasPreferEnclosureWidth(i)){
         create_element(preferenclosure,"WIDTH","value", layer->preferEnclosureMinWidth(i)); 
     }
  }
  if (layer->hasResistancePerCut()){
      create_element(layerNode,"RESISTANCE","resistancePerCut", layer->resistancePerCut()); 
  }         
  if (layer->hasCurrentDensityPoint()){
     create_element(layerNode,"CURRENTDEN","value", layer->currentDensityPoint()); 
  }   
  if (layer->hasCurrentDensityArray()) { 
     layer->currentDensityArray(&numPoints, &widths, &current);
     for (i = 0; i < numPoints; i++){
         create_element(layerNode,"CURRENTDENARRAY","withs", widths[i]); 
         create_element(layerNode,"CURRENTDENARRAY","currents", current[i]); 
     }    
  }
  if (layer->hasDirection()){
     create_element(layerNode,"DIRECTION","value", layer->direction()); 
  }   
  if (layer->hasResistance()){
      create_element(layerNode,"RESISTANCE","rpersq", layer->resistance()); 
  }           
  if (layer->hasCapacitance()){
     create_element(layerNode,"CAPACITANCE","cpersqdist", layer->capacitance());
  }           
  if (layer->hasEdgeCap()){
     create_element(layerNode,"EDGECAPACITANCE","value", layer->edgeCap());
  }   
  if (layer->hasHeight()){
      create_element(layerNode,"TYPE","value", layer->height());
  }   
  if (layer->hasThickness()){
     create_element(layerNode,"THICKNESS","value", layer->thickness());
  }   
  if (layer->hasWireExtension()){
      create_element(layerNode,"WIREEXTENSION","value", layer->wireExtension());
  }   
  if (layer->hasShrinkage()){
     create_element(layerNode,"SHRINKAGE","value", layer->shrinkage());
  }   
  if (layer->hasCapMultiplier()){
     create_element(layerNode,"CAPMULTIPLIER","value", layer->capMultiplier());
  }   
  if (layer->hasAntennaArea()){
      create_element(layerNode,"ANTENNAAREAFACTOR","value", layer->antennaArea());
  }           
  if (layer->hasAntennaLength()){
     create_element(layerNode,"ANTENNALENGTHFACTOR","value", layer->antennaLength());
  }
  for (i = 0; i < layer->numAntennaModel(); i++) {
     aModel = layer->antennaModel(i);
     create_element(layerNode,"ANTENNAMODEL","name", aModel->antennaOxide());     
     if (aModel->hasAntennaAreaRatio()){
        create_element(layerNode,"ANTENNAAREARATIO","value", aModel->antennaAreaRatio());     
     }           
     if (aModel->hasAntennaDiffAreaRatio()){
        create_element(layerNode,"ANTENNADIFFAREARATIO","value", aModel->antennaDiffAreaRatio());     
     }           
     else if (aModel->hasAntennaDiffAreaRatioPWL()) {
        pwl = aModel->antennaDiffAreaRatioPWL();
        xmlpp::Element* antennadiffarea = layerNode->add_child ( "ANTENNADIFFAREARATIOPWL" );
        for (j = 0; j < pwl->numPWL(); j++){
            create_element(antennadiffarea,"PWLDIFFUSION","value", pwl->PWLdiffusion(j));    
            create_element(antennadiffarea,"PWLRATIO","value", pwl->PWLratio(j));     
        }           
     }
     if (aModel->hasAntennaCumAreaRatio()){
        create_element(layerNode,"ANTENNADIFFAREARATIO","value", aModel->antennaCumAreaRatio());     
     }           
     if (aModel->hasAntennaCumDiffAreaRatio()){
        create_element(layerNode,"ANTENNACUMDIFFAREARATIO","value", aModel->antennaCumDiffAreaRatio());     
     }           
     if (aModel->hasAntennaCumDiffAreaRatioPWL()) {
        pwl = aModel->antennaCumDiffAreaRatioPWL();
        xmlpp::Element* antennadiffarea = layerNode->add_child ( "ANTENNACUMDIFFAREARATIOPWL" );
        for (j = 0; j < pwl->numPWL(); j++){
           create_element(antennadiffarea,"PWLDIFFUSION","value", pwl->PWLdiffusion(j));
           create_element(antennadiffarea,"PWLRATIO","value", pwl->PWLratio(j));     
        }           
     }
     if (aModel->hasAntennaAreaFactor()) {
        xmlpp::Element* antennaareafactor = create_element(layerNode,"ANTENNAAREAFACTOR","value", aModel->antennaAreaFactor());      
        if (aModel->hasAntennaAreaFactorDUO()){
           set_attribute( antennaareafactor, "antennaareafactor","DIFFUSEONLY");
        }   
     }
     if (aModel->hasAntennaSideAreaRatio()){
        create_element(layerNode,"ANTENNADIFFAREARATIO","value", aModel->antennaSideAreaRatio());     
     }           
     if (aModel->hasAntennaDiffSideAreaRatio()){
        create_element(layerNode,"ANTENNADIFFSIDEAREARATIO","value", aModel->antennaDiffSideAreaRatio());     
     }           
     else if (aModel->hasAntennaDiffSideAreaRatioPWL()) {
        pwl = aModel->antennaDiffSideAreaRatioPWL();
        xmlpp::Element* antennaDiffSideAreaRatio = layerNode->add_child ( "ANTENNADIFFSIDEAREARATIOPWL" );
        for (j = 0; j < pwl->numPWL(); j++){
           create_element(antennaDiffSideAreaRatio,"PWLDIFF","value", pwl->PWLdiffusion(j)); 
           create_element(antennaDiffSideAreaRatio,"PWLRATIO","value", pwl->PWLratio(j)); 
        }           
     }
     if (aModel->hasAntennaCumSideAreaRatio()){
        create_element(layerNode,"ANTENNADIFFSIDEAREARATIO","value", aModel->antennaCumSideAreaRatio());     
     }           
     if (aModel->hasAntennaCumDiffSideAreaRatio()){
        create_element(layerNode,"ANTENNADIFFSIDEAREARATIO","value", aModel->antennaCumDiffSideAreaRatio());     
     }           
     else if (aModel->hasAntennaCumDiffSideAreaRatioPWL()) {
        pwl = aModel->antennaCumDiffSideAreaRatioPWL();
        xmlpp::Element* antennaCumDiffSideAreaRatio = layerNode->add_child ( "ANTENNACUMDIFFSIDEAREARATIOPWL" );
        for (j = 0; j < pwl->numPWL(); j++){
           create_element(antennaCumDiffSideAreaRatio,"PWLDIFF","value", pwl->PWLdiffusion(j));     
           create_element(antennaCumDiffSideAreaRatio,"PWLRATIO","value", pwl->PWLratio(j));      
        }           
     }
     if (aModel->hasAntennaSideAreaFactor()) {
        create_element(layerNode,"ANTENNASIDEAREAFACTOR","value", aModel->antennaSideAreaFactor());   
        if (aModel->hasAntennaSideAreaFactorDUO()){
           xmlpp::Element* AntennaSideAreaFactorDUO = layerNode->add_child ( "DIFFUSEONLY" );
        }
     }
     if (aModel->hasAntennaCumRoutingPlusCut()){
         xmlpp::Element* AntennaCumRoutingPlusCut = layerNode->add_child ( "ANTENNACUMROUTINGPLUSCUT" );
     }   
     if (aModel->hasAntennaGatePlusDiff()){
         create_element(layerNode,"ANTENNAGATEPLUSDIFF","value", aModel->antennaGatePlusDiff());   
     }           
     if (aModel->hasAntennaAreaMinusDiff()){
         create_element(layerNode,"ANTENNAGATEPLUSDIFF","value", aModel->antennaAreaMinusDiff());   
     }           
     if (aModel->hasAntennaAreaDiffReducePWL()) {
        pwl = aModel->antennaAreaDiffReducePWL();
        xmlpp::Element* antennaAreaDiffReducePWL = layerNode->add_child ( "ANTENNAAREADIFFREDUCEPWL" );
        for (j = 0; j < pwl->numPWL(); j++){
           create_element(antennaAreaDiffReducePWL,"PWLDIFF","value", pwl->PWLdiffusion(j));     
           create_element(antennaAreaDiffReducePWL,"PWLRATIO","value", pwl->PWLratio(j));      
        }           
     }
  }
  if (layer->numAccurrentDensity()) {
     for (int i = 0; i < layer->numAccurrentDensity(); i++) {
         density = layer->accurrent(i);
         create_element(layerNode,"ACCURRENTDENSITY","type", density->type());
         if (density->hasOneEntry()){
             create_element(layerNode,"ONEENTRY","value", density->oneEntry());
         }
         else {
             if (density->numFrequency()) {
                xmlpp::Element* density_node = layerNode->add_child ( "FREQUENCY" ); 
                for (j = 0; j < density->numFrequency(); j++){
                    set_attribute ( density_node, "value", density->frequency ( j ) );
                }
             }
             if (density->numCutareas()) {
                xmlpp::Element* density_node = layerNode->add_child ( "CUTAREA" ); 
                for (j = 0; j < density->numCutareas(); j++){
                      set_attribute ( density_node, "value", density->cutArea ( j ) );
                }
             }
             if (density->numWidths()) {
                xmlpp::Element* density_node = layerNode->add_child ( "WIDTH" ); 
                for (j = 0; j < density->numWidths(); j++){
                    set_attribute ( density_node, "value", density->width ( j ) );
                }    
             }
             
             if (density->numTableEntries()) {
                k = 5;
                xmlpp::Element* density_node = layerNode->add_child ( "TABLEENTRIES" ); 
                for (j = 0; j < density->numTableEntries(); j++){
                    if (k > 4) {
                      set_attribute ( density_node, "value", density->tableEntry ( j ) );  
                      k = 1;
                   } else {
                      set_attribute ( density_node, "value", density->tableEntry ( j ) );  
                      k++;
                   }
                }   
             }
         }
     }
  }
  if (layer->numDccurrentDensity()) {
     for (i = 0; i < layer->numDccurrentDensity(); i++) {
         density = layer->dccurrent(i);
         xmlpp::Element* current_node = layerNode->add_child ( "DCCURRENTDENSITY" );
         //xmlpp::Element* current_node = create_element(layerNode,"DCCURRENTDENSITY","type", density->type(i));     
         test_set(current_node,density->hasOneEntry(),"value",density->oneEntry());
         /*if (density->hasOneEntry()){
             fprintf(fout, " %g ;\n", density->oneEntry()); 
         }*/    
         if (!density->hasOneEntry()) {
             if (density->numCutareas()) {
                xmlpp::Element* cutarea = layerNode->add_child ( "CUTAREA" );
                for (j = 0; j < density->numCutareas(); j++){
                   set_attribute ( cutarea, "value", density->cutArea ( j ) );   
                }
             }
             if (density->numWidths()) {
                xmlpp::Element* width = layerNode->add_child ( "WIDTH" );
                for (j = 0; j < density->numWidths(); j++){
                   set_attribute ( width, "value", density->width ( j ) ); 
                }   
             }
             if (density->numTableEntries()) {
                 xmlpp::Element* tableentry = layerNode->add_child ( "TABLEENTRIES" );
                for (j = 0; j < density->numTableEntries(); j++){
                   set_attribute ( tableentry, "value", density->tableEntry ( j ) );   
                }   
             }
         }
     }
  }

  for (i = 0; i < layer->numSpacingTable(); i++) {
     spTable = layer->spacingTable(i);
     xmlpp::Element* spacingtable_node = layerNode->add_child ( "SPACINGTABLE" );
     if (spTable->isInfluence()) {
        influence = spTable->influence();
        xmlpp::Element* influence_node = spacingtable_node->add_child ( "INFLUENCE" );
        for (j = 0; j < influence->numInfluenceEntry(); j++) {
            set_attribute ( influence_node, "WIDTH", influence->width( j ) );   
            set_attribute ( influence_node, "WITHIN", influence->distance( j ) );   
            set_attribute ( influence_node, "SPACING", influence->spacing( j ) );   
        }   
     } else if (spTable->isParallel()){
        parallel = spTable->parallel();
        xmlpp::Element* parallel_node = spacingtable_node->add_child ( "PARALLELRUNLENGTH" );
        for (j = 0; j < parallel->numLength(); j++) {
            set_attribute ( parallel_node, "value", parallel->length ( j ) );   
        }
        for (j = 0; j < parallel->numWidth(); j++) {
           xmlpp::Element* width_node = create_element ( parallel_node,"WIDTH","value", parallel->width ( j ) );           
           for (k = 0; k < parallel->numLength(); k++) {
              set_attribute ( width_node, "value", parallel->widthSpacing ( j,k ) );    
           }
        }
     } else {    // 5.7 TWOWIDTHS
        twoWidths = spTable->twoWidths();
        xmlpp::Element* parallel_node = spacingtable_node->add_child ( "TWOWIDTHS" ); 
        for (j = 0; j < twoWidths->numWidth(); j++) {
           xmlpp::Element* twowidth_node = create_element ( parallel_node,"WIDTH","value", twoWidths->width ( j ) );            
           test_create ( twowidth_node, twoWidths->hasWidthPRL(j), "PRL", "hasPRL", twoWidths->widthPRL( j ) );              
           for (k = 0; k < twoWidths->lefiTwoWidths::numWidthSpacing(j); k++){
               xmlpp::Element* twowidth_node = parallel_node->add_child ( "WIDTH" );
               set_attribute ( twowidth_node, "value", twoWidths->widthSpacing ( j,k ) ); 
           }   
        }
     }
  }
  propNum = layer->numProps();
  if (propNum > 0) {
     xmlpp::Element* property_node = layerNode->add_child ( "PROPERTY" ); 
     for (i = 0; i < propNum; i++) {
        // value can either be a string or number
        set_attribute ( property_node, "name", layer->propName( i ) ); 
        test_set(property_node,layer->propIsNumber(i),"value",layer->propNumber(i));
        test_set(property_node,layer->propIsString(i),"value",layer->propValue(i));
        pType = layer->propType(i);
        set_datatype(property_node,pType);      
     }  
  }
  test_create ( layerNode, layer->hasDiagMinEdgeLength(), "DIAGMINEDGELENGTH", "hasDiagMinEdgeLength", layer->diagMinEdgeLength());              
  if (layer->numMinSize()) {
      xmlpp::Element* size_node = layerNode->add_child ( "MINSIZE" ); 
     for (i = 0; i < layer->numMinSize(); i++) {
         set_attribute ( size_node, "name", layer->minSizeWidth( i ) ); 
         set_attribute ( size_node, "name", layer->minSizeLength( i ) ); 
     }
  }
  create_element (parent,"END","layername", layer->name () );
  // Set it to case sensitive from here on
  lefrSetCaseSensitivity(1);
  return 0;
  //fine layer
}

int readLefMacroBeginCallback ( lefrCallbackType_e c, const char* macroName ,xmlpp::Element* parent )
{
    xmlpp::Element* macro_node = create_element ( parent,"MACRO","name", macroName );            
    return 0;
}

int readLefMacroCallback ( lefrCallbackType_e c, lefiMacro* macro ,xmlpp::Element* parent )
{
    lefiSitePattern* pattern;
    int propNum;
    xmlpp::Element* macro_node = create_element ( parent,"MACRO","name", macro->name() );            
    test_create(macro_node,macro->hasClass(),"CLASS","name",macro->macroClass());
    test_create(macro_node,macro->hasEEQ(),"EEQ","name",macro->EEQ());
    test_create(macro_node,macro->hasLEQ(),"LEQ","name",macro->LEQ());
    test_create(macro_node,macro->hasSource(),"SOURCE","name",macro->source());
    test_create(macro_node,macro->hasXSymmetry(),"SYMMETRYX","name",macro->EEQ());
    if (macro->hasXSymmetry()) {
        macro_node->add_child("SYMMETRYX");
    }
    if (macro->hasYSymmetry()) {   // print X Y & R90 in one line
     macro_node->add_child("SYMMETRYY");
    }
    else
        macro_node->add_child ( "Y" );

    if (macro->has90Symmetry()) {
        macro_node->add_child ( "SYMMETRYR90" );
    }
    else
        macro_node->add_child ( "R90" );

    if (macro->hasSiteName()) {
        xmlpp::Element* macroNode = parent->add_child ( "SITE" );
        macroNode->set_attribute ( "name", macro->siteName() );
    }
    if (macro->hasSitePattern()) {
        for (int i = 0; i < macro->numSitePattern(); i++ ) {
            pattern = macro->sitePattern(i);
            if (pattern->hasStepPattern()) {
                xmlpp::Element* macroNode = parent->add_child ( "MACRO" );
                xmlpp::Element* site = macroNode->add_child ( "SITE" );
                xmlpp::Element* sitepattern = site->add_child ( "SITEPATTERN" );
                sitepattern->set_attribute ( "name", pattern->name() );
                set_attribute ( sitepattern,"x", pattern->x() );
                set_attribute ( sitepattern,"y", pattern->y() );
                set_attribute ( sitepattern,"orient", pattern->orient() );
                xmlpp::Element* do_node = sitepattern->add_child ( "DO" );
                set_attribute ( do_node,"xStart", pattern->xStart() );
                xmlpp::Element* by_node = sitepattern->add_child ( "BY" );
                set_attribute ( by_node,"yStart", pattern->yStart() );
                xmlpp::Element* step = sitepattern->add_child ( "STEP" );
                set_attribute ( step,"xStart", pattern->xStep() );
                set_attribute ( step,"yStart", pattern->yStep() );
            } else {
                xmlpp::Element* macroNode = parent->add_child ( "MACRO" );
                xmlpp::Element* site = macroNode->add_child ( "SITE" );
                xmlpp::Element* sitepattern = site->add_child ( "SITEPATTERN" );
                sitepattern->set_attribute ( "name", pattern->name() );
                set_attribute ( sitepattern,"x", pattern->x() );
                set_attribute ( sitepattern,"y", pattern->y() );
                set_attribute ( sitepattern,"orient", pattern->orient() );
            }
        }
    }
    if (macro->hasSize()) {
            xmlpp::Element* macroNode = parent->add_child ( "MACRO" );
            xmlpp::Element* node = macroNode->add_child ( "SIZE" );
            set_attribute ( node ,"sizeX", macro->sizeX() );
            set_attribute ( node ,"sizeY", macro->sizeY() );
        }
        
        if (macro->hasForeign()) {
            for (int i = 0; i < macro->numForeigns(); i++) {
                xmlpp::Element* macroNode = parent->add_child ( "MACRO" );
                xmlpp::Element* foreign = macroNode->add_child ( "FOREIGN" );
                macroNode->set_attribute ("name",macro->foreignName(i));
                if (macro->hasForeignPoint(i)) {
                    xmlpp::Element* macroNode = parent->add_child ( "MACRO" );
                    xmlpp::Element* foreign = macroNode->add_child ( "FOREIGNXY" );
                    set_attribute (foreign,"foreignPointX",macro->foreignX(i));
                    set_attribute (foreign,"foreignPointY",macro->foreignY(i));
                }
                if (macro->lefiMacro::hasForeignOrient(i)) {
                    xmlpp::Element* macroNode = parent->add_child ( "MACRO" );
                    xmlpp::Element* foreign = macroNode->add_child ( "FOREIGNORIENT" );
                    foreign->set_attribute ("orient",macro->foreignOrientStr(i));
                }
            }
        }
        if (macro->hasOrigin()) {
            xmlpp::Element* macroNode = parent->add_child ( "ORIGIN" );
            set_attribute (macroNode,"originX",macro->originX());
            set_attribute (macroNode,"originY",macro->originY());
        }    
        
  if (macro->hasPower()) {
      xmlpp::Element* macroNode = parent->add_child ( "POWER" );
      set_attribute (macroNode,"power",macro->power());
  }
  propNum = macro->numProperties();
        
  if (propNum > 0) {
     xmlpp::Element* macroNode = parent->add_child ( "PROPERTY" );
     for (int i = 0; i < propNum; i++) {
        // value can either be a string or number
        if (macro->propValue(i)) {
            xmlpp::Element* macroNode = parent->add_child ( "PROPERTY" );
           macroNode->set_attribute("propName",macro->propName(i));
           macroNode->set_attribute("propValue",macro->propValue(i));
        }
        else {
            xmlpp::Element* macroNode = parent->add_child ( "PROPERTY" );
            macroNode->set_attribute("propName",macro->propName(i));
            set_attribute(macroNode,"propNum",macro->propNum(i));
        }
        string subtype;
        switch (macro->propType(i)) {
            case 'R':subtype = "REAL";
                     break;
           case 'I': subtype = "INTEGER";
                     break;
           case 'S': subtype = "STRING";
                     break;
           case 'Q': subtype = "QUOTESTRING";
                     break;
           case 'N': subtype = "NUMBER";
                     break;
        }
        xmlpp::Element* macroNode = parent->add_child ( "PROPERTYTYPE" );
        macroNode->set_attribute ( "type",subtype );
     }
  }
    
return 0;
}

int readLefArrayBeginCallback ( lefrCallbackType_e c, const char* name ,xmlpp::Element* parent ){
   int  status;
  // if ((long)ud != userData) dataError();
  // use the lef writer to write the data out
  status = lefwStartArray(name);
  if (status != LEFW_OK)
     return status;
  return 0;
}

int readLefArrayCallback ( lefrCallbackType_e c,lefiArray* array, xmlpp::Element* parent ){
  int              status, i, j, defCaps;
  lefiSitePattern* pattern;
  lefiTrackPattern* track;
  lefiGcellPattern* gcell;

  xmlpp::Element* array_node = create_element ( parent,"ARRAY","name", array->name() );
//   if (array->numSitePattern() > 0) {
//      for (i = 0; i < array->numSitePattern(); i++) {
//         pattern = array->sitePattern(i);
//         xmlpp::Element* site_node = array_node->add_child ( "SITE" );
//         site_node->set_attribute ( "name",pattern->name(i) );
//         site_node->set_attribute ( "x",pattern->x(i));
//         site_node->set_attribute ( "y",pattern->y(i));
//         site_node->set_attribute ( "orient",pattern->orientStr(i));
//         xmlpp::Element* do_node = array_node->add_child ( "DO" );
//         do_node->set_attribute ( "xstart",pattern->xStart(i));
//         do_node->set_attribute ( "ystart",pattern->yStart(i));
//         xmlpp::Element* step_node = array_node->add_child ( "STEP" );
//         step_node->set_attribute ( "xstep",pattern->xStep(i));
//         step_node->set_attribute ( "ystep",pattern->yStep(i));
//      }
//   }

  
//   if (a->numTrack() > 0) {
//      
//      for (i = 0; i < a->numTrack(); i++) {
//         track = a->track(i);
//         
//         fprintf(fout, "  TRACKS %s, %g DO %d STEP %g\n",
//                 track->lefiTrackPattern::name(),
//                 track->lefiTrackPattern::start(), 
//                 track->lefiTrackPattern::numTracks(), 
//                 track->lefiTrackPattern::space()); 
//         if (track->lefiTrackPattern::numLayers() > 0) {
//            fprintf(fout, "  LAYER ");
//            for (j = 0; j < track->lefiTrackPattern::numLayers(); j++)
//               fprintf(fout, "%s ", track->lefiTrackPattern::layerName(j));
//            fprintf(fout, ";\n"); 
//         }
//      }
//   }
// 
//   if (a->lefiArray::numGcell() > 0) {
//      for (i = 0; i < a->lefiArray::numGcell(); i++) {
//         gcell = a->lefiArray::gcell(i);
//         fprintf(fout, "  GCELLGRID %s, %g DO %d STEP %g\n",
//                 gcell->lefiGcellPattern::name(),
//                 gcell->lefiGcellPattern::start(), 
//                 gcell->lefiGcellPattern::numCRs(), 
//                 gcell->lefiGcellPattern::space()); 
//      }
//   }

  

}

void setupLefLibraryTopCallbacks()

{
    lefrSetUnitsCbk ( ( lefrUnitsCbkFnType ) readLefUnitsCallback );
    lefrSetClearanceMeasureCbk ( ( lefrStringCbkFnType ) readLefClearanceMeasureCallback );
    lefrSetExtensionCbk ( ( lefrStringCbkFnType ) readLefExtensionCallback );//not parserized
    lefrSetManufacturingCbk ( ( lefrDoubleCbkFnType ) readLefManufacturingCallback );
    lefrSetMaxStackViaCbk ( ( lefrMaxStackViaCbkFnType ) readLefMaxStackViaCallback );
    lefrSetNonDefaultCbk ( ( lefrNonDefaultCbkFnType ) readLefNonDefaultCallback );
    lefrSetPropCbk ( ( lefrPropCbkFnType ) readLefPropCbkCallback );
    lefrSetSpacingCbk ( ( lefrSpacingCbkFnType ) readLefSpacingCallback );
    lefrSetSiteCbk ( ( lefrSiteCbkFnType ) readLefSiteCallback );
    lefrSetUseMinSpacingCbk ( ( lefrUseMinSpacingCbkFnType ) readLefUseMinSpacingCallback );
    lefrSetMacroBeginCbk( (lefrStringCbkFnType) readLefMacroBeginCallback );
    lefrSetMacroCbk( (lefrMacroCbkFnType) readLefMacroCallback );
    lefrSetViaRuleCbk((lefrViaRuleCbkFnType) readLefViaRuleCallback );
    lefrSetLayerCbk((lefrLayerCbkFnType) readLefLayerCallback);
    lefrSetArrayBeginCbk((lefrStringCbkFnType) readLefArrayBeginCallback );
    lefrSetArrayCbk((lefrArrayCbkFnType) readLefArrayCallback );
    //lefrSetArrayEndCbk((lefrStringCbkFnType) readLefArrayEndCallback );
}

}

// kate: indent-mode cstyle; space-indent on; indent-width 4;

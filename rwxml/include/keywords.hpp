#ifndef KEYWORDS_HPP
#define KEYWORDS_HPP
#define _EX extern const char*

_EX VERSION;
_EX BEGINEXT;
_EX DESIGN;
_EX DIVIDERCHAR; 
_EX BUSBITCHAR;
_EX TECHNOLOGY;
_EX UNITS;
_EX HISTORY;
_EX MUSTJOIN;
_EX PROPERTY;

// DefSimpleStatement.cpp
_EX PROPERTYDEFINITIONS;
_EX DIEAREA;
_EX POINT;
_EX ROW;
_EX DOBYSTEP;
_EX DOBY;
_EX TRACKS;
_EX LAYER;
_EX GCELLGRID;
// USED IN THE VIAS
_EX VIAS;
_EX VIA;
_EX VIARULE;
_EX RECTANGLES;
_EX RECTANGLE;
_EX POLYGONS;
_EX POLYGON;
_EX ROWCOL;
_EX ORIGIN;
_EX OFFSET;
_EX CUTPATTERN;
_EX STYLES;
_EX STYLE;
_EX REGIONS;
_EX SLOTS;
_EX FILLS;

//COMMON ATTRIBUTES

_EX A_items;
_EX A_name;
_EX A_value;

_EX A_type;
_EX REAL;
_EX STRING;
_EX INTEGER;
_EX A_objectType;
_EX A_left;
_EX A_right;

_EX A_patternname;
_EX A_macro;
_EX A_orientation;
_EX A_start;
_EX A_time;
_EX A_step;

_EX A_x;
_EX A_y;
_EX A_xTime;
_EX A_yTime;
_EX A_xStep;
_EX A_yStep;
_EX A_xCount;
_EX A_yCount;

_EX A_xCutSize;
_EX A_yCutSize;
_EX A_xCutSpacing;
_EX A_yCutSpacing;

_EX A_botMetalLayer;
_EX A_cutLayer;
_EX A_topMetalLayer;

_EX A_xBotEnc;
_EX A_yBotEnc;
_EX A_xTopEnc;
_EX A_yTopEnc;

_EX A_xl;
_EX A_xh;
_EX A_yl;
_EX A_yh;


_EX A_rows;
_EX A_cols;
_EX A_xOffset;
_EX A_yOffset;
_EX A_xOrigin;
_EX A_yOrigin;
_EX A_xBotOffset;
_EX A_yBotOffset;
_EX A_xTopOffset;
_EX A_yTopOffset;
_EX A_opc;

#endif

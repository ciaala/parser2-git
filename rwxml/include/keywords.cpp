#include "keywords.hpp"
#define _CH  const char*

_CH VERSION        = "VERSION";
_CH BEGINEXT       = "BEGINEXT";
_CH DESIGN         = "DESIGN";
_CH DIVIDERCHAR    = "DIVIDERCHAR";
_CH BUSBITCHAR     = "BUSBITCHAR";
_CH HISTORY        = "HISTORY";
_CH TECHNOLOGY     = "TECHNOLOGY";
_CH UNITS          = "UNITS";
_CH MUSTJOIN        = "MUSTJOIN";

// DefSimpleStatement.cpp
_CH PROPERTYDEFINITIONS = "PROPERTYDEFINITIONS";
_CH PROPERTY = "PROPERTY";
_CH DIEAREA = "DIEAREA";
_CH POINT = "POINT";
_CH ROW = "ROW";
_CH DOBY = "DOBY";
_CH DOBYSTEP = "DOBYSTEP";
_CH TRACKS = "TRACKS";
_CH LAYER = "LAYER";
_CH GCELLGRID = "GCELLGRID";
_CH VIAS = "VIAS";
_CH VIA = "VIA";
_CH VIARULE = "VIARULE";
_CH RECTANGLES = "RECTANGLES";
_CH RECTANGLE = "RECTANGLE";
_CH POLYGONS = "POLYGONS";
_CH POLYGON = "POLYGON";
_CH ROWCOL = "ROWCOL";
_CH ORIGIN = "ORIGIN";
_CH OFFSET = "OFFSET";
_CH CUTPATTERN = "PATTERN";
_CH STYLES = "STYLES";
_CH STYLE = "STYLE";
_CH FILLS = "FILLS";
_CH SLOTS = "SLOTS";
_CH REGIONS = "REGIONS";


_CH A_items = "items";
_CH A_name = "name";
_CH A_value = "value";

_CH A_type = "type";
_CH REAL = "REAL";
_CH STRING = "STRING";
_CH INTEGER = "INTEGER";
_CH A_objectType = "objectType";
_CH A_left = "left";
_CH A_right = "right";
_CH A_patternname = "patternname";
_CH A_macro = "macro";
_CH A_orientation = "orientation";
_CH A_x = "x";
_CH A_y = "y";
_CH A_xTime = "xTime";
_CH A_yTime = "yTime";
_CH A_xStep = "xStep";
_CH A_yStep = "yStep";
_CH A_xCount = "xCount";
_CH A_yCount = "yCount";

_CH A_start = "start";
_CH A_time = "time";
_CH A_step = "step";

_CH A_xCutSize = "xCutSize";
_CH A_yCutSize = "yCutSize";
_CH A_xCutSpacing = "xCutSpacing";
_CH A_yCutSpacing = "yCutSpacing";

_CH A_botMetalLayer = "botMetalLayer";
_CH A_cutLayer = "cutLayer";
_CH A_topMetalLayer = "topMetalLayer";

_CH A_xBotEnc = "xBotEnc";
_CH A_yBotEnc = "yBotEnc";
_CH A_xTopEnc = "xTopEnc";
_CH A_yTopEnc = "yTopEnc";

_CH A_xl= "xl";
_CH A_xh= "xh";
_CH A_yl= "yl";
_CH A_yh= "yh";

_CH A_rows = "rows";
_CH A_cols = "cols";
_CH A_xOffset = "xOffset";
_CH A_yOffset = "yOffset";
_CH A_xOrigin = "xOrigin";
_CH A_yOrigin = "yOrigin";
_CH A_xBotOffset = "xBotOffset";
_CH A_yBotOffset = "yBotOffset";
_CH A_xTopOffset = "xTopOffset";
_CH A_yTopOffset = "yTopOffset";

_CH A_opc = "opc";
// kate: indent-mode cstyle; replace-tabs off; tab-width 4;  replace-tabs off;

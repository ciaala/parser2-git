#ifndef ALLIN_HPP
#define ALLIN_HPP


#include "xml.hpp"
#include "defrReader.hpp"
#include "lefrReader.hpp"
#include "defiAlias.hpp"


#include "defwWriter.hpp"
#include "lefwWriter.hpp"

#include <string>
using std::string;
using std::list;
#include <libxml++/libxml++.h>
#include <iostream>

#include "../common/utility.hpp"
#include "keywords.hpp"


#define CS(status) \
{\
int result = status; \
if (result != DEFW_OK) { \
	fprintf(stderr,"<%s:%d> ",__FILE__,__LINE__);\
	defwPrintError(result); \
	return result;         \
	}\
	}

#endif

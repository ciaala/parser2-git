#include "design_top.hpp"

namespace deflef
{


int readDefDesignCallback(defrCallbackType_e c, const char* string, xmlpp::Element* parent)
{

    create_element(parent, DESIGN, "name", string);
    return 0;
}
int readDefDesignEndCallback(defrCallbackType_e c, void* data, xmlpp::Element* parent)
{

    return 0;
}

int readDefTechnologyCallback(defrCallbackType_e c, const char* string, xmlpp::Element* parent)
{
    parent->add_child(TECHNOLOGY)->set_attribute("name", string);
    return 0;
}

int readDefHistoryCallback(defrCallbackType_e c, const char* string, xmlpp::Element* parent)
{
    parent->add_child(HISTORY)->set_attribute("value", string);
    return 0;
}

int readDefPropertyCallback(defrCallbackType_e c, defiProp* property, xmlpp::Element* parent)
{
    //TODO check that every other callback use create_property and do not use create property definition
    xmlpp::Element* property_e = create_property_definition(parent, property-> propName(), property->string(), property->number(), property->dataType(), property->propType());
    if (property->hasRange()) {
        set_attribute(property_e, A_left, property->left());
        set_attribute(property_e, A_right, property->right());
    }
    return 0;
}

int readDefDieAreaCallback(defrCallbackType_e c, defiBox* area, xmlpp::Element* parent)
{
    xmlpp::Element* diearea_e = parent->add_child(DIEAREA);
    create_points(diearea_e, area->getPoint());
}


int readDefUnitsCallback(defrCallbackType_e c, double value, xmlpp::Element* parent)
{
    create_element(parent, "UNITS", "value", value);
    return 0;
}

int readDefRowCallback(defrCallbackType_e c, defiRow* row, xmlpp::Element* parent)
{
    xmlpp::Element* row_e = create_element(parent, ROW, A_name, row->name());

    set_attribute(row_e, A_macro, row->macro());
    set_attribute(row_e, A_orientation, row->orientStr());
    set_attribute(row_e, A_x, row->x());
    set_attribute(row_e, A_y, row->y());

    if (row->hasDo()) {
        if (row->hasDoStep()) {
            create_doby_step(row_e, row->xNum(), row->yNum(), row->xStep(), row->yStep());
        } else {
            create_doby(row_e, row->xNum(), row->yNum());
        }
    }
    for (int i = 0, size = row->numProps(); i < size; i++) {
        create_property(row_e, row->propName(i), row->propValue(i), row->propNumber(i), row->propType(i));
    }
}

int readDefTrackCallback(defrCallbackType_e c, defiTrack* track, xmlpp::Element* parent)
{
    xmlpp::Element* track_e =  parent->add_child(TRACKS);
    set_attribute(track_e,  A_macro, track->macro());
    set_attribute(track_e, A_start, track->x());
    set_attribute(track_e, A_time, track->xNum());
    set_attribute(track_e, A_step, track->xStep());

    for (int i = 0, size = track->numLayers(); i < size; i++) {
        create_element(track_e, LAYER, A_name, track->layer(i));
    }
    return 0;
}

int readDefGCellGridCallback(defrCallbackType_e c, defiGcellGrid* grid, xmlpp::Element* parent)
{
    xmlpp::Element* grid_e = parent->add_child(GCELLGRID);
    set_attribute(grid_e, A_macro, grid->macro());
    set_attribute(grid_e, A_start, grid->x());
    set_attribute(grid_e, A_time, grid->xNum());
    set_attribute(grid_e, A_step, grid->xStep());
    return 0;

}

int readDefViaCallback(defrCallbackType_e c, defiVia* via, xmlpp::Element* parent)
{
	parent = create_element(parent, VIA, A_name, via->name() );  
	test_create( parent, via->hasPattern(), CUTPATTERN, A_name, via->pattern() );
    if (via->numLayers() > 0) {
        xmlpp::Element* rectangles = create_element(parent, RECTANGLES, A_items, via->numLayers());
        for (int i = 0, size = via->numLayers();i < size;i++) {
            int xl, yl, xh, yh;
            char* name;
            via->layer(i, &name, &xl, &yl, &xh, &yh);
            set_attribute(create_rectangle_element(rectangles, xl, yl, xh, yh), A_name, name);
        }
    }
    if (via->numPolygons() > 0) {
        xmlpp::Element* polygons = create_element(parent, POLYGONS, A_items, via->numPolygons());
        for (int i = 0, size =  via->numPolygons();i < size;i++) {
            struct defiPoints points = via->getPolygon(i);
            create_polygon(polygons, points);
        }
    }

    if (via->hasViaRule()) {
        char *viaRuleName, *bottomLayer, *cutLayer, *topLayer;
        int xSize, ySize, xCutSpacing, yCutSpacing, xBottomEnclosure, yBottomEnclosure, xTopEnclosure, yTopEnclosure;
        int cr, cc, xOrigin, yOrigin, xBottomOffset, yBottomOffset, xTopOffset, yTopOffset;
        (void) via->defiVia::viaRule(&viaRuleName, &xSize, &ySize, &bottomLayer, &cutLayer, &topLayer, &xCutSpacing, &yCutSpacing,
                                     &xBottomEnclosure, &yBottomEnclosure, &xTopEnclosure, &yTopEnclosure);
// VIARULE
        xmlpp::Element *viarule_e = create_element(parent, VIARULE, A_name, viaRuleName);
// CUTSIZE
		set_attribute(viarule_e, A_xCutSize, xSize);
		set_attribute(viarule_e, A_yCutSize, ySize);
// LAYERS
       // viarule_e = viarule_e->add_child( LAYERS);
		viarule_e->set_attribute(A_botMetalLayer, bottomLayer);
		viarule_e->set_attribute(A_cutLayer, cutLayer);
		viarule_e->set_attribute(A_topMetalLayer, topLayer);
// CUT SPACING
     
     set_attribute(viarule_e, A_xCutSpacing, xCutSpacing);
	 set_attribute(viarule_e, A_yCutSpacing, yCutSpacing);
// ENCLOSURE
     
     set_attribute(viarule_e, A_xBotEnc, xBottomEnclosure);
	 set_attribute(viarule_e, A_yBotEnc, yBottomEnclosure);
	 set_attribute(viarule_e, A_xTopEnc, xTopEnclosure);
	 set_attribute(viarule_e, A_yTopEnc, yTopEnclosure);
// ROW COL
        if (via->hasRowCol()) {
            xmlpp::Element* node = viarule_e->add_child(ROWCOL);
			int cols, rows;
			via->rowCol(&cols,&rows);
            set_attribute(node, A_rows, rows);
            set_attribute(node, A_cols, cols);
// ORIGIN
        }
        if (via->hasOrigin()) {
            xmlpp::Element* node = viarule_e->add_child(ORIGIN);
            via->origin(&xOrigin, &yOrigin);
            set_attribute(node, A_xOrigin, xOrigin);
            set_attribute(node, A_yOrigin, yOrigin);
        }
// OFFSET
        if (via->hasOffset()) {
            xmlpp::Element* node = viarule_e->add_child(OFFSET);
            via->offset(&xBottomOffset, &yBottomOffset, &xTopOffset, &yTopOffset);
            set_attribute(node, A_xBotOffset, xBottomOffset);
            set_attribute(node, A_yBotOffset, yBottomOffset);
            set_attribute(node, A_xTopOffset, xTopOffset);
            set_attribute(node, A_yTopOffset, yTopOffset);
        }

// PATTERN
        if (via->hasCutPattern() && via->pattern() && *via->pattern()) {
            xmlpp::Element* node = viarule_e->add_child(CUTPATTERN);
            node->set_attribute(A_name, via->pattern());
        }
    }
    
    return 0;
}


int readDefStylesStartCallback(defrCallbackType_e c, int value, xmlpp::Element* parent)
{
    xmlpp::Element* node = parent->add_child("STYLES");
    set_attribute(node, "items", value);
    defrSetUserData(node);
    return 0;
}

int readDefStylesCallback(defrCallbackType_e c, defiStyles * styles, xmlpp::Element* parent)
{
    xmlpp::Element* style = parent->add_child(STYLE);
    int i;
    struct defiPoints points = styles->getPolygon();
    for (i = 0; i < points.numPoints; i++) {
        xmlpp::Element* point = style->add_child(POINT);
        set_attribute(point, A_x, points.x[i]);
        set_attribute(point, A_y, points.y[i]);
    }
    return 0;
}
int readDefStylesEndCallback(defrCallbackType_e c, defiStyles * styles, xmlpp::Element* parent)
{
    defrSetUserData(parent->get_parent());
    return 0;
}


void setupDefDesignTopCallbacks()
{
// DESIGN
    defrSetDesignCbk((defrStringCbkFnType) readDefDesignCallback);
    defrSetDesignEndCbk((defrVoidCbkFnType) readDefDesignEndCallback);
// TECHNOLOGY
    defrSetTechnologyCbk((defrStringCbkFnType) readDefTechnologyCallback);
// UNITS
    defrSetUnitsCbk((defrDoubleCbkFnType) readDefUnitsCallback);
// HISTORY
    defrSetHistoryCbk((defrStringCbkFnType) readDefHistoryCallback);
// PROPERTIES
    defrSetPropDefStartCbk((defrVoidCbkFnType) readDefCommonStartCallback);
    defrSetPropCbk((defrPropCbkFnType) readDefPropertyCallback);
    defrSetPropDefEndCbk((defrVoidCbkFnType)  readDefCommonEndCallback);
// DIE AREA
    defrSetDieAreaCbk((defrBoxCbkFnType) readDefDieAreaCallback);
// ROWS
    defrSetRowCbk((defrRowCbkFnType) readDefRowCallback);
// TRACKS
    defrSetTrackCbk((defrTrackCbkFnType) readDefTrackCallback);
// GCELL GRID
    defrSetGcellGridCbk((defrGcellGridCbkFnType) readDefGCellGridCallback);
// VIAS
    defrSetViaStartCbk((defrIntegerCbkFnType) readDefCommonStartCallback);
    defrSetViaCbk((defrViaCbkFnType) readDefViaCallback);
    defrSetViaEndCbk((defrVoidCbkFnType) readDefCommonEndCallback);
// STYLES
    defrSetStylesStartCbk((defrIntegerCbkFnType) readDefStylesStartCallback);
    defrSetStylesCbk((defrStylesCbkFnType) readDefStylesCallback);
    defrSetStylesEndCbk((defrVoidCbkFnType) readDefStylesEndCallback);

}




}
// kate: indent-mode cstyle; replace-tabs off; tab-width 4; 

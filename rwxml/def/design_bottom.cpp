#include "design_bottom.hpp"
namespace deflef
{
int readDefNonDefaultStartCallback ( defrCallbackType_e c, int value, xmlpp::Element* parent )
{
    xmlpp::Element* node = parent->add_child ( "NONDEFAULTRULES" );
    set_attribute ( node, "items", value );
    defrSetUserData ( node );
    return 0;
}

int readDefNonDefaultCallback ( defrCallbackType_e c, defiNonDefault * nondefault, xmlpp::Element* parent )
{
    xmlpp::Element* rule = parent->add_child ( "RULE" );
    rule->set_attribute ( "name", nondefault->name() );

    if ( nondefault->hasHardspacing() )
    {
        xmlpp::Element* node = rule->add_child ( "HARDSPACING" );
    }
    for ( int i = 0; i < nondefault->numLayers(); i++ )
    {
        xmlpp::Element* layer = rule->add_child ( "LAYER" );
        layer->set_attribute ( "name",nondefault->layerName ( i ) );
        if ( nondefault->hasLayerDiagWidth ( i ) )
        {
     
            set_attribute ( layer,"width",nondefault->layerWidthVal ( i ) );
        }
        if ( nondefault->hasLayerDiagWidth ( i ) )
        {
     
            set_attribute ( layer,"diagWidth",nondefault->layerDiagWidthVal ( i ) );
        }
        if ( nondefault->hasLayerSpacing ( i ) )
        {

            set_attribute ( layer,"spacing",nondefault->layerSpacingVal ( i ) );
        }
        if ( nondefault->hasLayerWireExt ( i ) )
        {
        
            set_attribute ( layer,"wireExt",nondefault->layerWireExtVal ( i ) );
        }
    }
    if ( nondefault->numVias() > 0 )
    {
        xmlpp::Element* vias = rule->add_child ( "VIAS" );
        set_attribute ( vias,"items",nondefault->numVias() );
        for ( int i = 0; i < nondefault->numVias(); i++ )
        {
            xmlpp::Element* via = vias->add_child ( "VIA" );
            via->set_attribute ( "name", nondefault->viaName ( i ) );
        }
    }
    if ( nondefault->numViaRules() > 0 )
    {
        xmlpp::Element* viarules = rule->add_child ( "VIARULES" );
        set_attribute ( viarules,"items",nondefault->numVias() );
        for ( int i = 0; i < nondefault->numViaRules(); i++ )
        {
            xmlpp::Element* viarule = viarules->add_child ( "VIARULE" );
            if ( nondefault->viaRuleName ( i ) )
            {
                viarule->set_attribute ( "name", nondefault->viaRuleName ( i ) );
            }
            else
            {
                fprintf ( stderr,"Error: ViaRule without name\n" );
            }
        }
    }
    for ( int i = 0; i < nondefault->numMinCuts(); i++ )
    {
        xmlpp::Element* mincuts = rule->add_child ( "MINCUTS" );
        mincuts->set_attribute ( "minCutsName", nondefault->cutLayerName ( i ) );
    }
    for ( int i = 0; i < nondefault->numProps(); i++ )
    {
        xmlpp::Element* property = rule->add_child ( "PROPERTY" );
        property->set_attribute ( "propertyName", nondefault->propName ( i ) );
        property->set_attribute ( "value", nondefault->propValue ( i ) );
        set_datatype ( property,nondefault->propType ( i ) );
    }
    return 0;
}

int readDefNonDefaultEndCallback ( defrCallbackType_e c, void* data, xmlpp::Element* parent )
{
    defrSetUserData ( parent->get_parent() );
    return 0;
}

int readDefRegionStartCallback ( defrCallbackType_e c, int value, xmlpp::Element* parent )
{
    defrSetUserData ( create_element ( parent,"REGIONS","value",value ) );
    return 0;
}

int readDefRegionCallback ( defrCallbackType_e c, defiRegion* region, xmlpp::Element* parent )
{

    xmlpp::Element* region_e = create_element ( parent,"REGION","name",region->name() );
    for ( int i=0; i<region->numRectangles(); i++ )
    {
        create_rectangle_element ( region_e,region->xl ( i ), region->yl ( i ), region->xh ( i ), region->yh ( i ) );
    }
    if ( region->hasType() )
    {
        create_element ( region_e,"TYPE","value",region->type() );
    }
    if ( region->numProps() > 0 )
    {
        for ( int i = 0, size = region->numProps(); i < size; i++ )
        {
            create_property ( region_e,region->propName ( i ), region->propValue ( i ), region->propNumber(i), region->propType ( i ) );

        }
    }

    return 0;
}

int readDefRegionEndCallback ( defrCallbackType_e c, void* data, xmlpp::Element* parent )
{
    defrSetUserData ( parent->get_parent() );
    return 0;
}
int readDefComponentsStartCallback ( defrCallbackType_e c, int counter, xmlpp::Element* parent )
{
    char buffer[16];
    snprintf ( buffer,16, "%d", counter );
    xmlpp::Element* node = parent->add_child ( "COMPONENTS" );
    node->set_attribute ( "items",buffer );
    defrSetUserData ( node );
    return 0;
}

int readDefComponentCallback ( defrCallbackType_e c, defiComponent* component, xmlpp::Element* parent )
{
    xmlpp::Element* node = parent->add_child ( "COMPONENT" );
    node->set_attribute ( "id", component->id() );
    node->set_attribute ( "name",component->name() );
    if ( component->hasWeight() )
    {
        char buffer[16];
        snprintf ( buffer,16,"%d", component->weight() );
        node ->add_child ( "WEIGHT" )->set_attribute ( "value", buffer );
    }
    if ( component->hasSource() )
    {
        node->add_child ( "SOURCE" )->set_attribute ( "value",component->source() );
    }
    if ( component->hasRouteHalo() )
    {
        xmlpp::Element* halo = node->add_child ( "ROUTEHALO" );
        char buffer[16];
        snprintf ( buffer,16,"%d",component->haloDist() );
        halo->set_attribute ( "halodistance", buffer );
        halo->set_attribute ( "minLayer",component->minLayer() );
        halo->set_attribute ( "maxLayer",component->maxLayer() );
    }
    if ( component->hasRegionName() )
    {
        node->add_child ( "REGION" )->set_attribute ( "name", component->regionName() );
    }
    if ( component->hasRegionBounds() )
    {
        int size;
        int *xl,*yl,*xh,*yh;
        component->regionBounds ( &size,&xl,&yl,&xh,&yh );
        while ( size )
        {
            char buffer[16];
            xmlpp::Element* region = node->add_child ( "REGION BOUND" );
            snprintf ( buffer,16, "%d", *xl );
            region->set_attribute ( "xl", buffer );
            snprintf ( buffer,16, "%d", *yl );
            region->set_attribute ( "yl", buffer );
            snprintf ( buffer,16, "%d", *xh );
            region->set_attribute ( "xh", buffer );
            snprintf ( buffer,16, "%d", *yh );
            region->set_attribute ( "yh", buffer );
            size--;
            xl++;
            yl++;
            xh++;
            yh++;
        }
    }
    if ( component->hasNets() )
    {
        xmlpp::Element* nets =  node->add_child ( "NETS" );
        int i;
        int size = component->numNets();
        char buffer[16];
        snprintf ( buffer,16, "%d", size );
        nets->set_attribute ( "items",buffer );
        for ( i=0; i< size;i++ )
        {
            nets->add_child ( "NET" )->set_attribute ( "name",component->net ( i ) );
        }
    }
    if ( component->hasHalo() )
    {
        int left, bottom, right, top;
        xmlpp::Element* halo;
        if ( component->hasHaloSoft() )
        {
            halo = node->add_child ( "HALOSOFT" );
        }
        else
        {
            halo = node->add_child ( "HALO" );
        }
        component->haloEdges ( &left, &bottom, &right, &top );
        char buffer[16];
        snprintf ( buffer,16,"%d", left );
        halo->set_attribute ( "left", buffer );
        snprintf ( buffer,16,"%d", bottom );
        halo->set_attribute ( "bottom", buffer );
        snprintf ( buffer,16,"%d", right );
        halo->set_attribute ( "right", buffer );
        snprintf ( buffer,16,"%d", top );
        halo->set_attribute ( "top", buffer );

    }
    if ( component->hasGenerate() )
    {
        xmlpp::Element* generate = node->add_child ( "GENERATE" );
        generate->set_attribute ( "name",component->generateName() );
        if ( component->macroName() && *component->macroName() )
        {
            generate->set_attribute ( "macro", component->macroName() );
        }
    }
    if ( component->hasFori() )
    {
        fprintf ( stderr,"ERROR unsupported component with 'Fori'\n" );
    }
    if ( component->hasForeignName() )
    {
        xmlpp::Element* foreign = node->add_child ( "FOREIGN" );
        foreign->set_attribute ( "name", component->foreignName() );
        char buffer[16];
        snprintf ( buffer,16,"%d", component->foreignX() );
        foreign->set_attribute ( "x", buffer );
        snprintf ( buffer,16, "%d", component->foreignY() );
        foreign->set_attribute ( "y", buffer );
        foreign->set_attribute ( "ori", component->foreignOri() );
        snprintf ( buffer, 16, "%d", component->foreignOrient() );
        foreign->set_attribute ( "orientation", buffer );
    }
    if ( component->hasEEQ() )
    {
        node->add_child ( "EEQMASTER" )->set_attribute ( "macro", component->EEQ() );
    }
    if ( component->isCover() )
    {
        xmlpp::Element* cover = node->add_child ( "COVER" );
        set_attribute ( cover,"x",component->placementX() );
        set_attribute ( cover,"y",component->placementY() );
        cover->set_attribute ( "orientation", component->placementOrientStr() );
    }
    if ( component->isFixed() )
    {
        xmlpp::Element* fixed = node->add_child ( "FIXED" );
        set_attribute ( fixed,"x",component->placementX() );
        set_attribute ( fixed,"y",component->placementY() );
        fixed->set_attribute ( "orientation", component->placementOrientStr() );
    }
    if ( component->isPlaced() )
    {
        xmlpp::Element* placed = node->add_child ( "PLACED" );
        set_attribute ( placed,"x",component->placementX() );
        set_attribute ( placed,"y",component->placementY() );
        placed->set_attribute ( "orientation", component->placementOrientStr() );
    }
    if ( component->isUnplaced() )
    {
        xmlpp::Element* unplaced = node->add_child ( "UNPLACED" );
        if ( component->placementX() != -1 || component->placementY() != -1 )
        {
            set_attribute ( unplaced,"x",component->placementX() );
            set_attribute ( unplaced,"y",component->placementY() );
            unplaced->set_attribute ( "orientation", component->placementOrientStr() );
        }
    }
    return 0;
}

int readDefPinCallback ( defrCallbackType_e c, defiPin* pin, xmlpp::Element* parent )
{
    xmlpp::Element* pin_e = create_element ( parent,"PIN","name",pin->pinName() );
    set_attribute ( pin_e,"netName", pin->netName() );
    if ( pin->hasSpecial() )
    {
        pin_e->add_child ( "SPECIAL" );
    }
    test_create ( pin_e,pin->hasDirection(),"DIRECTION","value",pin->direction() );
    test_create ( pin_e,pin->hasNetExpr(),"NETEXPR", "value", pin->netExpr() );
    test_create ( pin_e,pin->hasSupplySensitivity(),"SUPPLYSENSITIVITY","powerPinName", pin->supplySensitivity() );
    test_create ( pin_e,pin->hasGroundSensitivity(),"GROUNDSENSITIVTY", "groundPin",pin->groundSensitivity() );
    test_create ( pin_e,pin->hasUse(),"USE","use",pin->use() );
    if ( pin->hasPlacement() )
    {
        create_placement ( pin_e,pin->isCover(), pin->isFixed(),pin->isPlaced(), pin->isUnplaced(),pin->placementX(),pin->placementY(),pin->orientStr() );
    }
    if ( pin->hasLayer() )
    {
        xmlpp::Element* layers = create_element ( pin_e, "LAYERS", "items",pin->numLayer() );
        for ( int i = 0, size = pin->numLayer(); i<size;i++ )
        {
            int xl,yl,xh,yh;
            pin->bounds ( i,&xl,&yl,&xh,&yh );
            create_layer ( layers, pin->layer ( i ), pin->hasLayerSpacing ( i ), pin->layerSpacing ( i ) ,pin->hasLayerDesignRuleWidth ( i ),pin->layerDesignRuleWidth ( i ),xl,yl,xh,yh );
        }

        xmlpp::Element* polygons = create_element ( pin_e, "POLYGONS", "items",pin->numPolygons() );
        for ( int i = 0, size = pin->defiPin::numPolygons(); i<size; i++ )
        {
            create_polygon ( polygons, pin->polygonName ( i ),pin->hasPolygonSpacing ( i ), pin->polygonSpacing ( i ),  pin->hasPolygonDesignRuleWidth ( i ), pin->polygonDesignRuleWidth ( i ), pin->getPolygon ( i ) );
        }
        if ( pin->numVias() > 0 )
        {
            xmlpp::Element* vias = create_element ( pin_e, "VIAS", "items", pin->numVias() );
            for ( int i = 0, size = pin->numVias(); i < size; i++ )
            {
                xmlpp::Element* via = create_element ( vias,"VIA","viaName",pin->viaName ( i ) );
                set_attribute ( via, "x", pin->viaPtX ( i ) );
                set_attribute ( via, "y", pin->viaPtY ( i ) );
            }
        }
    }
    if ( pin->hasPort() )
    {
        xmlpp::Element* ports = create_element ( pin_e, "PORTS", "items",pin->numLayer() );
        for ( int i=0; i<pin->numPorts(); i++ )
        {
            defiPinPort* port;
            port = pin->pinPort ( i );
            xmlpp::Element* port_e = pin_e->add_child ( "PORT" );
            if ( port->numLayer() > 0 )
            {
                xmlpp::Element* layers = create_element ( port_e, "LAYERS", "items",pin->numLayer() );
                for ( int j = 0; j < port->numLayer();j++ )
                {
                    int xl,yl,xh,yh;
                    port->bounds ( j,&xl,&yl,&xh,&yh );
                    create_layer ( layers,port->layer ( j ),port->hasLayerSpacing ( j ),port->layerSpacing ( j ), port->hasLayerDesignRuleWidth ( j ),port->layerDesignRuleWidth ( j ),
                                   xl,yl,xh,yh );
                }
            }
            else if ( port->numPolygons() > 0 )
            {
                xmlpp::Element* polygons = create_element ( port_e, "POLYGONS", "items",pin->numPolygons() );
                for ( int j=0; j< port->numPolygons(); j++ )
                {
                    create_polygon ( polygons, port->polygonName ( i ),port->hasPolygonSpacing ( i ), port->polygonSpacing ( i ), port->hasPolygonDesignRuleWidth ( i ),port->polygonDesignRuleWidth ( i ),port->getPolygon ( i ) );
                }

            }
            else if ( port->numVias() > 0 )
            {
                xmlpp::Element* vias = create_element ( port_e,"VIAS","items",port->numVias() );
                for ( int j; j < port->numVias(); j++ )
                {
                    xmlpp::Element* via = create_element ( vias, "VIA", "viaName", pin->viaName ( i ) );
                    set_attribute ( via, "x", port->viaPtX ( i ) );
                    set_attribute ( via, "y", port->viaPtY ( i ) );
                }
            }
            else if ( port->hasPlacement() )
            {
                create_placement ( port_e, port->isCover(),port->isFixed(),port->isPlaced(), 0, port->placementX(), port->placementY(), port->orientStr() );
            }

        }
    }
//METAL
    if ( pin->hasAPinPartialMetalArea() )
    {
        for ( int i = 0; i < pin->numAPinPartialMetalArea(); i++ )
        {
            xmlpp::Element* node = create_element ( pin_e,"ANTENNAPINPARTIALMETALAREA","area", pin->APinPartialMetalArea ( i ) );
            test_create ( node, pin->hasAPinPartialMetalAreaLayer ( i ), "LAYER", "layerName", pin->APinPartialMetalAreaLayer ( i ) );
        }
    }
//METAL SIDE
    if ( pin->hasAPinPartialMetalSideArea() )
    {
        for ( int i = 0; i < pin->numAPinPartialMetalSideArea(); i++ )
        {
            xmlpp::Element* node = create_element ( pin_e,"ANTENNAPINPARTIALMETALSIDEAREA","area", pin->APinPartialMetalSideArea ( i ) );
            test_create ( node, pin->hasAPinPartialMetalSideAreaLayer ( i ), "LAYER", "layerName", pin->APinPartialMetalSideAreaLayer ( i ) );
        }
    }
//DIFF
    if ( pin->hasAPinDiffArea() )
    {
        for ( int i = 0; i < pin->numAPinDiffArea(); i++ )
        {
            xmlpp::Element* node = create_element ( pin_e,"ANTENNAPINDIFFAREA","area", pin->APinDiffArea ( i ) );
            test_create ( node, pin->hasAPinDiffAreaLayer ( i ), "LAYER", "layerName", pin->APinDiffAreaLayer ( i ) );
        }
    }
//CUT
    if ( pin->hasAPinPartialCutArea() )
    {
        for ( int i = 0; i < pin->numAPinPartialCutArea(); i++ )
        {
            xmlpp::Element* node = create_element ( pin_e,"ANTENNAPINPARTIALMETALAREA","area", pin->APinPartialCutArea ( i ) );
            test_create ( node, pin->hasAPinPartialCutAreaLayer ( i ), "LAYER", "layerName", pin->APinPartialCutAreaLayer ( i ) );
        }
    }
    for ( int i = 0; i < pin->numAntennaModel(); i++ )
    {
        defiPinAntennaModel* aModel = pin->antennaModel ( i );
        xmlpp::Element* antenna = create_element ( pin_e,"ANTENNAMODEL","model",aModel->antennaOxide() );
        if ( aModel->hasAPinGateArea() )
        {
            for ( int j=0; j< aModel->numAPinGateArea ( ); j++ )
            {
                xmlpp::Element* node = create_element ( antenna, "ANTENNAPINGATEAREA","area",aModel->APinGateArea ( j ) );
                test_create ( node, aModel->hasAPinGateAreaLayer ( j ),"LAYER", "layerName",aModel->APinGateAreaLayer ( j ) );
            }
        }
        if ( aModel->hasAPinMaxAreaCar() )
        {
            for ( int j=0; j < aModel->numAPinMaxAreaCar(); j++ )
            {
                xmlpp::Element* node = create_element ( pin_e,"ANTENNAPINMAXAREACAR","areaCar",aModel->APinMaxAreaCar ( j ) );
                test_create ( node,aModel->hasAPinMaxAreaCarLayer ( j ),"LAYER", "layerName", aModel->APinMaxAreaCarLayer ( j ) );
            }
        }

        if ( aModel->hasAPinMaxCutCar() )
        {

            for ( int j=0; j < aModel->numAPinMaxSideAreaCar(); j++ )
            {
                xmlpp::Element* node = create_element ( pin_e,"ANTENNAPINMAXSIDEAREACAR","sideArea",aModel->APinMaxSideAreaCar ( j ) );
                test_create ( node, aModel->hasAPinMaxSideAreaCarLayer ( j ),"LAYER", "layerName", aModel->APinMaxSideAreaCarLayer ( j ) );
            }
        }
        if ( aModel->hasAPinMaxCutCar() )
        {
            for ( int j=0; j < aModel->numAPinMaxCutCar(); j++ )
            {
                xmlpp::Element* node = create_element ( pin_e,"ANTENNAPINMAXSIDEAREACAR","sideArea",aModel->APinMaxCutCar ( j ) );
                test_create ( node, aModel->hasAPinMaxCutCarLayer ( j ),"LAYER", "layerName", aModel->APinMaxCutCarLayer ( j ) );
            }
        }
    }
    return 0;
}

int readDefPinPropertiesCallback ( defrCallbackType_e c, defiPinProp* properties, xmlpp::Element* parent )
{
    xmlpp::Element* node;
    if ( properties->isPin() )
    {
        node = create_element ( parent, "PIN", "pinName", properties->pinName() );
    }
    else
    {
        node = create_element ( parent, properties->instName(), "pinName", properties->pinName() );
    }
    if ( properties->numProps() > 0 )
    {
        for ( int i=0; i < properties->numProps(); i++ )
        {
            create_property( node,properties->propName ( i ),properties->propValue ( i ), properties->propNumber(i), properties->propType ( i ));
        }
    }

    return 0;
}

int readDefBlockageCallback ( defrCallbackType_e c, defiBlockage* blockage, xmlpp::Element* parent )
{
    xmlpp::Element* block_e = parent->add_child ( "BLOCKAGE" );
    if ( blockage->hasLayer() )
    {
        xmlpp::Element* layer = create_element ( block_e,"LAYER","layerName",blockage->layerName() );

        test_create ( layer,blockage->hasComponent(),"COMPONENT","componentName", blockage->layerComponentName() );
        test_create ( layer,blockage->hasDesignRuleWidth(),"DESIGNRULEWIDTH","ruleWidth",blockage->designRuleWidth() );
        test_create ( layer,blockage->hasSpacing(),"SPACING","minSpacing",blockage->minSpacing() );

        if ( blockage->hasFills() ) layer->add_child ( "SLOTS" );
        if ( blockage->hasExceptpgnet() ) layer->add_child ( "EXCEPTGNET" );
        if ( blockage->hasFills() ) layer->add_child ( "FILLS" );
        if ( blockage->hasPushdown() ) layer->add_child ( "PUSHDOWN" );

    }
    else if ( blockage->hasPlacement() )
    {
        xmlpp::Element* placement = block_e->add_child ( "PLACEMENT" );
        test_create ( placement,blockage->hasComponent(),"COMPONENT","componentName",blockage->placementComponentName() );
        test_create ( placement,blockage->hasPartial(),"PARTIAL","maxDensity", blockage->placementMaxDensity() );
        if ( blockage->hasPushdown() ) placement->add_child ( "PUSHDOWN" );
        if ( blockage->hasSoft() ) placement->add_child ( "SOFT" );
    }
    if ( blockage->numRectangles() )
    {
        xmlpp::Element* rectangles = create_element ( block_e,"RECTANGLES","items",blockage->numRectangles() );
        for ( int i=0,size=blockage->numRectangles();i<size;i++ )
        {
            create_rectangle_element ( rectangles,blockage->xl ( i ),blockage->yl ( i ),blockage->xh ( i ),blockage->yh ( i ) );
        }
    }
    if ( blockage->numPolygons() )
    {
        xmlpp::Element* polygons = create_element ( block_e,"POLYGONS","items",blockage->numPolygons() );
        for ( int j=0, size = blockage->numPolygons(); j < size; j++ )
        {
            defiPoints points = blockage->getPolygon ( j );
            for ( int i=0; i < points.numPoints; i++ )
            {
                create_polygon ( polygons,points );
            }
        }
    }
    return 0;
}
int readDefSlotCallback ( defrCallbackType_e c, defiSlot* slot, xmlpp::Element* parent )
{

    xmlpp::Element* slot_e = parent->add_child ( "SLOT" );
    if ( slot->numRectangles() )
    {
        xmlpp::Element* rectangles = create_element ( slot_e,"RECTANGLES","items",slot->numRectangles() );
        for ( int i=0,size=slot->numRectangles();i<size;i++ )
        {
            create_rectangle_element ( rectangles,slot->xl ( i ),slot->yl ( i ),slot->xh ( i ),slot->yh ( i ) );
        }
    }
    if ( slot->numPolygons() )
    {
        xmlpp::Element* polygons = create_element ( slot_e,"POLYGONS","items",slot->numPolygons() );
        for ( int j=0, size = slot->numPolygons(); j < size; j++ )
        {
            defiPoints points = slot->getPolygon ( j );
            for ( int i=0; i < points.numPoints; i++ )
            {
                create_polygon ( polygons,points );
            }
        }
    }
    return 0;

}

int readDefFillCallback ( defrCallbackType_e c, defiFill* fill, xmlpp::Element* parent )
{

    xmlpp::Element* fill_e = parent->add_child ( "FILL" );
    if ( fill->numRectangles() )
    {
        xmlpp::Element* rectangles = create_element ( fill_e,"RECTANGLES","items",fill->numRectangles() );
        for ( int i=0,size=fill->numRectangles();i<size;i++ )
        {
            create_rectangle_element ( rectangles,fill->xl ( i ),fill->yl ( i ),fill->xh ( i ),fill->yh ( i ) );
        }
    }
    if ( fill->numPolygons() )
    {
        xmlpp::Element* polygons = create_element ( fill_e,"POLYGONS","items",fill->numPolygons() );
        for ( int j=0, size = fill->numPolygons(); j < size; j++ )
        {
            defiPoints points = fill->getPolygon ( j );
            for ( int i=0; i < points.numPoints; i++ )
            {
                create_polygon ( polygons,points );
            }
        }
    }
    if ( fill->hasVia() )
    {
        xmlpp::Element* via = create_element ( fill_e,"VIA","viaName",fill->viaName() );
        if ( fill->hasViaOpc() ) via->add_child ( "OPC" );

        for ( int j=0, size= fill->numViaPts(); j < size; j++ )
        {
            defiPoints points=fill->getViaPts ( j );
            create_points ( via,points );
        }
    }
    return 0;

}

// int readDefSpecialNetPathCallback ( defrCallbackType_e c, defiNet* net, xmlpp::Element* parent )
// {
//     xmlpp::Element* snet_e =  create_element ( parent,"PATHiii","name",net->name() );
// return 0;
// }
// int readDefSpecialNetWireCallback ( defrCallbackType_e c, defiNet* net, xmlpp::Element* parent )
// {
//     xmlpp::Element* snet_e =  create_element ( parent,"WIREiii","name",net->name() );
// return 0;
// }


void setupDefDesignBottomCallbacks()
{
// NON DEFAULT
    defrSetNonDefaultStartCbk ( ( defrIntegerCbkFnType ) readDefNonDefaultStartCallback );
    defrSetNonDefaultCbk ( ( defrNonDefaultCbkFnType ) readDefNonDefaultCallback );
    defrSetNonDefaultEndCbk ( ( defrVoidCbkFnType ) readDefNonDefaultEndCallback );
// REGIONS
    defrSetRegionStartCbk ( ( defrIntegerCbkFnType ) readDefRegionStartCallback );
    defrSetRegionCbk ( ( defrRegionCbkFnType ) readDefRegionCallback );
    defrSetRegionEndCbk ( ( defrVoidCbkFnType ) readDefRegionEndCallback );
// COMPONENT
    defrSetComponentStartCbk ( ( defrIntegerCbkFnType ) readDefComponentsStartCallback );
    defrSetComponentCbk ( ( defrComponentCbkFnType ) readDefComponentCallback );
    defrSetComponentEndCbk ( ( defrVoidCbkFnType ) readDefCommonEndCallback );
// PINS
    defrSetStartPinsCbk ( ( defrIntegerCbkFnType ) readDefCommonStartCallback );
    defrSetPinCbk ( ( defrPinCbkFnType ) readDefPinCallback );
    defrSetPinEndCbk ( ( defrVoidCbkFnType ) readDefCommonEndCallback );
// PIN PROPERTIES

    defrSetPinPropStartCbk ( ( defrIntegerCbkFnType ) readDefCommonStartCallback );
    defrSetPinPropCbk ( ( defrPinPropCbkFnType ) readDefPinPropertiesCallback );
    defrSetPinPropEndCbk ( ( defrVoidCbkFnType ) readDefCommonEndCallback );
// BLOCKAGES
    defrSetBlockageStartCbk ( ( defrIntegerCbkFnType ) readDefCommonStartCallback );
    defrSetBlockageCbk ( ( defrBlockageCbkFnType ) readDefBlockageCallback );
    defrSetBlockageEndCbk ( ( defrVoidCbkFnType ) readDefCommonEndCallback );
//SLOTS
    defrSetSlotStartCbk ( ( defrIntegerCbkFnType ) readDefCommonStartCallback );
    defrSetSlotCbk ( ( defrSlotCbkFnType ) readDefSlotCallback );
    defrSetSlotEndCbk ( ( defrVoidCbkFnType ) readDefCommonEndCallback );

//FILLS
    defrSetFillStartCbk ( ( defrIntegerCbkFnType ) readDefCommonStartCallback );
    defrSetFillCbk ( ( defrFillCbkFnType ) readDefFillCallback );
    defrSetFillEndCbk ( ( defrVoidCbkFnType ) readDefCommonEndCallback );

}
// kate: indent-mode cstyle; space-indent on; indent-width 4; 
}

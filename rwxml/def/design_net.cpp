#include "design_net.hpp"

namespace deflef
{

int readDefSpecialNetCallback ( defrCallbackType_e c, defiNet* net, xmlpp::Element* parent )
{
//     xmlpp::Element* parent =  create_element ( parent,"NET","name",net->name() );
    if ( net->numConnections() > 0 )
    {
        xmlpp::Element* connections = create_element ( parent, "CONNECTIONS", "items", net->numConnections() );
        for ( int i = 0, size = net->numConnections(); i < size; i++ )
        {
            create_connection ( connections, net->instance ( i ),net->pin ( i ), net->pinIsSynthesized ( i ) );
        }
    }
    if ( net->numWires() > 0 )
    {
        xmlpp::Element* wires_e = create_element ( parent,"WIRES","items",net->numWires() );
        for ( int i = 0, size = net->numWires(); i < size; i++ )
        {
            create_wire ( wires_e,net->wire ( i ) );
        }
    }
    if ( net->numRectangles() > 0 )
    {
        xmlpp::Element* rectangles = create_element ( parent,"RECTANGLES","items",net->numRectangles() );
        for ( int i=0,size=net->numRectangles(); i < size; i++ )
        {
            set_attribute ( create_rectangle_element ( rectangles, net->xl ( i ),net->yl ( i ), net->xh ( i ), net->yh ( i ) ), "name",  net->rectName ( i ) );
        }
    }
    if ( net->numPolygons() > 0 )
    {
        xmlpp::Element* polygons = create_element ( parent, "POLYGONS", "items", net->numPolygons() );
        for ( int i=0,size=net->numRectangles(); i < size; i++ )
        {
            set_attribute ( create_polygon ( polygons, net->getPolygon ( i ) ),"name", net->polygonName ( i ) );
        }
    }
    if ( net->numSubnets() > 0 )
    {
        xmlpp::Element* subnets_e = create_element ( parent, "SUBNETS", "items", net->numPolygons() );
        for ( int i=0, size=net->numSubnets(); i<size; i++ )
        {
            create_subnet ( subnets_e, net->subnet ( i ) );
        }
    }
    if ( net->numProps() > 0 )
    {
        xmlpp::Element* properties = create_element ( parent, "PROPERTIES","items", net->numProps() );
        for ( int i=0, size = net->numProps(); i < size; i++ )
        {
            create_property ( properties, net->propName ( i ),net->propValue ( i ), net->propNumber(i), net->propType ( i ) );
        }
    }
    if ( net->numShields() > 0 )
    {
        xmlpp::Element* shields_e = create_element ( parent,"SHIELDS","items",net->numShields() );
        for ( int i = 0; i < net->numShields(); i++ )
        {
            defiShield* shield = net->shield ( i );
            xmlpp::Element* shield_e= create_element ( shields_e, "SHIELD", "name", shield->shieldName() );
            for ( int j=0; j < shield->numPaths(); j++ )
            {
                create_path ( shield_e, shield->path ( j ) );
            }

        }
    }
    if ( net->hasWidthRules() > 0 )
    {
        xmlpp::Element* swidths_e = create_element ( parent,"WIDTHS","items",net->numWidthRules() );
        for ( int i = 0; i < net->numWidthRules(); i++ )
        {
            char* layerName;
            double dist;
            net->widthRule ( i, &layerName, &dist );
            set_attribute ( create_element ( swidths_e, "WIDTH","layerName", layerName ),"dist", dist );
        }

    }

    if ( net->hasSpacingRules() )
    {
        xmlpp::Element* rules_e = create_element ( rules_e,"SPACINGRULES","items",net->numSpacingRules() );
        for ( int i=0; i < net->numSpacingRules(); i++ )
        {
            char * layerName;
            double dist, left, right;
            net->spacingRule ( i,&layerName, &dist,&left, &right );
            xmlpp::Element* spacing =  create_element ( rules_e, "SPACING", "layerName", dist );
            set_attribute ( spacing, "distance", dist );
            set_attribute ( spacing, "left", left );
            set_attribute ( spacing,"right", right );

        }
    }
    defrSetUserData ( parent->get_parent() );
    return 0;
}
int readDefNetCallback ( defrCallbackType_e c, defiNet* net, xmlpp::Element* parent )
{
    if ( net->pinIsMustJoin(0)){
        parent = parent->add_child(MUSTJOIN);
        
    }
    test_set ( parent, net->pinIsMustJoin ( 0 ),"mustJoin", "MUSTJOIN" );
    test_set ( parent, net->hasNonDefaultRule(), "nonDefaultRule", net->nonDefaultRule() );
    if ( net->numConnections() > 0 )
    {
        xmlpp::Element* connections = create_element ( parent, "CONNECTIONS", "items", net->numConnections() );
        for ( int i = 0, size = net->numConnections(); i < size; i++ )
        {
            create_connection ( connections, net->instance ( i ),net->pin ( i ), net->pinIsSynthesized ( i ) );
        }
    }
    if ( net->numVpins() > 0 )
    {
        xmlpp::Element* vpins_e = create_element ( parent, "VPINS", "items", net->numVpins() );
        for ( int i = 0; i < net->numVpins() ;i++ )
        {
            defiVpin* vpin = net->vpin ( i );
            create_vpin ( vpins_e, vpin );
        }
    }

    if ( net->numWires() > 0 )
    {
        xmlpp::Element* wires_e = create_element ( parent, "WIRES", "items", net->numWires() );
        for ( int i=0, size = net->numWires(); i < size; i++ )
        {
            create_wire ( wires_e, net->wire ( i ) );
        }
    }
    if ( net->numShieldNets() > 0 )
    {
        xmlpp::Element* shields_e = create_element ( parent, "SHIELDNETS", "items", net->numShieldNets() );
        for ( int i = 0, size = net->numShieldNets(); i < size ; i++ )
        {
            create_element ( shields_e, "SHIELDNET", "name", net->shieldNet ( i ) );
        }
    }
    if ( net->hasSubnets() > 0 )
    {
        xmlpp::Element* subnets_e = create_element ( parent, "SUBNETS","items",net->numSubnets() );
        for ( int i=0, size=net->numSubnets(); i < size; i++ )
        {
            create_subnet ( subnets_e, net->subnet ( i ) );
        }
    }
    defrSetUserData ( parent->get_parent() );
    return 0;
}


int readDefScanchainCallback ( defrCallbackType_e c, defiScanchain* scan, xmlpp::Element* parent )
{
    xmlpp::Element* scan_e = create_element ( parent,"SCANCHAIN","name",scan->name() );
    if ( scan->hasStart() )
    {
        char* a1, *a2;
        scan->start ( &a1,&a2 );
        xmlpp::Element* start_e = create_element ( scan_e,"START", "component1",a1 );
        set_attribute ( start_e,"component2",a2 );
    }
    if ( scan->hasStop() )
    {
        char* a1, *a2;
        scan->stop ( &a1,&a2 );
        xmlpp::Element* stop_e = create_element ( scan_e,"STOP", "component1",a1 );
        set_attribute ( stop_e,"component2",a2 );
    }
    if ( scan->hasCommonInPin() || scan->hasCommonInPin() )
    {
        xmlpp::Element* stop_e = scan_e->add_child ( "COMMONSCANPINS" );
        if ( scan->hasCommonInPin() )
        {
            xmlpp::Element* commonScanPins_e = create_element ( scan_e,"IN", "commonInPin",scan->commonInPin() );
        }
        if ( scan->hasCommonOutPin() )
        {
            xmlpp::Element* commonScanPins_e = create_element ( scan_e,"OUT", "commonOutPin", scan->commonOutPin() );
        }
    }
    if ( scan->hasFloating() )
    {
        int size, *bits;
        char** inPin, **outPin, **instance;
        scan->floating ( &size,&instance,&inPin,&outPin,&bits );
        if ( size > 0 )
        {
            xmlpp::Element* floatings_e = create_element ( scan_e, "FLOATINGS", "items", size );
            for ( int i = 0; i < size; i++ )
            {
                create_floating ( floatings_e, instance[i], inPin[i], outPin[i], bits[i] );
            }
        }
    }
    if ( scan->hasOrdered() )
    {
        for ( int j=0; j < scan->numOrderedLists(); j++ )
        {
            xmlpp::Element* orderedset_e = create_element ( scan_e, "ORDEREDSET", "items", scan->numOrderedLists() );
            int size, *bits;
            char** inPin, **outPin, **instance;
            scan->ordered ( j, &size,&instance,&inPin,&outPin,&bits );
            if ( size > 0 )
            {
                xmlpp::Element* ordered_e = create_element ( orderedset_e, "ORDEREDLIST", "items", size );
                for ( int i = 0; i < size; i++ )
                {
                    // same type as the floating
                    create_ordered ( ordered_e, instance[i], inPin[i], outPin[i], bits[i] );
                }
            }
        }
    }
    if ( scan->hasPartition() )
    {
        xmlpp::Element* partition = create_element ( scan_e,"PARTITION","name",scan->partitionName() );
        test_set ( partition,scan->hasPartitionMaxBits(),"maxBits", scan->partitionMaxBits() );
    }
    return 0;
}

int readDefGroupCallback(defrCallbackType_e c, defiGroup* group, xmlpp::Element* parent){
    
    if ( group->hasMaxX() || group->hasMaxY() || group->hasPerim()){
        set_attribute(parent, "soft","SOFT");
        test_set(parent, group->hasPerim(),"maxHalfPerimeter", group->perim());
        test_set(parent, group->hasMaxX(), "maxX", group->maxX());
        test_set(parent, group->hasMaxY(), "maxY", group->maxY());
    }
    
        test_create(parent, group->hasRegionName(),"REGION", "name", group->regionName());
    if ( group->hasRegionBox() ){
        int *xl, *yl, *xh, *yh;
        int size;
        xmlpp::Element* regionbox_e = create_element(parent,"REGIONBOX","items",size);
        group->regionRects(&size,&xl,&yl,&xh, &yh);
        for( int i=0;i < size;i++){
            create_rectangle_element(regionbox_e,xl[i],yl[i],xh[i],yh[i]);
        }        
    }
    if ( group->numProps() >0){
        xmlpp::Element* properties_e = create_element(parent,"PROPERTIES", "items", group->numProps());
        for ( int i = 0; i < group->numProps(); i++){
            create_property(properties_e,group->propName(i), group->propValue(i), group->propNumber(i), group->propType(i));                      
        }        
    }
    defrSetUserData(parent->get_parent());
    return 0;
}

int readDefGroupMemeberCallback(defrCallbackType_e c, char* name, xmlpp::Element* parent){
    xmlpp::Element* components_e;
    xmlpp::Node::NodeList list = parent->get_children("COMPONENTS");
    if ( list.empty()){
        components_e = parent->add_child("COMPONENTS");
    } else {
        components_e =new  xmlpp::Element( list.front()->cobj());
    }
    
    create_element(components_e, "MEMBER", "name", name);
    return 0;
}

int readDefExtensionCallback(defrCallbackType_e c, char * extension, xmlpp::Element* parent){
    create_element(parent, "BEGINEXT", "name" ,extension); 
    return 0;
}

void setupDefDesignNetCallbacks()
{
//SPECIAL NETS
    defrSetSNetStartCbk ( ( defrIntegerCbkFnType ) readDefCommonStartCallback );
    defrSetSNetCbk ( ( defrNetCbkFnType ) readDefSpecialNetCallback );
//   defrSetSNetWireCbk((defrNetCbkFnType) readDefSpecialNetWireCallback );
//   defrSetSNetPartialPathCbk( (defrNetCbkFnType) readDefSpecialNetPathCallback);
    defrSetSNetEndCbk ( ( defrVoidCbkFnType ) readDefCommonEndCallback );

// NETS
    defrSetNetCbk ( ( defrNetCbkFnType ) readDefNetCallback );
    defrSetNetNameCbk ( ( defrStringCbkFnType ) readDefCommonNameCallback );
    defrSetNetStartCbk ( ( defrIntegerCbkFnType ) readDefCommonStartCallback );
    defrSetNetEndCbk ( ( defrVoidCbkFnType ) readDefCommonEndCallback );
// SCANCHAINS
    defrSetScanchainsStartCbk ( ( defrIntegerCbkFnType ) readDefCommonStartCallback );
    defrSetScanchainCbk ( ( defrScanchainCbkFnType ) readDefScanchainCallback );
    defrSetScanchainsEndCbk ( ( defrVoidCbkFnType ) readDefCommonEndCallback );
    
// GROUPS
    defrSetGroupsStartCbk( (defrIntegerCbkFnType) readDefCommonStartCallback);
    defrSetGroupCbk((defrGroupCbkFnType) readDefGroupCallback);
    defrSetGroupsEndCbk( (defrVoidCbkFnType) readDefCommonEndCallback);
    defrSetGroupNameCbk( (defrStringCbkFnType) readDefCommonNameCallback);
    defrSetGroupMemberCbk( (defrStringCbkFnType) readDefGroupMemeberCallback);
// EXTENSION    
    defrSetExtensionCbk((defrStringCbkFnType) readDefExtensionCallback);
}

}
// kate: indent-mode cstyle; space-indent on; indent-width 4; 
